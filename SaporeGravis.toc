\contentsline {section}{\numberline {1}Introduction}{6}{section.1}
\contentsline {section}{\numberline {2}Heavy flavour and quarkonium production in \ensuremath {\rm pp}\xspace collisions}{8}{section.2}
\contentsline {subsection}{\numberline {2.1}Production mechanisms of open and hidden heavy-flavour in \ensuremath {\rm pp}\xspace collisions}{8}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Open-heavy-flavour production}{8}{subsubsection.2.1.1}
\contentsline {paragraph}{Fixed-Flavour-Number Scheme}{8}{section*.2}
\contentsline {paragraph}{ZM-VFNS}{9}{section*.3}
\contentsline {paragraph}{GM-VFNS}{10}{section*.4}
\contentsline {paragraph}{FONLL}{11}{section*.5}
\contentsline {paragraph}{Monte Carlo generators}{11}{section*.6}
\contentsline {subsubsection}{\numberline {2.1.2}Quarkonium-production mechanism}{12}{subsubsection.2.1.2}
\contentsline {paragraph}{The Colour-Evaporation Model (CEM)}{12}{section*.7}
\contentsline {paragraph}{The Colour-Singlet Model (CSM)}{12}{section*.8}
\contentsline {paragraph}{The Colour-Octet Mechanism (COM) and NRQCD}{13}{section*.9}
\contentsline {paragraph}{Theory prospects}{13}{section*.10}
\contentsline {subsection}{\numberline {2.2}Recent cross section measurements at hadron colliders}{14}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Leptons from heavy-flavour decays}{15}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Open charm}{15}{subsubsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.3}Open beauty}{17}{subsubsection.2.2.3}
\contentsline {subsubsection}{\numberline {2.2.4}Prompt charmonium}{20}{subsubsection.2.2.4}
\contentsline {subsubsection}{\numberline {2.2.5}Bottomonium}{22}{subsubsection.2.2.5}
\contentsline {subsubsection}{\numberline {2.2.6}${\rm B}_c$ and multiple-charm baryons}{24}{subsubsection.2.2.6}
\contentsline {subsection}{\numberline {2.3}Quarkonium polarization studies}{24}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}New observables}{32}{subsection.2.4}
\contentsline {subsubsection}{\numberline {2.4.1}Production as a function of multiplicity}{32}{subsubsection.2.4.1}
\contentsline {subsubsection}{\numberline {2.4.2}Associated production}{34}{subsubsection.2.4.2}
\contentsline {subsection}{\numberline {2.5}Summary and outlook}{38}{subsection.2.5}
\contentsline {section}{\numberline {3}Cold nuclear matter effects on heavy flavour and quarkonium production in p-A collisions}{39}{section.3}
\contentsline {subsection}{\numberline {3.1}Heavy flavour in {\rm p--A} collisions}{39}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Theoretical models for CNM effects}{41}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}Typical timescales}{41}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}Nuclear PDFs}{43}{subsubsection.3.2.2}
\contentsline {paragraph}{$\ensuremath {\mathrm {J}/\psi }\xspace $ and $\Upsilon $ production}{44}{section*.11}
\contentsline {paragraph}{Non-prompt $\ensuremath {\mathrm {J}/\psi }\xspace $ production}{45}{section*.12}
\contentsline {subsubsection}{\numberline {3.2.3}Saturation in the Colour Glass Condensate approach}{45}{subsubsection.3.2.3}
\contentsline {subsubsection}{\numberline {3.2.4}Multiple scattering and energy loss}{47}{subsubsection.3.2.4}
\contentsline {paragraph}{\ensuremath {{Q\overline {Q}}}\xspace \ propagation and attenuation in nuclei}{47}{section*.13}
\contentsline {paragraph}{Initial and final state energy loss, power corrections and Cronin effect}{48}{section*.14}
\contentsline {paragraph}{Coherent energy loss}{49}{section*.15}
\contentsline {subsubsection}{\numberline {3.2.5}Nuclear absorption}{50}{subsubsection.3.2.5}
\contentsline {subsubsection}{\numberline {3.2.6}Summary of CNM models}{51}{subsubsection.3.2.6}
\contentsline {subsection}{\numberline {3.3}Recent RHIC and LHC results}{51}{subsection.3.3}
\contentsline {subsubsection}{\numberline {3.3.1}Reference for {p--A}\xspace measurements at the LHC}{52}{subsubsection.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.2}Open heavy-flavour measurements}{53}{subsubsection.3.3.2}
\contentsline {paragraph}{Heavy-flavour decay leptons}{53}{section*.16}
\contentsline {paragraph}{Dilepton invariant mass}{55}{section*.17}
\contentsline {paragraph}{{\rm D} mesons}{55}{section*.18}
\contentsline {paragraph}{Open beauty measurements}{57}{section*.19}
\contentsline {paragraph}{Heavy-flavour azimuthal correlations}{57}{section*.20}
\contentsline {subsubsection}{\numberline {3.3.3}Quarkonium measurements}{58}{subsubsection.3.3.3}
\contentsline {paragraph}{Charmonium}{58}{section*.21}
\contentsline {paragraph}{Bottomonium measurements}{63}{section*.22}
\contentsline {subsection}{\numberline {3.4}Extrapolation of CNM effects from {\rm p--A} to {\rm A--A} collisions}{66}{subsection.3.4}
\contentsline {paragraph}{Nuclear PDF}{66}{section*.23}
\contentsline {paragraph}{Multiple scattering and energy loss}{66}{section*.24}
\contentsline {paragraph}{Data-driven extrapolation}{67}{section*.25}
\contentsline {subsection}{\numberline {3.5}Summary and outlook}{68}{subsection.3.5}
\contentsline {section}{\numberline {4}Open heavy flavour in nucleus--nucleus collisions}{71}{section.4}
\contentsline {subsection}{\numberline {4.1}Experimental overview: production and nuclear modification factor measurements}{72}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}Inclusive measurements with leptons}{72}{subsubsection.4.1.1}
\contentsline {subsubsection}{\numberline {4.1.2}{\rm D} meson measurements}{75}{subsubsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.3}Beauty production measurements}{77}{subsubsection.4.1.3}
\contentsline {subsubsection}{\numberline {4.1.4}Comparison of \ensuremath {R_{\mathrm {AA}}}\xspace for charm, beauty and light flavour hadrons}{78}{subsubsection.4.1.4}
\contentsline {subsection}{\numberline {4.2}Experimental overview: azimuthal anisotropy measurements}{79}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}Inclusive measurements with electrons}{79}{subsubsection.4.2.1}
\contentsline {subsubsection}{\numberline {4.2.2}{\rm D} meson measurements}{81}{subsubsection.4.2.2}
\contentsline {subsection}{\numberline {4.3}Theoretical overview: heavy flavour interactions in the medium}{82}{subsection.4.3}
\contentsline {subsubsection}{\numberline {4.3.1}pQCD energy loss in a dynamical QCD medium}{83}{subsubsection.4.3.1}
\contentsline {subsubsection}{\numberline {4.3.2}A pQCD-inspired running $\alpha _s$ energy loss model in MC$@_s$HQ and BAMPS}{86}{subsubsection.4.3.2}
\contentsline {subsubsection}{\numberline {4.3.3}Collisional dissociation of heavy mesons and quarkonia in the QGP}{87}{subsubsection.4.3.3}
\contentsline {subsubsection}{\numberline {4.3.4}$T$-Matrix approach to heavy-quark interactions in the QGP}{88}{subsubsection.4.3.4}
\contentsline {subsubsection}{\numberline {4.3.5}Lattice-QCD}{90}{subsubsection.4.3.5}
\contentsline {subsubsection}{\numberline {4.3.6}Heavy-flavour interaction with medium in AdS/CFT}{91}{subsubsection.4.3.6}
\contentsline {subsection}{\numberline {4.4}Theoretical overview: medium modelling and medium-induced modification of heavy-flavour production}{92}{subsection.4.4}
\contentsline {subsubsection}{\numberline {4.4.1}pQCD energy loss in a static fireball (Djordjevic \emph {et al.}\xspace )}{92}{subsubsection.4.4.1}
\contentsline {subsubsection}{\numberline {4.4.2}pQCD embedded in viscous hydro (POWLANG and Duke)}{93}{subsubsection.4.4.2}
\contentsline {subsubsection}{\numberline {4.4.3}pQCD-inspired energy loss with running $\alpha _s$ in a fluid-dynamical medium and in Boltzmann transport}{94}{subsubsection.4.4.3}
\contentsline {subsubsection}{\numberline {4.4.4}Non-perturbative $T$-matrix approach in a fluid-dynamic model (TAMU) and in UrQMD transport}{94}{subsubsection.4.4.4}
\contentsline {subsubsection}{\numberline {4.4.5}lattice-QCD embedded in viscous fluid dynamics (POWLANG)}{95}{subsubsection.4.4.5}
\contentsline {subsubsection}{\numberline {4.4.6}AdS/CFT calculations in a static fireball}{96}{subsubsection.4.4.6}
\contentsline {subsection}{\numberline {4.5}Comparative overview of model features and comparison with data}{96}{subsection.4.5}
\contentsline {subsection}{\numberline {4.6}Heavy-flavour correlations in heavy-ion collisions: status and prospects}{104}{subsection.4.6}
\contentsline {subsection}{\numberline {4.7}Summary and outlook}{106}{subsection.4.7}
\contentsline {section}{\numberline {5}Quarkonia in nucleus--nucleus collisions}{108}{section.5}
\contentsline {subsection}{\numberline {5.1}Theory overview}{112}{subsection.5.1}
\contentsline {subsubsection}{\numberline {5.1.1}Sequential suppression and lattice QCD}{112}{subsubsection.5.1.1}
\contentsline {subsubsection}{\numberline {5.1.2}Effect of nuclear PDFs on quarkonium production in nucleus--nucleus collisions}{113}{subsubsection.5.1.2}
\contentsline {subsubsection}{\numberline {5.1.3}Statistical (re)generation models}{116}{subsubsection.5.1.3}
\contentsline {subsubsection}{\numberline {5.1.4}Transport approach for in-medium quarkonia}{118}{subsubsection.5.1.4}
\contentsline {subsubsection}{\numberline {5.1.5}Non-equilibrium effects on quarkonium suppression}{121}{subsubsection.5.1.5}
\contentsline {subsubsection}{\numberline {5.1.6}Collisional dissociation of quarkonia from final-state interactions}{122}{subsubsection.5.1.6}
\contentsline {subsubsection}{\numberline {5.1.7}Comover models}{122}{subsubsection.5.1.7}
\contentsline {subsubsection}{\numberline {5.1.8}Summary of theoretical models for experimental comparison}{123}{subsubsection.5.1.8}
\contentsline {subsection}{\numberline {5.2}Experimental overview of quarkonium results at RHIC and LHC}{124}{subsection.5.2}
\contentsline {subsubsection}{\numberline {5.2.1}Proton--proton collisions as a reference for \ensuremath {R_{\mathrm {AA}}}\xspace at the LHC}{124}{subsubsection.5.2.1}
\contentsline {subsubsection}{\numberline {5.2.2}$\rm J/\psi $ \ensuremath {R_{\mathrm {AA}}}\xspace results at low \ensuremath {p_{\mathrm {T}}}\xspace }{124}{subsubsection.5.2.2}
\contentsline {subsubsection}{\numberline {5.2.3}$\rm J/\psi $ \ensuremath {R_{\mathrm {AA}}}\xspace results at high \ensuremath {p_{\mathrm {T}}}\xspace }{127}{subsubsection.5.2.3}
\contentsline {subsubsection}{\numberline {5.2.4}$\rm J/\psi $ azimuthal anisotropy}{129}{subsubsection.5.2.4}
\contentsline {subsubsection}{\numberline {5.2.5}$\rm J/\psi $ \ensuremath {R_{\mathrm {AA}}}\xspace results for various colliding systems and beam energies at RHIC}{130}{subsubsection.5.2.5}
\contentsline {subsubsection}{\numberline {5.2.6}Excited charmonium states}{131}{subsubsection.5.2.6}
\contentsline {subsubsection}{\numberline {5.2.7}Bottomonium \ensuremath {R_{\mathrm {AA}}}\xspace results}{132}{subsubsection.5.2.7}
\contentsline {subsection}{\numberline {5.3}Alternative references for quarkonium production in nucleus--nucleus collisions}{136}{subsection.5.3}
\contentsline {subsubsection}{\numberline {5.3.1}Proton--nucleus collisions}{136}{subsubsection.5.3.1}
\contentsline {subsubsection}{\numberline {5.3.2}Open heavy flavour}{136}{subsubsection.5.3.2}
\contentsline {subsection}{\numberline {5.4}Summary and outlook}{137}{subsection.5.4}
\contentsline {section}{\numberline {6}Quarkonium photoproduction in nucleus-nucleus collisions}{139}{section.6}
\contentsline {subsection}{\numberline {6.1}The flux of photons from lead ions at the LHC}{139}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Measurements of photonuclear production of charmonium during the Run\nobreakspace {}1\xspace at the LHC}{140}{subsection.6.2}
\contentsline {subsubsection}{\numberline {6.2.1}Photonuclear production of $\ensuremath {\mathrm {J}/\psi }\xspace $ at RHIC}{141}{subsubsection.6.2.1}
\contentsline {subsubsection}{\numberline {6.2.2}Coherent production of $\ensuremath {\mathrm {J}/\psi }\xspace $ in {Pb--Pb}\xspace UPC at the LHC}{142}{subsubsection.6.2.2}
\contentsline {subsubsection}{\numberline {6.2.3}Coherent production of $\ensuremath {\psi \text {(2S)}}\xspace $ in {Pb--Pb}\xspace UPC at the LHC}{142}{subsubsection.6.2.3}
\contentsline {subsubsection}{\numberline {6.2.4}Incoherent production of \ensuremath {\mathrm {J}/\psi }\xspace in {Pb--Pb}\xspace UPC at the LHC}{143}{subsubsection.6.2.4}
\contentsline {subsubsection}{\numberline {6.2.5}Coherent photonuclear production of $\ensuremath {\mathrm {J}/\psi }\xspace $ in coincidence with a hadronic {Pb--Pb}\xspace collision at the LHC}{143}{subsubsection.6.2.5}
\contentsline {subsection}{\numberline {6.3}Models for photonuclear production of charmonium}{143}{subsection.6.3}
\contentsline {subsubsection}{\numberline {6.3.1}Models based on vector dominance}{144}{subsubsection.6.3.1}
\contentsline {subsubsection}{\numberline {6.3.2}Models based on LO pQCD}{144}{subsubsection.6.3.2}
\contentsline {subsubsection}{\numberline {6.3.3}Models based on the colour dipole approach}{145}{subsubsection.6.3.3}
\contentsline {subsection}{\numberline {6.4}Photonuclear production of charmonium: comparing models to measurements}{146}{subsection.6.4}
\contentsline {subsection}{\numberline {6.5}Summary and outlook}{147}{subsection.6.5}
\contentsline {section}{\numberline {7}Upgrade programmes and planned experiments}{148}{section.7}
\contentsline {subsection}{\numberline {7.1}Introduction}{148}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Collider experiments}{148}{subsection.7.2}
\contentsline {subsubsection}{\numberline {7.2.1}The LHC upgrade programme}{148}{subsubsection.7.2.1}
\contentsline {paragraph}{The ALICE experiment}{148}{section*.26}
\contentsline {paragraph}{The ATLAS experiment}{150}{section*.27}
\contentsline {paragraph}{The CMS experiment}{151}{section*.28}
\contentsline {paragraph}{The LHCb experiment}{152}{section*.29}
\contentsline {subsubsection}{\numberline {7.2.2}The RHIC programme}{152}{subsubsection.7.2.2}
\contentsline {paragraph}{The sPHENIX project}{152}{section*.30}
\contentsline {paragraph}{STAR experiment}{153}{section*.31}
\contentsline {subsection}{\numberline {7.3}The fixed target experiments}{154}{subsection.7.3}
\contentsline {subsubsection}{\numberline {7.3.1}Low energy projects at SPS, Fermilab and FAIR}{154}{subsubsection.7.3.1}
\contentsline {subsubsection}{\numberline {7.3.2}Plans for fixed-target experiments using the LHC beams}{154}{subsubsection.7.3.2}
\contentsline {paragraph}{SMOG -- the first step}{155}{section*.32}
\contentsline {paragraph}{A Fixed-Target ExpeRiment at the LHC, AFTER@LHC}{155}{section*.33}
\contentsline {section}{\numberline {8}Concluding remarks}{157}{section.8}
