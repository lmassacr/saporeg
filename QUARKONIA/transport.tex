In the transport models, there is continuous dissociation and (re)generation of
quarkonia over the entire lifetime of the deconfined stage. The space-time
evolution of the phase-space distribution, $f_{\cal Q}$, of a quarkonium state
${\cal Q} = \Psi, \Upsilon$ ($\Psi$=$J/\psi, \chi_c, \dots $;
$\Upsilon$=$\Upsilon(1S),\chi_b, \dots$) in hot and dense matter may be
described by the relativistic Boltzmann equation,
\begin{equation}
  p^\mu \partial_\mu f_{\cal Q}(\vec r,\tau;\vec p) =
  - E_p \ \Gamma_{\cal Q}(\vec r,\tau;\vec p) \ f_{\cal Q}(\vec r,\tau;\vec p) +
  E_p \ \beta_{\cal Q}(\vec r,\tau;\vec p)  \ 
\label{eq:boltz}
\end{equation}   
where $p_0=E_p=(\vec p^2 +m_{\cal Q}^2)^{1/2}$, $\tau$ is the proper time, and
$\vec{r}$ is the spatial coordinate. $\Gamma_{\cal Q}$ denotes the dissociation
rate\footnote{A possible mean-field term has been neglected.} and the gain term,
$\beta_{\cal Q}$, depends on the phase-space distribution of the individual
heavy (anti-)quarks, $Q=c,\,b$ in the QGP (or $\rm D$, $\overline{\rm D}$ mesons in
hadronic matter). If the open charm states are thermalised, and in the limit of
a spatially homogeneous medium, one may integrate over the spatial and
3-momentum dependencies to obtain the rate
equation~\cite{Grandchamp:2003uw,Grandchamp:2005yw,Rapp:2009my}
\begin{equation}
  \frac{dN_{\cal Q}}{d\tau} =  -\Gamma_{\cal Q}(T) [ N_{\cal Q} - N_{\cal Q}^{\rm eq}(T) ] \,. 
  \label{eq:rate}
\end{equation}
%which explicitly exhibits the approach toward equilibrium ---when both sides
%vanish---. 
The key ingredients to the rate equation are the {\it transport
coefficients}: the inelastic reaction rate, $\Gamma_{\cal Q}$, for both
dissociation and formation ---{\it detailed balance}---, and the quarkonium
equilibrium limit, $N_{\cal Q}^{\rm eq}(T)$.

The reaction rate can be calculated from inelastic scattering amplitudes of
quarkonia on the constituents of the medium (light quarks and gluons, or light
hadrons). The relevant processes depend on the (in-medium) properties of the
bound state~\cite{Grandchamp:2001pf}. In the QGP, for a tightly bound state
(binding energy $E_B\geq T$), an efficient process is
gluo-dissociation~\cite{Bhanot:1979vb}, $g+{\cal Q}\to Q+\overline{Q}$, where
all of the incoming gluon energy is available for break-up. However, for loosely
bound states ($E_B <T$ for excited and partially screened states), the phase
space for gluo-dissociation rapidly shuts off, rendering ``quasi-free"
dissociation, $p+{\cal Q}\to Q+\overline{Q}+p$ ($p=q,\overline{q},g$), the
dominant process~\cite{Grandchamp:2001pf}, cf.~\fig{fig:trans} (left).

The equilibrium number densities are simply those of $Q$ quarks (with
spin-colour and particle-antiparticle degeneracy $6 \times 2$) and quarkonium
states (summed over including their spin degeneracies $d_{\cal Q}$).

\noindent The quarkonium equilibrium number is given by:
\begin{equation}
  N_{\cal Q}^{\rm eq} = 
  V_{\mathrm{FB}} \sum\limits_{\cal Q}  n_{\cal Q}^{\rm eq}(m_{\cal Q};T,\gamma_Q)
  = V_{\mathrm{FB}} \ \sum\limits_{\cal Q} d_{\cal Q} \  \gamma_Q^2 \int \frac{d^3p}{(2\pi)^3} f_{\cal Q}^B(E_p;T) \ 
  \label{eq:Neq}
\end{equation}
where $V_{\rm FB}$ refers to the fireball volume, $d_{\cal Q}$ is the spin
degeneracy and $f_{\cal Q}^B$ corresponds to the Bose distribution.

\noindent The open heavy-flavour (HF) number, $N_{\rm{op}}$, follows from the
corresponding equilibrium densities, \eg
\begin{equation}
N_{\rm{op}}=N_{\cal Q}+N_{\overline{\cal Q}} = V_{\rm FB} 12\gamma_Q \int
\frac{d^3p}{(2\pi)^3} f_{Q}^F(E_p;T)
\end{equation}
for heavy (anti-)quarks in the QGP.

Assuming relative chemical equilibrium between all available states containing
heavy-flavoured quarks at a given temperature and volume of the system, the
number of \QQbar pairs in the fireball ---usually determined by the initial hard
production--- is matched to the equilibrium numbers of HF states, using a
fugacity factor $\gamma_Q=\gamma_{\overline{Q}}$, by the condition:
\begin{equation}
  N_{\QQbar}=\frac{1}{2} N_{\rm{op}}\frac{I_1(N_{\rm{op}})}{I_0(N_{\rm{op}})}+
  V_{\mathrm{FB}} \ \gamma_Q^2\sum\limits_{\cal Q} n_{\cal Q}^{\rm eq}(T)
  \ .
  \label{eq:NQQ}
\end{equation}
%
The ratio of Bessel functions above, $I_1/I_0$, enforces the canonical limit for
small $N_{\rm op} \le1$.

The quarkonium equilibrium limit is thus coupled to the open HF spectrum in
medium; \eg, a smaller $c$-quark mass increases the $c$-quark density, which
decreases $\gamma_c$ and therefore reduces $N_{J/\psi}^{\rm eq}$, by up to an
order of magnitude for $m_c=1.8 \to 1.5$\,GeV/$c^2$, cf.~\fig{fig:trans}
(right).

\begin{figure}[t]
  \centering
  \includegraphics[width=0.45\textwidth]{Gampsi}
  \includegraphics[width=0.45\textwidth]{Npsi-eq-T-prl92}
  \caption{Transport coefficients of charmonia in the QGP. Left: inelastic
    reaction rates for J$/\psi$ and $\chi_c$ in strong- ($V$=$U$) and
    weak-binding ($V$=$F$) scenarios defined in the text. Right: \jpsi
    equilibrium numbers for conditions in central \PbPb and \AuAu at full SPS
    and RHIC energies, respectively, using different values of the in-medium
    $c$-quark mass in the QGP ($T\ge180\MeV$) and for D-mesons in hadronic
    matter ($T\le180\MeV$); in practice the equilibrium numbers are constructed
    as continuous across the transition region. }
  \label{fig:trans}
\end{figure}

In practice, further corrections to $N_{\cal Q}^{\rm eq}$ are needed for more
realistic applications in heavy-ion collisions. First, heavy quarks cannot be
expected to be thermalised throughout the course of a heavy-ion collision;
harder heavy-quark (HQ) momentum distributions imply reduced phase-space overlap
for quarkonium production, thus suppressing the gain term. In the rate equation
approach this has been implemented through a relaxation factor ${\cal R} =
1-\exp(-\int \dd\tau/\tau_Q^{\rm therm})$ multiplying $N_{\cal Q}^{\rm eq}$, where
$\tau_Q^{\rm therm}$ represents the kinetic relaxation time of the HQ
distributions~\cite{Grandchamp:2002wp,Grandchamp:2003uw}. This approximation has
been quantitatively verified in Ref.~\cite{Song:2012at}. Second, since HQ pairs
are produced in essentially pointlike hard collisions, they do not necessarily
explore the full volume in the fireball. This has been accounted for by
introducing a correlation volume in the argument of the Bessel functions, in
analogy to strangeness production at lower energies~\cite{Hamieh:2000tk}.

An important aspect of this transport approach is a controlled implementation of
in-medium properties of the quarkonia~\cite{Grandchamp:2003uw,Zhao:2010nk}.
Colour-screening of the QCD potential reduces the quarkonium binding energies,
which, together with the in-medium HQ mass, $m_Q^*$, determines the bound-state
mass, $m_{\cal Q} = 2m_Q^*- E_B$. As discussed above, the interplay of $m_{\cal
  Q}$ and $m_Q^*$ determines the equilibrium limit, $N_{\cal Q}^{\rm eq}$, while
$E_B$ also affects the inelastic reaction rate, $\Gamma_{\cal Q}(T)$. To
constrain these properties, pertinent spectral functions have been used to
compute Euclidean correlators for charmonia, and required to approximately agree
with results from lattice QCD~\cite{Zhao:2010nk}. Two basic scenarios have been
put forward for tests against charmonium data at the SPS and RHIC: a
strong-binding scenario (SBS), where the J$/\psi$ survives up to temperatures of
about 2\,$T_c$, and a weak-binding scenario (WBS) with $T_{\rm diss}\simeq
1.2\,T_c$, cf.~\fig{fig:med}. These scenarios are motivated by microscopic
$T$-matrix calculations~\cite{Riek:2010fk} where the HQ internal ($U_{\QQbar}$)
or free energy ($F_{\QQbar}$) have been used as potential, respectively. A more
rigorous definition of the HQ potential, and a more direct implementation of the
quarkonium properties from the $T$-matrix approach is warranted for future work.
The effects of the hadronic phase are generally small for \jpsi and bottomonia,
but important for the \psiP, especially, if its direct decay channel $\psiP\to
\overline{\rm D}{\rm D}$ is opened (due to reduced masses and/or finite widths
of the D mesons)~\cite{Grandchamp:2002wp,Grandchamp:2003uw}.

\begin{figure}[t]
  \centering
  \includegraphics[width=0.45\textwidth]{Eb-jpsi}
  \includegraphics[width=0.45\textwidth]{mc-med}
  \caption{Temperature dependence of J$/\psi$ binding energy (left panel) and
    charm-quark mass (right panel) in the QGP in the strong- and weak-binding
    scenarios (solid ($V$=$U$) and dashed lines ($V$=$F$), respectively) as
    implemented into the rate equation approach~\cite{Zhao:2010nk}.}
  \label{fig:med}
\end{figure}

The rate equation approach has been extended to compute \pt spectra of charmonia
in heavy-ion collisions~\cite{Zhao:2007hh}. Toward this end, the loss term was
solved with a 3-momentum dependent dissociation rate and a spatial dependence of
the charmonium distribution function, while for the gain term blast-wave
distributions at the phase transition were assumed (this should be improved in
the future by an explicit evaluation of the gain term from the Boltzmann
equation using realistic time-evolving HQ distributions, see
Ref.~\cite{Zhao:2010ti} for initial studies)~\cite{Schnedermann:1993ws}. In
addition, formation time effects are included, which affect quarkonium
suppression at high \pt~\cite{Zhao:2008vu}.

To close the quarkonium rate equations, several input quantities are required
which are generally taken from experimental data in \pp and \pa collisions, \eg,
quarkonia and HQ production cross sections (with shadowing corrections), and primordial nuclear absorption effects
encoded in phenomenological absorption cross sections. Feed-down effects from
excited quarkonia (and $b$-hadron decays into charmonium) are accounted for. The
space-time evolution of the medium is constructed using an isotropically
expanding fireball model reproducing the measured hadron yields and their \pt
spectra. The fireball resembles the basic features of hydrodynamic
models~\cite{vanHees:2014ida}, but an explicit use of the latter is desirable
for future purposes.

Two main model parameters have been utilised to calibrate the rate equation
approach for charmonia using the centrality dependence of inclusive \jpsi
production in \PbPb collisions at the SPS (\snn= 17\,GeV) and in \AuAu
collisions at RHIC (\snn = 200\GeV): the strong coupling constant $\alpha_s$,
controlling the inelastic reaction rate, and the $c$-quark relaxation time
affecting the gain term through the amended charmonium equilibrium limit. With
$\alpha_s\simeq0.3$ and $\tau_c^{\text{therm}}\simeq$\,4--6 (1.5--2)~fm/$c$ for the
SBS (WBS), the inclusive \jpsi data at SPS and RHIC can be reasonably well
reproduced, albeit with different decompositions into primordial and regenerated
yields (the former are larger in the SBS than in the WBS). The
$\tau_c^{\text{therm}}$ obtained in the SBS is in the range of values calculated
microscopically from the $T$-matrix approach using the
$U$-potential~\cite{Riek:2010fk}, while for the WBS it is much smaller than
calculated from the $T$-matrix using the $F$-potential. Thus, from a theoretical
point of view, the SBS is the preferred scenario.

With this setup, namely the TAMU transport model, quantitative predictions for
\PbPb collisions at the LHC (\snn= 2.76\,TeV) have been carried out for the
centrality dependence and \pt spectra of \jpsi~\cite{Zhao:2011cv}, as well as
for \upsa, \chib, and \upsb production~\cite{Emerick:2011xu}.

Similar results are obtained in the transport approach THU developed by the
Tsinghua group~\cite{Yan:2006ve,Liu:2009nb}, which differs in details of the
implementation, but overall asserts the robustness of the conclusions. In the
THU model, the quarkonium distribution is also governed by the Boltzmann-type
transport equation.
%
The cold nuclear matter effects change the initial quarkonium distribution and
heavy quark distribution at $\tau_0$.
%
The interaction between the quarkonia and the medium is reflected in the loss
and gain terms and depends on the local temperature $T(\vec{r},\tau)$ and
velocity $u_\mu(\vec{r},\tau)$, which are controlled by the energy-momentum and
charge conservations of the medium, $\partial_\mu T^{\mu\nu}=0$ and
$\partial_\mu n^\mu=0$.

Within this approach, the centrality dependence of the nuclear modification
factor \raa can be obtained and compared to experimental results at low \pt. In
contrast to collisions at SPS and RHIC energies, at LHC energies the large
abundance of $c$ and $\overline{c}$ quarks increases their combining probability
to form charmonia. Hence this regeneration mechanism becomes the dominant source
of charmonium production for semi-central and central collisions at the LHC. The
competition between dissociation and regeneration leads to a flat structure of
the \jpsi yield as a function of centrality. This flat behaviour should
disappear at higher energies or, regeneration being a \pt-dependent mechanism,
with increasing \pt.

The charmonium transverse momentum distribution contains more dynamic
information on the hot medium and can be calculated within the transport
approach. The regeneration occurs in the fireball, and therefore the thermally
produced charmonia are mainly distributed at low \pt, their contribution
increasing with centrality. On the other hand, those charmonia from the initial
hard processes carry high momenta and dominate the high \pt region at all
centralities. This different \pt behaviour of the initially-produced and
regenerated charmonia can even lead to a minimum located at intermediate \pt.
%
Moreover, this particular \pt behaviour will lead to an evolution of the mean
transverse momentum, $\langle\pt\rangle$, with centrality that would be higher
for SPS than for LHC nuclear collisions, once normalised to the corresponding
proton-proton $\langle\pt\rangle$~\cite{Zhou:2009vz,Zhou:2014kka}.
%
At the SPS, almost all the measured \jpsi are produced through initial hard
processes and carry high momentum. At RHIC, the regeneration starts to play a
role and even becomes equally important as the initial production in central
collisions. At the LHC, regeneration becomes dominant, and results in a
decreasing of $\langle\pt\rangle$ with increasing centrality.

Concerning the \jpsi elliptic flow, due to the strong interaction between the
heavy quarks and the hot medium, the regenerated charmonia inherit collective
flow from the charm quarks. Furthermore, primordial \jpsi might acquire a \vtwo
induced by a path-length dependent suppression. As shown in \fig{fig:zhuang4},
the \jpsi \vtwo will, therefore, result from the interplay of two contributions,
a regeneration component, dominant at lower \pt and the primordial \jpsi
component that takes over at higher \pt. Hence, given the increasing
regeneration fraction with colliding energy, the \jpsi elliptic flow is expected
to become sizeable at LHC while it should be almost zero at RHIC.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[htb]
  \centering
  \includegraphics[width=0.50\textwidth]{zhuang_fig4}
  \caption{Elliptic flow \vtwo for prompt \jpsi in \PbPb
    collisions at \snn = 2.76\TeV as predicted by the THU model. The calculation is with impact parameter b = 8.4\,fm,
    corresponding to the 0--100\% centrality range. The dot-dashed, dashed and solid
    lines represent the initial, regeneration, and total contributions,
    respectively.}
  \label{fig:zhuang4}
\end{figure}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../SaporeGravis"
%%% End: 
