For the final quarkonium distributions emerging from high energy nuclear
collisions there are several genuine contributors, namely, (1) initial
production via pQCD process; (2) cold nuclear matter effects before the
formation of the hot medium including the shadowing effect, Cronin effect and
nuclear absorption; and (3) dissociation and regeneration.  While dissociation
and regeneration are both hot nuclear matter effects, they affect the quarkonium
production in an opposite way.  The yield is suppressed by the former but
enhanced by the latter.

To extract medium information from the quarkonium motion, both the medium and
the quarkonia created in high energy nuclear collisions must be treated
dynamically. Since a quarkonium is heavy, its phase space distribution
$f_\Psi({\bf p},{\bf x},t)\ (\Psi=J/\psi, \psi', \chi_c)$ is governed by a
Boltzmann-type transport equation~\cite{Yan:2006ve,Liu:2009nb}
\begin{equation}
  \partial f_\Psi/\partial t +{\bf v}_\Psi\cdot{\bf \nabla}f_\Psi=-\alpha_\Psi f_\Psi +\beta_\Psi,
  \label{eq:trans}
\end{equation}
where the initially produced and regenerated quarkonia are taken into account
through the initial distribution $f_\Psi$ at the medium formation time $\tau_0$
and the gain term $\beta_\Psi({\bf p},{\bf x},t)$. The reduction of quarkonium
due to the gluon dissociation $\Psi + g \to c+\bar c$ is described by the lose
term $\alpha_\Psi({\bf p},{\bf x},t)$. The lose and gain terms are related to
each other via the detailed balance. The cold nuclear matter effects change the
initial quarkonium distribution and heavy quark distribution at $\tau_0$. The
obtained quarkonium distribution includes two parts
$f_\Psi=f_\Psi^{ini}+f_\Psi^{reg}$ with the initial contribution $f_\Psi^{ini}$
and the regeneration $f_\Psi^{reg}$, and both suffer from the gluon
dissociation.  The interaction between the quarkonia and the medium is reflected
in the lose and gain terms and depends on the local temperature $T({\bf x},t)$
and velocity $u_\mu({\bf x},t)$ of the medium which are controlled by the
energy-momentum and charge conservations of the medium, $\partial_\mu
T^{\mu\nu}=0$ and $\partial_\mu n^\mu=0$.

Fig.~\ref{fig:zhuang1} shows the nuclear modification factor $R_{AA}$ for
$J/\psi$ in Pb+Pb collisions at ALICE energy $\sqrt {s_{NN}} =2.76$
TeV. Different from the collisions at SPS and RHIC energies, the regeneration at
LHC energy becomes the dominant source of charmonium production for semi-central
and central collisions. The competition between the dissociation and
regeneration leads to the flat structure of the $J/\psi$ yield as a function of
centrality.

The quarkonium transverse momentum distribution contains more dynamic
information on the hot medium and can be naturally calculated in the transport
approach.  Fig.~\ref{fig:zhuang2} shows the differential $R_{AA}(p_T)$ for
inclusive $J/\psi$'s at fixed centrality bins. The regeneration happens in the
fireball, and therefore the thermally produced charmonia are mainly distributed
at low $p_T$ and the contribution increases with increasing centrality. On the
other hand, those charmonia from the initial hard process carry high momentum
and dominate the high $p_T$ region at all centralities. This totally different
$p_T$ behavior of the initial production and regeneration can even lead to a
minimum located at intermediate $p_T$.

Fig.~\ref{fig:zhuang3} shows the nuclear modification factor for the transverse
momentum~\cite{Zhou:2009vz,Zhou:2014kka}
\begin{equation}
  r_{AA}=\langle p_t^2 \rangle_{AA}/\langle p_t^2
  \rangle_{pp}
  \label{eq:rpt}
\end{equation}
for $J/\psi$ as a function of centrality in nuclear collisions at SPS, RHIC and
LHC. At SPS, almost all the measured $J/\psi$s are produced through initial hard
processes and carry high momentum. The continuous increasing with centrality
arises from the Cronin effect and leakage effect. At RHIC, the regeneration
starts to play a role and even becomes equally important as the initial
production in central collisions, the cancelation between the suppression and
regeneration leads to a flat $r_{AA}$ around unity. At LHC, the regeneration
becomes dominant, the more and more important regeneration results in a
decreasing $r_{AA}$ with increasing centrality. This tremendous change in the
$p_T$ ratio comes from the nature of the hot medium at each corresponding
collision energy. To have a comparison with the LHC data, we calculated the
ratio $r_{AA}$ in the forward rapidity where the hot medium effect becomes
weaker. In this case the trend of the ratio is still very different at RHIC and
LHC.

Fig.~\ref{fig:zhuang4} shows the $J/\psi$ elliptic flow. Due to the strong
interaction between the heavy quarks and the hot medium, the regenerated
charmonia inherit collective flow from the thermalized charm quarks. With the
increasing regeneration fraction with colliding energy, the $J/\psi$ elliptic
flow is almost zero at RHIC but becomes sizeable at LHC.

The transport approach can describe well the quarkonium production in high
energy nuclear collisions, especially the transverse momentum distributions like
$r_{AA}=\langle p_T^2\rangle_{AA}/\langle p_T^2\rangle_{pp}$ and $v_2$ which can
be considered as sensitive probes of the hot medium. The regeneration is
controlled by the heavy quark cross section, and the suppression is governed by
the dissociation cross section. In the model the former is taken from p+p
collisions (with still large uncertainty) and the latter is from Operator
Production Expansion calculation. To distinguish between the different
production and suppression mechanisms, we need to investigate precisely the
excited states like $\psi'$ and the heavier $\Upsilon$ states where some of the
mechanisms disappear and the systems become more simple.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[htb]
  \centering
  \includegraphics[width=0.50\textwidth]{zhuang_fig1}
  \caption{(color online) The $J/\psi$ nuclear modification factor as a function
    of centrality for 2.76 TeV Pb+Pb collisions at forward rapidity (upper
    panel) and mid rapidity (lower panel). The dot-dashed lines represent the
    initial fraction. The thick and thin dashed lines represent the regeneration
    fraction with the upper and lower limits of charm quark cross-sections
    $d\sigma_{c\bar c}^{pp}/dy=0.8$ and $0.65$ mb at mid rapidity and $0.5$ and
    $0.4$ mb at forward rapidity.  The hatched bands represent the full
    results. The data points are taken from ALICE
    experiment~\cite{PereiraDaCosta:2011nb}.}
  \label{fig:zhuang1}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[htb]
  \centering
  \includegraphics[width=0.50\textwidth]{zhuang_fig2}
  \caption{(color online) The $J/\psi$ nuclear modification factor as a function
    of transverse momentum for 2.76 TeV Pb+Pb collisions at forward rapidity and
    in different centrality bins, the dot-dashed lines are the initial
    fraction. The thick and thin dashed lines are the regeneration fraction with
    charm quark cross section $d\sigma_{c\bar c}^{pp}/dy=0.5$ and $0.4$ mb, and
    the bands are the full result. The data are from the ALICE
    collaboration~\cite{Das:2012ct}.}
  \label{fig:zhuang2}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[htb]
  \centering
  \includegraphics[width=0.50\textwidth]{zhuang_fig3}
  \caption{(color online) The newly defined nuclear modification factor
    $r_{AA}=\langle p_t^2 \rangle_{AA}/\langle p_t^2 \rangle_{pp}$ for $J/\psi$
    as a function of centrality at SPS, RHIC and LHC energies. The bands at LHC
    are due to the uncertainty in the charm quark cross section 0.4 <
    $d\sigma_{pp}^{c\bar c}/dy$ < 0.5 mb at forward rapidity (lower panel) and
    0.65 < $d\sigma_{pp}^{c\bar c}/dy$ < 0.8 mb at mid rapidity (upper panel),
    and the data are from NA50~\cite{Alessandro:2004ap,Topilskaya:2003iy},
    PHENIX~\cite{Adare:2006ns} and ALICE~\cite{Scomparin:2012vq}.}
  \label{fig:zhuang3}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[htb]
  \centering
  \includegraphics[width=0.50\textwidth]{zhuang_fig4}
  \caption{The elliptic flow $v_2$ for prompt (upper panel) and inclusive (lower
    panel) $J/\psi$s in $\sqrt {s_{NN}}= 2.76$ TeV Pb+Pb collisions. The
    calculation is with impact parameter b = 8.4 fm, corresponding to the
    minimum bias event. The dot-dashed, dashed and solid lines represent the
    initial, regeneration and total contributions.  In the lower panel, the
    thick and thin lines indicate the total results including thermal bottom
    decay and pQCD bottom decay, respectively.  The experimental data are taken
    from ALICE~\cite{Yang:2012fw}.}
  \label{fig:zhuang4}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../SaporeGravis"
%%% End: 
