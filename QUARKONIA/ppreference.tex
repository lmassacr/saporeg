The medium effects on quarkonia are
usually quantified via the nuclear modification factor \raa, basically comparing
the quarkonium yields in \AAcoll to the \pp ones. A crucial ingredient for the
\raa evaluation is, therefore, $\sigma_{\rm pp}$, the quarkonium production cross
section in \pp collisions measured at the same energy as the \AAcoll data.

During LHC Run~1, \pp data at \s = 2.76 \TeV were collected in two short
data taking periods in 2011 and 2013. When the collected data sample was large
enough, the quarkonium $\sigma_{\rm pp}$ was experimentally measured, otherwise
an interpolation of results obtained at other  energies was made.

More in detail, the \jpsi\ cross section ($\sigma_{\rm pp}^{\jpsi}$) adopted by
ALICE for the forward rapidity \raa results is based on the 2011 \pp
data-taking. The $\lumi_{\text{int}} = 19.9\nbinv$ integrated luminosity,
corresponding to $1364\pm53$ \jpsi reconstructed in the dimuon decay channel,
allows for the extraction of both the integrated as well as the \pt and $y$
differential cross sections~\cite{Abelev:2012kr}. The statistical
uncertainty is 4\% for the integrated result, while it ranges between 6\% and
20\% for the differential measurement. Systematic uncertainties are $\sim$8\%.
The collected data ($\lumi_{\text{int}} = 1.1\nbinv$) allow for the evaluation
of $\sigma_{\rm pp}^{\jpsi}$ also in the ALICE mid-rapidity region, where \jpsi are
reconstructed through their dielectron decay. The measurement is, in this case,
affected by larger statistical and systematic uncertainties, of about 
23\% and 18\%, respectively. Therefore, the $\sigma_{\rm pp}^{\jpsi}$ reference for
the \raa result at mid-rapidity was obtained performing an interpolation
based on mid-rapidity results from PHENIX at \s = 0.2\TeV~\cite{Adare:2006kf},
CDF at \s = 1.96\TeV~\cite{Acosta:2004yw}, and ALICE at \s =
2.76~\cite{Abelev:2012kr} and 7\TeV~\cite{Aamodt:2011gj}. The
interpolation is done by fitting the data points with several functions assuming
a linear, an exponential, a power law, or a polynomial \s-dependence. The
resulting systematic uncertainty is, in this case, 10\%, \ie smaller than the
one obtained directly from the data at \s = 2.76\TeV.

The \jpsi \pp cross section used as a reference for the \raa measurements
obtained by CMS is based on the results extracted from the data
collected at \s = 2.76\TeV in 2011, corresponding to an integrated luminosity
$\lumi_{\text{int}} = 231\nbinv$~\cite{Chatrchyan:2012np}. The number of prompt
and non-prompt \jpsi in the range $|y|<2.4$ and $6.5<\pt<30\GeVc$ are $830\pm
34$ and $206\pm20$, respectively. The systematic uncertainty on the signal
extraction varies between 0.4\% and 6.2\% for prompt \jpsi and 5\% and 20\% for
non-prompt \jpsi. Since the adopted reconstruction procedure is the same in \pp
and \PbPb collisions, many of the reconstruction-related systematic
uncertainties cancel when \raa is computed.

The limited size of the \pp data sample at \s = 2.76\TeV has not allowed ALICE
to measure the \ups\ cross section. The reference adopted by ALICE for the \raa
studies~\cite{Abelev:2014nua} is, in this case, based on the \pp\ 
measurement by LHCb~\cite{Aaij:2014nwa}. However, since the LHCb result is obtained in a
rapidity range ($2<y<4.5$) not exactly matching the ALICE one ($2.5<y<4$), the
measurement is corrected through a rapidity interpolation based on a Gaussian
shape.

For the \ups \raa, CMS results are based on the \pp reference cross section
extracted from \pp data at \s = 2.76\TeV~\cite{Chatrchyan:2012np}.
The number of \upsa with $|y|<2.4$
and $0<\pt<20\GeVc$ is $101\pm12$, with a systematic uncertainty on the signal
extraction of $\sim10\%$.

In \tab{tab:ppref_summary} the datasets and the approach adopted for the
evaluation of the \pp reference are summarised.

\begin{table*}[t]
  \centering
  \caption{Overview of the \pp datasets and approaches adopted for the
    evaluation of the $\sigma_{\rm pp}$ production cross section for the quarkonium states under study.}
  \label{tab:ppref_summary}
  \begin{tabular*}{\textwidth}{@{\extracolsep{\fill}}ccc@{}}
    \hline
    & ALICE & CMS \\
    \hline
    \jpsi & forward-$y$: $\sigma^{\jpsi}_{\pp}$ from \pp data at \s = 2.76\TeV&  $\sigma^{\jpsi}_{\pp}$ from \pp data at \s = 2.76\TeV \\
    & mid-$y$: $\sigma^{\jpsi}_{\pp}$ from interpolation of ALICE, CDF and PHENIX data& \\
    $\Upsilon$ & $\sigma^{\ups}_{\pp}$ from LHCb \pp data at \s = 2.76\TeV +
    $y$-interpolation & $\sigma^{\ups}_{\pp}$ from \pp data at \s = 2.76\TeV\\
    \hline   
  \end{tabular*}
\end{table*}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../SaporeGravis"
%%% End: 
