The experiments ALICE at the LHC and PHENIX at RHIC measure the inclusive \jpsi
production (prompt \jpsi plus those coming from $b$-hadron decays) in the low
\pt region, down to $\pt = 0$. PHENIX detects charmonia in two rapidity ranges: at
mid-rapidity ($|y|<0.35$) in the \ee decay channel and at forward rapidity
($1.2<|y|<2.2$) in the \mumu decay channel. Similarly, ALICE studies the
inclusive \jpsi production in the \ee decay channel at mid-rapidity ($|y|<0.9$)
and in the \mumu decay channel at forward rapidity ($2.5<y<4$). A summary of the
main experimental results, together with their kinematic coverage and
references, is given in Tables~\ref{tab:expSummary_RHIC} and
~\ref{tab:expSummary_LHC}.

Both experiments have investigated the centrality dependence of the \jpsi
nuclear modification factor measured in \AAcoll collisions, \ie\ \AuAu at \snn =
200\GeV for PHENIX~\cite{Adare:2011yf} and \pb at \snn = 2.76\TeV in the ALICE
case~\cite{Abelev:2013ila,Abelev:2012rv}. Results are shown in
\fig{fig:Alice_Phenix_Centr} for the forward (left) and the mid-rapidity (right)
regions.

\begin{figure}[t]
\centering
\includegraphics[width=7cm,clip]{2014-Jun-17-RAACent_ALICE_PHENIX_forward.pdf}
\includegraphics[width=7cm,clip]{2014-Jun-17-RAACent_ALICE_PHENIX_mid.pdf}
\caption{ALICE~\cite{Abelev:2013ila,Abelev:2012rv} (closed symbols) and
  PHENIX~\cite{Adare:2011yf} (open symbols) inclusive \jpsi nuclear modification
  factor versus the number of participant nucleons, at forward rapidity (left)
  and at mid-rapidity (right).}
\label{fig:Alice_Phenix_Centr}       
\end{figure}

While the PHENIX result indicates that at RHIC energy there is an increasing
suppression towards more central collisions, the ALICE \raa shows a flattening
both at forward and at mid-rapidity. In the two $y$ ranges there is a clear
evidence for a smaller suppression at the LHC than at RHIC.

Partonic transport models that include a (re)generation process for \jpsi due to
the (re)combination of \ccbar pairs along the history of the collision indeed
predict such a behaviour~\cite{Zhao:2011cv,Liu:2009nb,Ferreiro:2012rq}, the
smaller suppression at the LHC being due to the larger \ccbar pair multiplicity
which compensates the suppression from colour screening in the deconfined phase.
\begin{figure}[htb]
  \centering
  \includegraphics[width=0.45\textwidth]{2014-Jun-17-RAACentvsModels_forward}
  \includegraphics[width=0.45\textwidth]{2014-Jun-17-RAACentvsModels_mid}
  \caption{Comparison of the ALICE \jpsi \raa at forward rapidity (left) and
    mid-rapidity (right) with the theory predictions based on the TAMU (X. Zhao
    et al.) and THU (Y.P. Liu et al.) transport models discussed in
    \sect{sec:transport}. Bands correspond to the uncertainty associated to the
    model, i.e. to the variation of the charm cross-section for the THU model
    and a variation of the shadowing amount for the TAMU approach. Predictions
    from the statistical model discussed in \sect{sec:regeneration} (A. Andronic
    et al.) are also shown. The two curves correspond, in this case, to two
    assumptions on the values of the $d\sigma_{c\bar{c}}/dy$ cross sections.
    Calculations based on the comover model (E. Ferreiro), presented in
    \sect{sec:comovers}, are included in the plot. The lower and upper curves
    correspond to variations of the charm cross-section.}
  \label{fig:RAA_models_NPart}
\end{figure}
The \raa centrality dependence was predicted by the TAMU and THU
transport models, discussed in \sect{sec:transport}. For both models,
(re)generation becomes the dominant source for charmonium production for
semi-central and central collisions and the competition between the dissociation
and (re)generation mechanisms leads to the observed flat structure of the \jpsi
\raa as a function of centrality. The comparison of the predictions of the two
transport models with the ALICE data is shown in \fig{fig:RAA_models_NPart} for
the forward (left) and mid-rapidity (right) regions.

\begin{figure}[h]
\vspace*{-0mm}
  \centering
  \includegraphics[width=0.42\textwidth]{jpsi_stat_forwy}
  \includegraphics[width=0.42\textwidth]{jpsi_stat_midy}
  \caption{\jpsi \raa from ALICE~\cite{Abelev:2013ila} and
    PHENIX~\cite{Adare:2011yf} compared to predictions from the statistical
    hadronization model~\cite{Andronic:2011yq}.}
\label{fig:stat_mod_charm}
\end{figure}

A similar behaviour is expected by the statistical model~\cite{Andronic:2011yq},
discussed in \sect{sec:regeneration}, where the \jpsi yield is completely
determined by the chemical freeze-out conditions and by the abundance of
$c\overline{c}$ pairs. In Figures~\ref{fig:RAA_models_NPart} and
\ref{fig:stat_mod_charm}, the statistical model predictions are compared to the
ALICE \raa in the two covered rapidity ranges. As discussed in
\sect{sec:regeneration}, a crucial ingredient in this approach is the \ccbar
production cross section: the error band in the figures stems from the
measurement of the \ccbar cross section itself and from the correction
introduced to take into account the \s extrapolation to
evaluate the cross section at the \PbPb energy (\snn = 2.76\TeV). In
\fig{fig:stat_mod_charm} the RHIC data~\cite{Adare:2011yf} and the corresponding
statistical model calculations are also shown. Inspecting
\fig{fig:stat_mod_charm}, for central collisions a significant increase in the
\jpsi \raa at LHC as compared to RHIC is visible and well reproduced by the
statistical hadronization model. In particular, as a characteristic feature of
the model, the shape as a function of centrality is entirely given by the charm
cross section at a given energy and rapidity and is well reproduced both at RHIC
and LHC. This applies also to the maximum in \raa at mid-rapidity due to the
peaking of the charm cross section there.

The (re)combination or the statistical hadronization process are expected to be
dominant in central collisions and, for kinematical reasons, they should
contribute mainly at low \pt, becoming negligible as the \jpsi \pt increases.
This behaviour is investigated by further studying the \raa \pt-dependence. In
\fig{fig:ALICE_RAAvspT}, the ALICE \jpsi \raa(\pt), measured at forward
rapidity (left) or at mid-rapidity~\cite{Adam:2015rba} (right), are compared to
corresponding PHENIX results obtained in similar rapidity ranges. The forward
rapidity result has been obtained in the centrality class 0--20\%, while
the mid-rapidity one in 0--40\%. In both rapidity
regions, a striking different pattern is observed: while the ALICE \jpsi \raa
shows a clear decrease from low to high \pt, the pattern observed at low
energies is rather different, being almost flat versus \pt, with a suppression
up to a factor four (two) stronger than at LHC at forward rapidy (mid-rapidity).

\begin{figure}[t]
\centering
\includegraphics[width=0.42\textwidth]{RAAPtvsModels2-4514}
\includegraphics[width=0.42\textwidth]{Raa-Mpt-sysBar-compPHENIX-compCMS-tcompZhou-tcompZhao}
\caption{ALICE inclusive \jpsi \raa versus \pt~\cite{Abelev:2013ila}. Left:
  forward rapidity result compared to the PHENIX result~\cite{Adare:2011yf} in a
  similar rapidity region. Both results are obtained in the 0-20\% most central
  collisions. Right: mid-rapidity result~\cite{Adam:2015rba} compared to the
  PHENIX result~\cite{Adare:2011yf}, both evaluated in the 0-40\% most central
  collisions}
\label{fig:ALICE_RAAvspT}       
\end{figure}
Models, such as TAMU and the THU that include a \pt-dependent contribution from
(re)combination, amounting to $\approx50\%$ at low \pt and vanishing for high
\pt~\cite{Zhao:2011cv,Liu:2009nb}, are found to provide, also in this case, a
reasonable description of the data, as it can be observed in
\fig{fig:ALICE_RAAvspT_models} for the forward rapidity result or in
\fig{fig:ALICE_RAAvspT} (left) for the mid-rapidity one.

\begin{figure}[t]
\centering
\includegraphics[width=0.42\textwidth]{2014-Jun-17-RAAPtvsModels_090_Zhao}
\includegraphics[width=0.42\textwidth]{2014-Jun-17-RAAPtvsModels_090_Liu}
\caption{ALICE inclusive \jpsi \raa, measured in the forward rapidity region,
  versus \pt~\cite{Abelev:2013ila}, compared to the TAMU (left) and THU (right)
  theoretical transport calculations including a (re)combination component to
  the \jpsi production.}
\label{fig:ALICE_RAAvspT_models}       
\end{figure}

Finally, the rapidity dependence of the \jpsi \raa is shown in
\fig{fig:ALICE_RAAvsy}. At forward-$y$ the \jpsi \raa decreases by about 40\%
from $y = 2.5$ to $y = 4$. The \raa $y$-dependence is compared to shadowing
calculations discussed in \sect{sec:npdf_aa}. As expected, the contribution of
cold nuclear matter alone, such as shadowing, cannot account for the observed
suppression, clearly indicating the need of the aforementioned hot matter
effects.

\begin{figure}[t]
\centering
\includegraphics[width=0.42\textwidth]{2014-Jun-17-RAARapvsModels_derived.pdf}
\caption{ALICE inclusive \jpsi \raa versus rapidity~\cite{Abelev:2013ila},
  compared to nPDF calculations (see \sect{sec:npdf_aa}).}
\label{fig:ALICE_RAAvsy}       
\end{figure}

As discussed, the ALICE results are for inclusive \jpsi, therefore including two
contributions: the first one from \jpsi direct production and feed-down from
higher charmonium states and the second one from \jpsi originating from
$b$-hadron decays. Beauty hadrons decay mostly outside the fireball, hence the
measurement of non-prompt \jpsi \raa is mainly connected to the $b$ quark
in-medium energy loss, discussed in \sect{sec:OHFbeauty}. Non-prompt \jpsi are,
therefore, expected to behave differently with respect to the prompt ones.
%, being insensitive to colour screening or regeneration mechanisms. 
In the low-\pt
region covered by ALICE the fraction of non-prompt \jpsi is smaller than
15\%~\cite{Book:2014mia} (slightly depending on the $y$ range). Based on this
fraction, the ALICE Collaboration has estimated the influence of the non-prompt
contribution on the measured inclusive \raa. At mid-rapidity the prompt \jpsi
\raa can vary within $-7\%$ and $+17\%$ with respect to the inclusive \jpsi \raa
assuming no suppression ($\raa^{\text{\rm non-prompt}}=1$) or full suppression
($\raa^{\text{\rm non-prompt}}=0$) for beauty, respectively. At forward-$y$, the
prompt \jpsi \raa would be 6\% lower or 7\% higher than the inclusive result in
the two aforementioned cases~\cite{Abelev:2013ila}. 
%In both cases, the variation
%between inclusive and prompt \jpsi is well within the quoted systematic
%uncertainties.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../SaporeGravis"
%%% End: 
