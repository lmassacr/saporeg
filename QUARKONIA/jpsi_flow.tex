Further information on the \jpsi production mechanism can be accessed by studying
the azimuthal distribution of \jpsi with respect to the reaction plane.
%, defined
%by the beam axis and the impact parameter vector of the colliding nuclei. This
%distribution is expected to be sensitive to the dynamics of the partonic stage
%of heavy-ion collisions. In particular, in non-central collisions the
%geometrical overlap region and, therefore, the initial matter distribution is
%anisotropic (almond-shaped). This spatial asymmetry is converted via multiple
%collisions into an anisotropic momentum distribution. The second coefficient of
%the Fourier expansion describing the final state particle azimuthal distribution
%with respect to the reaction plane, \vtwo, is called elliptic flow.
As discussed in \sect{OHF}, the positive $v_2$ measured for D mesons at LHC and 
heavy-flavour decay electrons at RHIC suggests that charm quarks participate
in the collective expansion of the medium and do acquire some elliptic flow 
as a consequence of the multiple collisions with the medium constituents. 
\jpsi produced through a recombination mechanism, should inherit the elliptic
flow of the charm quarks in the QGP and, as a consequence, \jpsi are expected to
exhibit a large \vtwo. Hence this quantity is a further signature to identify
the charmonium production mechanism.

ALICE measured the inclusive \jpsi elliptic flow in \pb collisions at
forward rapidity~\cite{ALICE:2013xna}, using the event-plane technique. For
semi-central collisions there is an indication of a positive \vtwo, reaching
$\vtwo = 0.116\pm0.046\,\text{(stat.)}\pm0.029\,\text{(syst.)}$ in the
transverse momentum range $2<\pt<4\GeVc$, for events in the 20--40\% centrality
class. In \fig{fig:jpsi_v2} (left), the \jpsi \vtwo in the 20--60\% centrality class is compared with the TAMU
and THU transport model calculations, which also provide a fair description of
the \raa results, discussed in \sect{sec:raa_lowpt}. Both models, which
reasonably describe the data, include a fraction ($\approx30\%$ in the
centrality range 20--60\%) of \jpsi produced through (re)generation mechanisms,
under the hypothesis of thermalisation or non-thermalisation of the $b$-quarks.
More in details, charm quarks, in the hot medium created in \pb collisions at
the LHC, should transfer a significant elliptic flow to regenerated \jpsi.
Furthermore, primordial \jpsi might acquire a \vtwo induced by a path-length
dependent suppression due to the fact that \jpsi emitted out-of-plane traverse a
longer path through the medium than those emitted in-plane. Thus, out-of-plane
emitted \jpsi will spend a longer time in the medium and have a higher chance to
melt. The predicted maximum \vtwo at \pt = 2.5\GeVc is, therefore, the result of
an interplay between the regeneration component, dominant at low \pt and the
primordial \jpsi component which takes over at high \pt (see
\fig{fig:zhuang4}). The \vtwo measurement complements the \raa results,
favouring a scenario with a significant fraction of \jpsi produced by
(re)combination in the ALICE kinematical range.

\begin{figure}[!t]
  \centering
  \includegraphics[width=0.45\textwidth]{jpsi_v2_ALICE}
%  \includegraphics[width=0.50\textwidth]{Flow_STAR}
  \includegraphics[width=0.485\textwidth]{STAR_flow}
  \caption{Left: ALICE inclusive \jpsi measurement as a function of transverse
    momentum for semi-central \mbox{Pb-Pb} collisions~\cite{ALICE:2013xna},
    compared to TAMU~\cite{Zhao:2012gc} and THU~\cite{Liu:2009gx} transport
    models calculations. Right: STAR inclusive \jpsi measurement as a function
    of transverse momentum in different centrality bins~\cite{Adamczyk:2012pw}.}
  \label{fig:jpsi_v2}
\end{figure}

At RHIC, measurements by the STAR Collaboration~\cite{Adamczyk:2012pw} of the
\jpsi \vtwo in \AuAu collisions at \snn = 200\GeV are consistent with zero for
$\pt>2\GeVc$ albeit with large uncertainties, as shown in \fig{fig:jpsi_v2}
(right), while a hint for a positive \vtwo might be visible in the lowest \pt
bin ($0<\pt<2\GeVc$). Results do not show a dependence on centrality. The
measurement seems to disfavour the \jpsi formation through recombination
mechanisms at RHIC energies, contrarily to what happens in \PbPb collisions at
the LHC.

CMS has investigated the prompt \jpsi \vtwo
as a function of the centrality of the collisions and as a
function of transverse momentum~\cite{CMS:2013dla}. Preliminary results indicate
a positive \vtwo. The observed
anisotropy shows no strong centrality dependence when integrated over rapidity
and \pt. The \vtwo of prompt \jpsi, measured in the 10--60\% centrality class,
has no significant \pt dependence either, whether it is measured at low \pt,
$3<\pt<6.5\GeVc$, in the forward rapidity interval $1.6<|y|<2.4$, or at
high \pt, $6.5<\pt<30\GeVc$, in the rapidity interval $|y|<2.4$. The preliminary CMS
result supports the presence of a small anisotropy over the whole \pt range, 
but the present level of precision does not allow for a definitive
answer on whether this anisotropy is constant or not. In the rapidity interval
$|y|<2.4$, for $\pt>8\GeVc$, the anisotropy is similar to that observed for
charged hadrons, the latter being attributed to the path-length dependence of
 partonic energy loss~\cite{Chatrchyan:2012xq}.


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../SaporeGravis"
%%% End: 
