The large masses of charm and beauty quarks provide the basis for a quarkonium spectroscopy through
non-relativistic potential theory, introducing a confining potential in terms of
a string tension. The results are summarised below~\cite{Satz:2005hx}.

\begin{table*}[!b]
  \centering
  \caption{Mass, binding energy, and radius for charmonia and bottomonia~\cite{Satz:2005hx}.}
  \label{tab:resonances}
  \begin{tabular*}{\textwidth}{@{\extracolsep{\fill}}ccccccccc@{}}
    \hline
    {\rm state}& $J/\psi$ & $\chic\text{(1P)}$ & $\psiP$ &
    $\upsa$ & $\chib\text{(1P)}$ &  \upsb & $\chib\text{(2P)}$ & $\upsc$ \\
    \hline
    {\rm mass~[GeV$/c^2$]}&
    3.07 & 3.53 & 3.68 & 9.46 & 9.99 & 10.02 & 10.26 & 10.36 \\
    ${\rm binding}$ {\rm[GeV]}&
    0.64 & 0.20 & 0.05 & 1.10 & 0.67 & 0.54 & 0.31 & 0.20 \\
    {\rm radius~[fm]}&
    0.25 & 0.36 & 0.45 & 0.14 & 0.22 & 0.28 & 0.34 & 0.39 \\
    \hline
  \end{tabular*}
\end{table*}

The QGP consists of deconfined colour charges, so that the
binding of a \QQbar pair is subject to the effect of colour screening which
limits the range of strong interactions. More in details, the fate of heavy
quark bound states in a QGP depends on the size of the colour screening radius
$r_D$ (which is inversely proportional to the temperature, so that it decreases
with increasing temperature) in comparison to the quarkonium binding radius
$r_Q$: if $r_D \gg r_Q$, the medium does not really affect the heavy quark
binding. Once $r_D \ll r_Q$, however, the two heavy quarks cannot ``see'' each
other any more and hence the bound state will melt. It is therefore expected
that quarkonia will survive in a QGP through some range of
temperatures above $T_c$, and then dissociate once $T$ becomes large enough.
This behaviour is in fact confirmed by analytical as well as lattice
computations~\cite{Brambilla:2010cs}.

Finite-temperature lattice studies on quarkonium mostly consist of calculations
of spectral functions for temperatures in the range explored by the experiments.
The spectral function $\rho(\omega)$ is the basic quantity encoding the
equilibrium properties of a quarkonium state. It characterises the spectral
distribution of binding strength as a function of energy $\omega$. Bound or resonance
states manifest themselves as peaks with well defined mass and spectral width.
The in-medium spectral properties of quarkonia are related to phenomenology,
since the masses determine the equilibrium abundances, their inelastic widths
determine formation and destruction rates (or chemical equilibration times) and
their elastic widths affect momentum spectra (and determine the kinetic
equilibration times).
% 
% 
% To motivate
% these studies and, at the same time, to get a feeling of their limitations, we
% can consider the relation between the rate of production of muon pairs $dN_{\mu
%   \overline{\mu}}\slash (d^4xd^4q)$ and the spectral functions $\rho(\omega)$:
% $dN_{\mu\overline{\mu}}\slash(d^4xd^4q) = F(q,T,...)\rho(\omega)$. The
% connection between the invariant mass distribution and the spectral function is
% clear, although the dynamical factor $F$ is largely unknown. Understanding in
% detail this connection is an important aspect of ongoing research, outside the
% scope of lattice studies: on the lattice we concentrate on the calculations of
% spectral functions at equilibrium.

Spectral functions play an important role in understanding how elementary
excitations are modified in a thermal medium. They are the power spectrum of
autocorrelation functions in real time, hence provide a direct information on
large time propagation. In the lattice approach such real time evolution is not
directly accessible: the theory is formulated in a four dimensional box -- three
dimensions are spatial dimensions, the fourth is the imaginary (Euclidean) time
$\tau$. The lattice temperature $T_L$ is realised through (anti)periodic
boundary conditions in the Euclidean time direction -- $T_L = 1/N_\tau$, where
$N_\tau$ is the extent of the time direction, and can be converted to physical
units once the lattice spacing is known. The spectral functions appear now in
the decomposition of a (zero-momentum) Euclidean propagator $G(\tau)$: $ G(\tau)
= \int_{0}^\infty \rho(\omega) \frac{\dd\omega}{2\pi}\, K(\tau,\omega)$, with
$K(\tau,\omega) = \frac{\left(e^{-\omega\tau} + e^{-\omega(1/T - \tau)}\right)}
{1 - e^{-\omega/T}}$. The $\tau$ dependence of the kernel $K$ reflects the
periodicity of the relativistic propagator in imaginary time, as well as its $T$
symmetry. The Bose--Einstein distribution, intuitively, describes the wrapping
around the periodic box, which becomes increasingly important at higher
temperatures.

The procedure is, then, based on the generation of an appropriate ensemble of
lattice gauge fields at a temperature of choice, on the computation on such an
ensemble of the Euclidean propagators $G(\tau)$, and on the extraction of the
spectral functions.
% To briefly summarise the current outcome of such program, any study of
% charmonium and bottomonium produce sensible qualitative results: at {\em some}
% temperature T above, and {\em not too far} from the critical temperature, and
% possibly coinciding with it, a given status melts. However, a final consensus
% on quantitative issues has not been reached yet. Why? First, experiences with
% lattice calculations has taught that it is extremely important to have results
% in the continuum limit, and with the proper matter content.
%
All such quarkonium studies yield qualitatively the same result: a given
quarkonium state melts at a temperature above, or possibly at, the phase
transition temperature. There are, however, disagreements between different
calculations in the precise temperatures for the following reasons. First,
experiences with lattice calculations have demonstrated that it is extremely
important to have results in the continuum limit, and with the proper matter
content.
%
This means that the masses of the dynamical quark fields which are used in the
generation of the gauge ensembles must be as close as possible to the physical
ones, and the lattice spacing should be fine enough to allow for making contact with
continuum physics. These systematic effects, which have been studied in detail
for bulk thermodynamics, are still under scrutiny for the spectral functions.
Second, the calculation of spectral functions using Euclidean propagators as an
input is a difficult, possibly ill-defined, problem. It has been mostly tackled
by using the Maximum Entropy Method (MEM)~\cite{Asakawa:2000tr}, which has
proven successful in a variety of applications. Recently, an alternative
Bayesian reconstruction of the spectral functions has been proposed in
Refs.~\cite{Rothkopf:2011ef,Burnier:2013nla} and applied to the analysis of
configurations from the HotQCD Collaboration~\cite{Kim:2014iga}.

Most calculations of charmonium spectral functions have been performed in the
quenched approximation ---neglecting quark loops---, although recently the
spectral functions of the charmonium states have been studied as a function of
both temperature and momentum, using as input relativistic propagators with two
light quarks~\cite{Aarts:2007pk,Kelly:2013cpa} and, more recently, including the
strange quark, for temperatures ranging between $0.76 T_c$ and $1.9 T_c$. The
sequential dissolution of the peaks corresponding to the S- and P-wave states is
clearly seen. The results are consistent with the expectation that charmonium
melts at high temperature, however as of today they lack quantitative precision
and control over systematic errors.

The survival probability for a given quarkonium state
depends on its size and binding energy (see \tab{tab:resonances} for details).
Hence the excited states will be dissolved at a lower initial temperature than
the more tightly-bound ground states. However, only a
fraction (about 60\%) of the observed \jpsi is a directly produced $\text{(1S)}$
state, the remainder is due to the feed-down of excited states, with about 30\%
from $\chic\text{(1P)}$ and 10\% from \psiP
decays~\cite{Antoniazzi:1992af,Antoniazzi:1992iv,Lemoigne:1982jc}. A similar
decay pattern arises for \ups
production~\cite{Abe:1995an,Affolder:1999wm,Aaij:2012se,Aad:2011ih,Aaij:2014caa}.
The decay processes occur far outside the produced medium, so that the medium
affects only the excited states. As a result, the formation of a hot deconfined
medium in nuclear collisions will produce a sequential quarkonium suppression
pattern~\cite{Karsch:2005nk}, as illustrated in \fig{fig:seq}. Increasing the
energy density of the QGP above deconfinement first leads to \psiP dissociation,
removing those \jpsi's which otherwise would have come from \psiP decays. Next
the \chic melts, and only for a sufficiently hot medium also the direct \jpsi
disintegrate. For the bottomonium states, a similar pattern
holds~\cite{Aarts:2013kaa,Aarts:2011sm,Aarts:2010ek,Aarts:2014cda}.

\begin{figure}[!t]
  \centering
  \includegraphics[height=4cm]{seq-charm}\hskip1cm
  \includegraphics[height=4cm]{seq-bottom}
  \caption{Sequential quarkonium suppression for \jpsi (left) and \upsa (right)
    states~\cite{Karsch:2005nk}.}
  \label{fig:seq}
\end{figure}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../SaporeGravis"
%%% End: 
