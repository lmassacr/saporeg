%\subsection{Production mechanisms of open and closed heavy-flavour in $pp$ collisions}
% (6 pages)
%\subsubsection{Open heavy-flavour production (IS)}
 %(2 pages)
%FONLL, different schemes, where they agree/differ, comparison with $k_T$ factorisation, ...
%$B_c$ ? and multiply heavy baryon 

Open-heavy-flavour production in hadronic collisions provides important
tests of our understanding of various aspects of Quantum Chromo-dynamics (QCD).
%
First of all, the heavy-quark mass ($m_Q$) acts as a long distance cut-off so that
this process can be calculated in the framework of perturbative QCD
down to low \pt and it is possible to compute the 
total cross section by integrating over \pt.
%
Second, the presence of multiple hard scales ($m_Q$, \pt) allows us to study
the perturbation series in different kinematic regions ($\pt < m_Q$, $\pt\sim m_Q$, $\pt \gg m_Q$).
%
Multiple hard scales are also present in other collider processes of high interest
such as weak boson production, Higgs boson production and many cases of
physics Beyond the Standard Model.
%
Therefore, the detailed study of heavy-flavour production and the comparison to experimental data provides an important 
testing ground for the theoretical ideas that deal with this class of problems.

On the phenomenological side, the (differential) cross section for open-heavy-flavour production is sensitive to
the gluon and the heavy-quark content in the nucleon, so that LHC data in \pp and \pPb collisions can provide valuable 
constraints on these parton-distribution functions (PDFs) inside the proton and the lead nucleus, respectively.
%
In addition, these cross sections in \pp and \pA collisions establish the baseline for the study of heavy-quark production
in heavy-ion collisions. This aspect is a central point in heavy-ion physics since the suppression of heavy quarks at large \pt is
an important signal of the QGP (see \sect{OHF}).
%
Finally, let us also mention that a solid understanding of open-charm production is needed in cosmic-ray and neutrino astrophysics~\cite{Bhattacharya:2015jpa}.
%
In the following, we will focus on \pp collisions and review the different theoretical approaches 
to open-heavy-flavour production.

\paragraph{Fixed-Flavour-Number Scheme}%: ($p_T \lesssim \text{few} \times m$)}
Conceptionally, the simplest scheme is the Fixed-Flavour-Number Scheme (FFNS) 
where the heavy quark is not an active parton in the proton.
Relying on a factorisation theorem, the differential cross section for the inclusive production of a heavy quark $Q$
can be calculated as follows:
\begin{equation}
\dd\sigma^{Q+X}[s,\pt,y, m_Q] \simeq \sum_{i,j} \int_0^1 \dd x_i \int_0^1 dx_j\,  f^A_i(x_i,\mu_F) f^B_j(x_j,\mu_F)  
\dd\tilde \sigma_{ij\to Q+X}[x_i,x_j,s,\pt,y,m_Q,\mu_F,\mu_R]\, , 
\label{eq:xsQ}
\end{equation}
or in short
\begin{equation}
\dd\sigma^{Q+X} \simeq \sum_{i,j} f^A_i \otimes f^B_j \otimes 
\dd\tilde \sigma_{ij\to Q+X}\, ,
\label{eq:xsQ2}
\end{equation}
where \pt and $y$ are the transverse momentum and the rapidity of the heavy quark and $s$ is the
square of the hadron centre-of-mass energy.
%
The PDFs $f_i^A$ ($f_j^B$) give the number density of the parton of flavour `$i$' (`$j$') inside the hadron `$A$' (`$B$')
carrying a fraction $x_i$ ($x_j$) of the hadron momentum
at the factorisation scale \muf. Furthermore, the short-distance cross section $\dd \tilde \sigma$ is the partonic cross section 
from which the so-called mass singularities or collinear singularities associated to the light quarks and the gluon have been removed 
via the mass-factorisation procedure and which therefore also depends on \muf. 
The partonic cross section also depends on the strong coupling constant $\alpha_s$, which is evaluated at the renormalization scale 
$\mu_R$.
As a remainder of this procedure, the
short-distance cross section will depend on logarithms of the ratio of \muf with the hard scale. In order to avoid large
logarithmic contributions, the factorisation scale \muf should be chosen in the vicinity of the hard scale. Also the renormalisation scale
$\mu_R$ is determined by the hard scale.
The tilde is used to indicate that the finite collinear logarithms of the heavy-quark mass present in the partonic cross section
have not been removed by the mass-factorisation procedure. These logarithms are therefore not resummed to all orders in the FFNS
but are accounted for in Fixed-Order (FO) perturbation theory. 
%
The error of the approximation in~\eq{eq:xsQ} is suppressed by an inverse power of the hard scale
which is set by the mass or the transverse momentum of the heavy quark, \ie it is 
on the order of ${\cal O}((\Lambda/\muf)^p)$ where $\Lambda\sim 200\MeV$ is a typical hadronic scale, and $p=1$ or 2.

In \eq{eq:xsQ}, a sum over all possible subprocesses $i+j \to Q + X$ is understood, where $i,j$ are the 
active partons in the proton:
$i,j \in \{q,\overline q=(u,\overline u, d, \overline d, s, \overline s), g\}$ for a FFNS with three active flavours (3-FFNS) usable for both
charm and beauty production, and
$i,j \in \{q,\overline q=(u,\overline u, d, \overline d, s, \overline s, c, \overline c), g\}$ in the case of four active flavours (4-FFNS) 
often used for beauty production. In the latter case, the charm quark is also an active parton (for $\muf > m_c$)
and the charm-quark mass is neglected in the hard-scattering cross section $\dd \tilde \sigma$ whereas the beauty
quark mass $m_b$ is retained.
%
At the leading order (LO) in $\alpha_S$, there are only two subprocesses which contribute: 
(i) $q+\overline q \to Q+ \overline Q$, (ii) $g + g \to Q+ \overline Q$.
At the next-to-leading order (NLO), the virtual one-loop corrections to these $2\to 2$ processes have to be included
in addition to the following $2\to 3$ processes:
(i) $q+\overline q \to Q+ \overline Q + g$, (ii) $g + g \to Q+ \overline Q + g$, (iii) $g+q\to q + Q+ \overline Q$ and $g+\overline q\to \overline q + Q+ \overline Q$.
Complete NLO calculations of the integrated/total cross section 
%which can be reliably computed in the FFNS due to the heavy quark mass 
and of one-particle inclusive distributions
were performed in the late 80's~\cite{Nason:1987xz,Nason:1989zy,Beenakker:1988bq,Beenakker:1990ma}.
%
These calculations form also the basis for more differential observables/codes~\cite{Mangano:1991jk} 
(where the phase space of the second heavy quark has not been integrated out) allowing us to study the correlations 
between the heavy quarks -- sometimes referred to as NLO MNR.
%
They are also an important ingredient to the other theories discussed below (FONLL, GM-VFNS, POWHEG, MC@NLO).


%\begin{figure}[!htp]
%	\centering
%	\includegraphics[width=0.4\columnwidth]{pp/Figures/tev_b_ptlffns_logpt.pdf}
%	\includegraphics[width=0.5\columnwidth]{pp/Figures/correlation/2012-Sep-11-yields_vs_mult.eps}
%	\caption{Comparison of the predictions for $B^+$ production in the FFNS with CDF data \protect\cite{Acosta:2004yw}.
%	The dashed lines represent the theory error obtained by varying renormalisation and factorisation scales
%	by factors of 2 and 1/2 around the default value $\mu_F=\mu_R=\sqrt{m_Q^2+p_T^2}$. Figure taken from
%	\protect\cite{Kniehl:2015fla}.}
%	\label{fig:ffns1}
%\end{figure}




The typical range of applicability of the FFNS at NLO is roughly $0 \le \pt \lesssim 5 \times m_Q$.
A representative comparison with data has been made for $\rm B^+$ production in~\cite{Kniehl:2015fla}
where it is clear that the predictions of the FFNS at NLO using the branching fraction $B(b \to {\rm B}) = 39.8\%$ 
starts to overshoot the Tevatron data for $\pt \gtrsim 15\GeV/c$ 
even considering the theoretical uncertainties here that has been evaluated by varying the renormalisation and factorisation scales
by factors of 2 and 1/2 around the default value\footnote{
We stress here that this widespread procedure to assess theoretical uncertainties associated with pQCD computations does not provide
values which should be interpreted as coming from a statistical procedure. 
%To phrase it differently, 
%theorist do not mean that there are 5 chances per cent  that the true value lies outside the 2-$\sigma$ range. 
This is only an estimate of the missing contributions at higher order QCD corrections.
} $\muf =\mur =m_{\rm T}$ with $m_{\rm T}=\sqrt{m_Q^2+\pt^2}$.

%This is illustrated in Fig.\ \ref{fig:ffns1} where the predictions for $B^+$ production
%are shown in comparison with CDF data \cite{Acosta:2004yw}.
%Here, the dashed lines represent the theory error obtained by varying renormalisation and factorisation scales
%by factors of 2 and 1/2 around the default value $\mu_F=\mu_R=m_T$ with $m_T=\sqrt{m_Q^2+p_T^2}$.
%
Such a kind of discrepancies at increasing \pt can be attributed to the shift of the momentum between the $b$ quark and
the $B$ meson which can be accounted for by a fragmentation function (FF).
Indeed, the scope of the FFNS can be extended to slightly larger \pt by convoluting the differential cross section
for the production of the heavy quark $Q$ with a suitable, scale-independent, FF $D_Q^H(z)$ describing the
transition of the heavy quark with momentum $p_Q$ into the observed heavy-flavoured hadron $H$ 
with momentum $p_H=z\,p_Q$
(see~\cite{Kniehl:2015fla}):
\begin{equation}
\dd\sigma^H = \dd\sigma^Q \otimes D_Q^H(z)\, .
\label{eq:xsB}
\end{equation}
At large transverse momenta, the differential cross section falls off with a power-like behavior 
$\dd\sigma^Q/\dd\pt \propto 1/\pt^n$ with $n=4,5$ so that the convolution with the fragmentation function (FF) effectively
corresponds to a multiplication with the fourth or fifth Mellin moment of this FF which lowers the cross section and leads to 
an improved agreement with the data at large \pt.
It should be noted that this FF is included on a purely phenomenological basis and
there are ambiguities on how the convolution prescription is implemented ($E_H = z\, E_Q$, $\vec{p}_{\rm T}^H = z\, \vec{p}_{\rm T}^Q$) leading to differences at $\pt \simeq m_Q$. Furthermore, at NLO, a harder FF should be used than at LO
in order to compensate for the softening effects of the gluon emissions.
%
Apart from this, it is generally believed that this scale-independent FF is universal and can be extracted from data, \eg from
$\ee$ data.

The same conclusions about the range of applicability of the FFNS apply at the LHC 
where the heavy-quark production is dominated by the $gg$-channel (see, \eg Figure~3 in~\cite{Kniehl:2015fla}) over
the \qqbar one.
%
As can be seen, the uncertainty at NLO due to the scale choice is very large (about a factor of two). 
For the case of top pair production, complete NNLO calculations are now available for both
the total cross section~\cite{Czakon:2013goa} and, most recently, differential distributions~\cite{Czakon:2014xsa}.
%
To make progress, it will be crucial to have NNLO predictions for charm and beauty production as well.
%Once such results are available, the FFNS will again play a major role in open-heavy-flavour production 
%due to the reduced theoretical uncertainties and because it will most likely be applicable in a wider \pt-range.
%

%The details depend (which subprocesses, fragmentation functions) the scheme

%\begin{equation}
%d\sigma^Q = \sum_{a,b,c} f^A_a \otimes f^B_b \otimes 
%d\sigmahat_{ab\to c}[p_T,m] \otimes D_{c}^Q}
%\label{eq:xsQ}
%\end{equation}
%where the $D_{c}^{Q}(z,\muf)$ are perturbatively calculable fragmentation functions
%(PFFs) which depend on the factorization scale $\muf$.
%Note that the PFFs depend also on the heavy quark mass $m_Q$ which enters
%the boundary conditions for the DGLAP evolution of the PFFs \cite{Mele:1991cw}.


%\begin{itemize}
%\item FFNS at NNLO will be interesting as well!
%\item FFNS testing lab to combine with energy loss simulations; in POWHEG PS predefined
%\item Comment on heavy quark energy loss in FFNS and VFNS, POWHEG
%\item Mention existing studies and comparisons of different schemes.
%\item Make a table of different calculations?
%\item Discuss the mass terms which have been used. 
%Values, schemes, consistency with PDFs and FFs
%\item Comment on gluon FF. No error FFs exist.
%\item Make a comment on the gluon fragmentation function; perturbatively calculated;
%important contribution; not really tested; sensitivity to the value of the quark mass in the boundary condition(?)
%\end{itemize}



\paragraph{ZM-VFNS}
For $\pt \gg m_Q$, the logarithms of the heavy-quark mass ($\frac{\alpha_s}{2\pi} \ln (\pt^2/m_Q^2)$)
become large and will eventually have to be resummed to all orders in the perturbation theory. 
%
This resummation is realised by absorbing the large logarithmic terms into the PDFs and FFs whose scale-dependence is governed by 
renormalisation group equations, the DGLAP evolution equations.
%
This approach requires that the heavy quark is treated as an active parton for factorisation scales
$\muf \ge \mu_T$ where the transition scale $\mu_T$ is usually (for simplicity) identified with the heavy-quark mass. 
%
Such a scheme, where the number of active flavours is changed when crossing the transition scales is
called a Variable-Flavour-Number Scheme (VFNS).
%
If, in addition, the heavy-quark mass $m_Q$ is neglected in the calculation of the short-distance cross sections,
the scheme is called Zero-Mass VFNS (ZM-VFNS).
%
The theoretical foundation of this scheme is provided by a well-known factorisation theorem
and the differential cross section for the production of a heavy-flavoured hadron ($A+B \to H +X$)
is calculated as follows:
%
\begin{equation}
\dd\sigma^{H+X} \simeq \sum_{i,j,k} \int_0^1 \dd x_i \int_0^1 \dd x_j\, \int_0^1 \dd z\ 
f^A_i(x_i,\mu_F) f^B_j(x_j,\mu_F)  
\dd\hat \sigma_{ij\to k+X} D_k^H(z,\mu_F')
+{\cal O}(m_Q^2/\pt^2)
\, .
\label{eq:xsH}
\end{equation}
Because the heavy-quark mass is neglected in the short-distance cross sections ($\dd\hat \sigma$), the predictions
in the ZM-VFNS are expected to be reliable only for very large transverse momenta.
%
The sum in \eq{eq:xsH} extends over a large number of subprocesses $i+j\to k+X$ since $a,b,c$ can be gluons, light quarks,
and heavy quarks. A calculation of all subprocesses at NLO has been performed in the late 80's~\cite{Aversa:1988vb}.

Concerning the FFs into the heavy-flavoured hadron $H={\rm D,B},\Lambda_c,\ldots$, two main approaches are employed in the literature:
\begin{itemize}
\item In the  Perturbative-Fragmentation Functions (PFF) approach~\cite{Cacciari:1993mq}, 
the FF $D_k^H(z,\mu_F')$ is given by a convolution of a PFF accounting for the fragmentation of the parton $k$ into the heavy quark $Q$, $D_k^Q(z,\mu_F')$, with a scale-independent FF $D_Q^H(z)$ describing the hadronisation of the heavy quark into the hadron $H$:
\begin{equation}
D_k^H(z,\mu_F') = D_k^Q(z,\mu_F') \otimes D_Q^H(z)\, .
\label{eq:pff}
\end{equation}
The PFFs resum the final-state collinear logarithms of the heavy-quark mass.
Their scale-dependence is governed by the DGLAP evolution equations and the boundary conditions for the PFFs
at the initial scale are calculable in the perturbation theory.
%
On the other hand, the scale-independent FF is a non-perturbative object (in the case of heavy-light flavoured hadrons)
which is assumed to be universal. It is usually determined by a fit to $\ee$ data, although approaches exist in the literature which 
attempt to compute these functions.
%
It is reasonable to identify the scale-independent fragmentation function in~\eq{eq:xsB} with the one in~\eq{eq:pff}.
%
This function describing the hadronisation process involves long-distance physics and might be modified in the presence of a 
QGP, whereas the PFFs (or the unresummed collinear logarithms $\ln \pt^2/m_Q^2$ in the FFNS) involve only short-distance 
physics and are the same in \pp, \pA, and \AAcoll collisions.
%
\item In the Binnewies-Kniehl-Kramer (BKK) approach~\cite{Binnewies:1998xq,Binnewies:1997gz,Binnewies:1998vm}, 
the FFs are not split up into a perturbative and a non-perturbative piece.
Instead, boundary conditions at an initial scale $\mu_F' \simeq m_Q$ 
are determined from \ee data for the full non-perturbative FFs, $D_k^H(z,\mu_F')$,
in complete analogy with the treatment of FFs into light hadrons (pions, kaons). 
These boundary conditions are again evolved to larger scales $\mu_F'$ with the help of the DGLAP equations.
\end{itemize}
%
It is also noteworthy that the BKK FFs ($D(z,\mu_F')$) are directly determined as functions in $z$-space whereas the FFs in the PFF approach are determined in Mellin-N-space where the N-th Mellin moment of a function $f(z)$ ($0<z<1$) is defined as $f(N) = \int_0^1 dz\ z^{N-1} f(z)$.
%
\paragraph{GM-VFNS}
The FFNS and the ZM-VFNS are valid only in restricted and complementary regions of the transverse momentum.
For this reason, it is crucial to have a unified framework which combines the virtues of the massive FO calculation in the FFNS 
and the massless calculation in the ZM-VFNS.
%
The General-Mass VFNS (GM-VFNS)~\cite{Kniehl:2004fy,Kniehl:2005mk} 
is such a framework which is valid in the entire kinematic range from the smallest to the largest transverse momenta ($\pt \ll m_Q, \pt \simeq m_Q, \pt \gg m_Q$).
%
It is very similar to the ACOT 
heavy-flavour scheme~\cite{Aivazis:1993kh,Aivazis:1994pi} which has been formulated for structure functions in deep inelastic scattering (DIS).
Different variants of the ACOT scheme exist like the S-ACOT scheme~\cite{Kramer:2000hn} and the (S)-ACOT$_\chi$ scheme \cite{Tung:2001mv}
which are used in global analyses of PDFs by the CTEQ collaboration and
the ACOT scheme has been extended to higher orders in Refs.\ \cite{Kretzer:1998ju,Stavreva:2012bs,Guzzi:2011ew}.
%
The theoretical basis for the ACOT scheme has been laid out in an all-order proof of a factorisation theorem with massive quarks by 
Collins~\cite{Collins:1998rz}.
%
While the discussion in~\cite{Collins:1998rz} deals with inclusive DIS, it exemplifies the general principles for the treatment of heavy quarks 
in perturbative QCD (see also~\cite{Thorne:2008xf,Olness:2008px}) which should be applicable to other processes as well.
%
Therefore, it is very important to test these ideas also in the context of less inclusive observables.
%
First steps in this direction had been undertaken in~\cite{Kretzer:1997pd,Kretzer:1998nt}
where the ACOT scheme had been applied to inclusive D meson production in DIS.
%
The case of hadroproduction in the ACOT scheme had been studied for the first time in~\cite{Olness:1997yc} 
taking into account the contributions from the NLO calculation in the FFNS 
%\cite{Nason:1987xz,Nason:1989zy,Beenakker:1988bq,Beenakker:1990ma} 
combined with the massless contributions in the ZM-VFNS from all other subprocesses at ${\cal O}(\alpha_s^2)$ 
%\cite{Aversa:1988vb}
resumming the collinear logarithms associated to the heavy quark at the leading-logarithmic (LL) accuracy.
%
In contrast, the GM-VFNS has a NLO+NLL accuracy. It has been worked out for $\gamma\gamma$, \pp, \ppbar, $\ee$, $e$p, and $\gamma$p 
collisions in a series of papers~\cite{Kramer:2001gd,Kramer:2003cw,Kramer:2003jw,Kniehl:2004fy,Kniehl:2005ej,Kneesch:2007ey,Kniehl:2008zza,Kniehl:2009mh,Kniehl:2011bk,Kniehl:2012ti,Kniehl:2015fla}
and has been successfully compared to experimental data from LEP, HERA, Tevatron and the LHC.
%
Furthermore, inclusive lepton spectra from heavy-hadron decays have been studied for \pp collisions at the LHC at 2.76 and 7\TeV
centre-of-mass energy~\cite{Bolzoni:2012kx} and compared to data from ALICE, ATLAS and CMS. 
%
In addition, predictions have been obtained for D mesons produced at \s = 7\TeV from B decays~\cite{Bolzoni:2013vya}.
A number of comparisons with  hadroproduction data are discussed in \sect{sec:pp:Data}.


The cross section for inclusive heavy-flavour hadroproduction in the GM-VFNS is calculated using a factorisation formula similar to the one in \eq{eq:xsH}:
\begin{equation}
\dd\sigma^{H+X} \simeq \sum_{i,j,k} \int_0^1 \dd x_i \int_0^1 \dd x_j\, \int_0^1 \dd z\ 
f^A_i(x_i,\mu_F) f^B_j(x_j,\mu_F)  
\dd\hat \sigma_{ij\to k+X}[\pt,m_Q] D_k^H(z,\mu_F')
%+{\cal O}(m^2/p_T^2)
\, .
\label{eq:xsH2}
\end{equation}
In particular, the same subprocesses as in the ZM-VFNS are taken into account. 
%
However, the finite heavy-quark-mass terms (powers of $m_Q^2/\pt^2$) are  retained in the short-distance cross sections
of subprocesses involving heavy quarks.
%
More precisely, 
the heavy-quark-mass terms are taken into account in the
subprocesses $q+\overline q \to Q+ X$, $g + g \to Q+X$, $g+q\to Q+ X$ and $g+\overline q\to Q+X$ which are also present
in the FFNS. 
However, in the current implementation, they are neglected in the heavy-quark-initiated subprocesses 
($Q+g\to Q+X$, $Q+g\to g+X$, \ldots) as it is done in the S-ACOT scheme~\cite{Kramer:2000hn}.
%
The massive hard-scattering cross sections are defined in a way that they approach, 
in the limit $m_Q/\pt \to 0$, the massless hard-scattering cross sections defined in the $\overline{\rm MS}$ scheme.
Therefore, the GM-VFNS approaches the ZM-VNFS at large $\pt \gg m_Q$.
%
It can be shown that the GM-VFNS converges formally to the FFNS at small \pt.
However, while the S-ACOT scheme works well for the computation of DIS structure functions
at NLO, this scheme causes problems in the hadroproduction case at low \pt because the 
massless $\bquark$-quark initiated cross sections diverge in the limit $\pt \to 0$. 
%
This problem can be circumvented by a suitable choice for the factorisation scale
so that the heavy-quark PDF is switched off sufficiently rapidly and the GM-VFNS approaches
the FFNS at small $\pt$~\cite{Kniehl:2015fla}.


\paragraph{FONLL}
Similar to the GM-VFNS, the Fixed-Order plus Next-to-Leading  Logarithms (FONLL) approach~\cite{Cacciari:1998it} 
is a unified framework which is valid in the entire kinematic range ($\pt \ll m_Q, \pt \simeq m_Q, \pt \gg m_Q$).
This approach has also been applied to DIS structure functions and is used in the global analyses of PDFs
by the NNPDF collaboration~\cite{Forte:2002fg,Ball:2011mu}. Predictions for $\cquark$ and $\bquark$ quark production at the LHC with a centre-of-mass energy
of 7\TeV have been presented in~\cite{Cacciari:2012ny}.
% Predictions for RHIC
The FONLL scheme is based on the matching of the massive NLO cross section in the FFNS (=FO)
with the massless NLO calculation in the ZM-VNFS (=RS) according to the prescription
\begin{equation}
\dd\sigma_{\rm FONLL} = \dd\sigma_{\rm FO} + G(m_Q,\pt) \times \left(\dd\sigma_{\rm RS} - \dd\sigma_{\rm FOM0}\right)
\end{equation}
where $\dd\sigma_{\rm FOM0}$ is the cross section $\dd\sigma_{\rm FO}$ in the asymptotic limit
$\pt \gg m_Q$ where the finite power-like mass terms can be neglected and the cross section is dominated
by the collinear logarithm of the heavy-quark mass.

The condition $\dd\sigma_{\rm FONLL} \to d\sigma_{\rm RS}$ for $\pt \gg m_Q$ implies that
the matching function $G(m_Q,p_T)$ has to approach unity in this limit.
%
Furthermore, in the limit of small transverse momenta $\dd\sigma_{\rm FONLL}$ has to approach the fixed-order
calculation $\dd\sigma_{\rm FO}$.
This can be achieved by demanding that $G(m_Q,p_T)\to 0$ for $\pt \to 0$ which effectively suppresses the contribution from the divergent $b$-quark initiated contributions in $\dd \sigma_{RS}$.
In the FONLL, the interpolating function is chosen to be
$G(m_Q,\pt) = \pt^2/\left(\pt^2 + a^2 m_Q^2\right)$
where the constant is set to $a=5$ on phenomenological grounds.
In this language the GM-VFNS is given by
%\begin{equation}
$\dd\sigma_{\rm GM-VFNS} = \dd\sigma_{\rm FO} + \dd\sigma_{\rm RS} - \dd\sigma_{\rm FOM0}$ ,
%\end{equation}
\ie no interpolating factor is used. 

Other differences concern the non-perturbative input. In particular, the FONLL scheme uses fragmentation functions 
in the PFF formalism whereas the GM-VFNS uses fragmentation functions which are determined in the $z$-space
in the BKK approach.
% Error bands: GM-VFNS, mu_F and mu_F' varied independently.


\paragraph{Monte Carlo generators}
The GM-VFNS and FONLL calculations are mostly analytic and provide a precise description of the inclusive production of a
heavy hadron or its decay products at NLO+NLL accuracy.
%
Compared to this, general-purpose Monte-Carlo generators like PYTHIA~\cite{Sjostrand:2007gs} or HERWIG~\cite{Corcella:2000bw}
allow for a more complete description of the hadronic final state but only work at LO+LL accuracy.
%
However, in the past decade, NLO Monte Carlo generators have been developed using the MC@NLO~\cite{Frixione:2003ei}
and POWHEG~\cite{Frixione:2007nw} methods for a consistent matching of NLO calculations with parton showers.
%
They, therefore, have all the strengths of Monte Carlo generators, which allow for a complete modeling of the
hadronic final state (parton showering, hadronisation, decay, detector response), while, at the same time, the NLO accuracy
in the hard scattering is kept and the soft/collinear regimes are resummed at the LL accuracy.
%
A comparison of POWHEG NLO Monte Carlo predictions for heavy-quark production in \pp collisions 
at the LHC with the ones from the GM-VFNS and FONLL can be found in~\cite{Klasen:2014dba}.

%\paragraph{$k_T$ factorisation}

