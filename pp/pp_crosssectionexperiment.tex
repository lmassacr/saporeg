
Due to their short lifetimes (at most a picosecond), the production of open-heavy-flavour particles is studied through their decay products.
Four main techniques are used:
\begin{enumerate}
\item Fully reconstruction of  exclusive decays, such as ${\rm B}^0 \to \jpsi \, {\rm K^0_S}$ or $\Dzero \to {\rm K}^- \, \pi^+$.
\item Selection of specific (semi-)inclusive decays. 
For beauty production, one looks for a specific particle, for example \jpsi, and imposes it to point to a secondary vertex displaced 
a few hundred~\footnote{
For larger \pt or $y$, such a distance can significantly be larger.
} $\mu$m from the primary vertex. 
Such {\it displaced} or {\it non-prompt} mesons are therefore supposed to come from $b$-decay only.
\item Detection of leptons from these decays. This can be done 
{\it (i)} by subtracting a cocktail of known/measured sources (photon conversions, Dalitz decays of $\pi^0$ and $\eta$ in the case of electrons, light hadron, Drell-Yan pair, \jpsi,\dots) to the lepton yield. 
Alternatively, the photon conversion and Dalitz decay contribution can be evaluated via an invariant mass analysis of the \ee pairs. 
{\it (ii)} By selecting displaced leptons with a track pointing to a secondary vertex separated by few hundred $\mu$m from the primary vertex.
\item Reconstruction of $\cquark$- and $\bquark$-jets. Once a jet is reconstructed, a variety of reconstructed objects, such as tracks, vertices and identified leptons, are used to distinguish between jets from light or from heavy flavour. A review of $\bquark$-tagging methods used by the CMS Collaboration can be found in~\cite{Chatrchyan:2012jua}.
\end{enumerate}
Different methods are used in different contexts, depending on the detector information available, the trigger strategy, the corresponding statistics 
(hadronic decays are less abundant than leptonic ones), the required precision (only exclusive decay channels 
allow for a full control of the kinematics), the kinematical range ($\bquark$-tagged jets give 
access to very large \pt whereas exclusive-decay channels allow for differential studies down to \pt equals to 0). A fifth method based on the indirect extraction of  the total charm- and beauty-production 
from {\it dileptons} -- as opposed to single leptons-- (see \eg~\cite{Abreu:2000nj}) is not discussed in this review.




Hidden-heavy-flavour, \ie quarkonia, are also analysed through their decay products.
The triplet $S$-waves are the most studied since they decay up to a few per cent of the time
in dileptons. This is the case for $\jpsi$, $\psiP$, $\upsa$, $\upsb$, $\upsc$. The triplet $P$-waves, 
such as the $\chic$ and $\chib$, are 
usually reconstructed via their radiative decays into a triplet $S$-wave. 
For other states, such as the singlet $S$-wave, studies are far more complex. The very first inclusive
hadroproduction study of $\eta_c$ was just carried out this year in the \ppbar 
decay channel by the LHCb collaboration~\cite{Aaij:2014bga}. 


%
%
A compilation of the measurements of the \pt-integrated \ccbar and \bbbar cross section, 
$\sigma_{\ccbar}$ and $\sigma_{\bbbar}$, is shown in \fig{fig:pp:CharmAndBottomXsec} from 
SPS to LHC energies. Let us stress that most of the \pt-integrated results and nearly all $y$-integrated ones
are based on different extrapolations, which significantly depend on theoretical inputs and which are not necessarily identical
in the presence of nuclear effects.  The results are described within the uncertainties by pQCD calculations, 
NLO MNR~\cite{Mangano:1991jk}  and FONLL~\cite{Cacciari:2003uh, Cacciari:2012ny} for the $\ccbar$ and $\bbbar$, 
respectively. Note that most of the experimental results for  $\sigma_{\ccbar}$, in particular
at high energies, lie on the upper edge of the NLO MNR uncertainties.
%
%
\begin{figure}[!t]
\begin{center}
\includegraphics[width=0.4\textwidth]{./pp/Figures/CharmCrossSection_vs_Energy.eps}
\includegraphics[width=0.41\textwidth]{./pp/Figures/ALICE_Fig6_1405_4144v1.pdf}
\caption[Compilation of total beauty cross section measurements]{
\label{fig:pp:CharmAndBottomXsec}
Left: Total (extrapolated) $\ccbar$ cross section as a function of \s~\cite{Bortoletto:1988kw,Barate:1999bg,Acosta:2003ax,Adamczyk:2012af,Abelev:2012vra,%ATLAS:2011wsp,
LHCb:2010lga,Aaij:2013mga}. 
Data in proton--nucleus (\pA) or deuteron--nucleus (d--A) collisions were scaled down assuming no nuclear effect. 
Right: A compilation of the $\bbbar$ differential cross section measurements at mid-rapidity in \pp and \ppbar collisions~\cite{Abelev:2014hla,Abelev:2012gx,Albajar:1990zu,Abe:1995dv,Adare:2009ic}.
Results are compared to pQCD calculations, NLO MNR~\cite{Mangano:1991jk} and FONLL~\cite{Cacciari:2003uh, Cacciari:2012ny} for $\ccbar$ and $\bbbar$, respectively. 
}
\end{center}
\end{figure}



%
%_____________________________________________________________________________
%
%
% HF decay leptons
%
\subsubsection{Leptons from heavy-flavour decays}
\label{subsubsec:pp:HFLeptons}

The first open-heavy-flavour measurements in heavy-ion collisions were performed by 
exploiting heavy-flavour decay leptons at RHIC by the PHENIX and STAR collaborations. These were done 
both in \pp and \AAcoll collisions~\cite{Adare:2006hc,Adare:2010ud,Adare:2013xlp,Aggarwal:2010xp,Agakishiev:2011mr}. 
At the LHC, the ATLAS and ALICE collaborations have also performed such studies in heavy-ion collisions~\cite{Abelev:2012xe,Abelev:2014gla,Abelev:2012pi,Abelev:2012qh,Aad:2011rr}. 
A selection of the \pt-differential production cross sections of heavy-flavour decay leptons in \pp collisions at different rapidities and energies is presented in \fig{fig:pp:HFlepton}. 
The measurements are reported together with calculations of FONLL~\cite{Cacciari:2003uh, Cacciari:2012ny} for  \s = 0.2 and 2.76 \TeV, GM-VFNS~\cite{Kniehl:2004fy,Kniehl:2005mk} and \kt-factorisation~\cite{Maciula:2013wg} at \s = 2.76\TeV. 
The POWHEG predictions~\cite{Klasen:2014dba}, not shown in this figure, show a remarkable agreement with the FONLL ones.
The differential cross sections of heavy-flavour-decay leptons are well described by pQCD calculations. 


In addition, leptons from open charm and beauty production can be separated out via:
{\it (i)} a cut on the lepton impact parameter, \ie~the distance between the origin of the lepton and the collision primary vertex, 
{\it (ii)} a fit of the lepton impact parameter distribution using templates of the different contributions to the inclusive spectra, 
{\it (iii)} studies of the azimuthal angular correlations between heavy-flavour decay leptons and charged hadrons (see \eg~\cite{Abelev:2012sca,Abelev:2014hla}). These measurements are also described by pQCD calculations.

\begin{figure}[!ht]
\begin{center}
%
\subfigure[PHENIX, $\hfe$, \s = 200\GeV]{
	\label{fig:pp:PHENIXHFe}
	% arXiv 0609010
		\includegraphics[width=0.31\columnwidth,height=0.31\columnwidth]{./pp/Figures/PHENIX_HFe.pdf}
	} 
\subfigure[ALICE, $\hfe$, $\s=2.76$\TeV]{
	\label{fig:pp:ALICE:HFe}
	% 1405.4117v1
	\includegraphics[width=0.31\columnwidth,height=0.31\columnwidth]{./pp/Figures/ALICE_hfe_vs_pp2dot76TeV.pdf}
	} 
\subfigure[ALICE, $\hfm$, $\s=2.76$\TeV]{
	\label{fig:pp:ALICE:HFm}
	% 1201.3791v1
	\includegraphics[width=0.31\columnwidth,height=0.31\columnwidth]{./pp/Figures/ALICE_HFmu-2_76-new.pdf}
	} 
\caption{
	\label{fig:pp:HFlepton}
	$\dsdpt$ for heavy-flavour decay leptons in \pp collisions: (a) electrons at mid-rapidity for \s = 200\GeV from PHENIX~\cite{Adare:2006hc}, (b) electrons at mid-rapidity for \s = 2.76\TeV~\cite{Abelev:2014gla} and (c) muons at forward-rapidity for \s = 2.76\TeV from ALICE~\cite{Abelev:2012pi}. 
	FONLL~\cite{Cacciari:2003uh, Cacciari:2012ny} predictions are also shown; in (a) and (c) the calculations for leptons from charm and beauty decays are shown separately (without theoretical uncertainty bands in (a)). 
	GM-VFNS~\cite{Kniehl:2004fy,Kniehl:2005mk} and \kt-factorisation~\cite{Maciula:2013wg} calculations are also drawn in (b). 
}
\end{center}
\end{figure}
%




%
%_____________________________________________________________________________
%
%
%
\subsubsection{Open charm}
\label{subsubsec:pp:OpenCharm}

Recently, D-meson production has been studied at RHIC, Tevatron and LHC energies~\cite{Adamczyk:2012af,%STAR
Acosta:2003ax,Reisert:2007zza,%CDF
ALICE:2011aa,Abelev:2012vra,Abelev:2012tca,%ALICE
Aaij:2013mga}. %LHCb 
The measurements were performed by fully reconstructing the hadronic decays of the D mesons, \eg~$\Dzero  \to {\rm K}^-\pi^+$ and charge conjugates. 
D-meson candidates are built up of pairs or triplets of tracks with the appropriate charge sign combination. The analyses exploit the detector particle identification abilities to reduce the combinatorial background, which is important at low \pt. 
For the measurements at Tevatron and LHC,
the background is also largely reduced by applying topological selections on the kinematics of the secondary decay vertices, typically displaced by few hundred $\mu$m from the interaction vertex. 
The results at RHIC energies report the {\it inclusive} D-meson yields~\cite{Adamczyk:2012af}, \ie those from both $\cquark$ and $\bquark$ quark fragmentation. The former are called {\it prompt}, and the later {\it secondary} D mesons. The measurements at Tevatron and LHC energies report prompt D-meson yields. Prompt yields include both the {\it direct} production and the {\it feed-down} from excited charmed resonances.
%
The secondaries contribution to the D-meson yields is evaluated and subtracted by: 
{\it (i)} either scrutinising the D-meson candidates impact parameter distribution, exploiting the larger lifetime of $\bquark$- than $\cquark$-flavoured hadrons~\cite{Acosta:2003ax,Reisert:2007zza,Aaij:2013mga}, which requires large statistics, 
{\it (ii)} or evaluating the beauty hadron contribution using pQCD-based calculations~\cite{ALICE:2011aa,Abelev:2012vra,Abelev:2012tca}, advantageous strategy for smaller data samples but limited by the theoretical uncertainties.

\begin{figure}[!ht]
\begin{center}
\subfigure[STAR, inclusive $\Dzero$ and $\Dstarplus$, \s = 200\GeV]{
	\label{fig:pp:STAR:D}
	% 1204.4244v3
	\includegraphics[height=0.24\columnwidth]{./pp/Figures/STAR_Dmeson_pp_Yields.eps}
	} 
\subfigure[ALICE, prompt $\Dplus$, \s = 7\TeV]{
	\label{fig:pp:ALICE:Dplus}
	\includegraphics[height=0.24\columnwidth]{./pp/Figures/ALICE_DplusCrossSection_pp7TeV-new.pdf}
	} 
	%1302.2864
\subfigure[LHCb, prompt $\Ds$, \s = 7\TeV]{
	\label{fig:pp:LHCb:Ds}
	\includegraphics[height=0.24\columnwidth]{./pp/Figures/LHCb_DsplusTH.pdf}
	} 
\caption{
\label{fig:pp:Dmeson}
$\dsdpt$ for D meson production  in \pp collisions at different energies. 
(a): $\Dzero$ and $\Dstarplus$ measurements at \s = 200\GeV with the STAR detector~\cite{Adamczyk:2012af}. 
(b): $\Dplus$ data at \s = 7\TeV with the ALICE detector~\cite{ALICE:2011aa}. 
(c): $\Ds$ data at \s = 7\TeV with the LHCb detector~\cite{Aaij:2013mga}, where data from different $y$ ranges are scaled by factors $10^{-m}$, with $m$ shown in the plot. 
The measurements are compared to FONLL~\cite{Cacciari:2003uh, Cacciari:2012ny} and GM-VFNS~\cite{Kniehl:2004fy,Kniehl:2005mk} calculations. 
}
\end{center}
\end{figure}


\begin{figure}[!ht]
\begin{center}
%
\subfigure[$\sigmacplus$ and $\lambdacplus$ mass difference]{
	\label{fig:pp:LcMassDifference}
	\includegraphics[height=0.3\columnwidth]{./pp/Figures/CDF_Sc2455Masses.png}
	} 
\subfigure[LHCb, prompt $\lambdacplus$, \s = 7\TeV]{
	\label{fig:pp:LambdaC}
	%1302.2864
	\includegraphics[height=0.3\columnwidth]{./pp/Figures/LHCb_LambdacTH.pdf}
	} 
\caption{
\label{fig:pp:EtaLambdaC}
(a): $\lambdacplus$ and $\sigmacplus(2455)$ mass differences as measured by CDF~\cite{Aaltonen:2011sf}, Fermilab E791~\cite{Aitala:1996cy}, FOCUS~\cite{Link:2001ee}, and CLEO~\cite{Artuso:2001us}. 
The vertical dashed line together with the surrounding box symbolizes the world average value and its uncertainty taken from~\ci{Agashe:2014kda}. 
(b) : $\dsdpt$ for $\lambdacplus$ production in \pp collisions at \s = 7\TeV~\cite{Aaij:2013mga} compared to GM-VFNS~\cite{Kniehl:2004fy,Kniehl:2005mk} calculations. 
}
\end{center}
\end{figure}

\fig{fig:pp:Dmeson} presents a selection of the D meson measurements compared to pQCD calculations. 
%
The $\Dzero$, $\Dplus$ and $\Dstarplus$ $\dsdpt$ are reproduced by the theoretical calculations within uncertainties. 
Yet, FONLL~\cite{Cacciari:2003uh, Cacciari:2012ny} and POWHEG~\cite{Frixione:2007nw} predictions tend to underestimate the data, whereas GM-VFNS~\cite{Kniehl:2004fy,Kniehl:2005mk} calculations tend to overestimate the data (see Figure~3 and 4 in~\cite{Klasen:2014dba}). 
At low \pt, where the quark mass effects are important, the FONLL and POWHEG predictions show a better agreement with data.
At intermediate to high \pt, where the quark mass effects are less important, all the FONLL, POWHEG, GM-VFNS and \kt-factorisation 
calculations agree with data. 
The agreement among the FONLL and POWHEG calculations is better for heavy-flavour decay leptons than for charmed mesons, which seems to be related to the larger influence of the fragmentation model on the latter. 
%
The $\Ds$ $\pt$-differential cross section is compared to calculations in \fig{fig:pp:LHCb:Ds}. 
The $\Ds$ measurements are also reproduced by FONLL, GM-VFNS and \kt-factorisation predictions, but POWHEG calculations predict a lower production cross section than data. 
%




Charmed baryon production measurements in hadron colliders are scarce. 
The properties and decay branching ratios of the $\Lambda_c$, $\Sigma_c$ and $\Xi_c$ states have been studied at the charm- and B-factories and fixed target experiments, see \eg~\cite{Briere:2006em,Klempt:2009pi,Butler:2013kdw,Bevan:2014iga}. 
An example are the results by Fermilab E791~\cite{Aitala:1996cy}, FOCUS~\cite{Link:2001ee}, and CLEO~\cite{Artuso:2001us} collaborations. 
The CDF collaboration measured charmed baryons in \ppbar collisions at \s = 1.96\TeV, see for example~\cite{Aaltonen:2011sf}. For illustration, a compilation of the $\sigmacplus$ and $\lambdacplus$ mass difference is shown in \fig{fig:pp:LcMassDifference}. 
The LHCb collaboration measured the \pt and $y$ differential production cross section of $\Lambda_c$ in \pp collisions at \s = 7\TeV~\cite{Aaij:2013mga}. 
\fig{fig:pp:LambdaC} shows the $\pt$-differential cross section compared to GM-VFNS calculations. 
No dedicated FONLL calculation is available for $\Lambda_c$ production due to the lack of knowledge of the fragmentation function. The GM-VFNS predictions include the fragmentation functions resulting from a fit to $\ee$ collider data~\cite{Kneesch:2007ey}, where the prompt and secondary contributions to the measurements were not separated. 




%
%_____________________________________________________________________________
%
%
%
\subsubsection{Open beauty}
\label{subsubsec:pp:OpenBeauty}


Open-beauty production is usually measured by looking for $\bquark$-jets or for beauty hadrons 
via their hadronic decays, similarly to D mesons. They have been traditionally studied 
at the $\ee$ B-factories (see \eg~\cite{Butler:2013kdw,Bevan:2014iga}), where, despite the small $\bquark$-quark production cross section, 
the large luminosity allows precise measurements, such as those of the CKM 
matrix. Yet, heavier states like the $\mathrm{B}_s$, $\mathrm{B}_c$ or $\Lambda_c$ 
cannot be produced at the B-factories. They are however studied at Tevatron and at the
 LHC hadron colliders. The higher collision energy increases their production cross section, 
although their reconstruction is more difficult at hadron colliders due to the larger 
combinatorics compared to the $\ee$ environment. It should also be kept in mind that the experiments 
optimised to study the high-$\pt$ probes, like top production, are not as good for low-$\pt$ 
measurements, and often require the usage of dedicated triggers. 


\begin{figure}[!ht]
\begin{center}
\subfigure[ATLAS, $\psi(2S)$, \s = 7\TeV]{
	\label{fig:pp:NonPromptOnia:Pis2S}
	\includegraphics[width=0.31\columnwidth,height=0.31\columnwidth]{./pp/Figures/ATLAS_NonPsi2S_dsigmadpt_14a.pdf}
	} 
\subfigure[ATLAS, $\chi_{c1}$ and $\chi_{c2}$, \s = 7\TeV]{
	\label{fig:pp:NonPromptOnia:Chic1}
	\includegraphics[width=0.31\columnwidth,height=0.31\columnwidth]{./pp/Figures/non-prompt-chi_c-ATLAS.pdf}
	} 
\subfigure[LHCb, $\etac$ (red filled circles) and $\jpsi$ (blue open circles), $2.0 < y < 4.5$, \s = 7\TeV]{
	\label{fig:pp:NonPromptOnia:Etac}
	\includegraphics[width=0.31\columnwidth,height=0.31\columnwidth]{./pp/Figures/LHCb_etac_pp_b.jpg}
	} 
\caption{
 \label{fig:pp:NonPromptOnia}
$\pt$-differential cross-sections for non-prompt charmonia --assumed to come from $\bquark$ decays-- for
(a) $\psi(2S)$~\cite{Aad:2014fpa} ${\rm d}^2\sigma/{\rm d}\pt{\rm d}y$ by ATLAS compared to NLO~\cite{Mangano:1991jk}, 
FONLL~\cite{Cacciari:2003uh, Cacciari:2012ny} and GM-VFNS~\cite{Kniehl:2004fy,Kniehl:2005mk} calculations, 
(b) $\chi_{c1}$ and $\chi_{c2}$  $\dsdpt$ by ATLAS~\cite{ATLAS:2014ala} compared to FONLL~\cite{Cacciari:2012ny},
(c) $\etac$ and $\jpsi$ $\dsdpt$ by LHCb~\cite{Aaij:2014bga}.
}
\end{center}
\end{figure}


As discussed in the \sect{sec:pp:Theory:OpenHF}, predictions for open-beauty cross sections rely on  
the fragmentation functions derived from fits to $\ee$ data~\cite{Mangano:1997ri,Kniehl:2008zza}.
A high accuracy on the $\ee$ measurements and on the fragmentation function parameterisations is 
required to calculate the $\bquark$-hadron production cross section at hadron colliders. $\bquark$-jet measurements 
have the advantage to be the least dependent on the $\bquark$-quark fragmentation characteristics. 

%
%
In addition, measurements of the B cross section via a displaced charmonium have been performed 
multiple times at Tevatron and at LHC. Charmonia from beauty decays are selected by 
fitting the pseudo-proper decay length distribution of the charmonium candidates, 
$L_{xy} \left( m / \pt \right)_{\jpsi}$. \fig{fig:pp:NonPromptOnia} presents a selection 
of the LHC results: the non-prompt $\pt$-differential cross section of $\jpsi$, $\psi(2S)$, $\etac$, 
$\chi_{c1}$ and $\chi_{c2}$ in \pp collisions at \s = 7\TeV~\cite{Aad:2014fpa,ATLAS:2014ala,Aaij:2014bga}. 
The results at intermediate to low \pt are well reproduced by the FONLL~\cite{Cacciari:2003uh, Cacciari:2012ny}, NLO GM-VFNS~\cite{Kniehl:2004fy,Kniehl:2005mk} and NLO~\cite{Mangano:1991jk} with FONLL 
fragmentation calculations. At high-$\pt$ the predictions tend to overestimate data.
This could be related to the usage of the $\ee$ fragmentation functions in an unexplored kinematic range. 
\fig{fig:pp:NonPromptOnia:Etac}, which reports the first measurement of non-prompt charmonium in a purely hadronic decay channel
at hadron colliders, shows a similar transverse-momentum spectrum for non-prompt singlet and triplet $S$-wave charmonia.



%
%
% CMS B+ 101.0131v1
\begin{figure}[!ht]
\begin{center}
\subfigure[$\pt$ differential]{
	\label{fig:pp:CMS:Bplus:pt}
	% 101.0131v1
	\includegraphics[height=0.3\columnwidth]{./pp/Figures/CMS_Bplus_dsigmadpt.pdf}
	} 
\subfigure[$y$ differential]{
	\label{fig:pp:CMS:Bplus:y}
	\includegraphics[height=0.3\columnwidth]{./pp/Figures/CMS_Bplus_dsigmady.pdf}
	} 
	%101.0131v1
\caption{\label{fig:pp:Bplusmeson}
$\mathrm{B}^+$differential cross sections $\dsdpt$ (a) and $\dsdy$ (b) in \pp collisions at \s = 7\TeV compared to theory predictions~\cite{Khachatryan:2011mk}. 
}
\end{center}
\end{figure}



\begin{figure}[!ht]
\begin{center}
%
\subfigure[CMS, $\lambdabplus$ $\dsdpt$, \s = 7\TeV]{
	\label{fig:pp:Lambdab:dsdpt}
	\includegraphics[height=0.275\columnwidth]{./pp/Figures/CMS_Lambdab_dsigmaLambPt.pdf}
	} 
%
\subfigure[CMS, $\lambdabplus$ $\dsdy$, \s = 7\TeV]{
	\label{fig:pp:Lambdab:dsdy}
	\includegraphics[height=0.275\columnwidth]{./pp/Figures/CMS_Lambdab_dsigmaLamby.pdf}
	} 
%
\subfigure[CMS, selfnormalized $\dsdpt$, \s = 7\TeV]{
	\label{fig:pp:Lambdab:B0Bp}
	\includegraphics[height=0.275\columnwidth]{./pp/Figures/CMS_Lambdab_dSigmaPtSummaryNorm.pdf}
	} 
\caption{
	\label{fig:pp:Lambdab}
$\lambdabplus$ studies at the LHC in \pp collisions at  \s = 7\TeV by CMS~\cite{Chatrchyan:2012xg} compared
to PYTHIA and POWHEG calculations for (a) $\dsdpt$ for $|y|<2.0$ and for (b) $\dsdy$ for $\lambdabplus$ for $|\pt| > 10$\GeV. (c) Comparison of the (self-normalised) \pt differential cross section for $\mathrm{B}^+$, $\mathrm{B}^0$ and \lambdabplus.
}
\end{center}
\end{figure}
%


Studies of open-beauty production have also been performed in exclusive channels at Tevatron and at the LHC, \eg in the case of $\mathrm{B}^{\pm}$, $\mathrm{B}^{0}$ and $\mathrm{B}^0_s$~\cite{Aaltonen:2009tz, Abazov:2013xda, ATLAS:2013cia, Aaij:2011ep, Aaij:2013noa, Aaij:2012jd, Khachatryan:2011mk, Chatrchyan:2011pw, Chatrchyan:2011vh}. 
As example, \fig{fig:pp:Bplusmeson} presents the $\mathrm{B}^+$ $\pt$ and $y$ differential cross section in \pp collisions at \s = 7\TeV compared to theory predictions~\cite{Khachatryan:2011mk}. 
PYTHIA (D6T tune), that has LO + LL accuracy, does not provide a good description of the data. 
This could be explained by the choice of $m_b$ and by the fact that for $\pt \simeq m_b$, 
NLO and resummation effects become important, which are, in part, accounted for in FONLL~\cite{Cacciari:2003uh, Cacciari:2012ny} or MC{@}NLO. 




% 
\begin{figure}[!ht]
\begin{center}
\subfigure[CMS, $\bquark$-jets, \s = 7\TeV]{
	\label{fig:pp:CMS:Bjet}
	% 1202.4617v2
	\includegraphics[height=0.3\columnwidth]{./pp/Figures/CMS_Bjet_muonComparison_pTspectrum_wATLAS.pdf}
	} 
\subfigure[ATLAS, $\bquark$-jet, \s = 7\TeV]{
	\label{fig:pp:ATLAS:Bjet}
	%1109.6833v2.
	\includegraphics[height=0.3\columnwidth]{./pp/Figures/ATLAS_Bjet_inclusive_xsec_pty.pdf}
	} 
\caption{
\label{fig:pp:Bjet}
$\bquark$-jet cross section as a function of \pt in \pp collisions at \s = 7\TeV: (a) $\dsdpt$ from the lifetime-based and muon-based analyses 
by CMS~\cite{Chatrchyan:2012dk} and ATLAS~\cite{ATLAS:2011ac} compared to the MC{@}NLO calculation, and
(b) ${\rm d}^2\sigma/{\rm d}\pt {\rm d}y$ by ATLAS from the lifetime-based analysis~\cite{ATLAS:2011ac} compared to the predictions of PYTHIA, POWHEG (matched to PYTHIA) and MC{@}NLO (matched to HERWIG)~\cite{Sjostrand:2006za,Alioli:2010xd,Frixione:2002ik,Frixione:2003ei,Corcella:2000bw}. 
}
\end{center}
\end{figure}



Measurements of the beauty and charm baryon production in \ppbar collisions at \s = 1.96\TeV are summarised in~\ci{Aaltonen:2014wfa}. 
In particular, the doubly strange $\bquark$-baryon $\Omega_b^-$ and measurement of the $\Xi_b^-$ and $\Omega_b^-$ properties can be found in~\cis{Abazov:2008qm,Aaltonen:2009ny}. 
Such measurements have also been performed at the LHC in \pp collisions at \s = 7\TeV and 8\TeV. For example, the observation of the $\Xi_b$ baryon states was reported in~\cis{Chatrchyan:2012ni,Aaij:2014yka}. 
The measured mass and width of the $\Xi_b$ baryon states is consistent with theoretical expectations~\cite{Ebert:2005xj,Liu:2007fg,Jenkins:2007dm,Karliner:2008sv,Lewis:2008fu,Karliner:2008in,Zhang:2008pm,Klempt:2009pi,Detmold:2012ge}. 
The $\lambdabplus$ $\pt$ and $y$ differential production cross section in \pp collisions at \s = 7\TeV by CMS~\cite{Chatrchyan:2012xg} is reported in \fig{fig:pp:Lambdab}. The $\lambdab$ $\dsdpt$ and $\dsdy$ are not reproduced by neither PYTHIA (Z2 tune) nor POWHEG calculations: PYTHIA expects a harder \pt-distribution and flatter $y$ distribution than data, while POWHEG underestimates its production cross section, particularly at low \pt, see \fig{fig:pp:Lambdab:dsdpt} and~\fig{fig:pp:Lambdab:dsdy}. 
The measured $\lambdab$ $\pt$-spectrum at mid-rapidity seems to fall more steeply than the $\mathrm{B}^0$ and $\mathrm{B}^+$ ones, see \fig{fig:pp:Lambdab:B0Bp}, and falls also faster than predicted by PYTHIA and POWHEG. 
As discussed for the non-prompt charmonium measurements, this could be influenced by the lack of data to extract the fragmentation functions in this kinematic region. 

%
%
%





%
%
The fragmentation of the $\bquark$ quark is relatively hard compared to that of lighter flavours, with the $\bquark$-hadron taking about 70\% of the parton momentum on average at the Z-pole~\cite{DELPHI:2011aa}.  
Identification of jets from beauty quark fragmentation or ``\bquark-tagging'' can be achieved by direct reconstruction of displaced vertices, although the efficiency for doing so is limited.  Higher efficiency can be achieved by looking for large impact parameter tracks inside jets, or by a combination of the two methods, which are collectively known as lifetime tagging.  Leptons inside jets can also be used for $\bquark$-tagging, but, due to the branching fraction, are usually only used as a calibration of the lifetime methods.  
At the LHC, both ATLAS and CMS have performed measurements of the $\bquark$-jet cross section~\cite{ATLAS:2011ac, Chatrchyan:2012dk}. Theoretical comparisons can be made to models which calculate fully exclusive final states, which can be achieved by matching NLO calculations to parton showers~\cite{Nason:2004rx}. 
\fig{fig:pp:Bjet} shows the $\bquark$-jet cross-section measurement by ATLAS and CMS in \pp collisions at \s = 7\TeV. The measurements are shown as a function of \pt and in several bins of rapidity. Calculations from POWHEG~\cite{Alioli:2010xd} (matched to PYTHIA~\cite{Sjostrand:2006za}) and MC{@}NLO~\cite{Frixione:2002ik,Frixione:2003ei} (matched to HERWIG~\cite{Corcella:2000bw}), are found to reproduce the data. Measurements from both lifetime- and lepton-based tagging methods are shown.

