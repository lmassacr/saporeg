
Measurements of quarkonium polarisation can shed more light on the long-standing puzzle 
of the quarkonium hadroproduction. Various models of the quarkonium production, described 
in the previous \sect{sec:pp:Theory:Onia}, are in reasonable agreement with the 
cross-section measurements but they usually fail to describe the measured polarisation.

We have collected in this section all results of polarisation measurements performed 
by different experiments at different colliding energies and in different kinematic regions.
The results for \jpsi and \psiP can be found in \tab{tab:jpsiPolarization} and \tab{tab:psiPolarization} for \pp and \pA collisions. Since there is no known mechanism that 
would change quarkonium polarisation from proton-proton to proton-nucleus collisions, results from \pA 
collisions are also shown in this section. Tables~\ref{tab:ups1Polarization},~\ref{tab:ups2Polarization},~\ref{tab:ups3Polarization} 
gather the results for, respectively, the \upsa, \upsb and \upsc in \pp collisions. 

Polarisation of a vector quarkonium state is analysed experimentally via the angular distribution of the leptons 
from the quarkonium dilepton decay, that is parametrized by:
\begin{equation}
\frac{\dd^{2}N}{\dd(\cos\theta)\dd\phi} \propto 1+\lambda_\theta \cos^2\theta + \\ \lambda_\phi \sin^2\theta \cos2\phi + \lambda_{\theta\phi}\sin2\theta \cos\phi \,,
\label{eq:angularDistribution}
\end{equation}
$\theta$ is the polar angle between the positive lepton in the quarkonium rest frame and the chosen 
polarisation axis and $\phi$ angle is the corresponding azimuthal angle defined with respect to the 
plane of colliding hadrons. The angular decay coefficients, $\lambda_\theta$, $\lambda_\phi$ and 
$\lambda_{\theta\phi}$, are the polarisation parameters. In the case of an unpolarised yield, one would have 
($\lambda_{\theta}, \lambda_{\phi}, \lambda_{\theta \phi}) = (0,0,0)$ for an isotropic decay angular distribution, whereas
 $(1,0,0)$ and $(-1,0,0)$ refers to fully transverse and fully longitudinal polarisation, respectively.  

It is however very important to bear in mind that the angular distribution of \eq{eq:angularDistribution} is frame dependent
as the polarisation parameters. All experimental analyses have been carried in a few specific reference frames,
essentially defined by their polarisation axis\footnote{
See~\cite{Faccioli:2010kd} for the definition of the corresponding axes.}, namely: the helicity ($HX$) frame , the Collins-Soper ($CS$)~\cite{Collins:1977iv} frame, the Gottfried-Jackson ($GJ$)~\cite{Gottfried:1964nx} frame as well as the perpendicular helicity ($PX$)~\cite{Braaten:2008mz} frame. 

In spite of the frame dependence of $\lambda_{\theta}, \lambda_{\phi}, \lambda_{\theta \phi}$, there exist some combinations which 
are frame invariant~\cite{Faccioli:2010kd,Palestini:2010xu}. An obvious one is the yield, another one is  
$\tilde{\lambda} = (\lambda_{\theta} + 3 \lambda_{\phi}) / (1 - \lambda_{\phi})$~\cite{Faccioli:2010kd}. 
%The $\tilde{\lambda}$ parameter is expected to be less sensitive to the acceptance effects and 
As such, it can be used as a good cross-check 
between measurements done in different reference frames. Different methods have been used  to extract 
the polarisation parameter(s) from the angular dependence of the yields. In the following, we divide them into two groups:
(i) $1-D$ technique: fitting $\cos\theta$ distribution with the angular distribution, \eq{eq:angularDistribution}, averaged over the azimuthal $\phi$ angle, and fitting the $\phi$ distribution, \eq{eq:angularDistribution}, averaged over the polar $\theta$ angle (ii) $2-D$ technique: fitting a two-dimensional $\cos\theta$ {\it vs} $\phi$ distribution with the full angular distribution, \eq{eq:angularDistribution}.

Beyond the differences in the methods employed to extract these parameters, one should also take into consideration that some
samples are cleaner than others, 
physicswise\footnote{
Irrespective of the experimental techniques used to extract it, a sample of {\it inclusive} low \pt $\psiP$ at energies around 100\GeV is essentially purely direct.}. Indeed, as we discussed in the previous section, a given quarkonium yield
can come from different sources, some of which are not of specific interests for data-theory comparisons. The most obvious
one is the non-prompt charmonium yield, which is expected to be the result of quite different mechanism that the prompt yield.
Nowadays, the majority of the studies are carried out on a prompt sample thanks to a precise vertexing of the events.
Yet, a further complication also comes from feed-down from the excited states in which case vertexing is of no help. 
As for now, no attempt of removing it
from \eg prompt \jpsi and inclusive \upsa samples has been made owing its intrinsic complication. We have therefore found it
important to specify what kind of feed-down could be expected in the analysed sample.


In view of this, the Tables~\ref{tab:jpsiPolarization}--\ref{tab:ups3Polarization} contain, in addition to the information on the collision systems and
the kinematical coverages, information on the fit technique and a short reminder of the expected feed-down. For each measurement, we also briefly summarise
the observed trend. The vast majority of the experimental data do not show a significant quarkonium polarisation, neither polar nor azimuthal anisotropy. 
Yet, values as large as $\pm 0.3$ are often not excluded either -- given the experimental uncertainties. Despite these, a simultaneous 
description of both measured quarkonium cross-sections and polarisations is still challenging for theoretical models of quarkonium hadroproduction. 

As example, we show in~\fig{quarkoniapolarization_LHCb} the \pt-dependence of $\lambda_{\theta}$ for prompt \jpsi~\cite{Aaij:2013nlm} (left panel) and \psiP~\cite{Aaij:2014qea} (right panel) measured by LHCb at $2.5<y<4.0$ in the helicity frame compared with a few theoretical predictions.
NLO NRQCD calculations~\cite{Butenschoen:2012px,Gong:2012ug,Chao:2012iv} show mostly positive or zero values of $\lambda_{\theta}$ with a trend towards 
the transverse polarisation with increasing \pt, and a magnitude of the $\lambda_{\theta}$ depending on the specific calculation and the kinematical region. 
On the other hand, NLO CSM models~\cite{Gong:2008sn,Lansberg:2010vq} tend to predict an unpolarised yield at low \pt and an increasingly 
longitudinal yield ($\lambda_{\theta} < 0$) for increasing \pt. None of these predictions correctly describes the measured \jpsi and \psiP $\lambda_{\theta}$ 
parameters and their \pt trends. The NLO NRQCD fits of the PKU group~\cite{Shao:2014yta,Han:2014jya} however open the possibility for an 
unpolarised {\it direct yield} but at the cost of
not describing the world existing data in $e$p and \ee collisions and data in \pp collisions for $\pt \leq 5$\GeVc.


In order to illustrate the recent progresses in these delicate studies, let us stress that LHC experiments 
have performed measurements of the three polarisation parameters as well as in different reference frames. 
This has not always been the case before by lack of statistics and of motivation since it is difficult 
to predict theoretically azimuthal effects, \eg $\lambda_{\theta \phi}$.
\fig{psiPolarization_CMS} and~\ref{upsilonPolarization_CMS} 
show CMS measurements of $\lambda_{\theta}$, $\lambda_{\phi}$ and $\lambda_{\theta \phi}$, in the $HX$ frame for 
\jpsi, \psiP~\cite{Chatrchyan:2013cla} and \upsa, \upsb, \upsc~\cite{Chatrchyan:2012woa} on the left and 
right panel, respectively. CMS has also conducted polarisation measurements in the $CS$ and $PX$ frames, 
in addition to the $HX$ frame and they could cross-check their analysis by obtaining 
the consistency in $\tilde{\lambda}$ in these three frames for different \pt and $y$. 
As for most of the previous measurements, no evidence of a large transverse or longitudinal quarkonium 
polarisation is observed in any reference frame, and in the whole measured kinematic range. 

To conclude, let us also mention the importance of measuring the polarisation of $P$-wave states in order to refine our test of \eg NRQCD~\cite{Shao:2012fs}. 
This can be done either directly via the measurement of the angular dependence of the emitted photon or indirectly via that of the polarisation 
of the $S$-wave (\jpsi or \ups) in which they decay~\cite{Faccioli:2011be}. Such studies are very important to constrain 
experimentally the effect of the feed-downs on the polarisation of the available samples. Let us also stress that such a measurement
in heavy-ion collisions (along the line of the first study in In--In collisions~\cite{Arnaldi:2009ph}) 
may also be used as a tool to study a possible sequential suppression of the quarkonia~\cite{Faccioli:2012kp}.


\begin{figure}[!t]
 \begin{center}
   \subfigure[]{
	\includegraphics[angle=0,width=0.48\textwidth]{jpsipolarizationParameter_LHCb.pdf}  
	}
  \subfigure[] {
   	\includegraphics[angle=0,width=0.48\textwidth]{psi2SpolarizationParameter_LHCb.pdf}
	}
 \end{center}
 \caption{Polarisation parameter $\lambda_{\theta}$ for prompt \jpsi~\cite{Aaij:2013nlm} (a) and \psiP~\cite{Aaij:2014qea} (b) from LHCb compared to different model predictions: direct NLO CSM~\cite{Butenschoen:2012px} and three NLO NRQCD calculations~\cite{Butenschoen:2012px,Gong:2012ug,Chao:2012iv}, at $2.5<y<4.0$ in the helicity frame.}
 \label{quarkoniapolarization_LHCb} 
\end{figure}

\begin{figure}[!htb]
 \begin{center}
  \subfigure[]
  {\includegraphics[angle=0,width=0.7\textwidth]{psiPolarizationParameters_CMS.pdf}\label{psiPolarization_CMS} }
\subfigure[]
{\includegraphics[angle=0,width=0.7\textwidth]{upsilonPolarizationParameters_CMS.pdf}\label{upsilonPolarization_CMS} }
 \end{center}
 \caption{
 (a) Polarisation parameters, $\lambda_{\theta}$, $\lambda_{\phi}$ and $\lambda_{\theta \phi}$, as a function of \pt measured in the $HX$ frame of  prompt \jpsi, \psiP~\cite{Chatrchyan:2013cla}. Upper panels show also NLO NRQCD calculations~\cite{Gong:2012ug} of $\lambda_{\theta}$ for prompt \jpsi and \psiP for $\vert y \vert <$ 2.4. 
 (b) Polarisation parameters, $\lambda_{\theta}$, $\lambda_{\phi}$ and $\lambda_{\theta \phi}$, as a function of \pt measured in the $HX$ frame of \upsa, \upsb, \upsc~\cite{Chatrchyan:2012woa} for $\vert y \vert < 0.6$.
}
 
\end{figure}





\begin{landscape}

%\begin{sidewaystable}[hbt!]
\begin{table}[hbt!]
\caption{World existing data for \jpsi polarisation in \pp and \pA collisions.}
\label{tab:jpsiPolarization}
\begin{threeparttable}
\small
%\begin{tabularx}{24cm}{|p{1.cm}| p{1cm}| p{1.6cm} | p{1.2cm} | p{1.2cm} | p{2.8cm} | p{0.6cm} | p{1.8cm} | p{6cm} |}
\begin{tabularx}{20cm}{ p{1.cm} p{1cm} p{1.6cm} p{1.2cm} p{1.2cm} p{2.8cm} p{0.6cm}  p{1.8cm} X }
\hline
\s [GeV] & Colliding system &   Experiment   &   \ycm range  &  \pt range [GeV/$c$]   &    Feed-down   &    Fit     &  Measured parameter(s) & Observed trend  \\
\hline
200  & \pp  &  PHENIX \cite{Adare:2009js} & $\vert y \vert <$ 0.35 & 0 -- 5 & $B$ : $2\div 15\%$ \cite{Adamczyk:2012ey} ~  \chic : $23\div 41\%$ \cite{Adare:2011vq} ~ \psiP: $5\div 20\%$ \cite{Adare:2011vq} & $1-D$ & $\lambda_{\theta}$ {\it vs} \pt in HX \&  GJ& $\lambda_{\theta}$ values from slightly positive (consistent with 0) to negative as \pt increases \\
\hline
200  & \pp  &  STAR \cite{Adamczyk:2013vjy} & $\vert y \vert <$ 1 & 2 -- 6 & $B$ : $2\div 15\%$ \cite{Adamczyk:2012ey} ~  \chic :  $23\div 41\%$ \cite{Adare:2011vq} ~ \psiP: $5\div 20\%$ \cite{Adare:2011vq} & $1-D$ & $\lambda_{\theta}$ {\it vs} \pt in HX & $\lambda_{\theta}$ values from slightly positive (consistent with 0) to negative as \pt increases \\
\hline
1800  & \ppbar  &  CDF \cite{Affolder:2000nn} & $\vert y \vert <$ 0.6 & 4 -- 20  &  \chic : $25\div 35\%$ \cite{Abe:1997yz} ~ \psiP: $10\div 25\%$ \cite{Abe:1997jz} &  $1-D$ & $\lambda_{\theta}$ {\it vs} \pt in HX & Small positive $\lambda_{\theta}$ at smaller \pt then for \pt $>$ 12 \GeV trend towards negative values \\
\hline
1960  & \ppbar  &  CDF \cite{Abulencia:2007us} & $\vert y \vert <$ 0.6 & 5 -- 30 & \chic : $25\div 35\%$ \cite{Abe:1997yz} ~ \psiP: $10\div 25\%$ \cite{Abe:1997jz} &  $1-D$ & $\lambda_{\theta}$ {\it vs} \pt   in HX  & $\lambda_{\theta}$ values from 0 to negative as \pt increases \\
\hline
7000  & \pp  &  ALICE \cite{Abelev:2011md} & 2.5 -- 4.0 & 2 -- 8  &  $B$ : $10\div 30\%$ \cite{Abelev:2012gx} ~  \chic : $15\div 30\%$ \cite{LHCb:2012af} ~ \psiP: $8\div 20\%$ \cite{Abelev:2014qha}  & $1-D$ & $\lambda_{\theta}$, $\lambda_{\phi}$ {\it vs} \pt in HX \& CS& $\lambda_{\theta}$ and $\lambda_{\phi}$ consistent with 0, with a possible hint for a longitudinal polarisation at low \pt in the $HX$ frame \\ 
\hline
7000  & \pp &  LHCb \cite{Aaij:2013nlm} & 2.0 -- 4.5 &  2 -- 15 &  \chic : $15\div 30\%$ \cite{LHCb:2012af} ~ \psiP: $8\div 25\%$ \cite{Aaij:2012ag} & $2-D$ &  $\lambda_{\theta}$, $\lambda_{\phi}$, $\lambda_{\theta \phi}$ {\it vs} \pt and $y$ in HX \& CS& $\lambda_{\phi}$ and $\lambda_{\theta \phi}$ consistent with 0 in the $HX$ frame and $\lambda_{\theta}$ ($\lambda_{\theta} = $ -0.145 $\pm$ 0.027) shows small longitudinal polarisation; $\tilde{\lambda}$ in agreement in the $HX$ and $CS$ frames  \\ 
\hline
7000  & \pp &  CMS \cite{Chatrchyan:2013cla} &  $\vert y \vert <$ 1.2  & 14 -- 70  &  \chic : $25\div 35\%$ ~ \psiP: $15\div 20\%$ \cite{Chatrchyan:2011kc} & $2-D$ & $\lambda_{\theta}$, $\lambda_{\phi}$, $\lambda_{\theta \phi}$, $\tilde{\lambda}$ {\it vs} \pt and in 2 $\vert y \vert$ bins in HX,CS,PX &  In the 3 frames, no evidence of 
large $|\lambda_{\theta}|$ anywhere; $\tilde{\lambda}$ in a good agreement in all reference frames \\ 
\hline
17.2  & \pA  &  NA60 \cite{Arnaldi:2009ph} & 0.28 -- 0.78 &  --  & $B$ : $2\div 8\%$ ~  \chic :$25\div 40\%$ \cite{Abt:2002vq} ~ \psiP: $7\div 10\%$ \cite{Alexopoulos:1995dt} & $1-D$  & $\lambda_{\theta}$, $\lambda_{\phi}$ {\it vs} \pt in HX & $\lambda _{\theta}$ and $\lambda _{\phi}$ consistent with 0; slight increase of the $\lambda _{\theta}$ value with increasing \pt, no \pt dependence $\lambda _{\phi}$ \\
\hline
27.4  & \pA &  NA60 \cite{Arnaldi:2009ph} & -0.17 -- 0.33 &  --  & $B$ : $2\div 8\%$ ~  \chic : $25\div 40\%$ \cite{Abt:2002vq} ~ \psiP: $7\div 10\%$ \cite{Alexopoulos:1995dt} & $1-D$  & $\lambda_{\theta}$, $\lambda_{\phi}$ {\it vs} \pt in HX & $\lambda_{\theta}$ and $\lambda _{\phi}$ consistent with 0, no \pt dependence observed \\ 
\hline
31.5  & $p\mathrm{-Be}$      &  E672/E706 \cite{Gribushin:1999ha} & $x_{F}$: 0.00 -- 0.60 & 0 -- 10 & $B$ : $2\div 8\%$ ~  \chic : $25\div 40\%$ \cite{Abt:2002vq} ~ \psiP: $7\div 10\%$ \cite{Alexopoulos:1995dt} & $1-D$  & $\lambda_{\theta}$ in GJ & $\lambda_{\theta} = $ 0.01 $\pm$ 0.12 $\pm$ 0.09, consistent with no polarisation \\ 
\hline
38.8  & $p\mathrm{-Be}$      &  E672/E706 \cite{Gribushin:1999ha} & $x_{F}$: 0.00 -- 0.60 & 0 -- 10 & $B$ : $2\div 8\%$ ~  \chic : $25\div 40\%$ \cite{Abt:2002vq} ~ \psiP: $7\div 10\%$ \cite{Alexopoulos:1995dt} & $1-D$  & $\lambda_{\theta}$ in GJ & $\lambda_{\theta} = $ -0.11 $\pm$ 0.12 $\pm$ 0.09, consistent with no polarisation  \\
\hline
38.8  & $p\mathrm{-Si}$      &  E771 \cite{Alexopoulos:1997yd} & $x_{F}$: -0.05 -- 0.25 & 0 -- 3.5  & $B$ : $2\div 8\%$ ~  \chic : $25\div 40\%$ \cite{Abt:2002vq} ~ \psiP: $7\div 10\%$ \cite{Alexopoulos:1995dt} & $1-D$  & $\lambda_{\theta}$ in GJ & $\lambda_{\theta} = $ -0.09 $\pm$ 0.12, consistent with no polarisation \\ 
\hline
38.8  & $p\mathrm{-Cu}$      &  E866/NuSea \cite{Chang:2003rz} & $x_{F}$: 0.25 -- 0.9 & 0 -- 4 & \chic : $25\div 40\%$ \cite{Abt:2002vq} ~ \psiP: $7\div 10\%$ \cite{Alexopoulos:1995dt} & $1-D$  & $\lambda_{\theta}$ {\it vs} \pt and $x_{F}$ in CS & $\lambda_{\theta}$ values from small positive to negative with increasing $x_{F}$, no significant \pt dependence observed \\
\hline
41.6  & $p\mathrm{-C,W}$  &  HERA-B \cite{Abt:2009nu} & $x_{F}$: -0.34 -- 0.14 & 0 -- 5.4 &  \chic : $25\div 40\%$ \cite{Abt:2002vq} ~ \psiP: $5\div 15\%$ \cite{Abt:2006va} & $1-D$  &  $\lambda_{\theta}$, $\lambda_{\phi}$, $\lambda_{\theta \phi}$ {\it vs} \pt and $x_{F}$ in HX, CS, GJ & $\lambda _{\theta}$ and $\lambda _{\phi}$ $<0$, $\lambda _{\theta}$ ($\lambda _{\phi}$) decrease (increase) with increasing \pt; no strong $x_{F}$ dependence; for $\pt > 1$\GeVc, $\lambda_{\theta,\phi}$ depends on the frame :
 $\lambda^{\rm CS}_{\theta}>\lambda^{\rm HX}_{\theta}\simeq 0$ and  $\lambda^{\rm HX} _{\phi} \neq 0$  \\
\hline
\end{tabularx}
%\begin{tablenotes}
%  	\item[*] Expected feed-down to \jpsi:
%\begin{itemize}
%\item $B$: $10 \div 25$\%
%\item \chic : $ 20 \div 35$~\%
%\item \psiP : $ 5 \div 11$~\%
%\end{itemize} 
%    \end{tablenotes}
\end{threeparttable}
\end{table}


%\begin{sidewaystable}[hbt!]
%\begin{table}[hbt!]
%\caption{World existing data for \jpsi polarisation in \pA.}
%\label{tab:jpsiPolarization_pA}
%\begin{threeparttable}
%\small
%\begin{tabularx}{21cm}{|p{1.cm}| p{1.1cm}| p{1.5cm} | p{1.2cm} | p{1.2cm} | p{1.6cm} | p{1.4cm} | p{1.6cm} | X |}
%\hline
%\s [\GeV]  & Colliding system &   Experiment   &   \ycm range  &  \pt range  [\GeVc]     &    Expected  feed-down  &    Extraction technique      &  Measured parameter(s) & Observed trend  \\
%\hline\hline
%17.2  & \pA  &  NA60~\cite{Arnaldi:2009ph} & 0.28 -- 0.78 &  --  & $B$, \chic , \psiP & $1-D$  & $\lambda_{\theta}^{HX}$, $\lambda_{\phi}^{HX}$ {\it vs} \pt & $\lambda _{\theta}$ and $\lambda _{\phi}$ consistent with 0; slight increase of the $\lambda _{\theta}$ value with increasing \pt, no \pt dependence $\lambda _{\phi}$ \\
%\hline
%27.4  & \pA &  NA60~\cite{Arnaldi:2009ph} & -0.17 -- 0.33 &  --  & $B$, \chic , \psiP & $1-D$  & $\lambda_{\theta}^{HX}$, $\lambda_{\phi}^{HX}$ {\it vs} \pt & $\lambda_{\theta}$ and $\lambda _{\phi}$ consistent with 0, no \pt dependence observed \\ 
%\hline
%31.5  & $p\mathrm{-Be}$      &  E672/E706~\cite{Gribushin:1999ha} & $x_{F}$: 0.00 -- 0.60 & 0 -- 10 & $B$,  \chic , \psiP & $1-D$  & $\lambda_{\theta}^{GJ}$ & $\lambda_{\theta} = $ 0.01 $\pm$ 0.12 $\pm$ 0.09, consistent with no polarisation \\ 
%\hline
%38.8  & $p\mathrm{-Be}$      &  E672/E706~\cite{Gribushin:1999ha} & $x_{F}$: 0.00 -- 0.60 & 0 -- 10 & $B$,  \chic , \psiP & $1-D$  & $\lambda_{\theta}^{GJ}$ & $\lambda_{\theta} = $ -0.11 $\pm$ 0.12 $\pm$ 0.09, consistent with no polarisation  \\
%\hline
%38.8  & $p\mathrm{-Si}$      &  E771~\cite{Alexopoulos:1997yd} & $x_{F}$: -0.05 -- 0.25 & 0 -- 3.5  & $B$,  \chic , \psiP & $1-D$  & $\lambda_{\theta}^{GJ}$ & $\lambda_{\theta} = $ -0.09 $\pm$ 0.12, consistent with no polarisation \\ 
%\hline
%38.8  & $p\mathrm{-Cu}$      &  E866/NuSea~\cite{Chang:2003rz} & $x_{F}$: 0.25 -- 0.9 & 0 -- 4 & $B$,  \chic , \psiP & $1-D$  & $\lambda_{\theta}^{CS}$ {\it vs} \pt and $x_{F}$ & $\lambda_{\theta}$ values from small positive to negative with increasing $x_{F}$, no significant \pt dependence observed \\
%\hline
%41.6  & $p\mathrm{-C,W}$  &  HERA-B~\cite{Abt:2009nu} & $x_{F}$: -0.34 -- 0.14 & 0 -- 5.4 & $B$,  \chic , \psiP & $1-D$  &  $\lambda_{\theta}^{HX, CS, GJ}$, $\lambda_{\phi}^{HX, CS, GJ}$, $\lambda_{\theta \phi}^{HX, CS, GJ}$ {\it vs} \pt and $x_{F}$ & negative values of $\lambda _{\theta}$ and $\lambda _{\phi}$, magnitude of the polar (azimuthal) anisotropy deceases (increases) with increasing \pt and no strong $x_{F}$ dependence observed; for \pt $>$ 1 \GeVc dependence on the reference frame observed; the polar anisotropy is more pronounced in the $CS$ frame and almost vanishes in the $HX$ frame, where a significant azimuthal anisotropy arises  \\
%\hline
%\end{tabularx}
%\end{threeparttable}
%\end{table}
%\end{sidewaystable}


\begin{table}[hbt!]
\caption{World existing data for \psiP polarisation in \pp.}
\label{tab:psiPolarization}
\begin{threeparttable}
\small
%\begin{tabularx}{24cm}{|p{1.cm}| p{1cm}| p{1.6cm} | p{1.2cm} | p{1.2cm} | p{2.8cm} | p{0.6cm} | p{1.8cm} | X |}
\begin{tabularx}{20cm}{p{1.cm} p{1cm} p{1.6cm} p{1.2cm} p{1.2cm} p{2.8cm} p{0.6cm} p{1.8cm} X }
\hline
\s [GeV]   & Colliding system &   Experiment   &   \ycm range  &  \pt range [GeV/$c$]   &    Feed-down   &    Fit     &  Measured parameter(s) & Observed trend  \\
\hline
1800  & \ppbar     &    CDF \cite{Affolder:2000nn}  &  $\vert y \vert <$ 0.6  & 5.5 -- 20 & none &  $1-D$  &   $\lambda_{\theta}$ {\it vs} $p_{T}$ in HX & $\lambda_{\theta}$ consistent with 0  \\
\hline
1960  & \ppbar      &  CDF \cite{Abulencia:2007us} & $\vert y \vert <$ 0.6 & 5 -- 30 & none & $1-D$  & $\lambda_{\theta}$ {\it vs} $p_{T}$ in HX & $\lambda_{\theta}$ values {\it vs} \pt go from slightly positive to small negative  \\
\hline
7000   & \pp    &    LHCb \cite{Aaij:2014qea}  &  2.0 -- 4.5 & 3.5 -- 15  &  none   &  $2-D$ &  $\lambda_{\theta}$, $\lambda_{\phi}$, $\lambda_{\theta \phi}$, $\tilde{\lambda}$ {\it vs} \pt and 3 $\vert y \vert$ bins in HX, CS & no significant polarisation found, with an indication of small longitudinal polarisation - $\tilde{\lambda}$ is negative with no strong \pt and $y$ dependence  \\
\hline
7000   & \pp    &    CMS \cite{Chatrchyan:2013cla} & $\vert y \vert <$ 1.5  & 14 -- 50  &   none  & $2-D$  & $\lambda_{\theta}$, $\lambda_{\phi}$, $\lambda_{\theta \phi}$, $\tilde{\lambda}$ {\it vs} \pt and in 3 $\vert y \vert$ bins in HX, CS, PX & non of the 3 reference frames show evidence of large transverse or longitudinal polarisation, in the whole measured kinematic range; $\tilde{\lambda}$ in a good agreement in all reference frames \\
\hline
\end{tabularx}
\end{threeparttable}
\end{table}


\begin{table}[hbt!]
\caption{World existing data for \upsa polarisation in \pp.}
\label{tab:ups1Polarization}
\begin{threeparttable}
\small
%\begin{tabularx}{24cm}{|p{1.cm}| p{1cm}| p{1.6cm} | p{1.2cm} | p{1.2cm} | p{2.8cm} | p{0.6cm} | p{1.8cm} | X |}
\begin{tabularx}{20cm}{ p{1.cm} p{1cm} p{1.6cm} p{1.2cm} p{1.2cm} p{2.8cm} p{0.6cm} p{1.8cm} X }
\hline
\s [GeV]   & Colliding system &   Experiment   &   \ycm range  &  \pt range [GeV/$c$]   &    Feed-down   &    Fit     &  Measured parameter(s) & Observed trend  \\
\hline
1800  & \ppbar & CDF \cite{Acosta:2001gv}  &  $\vert y \vert <$ 0.4  & 0 -- 20  & \upsb : $6\div 18\%$ \cite{Affolder:1999wm} ~  \upsc : $0.4\div 1.4\%$ \cite{Affolder:1999wm} ~ $\chi_{b}$ : $30\div 45\%$ \cite{Affolder:1999wm} & $1-D$ &   $\lambda_{\theta}$ {\it vs} \pt in HX & $\lambda_{\theta}$ consistent with 0, no significant \pt dependence  \\
\hline
1960  & \ppbar & CDF \cite{CDF:2011ag}  &  $\vert y \vert <$ 0.6  & 0 -- 40  & \upsb : $6\div 18\%$ \cite{Affolder:1999wm} ~  \upsc : $0.4\div 1.4\%$ \cite{Affolder:1999wm} ~ $\chi_{b}$ : $30\div 45\%$ \cite{Affolder:1999wm}  & $2-D$ &   $\lambda_{\theta}$, $\lambda_{\phi}$, $\lambda_{\theta \phi}$, $\tilde{\lambda}$ {\it vs} \pt in HX, CS & the angular distribution found to be nearly isotropic \\
\hline
1960  & \ppbar & D0 \cite{Abazov:2008aa}  &  $\vert y \vert <$ 0.4  & 0 -- 20  & \upsb : $6\div 18\%$ \cite{Affolder:1999wm} ~  \upsc : $0.4\div 1.4\%$ \cite{Affolder:1999wm} ~ $\chi_{b}$ : $30\div 45\%$ \cite{Affolder:1999wm} & $1-D$ &   $\lambda_{\theta}$ {\it vs} \pt in HX & significant negative $\lambda_{\theta}$ at low \pt with decreasing magnitude as \pt increases  \\
\hline
7000  & \pp & CMS \cite{Chatrchyan:2012woa} & $\vert y \vert <$ 1.2  & 10 -- 50  &  \upsb : $10\div 15\%$ ~  \upsc : $0.5\div 3\%$  \cite{Chatrchyan:2013yna} ~ $\chi_{b}$ : $25\div 40\%$ \cite{Aaij:2014caa} & $2-D$  & $\lambda_{\theta}$, $\lambda_{\phi}$, $\lambda_{\theta \phi}$, $\tilde{\lambda}$ {\it vs} \pt and in 2 $\vert y \vert$ bins in HX, CS, PX & no evidence of large transverse of longitudinal polarisation in the whole kinematic range in 3 reference frames  \\
\hline
\end{tabularx}
\end{threeparttable}
\end{table}


\begin{table}[hbt!]
\caption{World existing data for \upsb polarisation in \pp.}
\label{tab:ups2Polarization}
\begin{threeparttable}
\small
\begin{tabularx}{20cm}{p{1.cm} p{1cm} p{1.6cm} p{1.2cm} p{1.2cm} p{2.8cm} p{0.6cm} p{1.8cm} X }
\hline
\s [GeV]   & Colliding system &   Experiment   &   \ycm range  &  \pt range [GeV/$c$]   &    Feed-down   &    Fit     &  Measured parameter(s) & Observed trend  \\
\hline
1960  & \ppbar & D0 \cite{Abazov:2008aa}  &  $\vert y \vert <$ 0.4  & 0 -- 20  & \upsc :  $2.5\div 5\%$ \cite{Chatrchyan:2013yna} ~ $\chi_{b}$ : $25\div 35\%$ \cite{Aaij:2014caa}  & $1-D$ &   $\lambda_{\theta}$ {\it vs} \pt in HX & $\lambda_{\theta}$ consistent with zero at low \pt, trend towards strong transverse polarisation at $\pt>5$\GeVc  \\
\hline
1960  & \ppbar & CDF \cite{CDF:2011ag}  &  $\vert y \vert <$ 0.6  & 0 -- 40  & \upsc :  $2.5\div 5\%$ \cite{Chatrchyan:2013yna} ~ $\chi_{b}$ : $25\div 35\%$ \cite{Aaij:2014caa} & $2-D$ &   $\lambda_{\theta}$, $\lambda_{\phi}$, $\lambda_{\theta \phi}$, $\tilde{\lambda}$ {\it vs} \pt in HX, CS & the angular distribution found to be nearly isotropic    \\
\hline
7000  & \pp & CMS \cite{Chatrchyan:2012woa} & $\vert y \vert <$ 1.2  & 10 -- 50  &  \upsc : $2.5\div 5\%$ \cite{Chatrchyan:2013yna} ~ $\chi_{b}$ : $25\div 35\%$ \cite{Aaij:2014caa} & $2-D$  & $\lambda_{\theta}$, $\lambda_{\phi}$, $\lambda_{\theta \phi}$, $\tilde{\lambda}$ {\it vs} \pt and in 2 $\vert y \vert$ bins in HX, CS, PX & no evidence of large transverse of longitudinal polarisation in whole kinematic range in 3 reference frames  \\
\hline
\end{tabularx}
\end{threeparttable}
\end{table}

\begin{table}[hbt!]
\caption{World existing data for \upsc polarisation in \pp.}
\label{tab:ups3Polarization}
\begin{threeparttable}
\small
\begin{tabularx}{20cm}{p{1.cm} p{1cm} p{1.6cm} p{1.2cm} p{1.2cm} p{2.8cm} p{0.6cm} p{1.8cm} X }
\hline
\s [GeV]   & Colliding system &   Experiment   &   \ycm range  &  \pt range [GeV/$c$]   &    Feed-down   &    Fit     &  Measured parameter(s) & Observed trend  \\
\hline
1960  & \ppbar & CDF \cite{CDF:2011ag}  &  $\vert y \vert <$ 0.6  & 0 -- 40  & $\chi_{b}$ : $30\div 50\%$ \cite{Aaij:2014caa}   & $2-D$ &   $\lambda_{\theta}$, $\lambda_{\phi}$, $\lambda_{\theta \phi}$, $\tilde{\lambda}$ {\it vs} \pt in HX, CS & the angular distribution found to be nearly isotropic   \\
\hline
7000  & \pp & CMS \cite{Chatrchyan:2012woa} & $\vert y \vert <$ 1.2  & 10 -- 50  & $\chi_{b}$ : $30\div 50\%$ \cite{Aaij:2014caa} & $2-D$  & $\lambda_{\theta}$, $\lambda_{\phi}$, $\lambda_{\theta \phi}$, $\tilde{\lambda}$ {\it vs} \pt and in 2 $\vert y \vert$ bins in HX, CS, PX & no evidence of large transverse of longitudinal polarisation in whole kinematic range in 3 reference frames  \\
\hline
\end{tabularx}
\end{threeparttable}
\end{table}

\end{landscape}


