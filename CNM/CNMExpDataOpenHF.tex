
\subsubsection{Open heavy-flavour measurements}
\label{sec:CNM:OHF}

Open heavy-flavour production occurs in hard processes at the early stages of the collision (see \sect{sec:pp:Theory:OpenHF} for an introduction to the different calculations). 
As explained in \sect{CNM_theory}, their production in a nuclear environment is affected by the modification of the parton probability density in the nucleus (nPDFs or parton saturation formalisms)  and by the multiple scattering of partons in the nucleus (radiative or collisional parton energy loss, $\kt$ broadening). 
Due to their short lifetimes, open heavy-flavour hadrons are measured via their decay products. Different analyses methods exist: 
{\it (i)} study leptons from heavy-flavour decays;
{\it (ii)} examine the $\pt$-integrated di-lepton invariant mass distribution, to evaluate the charm and beauty cross sections; 
{\it (ii)} fully reconstruct exclusive decays, such as $\Dzero \to {\rm K}^+ \, \pi^-$ or ${\rm B}^0 \to \jpsi \, {\rm K}^0_{\rm S}$; 
{\it (iv)} identify $c$- or $b$-jets from reconstructed jets; 
{\it (v)} inspect heavy-flavour azimuthal correlations. 
%Inclusive heavy-flavour decay leptons report information of both open charm and beauty production, which can be disentangled via a cut or fit of the distribution of the distance between the lepton and the event production vertices, or studying the lepton angular correlation to charged hadrons. 
%Charm or beauty hadronic decays, though less abundant than the leptonic decays, can be fully reconstructed bringing direct information of heavy flavour hadron properties. 
%Beauty tagged jets give access to heavy-flavour production analyses at larger transverse momentum. 
%Additional information deduced from the dilepton analyses on the total charm and beauty production cross sections. 


\paragraph{Heavy-flavour decay leptons}

The production of heavy-flavour decay leptons has been studied at RHIC and at LHC energies in \dAu and \pPb collisions at $\snn=200$\GeV and 5.02\TeV respectively. 
The \pA measurements exploit the inclusive lepton $\pt$-spectrum, electrons at mid-rapidity ($|\eta|<0.5$ for PHENIX, $0<\eta<0.7$ for STAR and $|\eta|<0.6$ for ALICE) and muons at forward rapidities ($1.4<|\eta|<2.0$ for PHENIX and $2.5<\eta<4.0$ for ALICE). 
The heavy-flavour decay spectrum is determined by extracting the non-heavy-flavour contribution to the inclusive lepton distribution. 
The photonic background sources are electrons from photon conversions in the detector material and $\pi^0$ and $\eta$ Dalitz decays. 
The contribution of photon conversions is evaluated with the invariant-mass method or via Montecarlo simulations. 
The Dalitz decays contribution can be determined considering the measured $\pi^0$ and $\eta$ distributions. 
Background from light hadrons, hard processes (prompt photons and Drell-Yan) and quarkonia is determined with Montecarlo simulations, based, when possible, on the measured spectrum. 
STAR data is not corrected for the of $\jpsi$ decays contribution, which is non-negligible at high $\pt$. 
%
Beauty decay electron spectra can be obtained from the heavy-flavour decay electron spectra by a cut or fit of the lepton impact parameter distribution, \ie the distance between the lepton track and the interaction vertex, or exploiting the lepton azimuthal correlation to heavy flavours or charged hadrons. For the latter see the last paragraph of this section. 


Heavy-flavour decay lepton $\rdau$ measurements at mid-rapidity in minimum-bias d--Au collisions at $\snn=200$\GeV by STAR and PHENIX~\cite{Abelev:2006db,Adare:2012yxa} are consistent and suggest no modification of the multiplicity integrated yields for $1<\pt<10\GeVc$ within uncertainties. 
The $\pt$ dependence on $\rdau$ on the multiplicity and the rapidity was studied by PHENIX~\cite{Adare:2012yxa,Adare:2013lkk} and is reported in \fig{fig:HFlepton_RdAvspt_RHIC}. 
It shows a mild dependence with the multiplicity at mid-rapidity. The results at forward and backward rapidities are similar for peripheral collisions, but evidence for a strong deviation for the most central events. 
As shown in \fig{fig:HFlepton_RdAvspt_RHIC_model} and in~\cite{Adare:2012yxa}, 
the measurements at forward rapidity are described both by the model of Vitev {\it et al.}~\cite{Vitev:2006bi,Vitev:2007ve}  -- considering nPDFs, $\kt$ broadening and CNM energy loss -- (ELOSS model described in \sect{sec:eloss}) or by nPDFs alone.
Data at backward rapidity can not be described considering only the nPDFs, suggesting that other mechanisms are at work. 
\begin{figure}[!tbp]
	\centering
	\includegraphics[width=0.8\columnwidth]{hfleptonVsPt_phenix_020_6088.eps}
	\caption{Nuclear modification factor of heavy-flavour decay leptons in \dAu collisions at $\snn=200$\GeV as a function of transverse momentum in the 0--20\% and 60--88\% centrality classes, as measured with the PHENIX detector~\cite{Adare:2012yxa,Adare:2013lkk}.
	\label{fig:HFlepton_RdAvspt_RHIC}
	}
\end{figure}
%
\begin{figure}[!tbp]
	\centering
	\includegraphics[width=0.95\columnwidth]{hfleptonVsPt_phenix_020_6088_models.eps}
	\caption{Nuclear modification factor of heavy-flavour decay leptons in \dAu collisions at $\snn=200$\GeV as a function of transverse momentum in the 0--20\% and 60--88\% centrality classes, as measured with the PHENIX detector~\cite{Adare:2012yxa,Adare:2013lkk}. 
	A PYTHIA calculation considering EPS09 LO is also shown, courtesy of Sanghoon Lim. 
	The calculation by Vitev {\it et al.} considering nPDFs, $\kt$ broadening and CNM energy loss is also shown~\cite{Vitev:2006bi,Vitev:2007ve}. 
	\label{fig:HFlepton_RdAvspt_RHIC_model}
	}
\end{figure}


The preliminary results at LHC energies by the ALICE Collaboration~\cite{Li:2014dha} present $\rppb$ multiplicity-integrated values close to unity at mid-rapidity, as observed at lower energies. The rapidity dependence of the multiplicity-integrated $\rppb$ is also similar to that observed at RHIC. In contrast to RHIC, model calculations with nPDFs present a fair agreement with LHC data. 
%
The first preliminary measurements of the beauty-hadron decay electron $\rppb$ at mid-rapidity by ALICE are consistent with unity within larger uncertainties~\cite{Li:2014dha}. 


The similar behaviour of RHIC and LHC heavy-flavour decay lepton $\rpa$, within the large uncertainties, despite the different $x$-Bjorken ranges covered, suggests that nPDFs might not be the dominant effect in heavy-flavour production. Additional mechanisms like $k_{\rm T}$-broadening, initial or final-state energy loss could be at play. 



\paragraph{Dilepton invariant mass}

The $\ccbar$ and $\bbbar$ production cross sections can be obtained by a fit of the $\pt$-integrated dilepton yields as a function of the pair mass. Such measurement has been performed by PHENIX at mid-rapidity in \dAu collisions~\cite{Adare:2014iwg} at $\snn=200$\GeV (see \fig{fig:HFdilepton_dAu_PHENIX}). 
The contributions of pseudo-scalar mesons, $\pi^0$ and $\eta$, and vector mesons, $\omega$, $\phi$, $\jpsi$ and $\Upsilon$ were simulated based on the measured \dAu cross sections. The sources not directly measured  ($\eta^{\prime}$, $\rho$, $\psi^{\prime}$) were studied in simulation and their contribution determined relative to the measured particles. The Drell-Yan mechanism contribution was simulated with the PYTHIA event generator, and its normalisation was one of the fit parameters. 
The resulting $\bbbar$ production cross section is: ${\rm d}\sigma_{\bbbar}/ {\rm d}y = 0.54 \pm 0.11 \, \rm{(stat)} \pm 0.18 \, \rm{ (syst)}$~mb. 
The large model dependence prevents an accurate measurement of $\sigma_{\ccbar}$.
% 
\begin{figure}[!tbp]
	\centering
	\includegraphics[width=0.75\columnwidth]{dielectronMassSpectrum_Phenix_dAu.pdf}
	\caption{
	Inclusive $\ee$ pair yield from minimum bias \dAu collisions at $\snn=200$\GeV as a function of dilepton invariant mass~\cite{Adare:2014iwg}. The data are compared to the PHENIX model of expected sources. The insert shows in detail the mass range up to $4.5~\GeVc^{2}$. In the lower panel, the ratio of data to expected sources is shown with systematic uncertainties.
	\label{fig:HFdilepton_dAu_PHENIX}
	}
\end{figure}


\paragraph{{\rm D} mesons}

The $\pt$-differential production cross section of $\Dzero$, $\Dplus$, $\Dstarplus$ and $\Ds$ in minimum bias \pPb collisions at $\snn=5.02$~TeV for $|y_{\rm lab}|<0.5$ was published in~\cite{Abelev:2014hha} by ALICE. 
D mesons are reconstructed via their hadronic decays in different $\pt$ intervals from $1\GeVc$ up to $24\GeVc$. 
%Up-to-today, we are not aware of any other D-meson measurement in \pA or $d\mathrm{-A}$ collisions at lower energies. 
Prompt D-meson yields are obtained subtracting the contribution of secondaries from B-hadron decays, determined using pQCD-based estimates~\cite{ALICE:2011aa,Abelev:2014hha}. %This calculation was verified inspecting the D-meson impact parameter distribution. 
No significant variation of the $\rppb$ among the D-meson species is observed within uncertainties. 
The multiplicity-integrated prompt D (average of $\Dzero$, $\Dplus$ and $\Dstarplus$) meson $\rppb$ is shown in \fig{fig:D_RpAvspt_LHC} together with model calculations. 
$\rppb$ is compatible with unity in the measurement $\pt$ interval, indicating smaller than 10--20\% nuclear effects for $\pt>2\GeVc$. %in this kinematic region. 
Data are described by calculations considering only initial-state effects: NLO pQCD estimates (MNR~\cite{Mangano:1991jk}) considering EPS09 nPDFs~\cite{Eskola:2009uj} or Colour Glass Condensate computations~\cite{Fujii:2013yja} (SAT model described in \sect{sec:saturation}). Predictions including nPDFs, initial or final state energy loss and $\kt$-broadening~\cite{Sharma:2009hn} (ELOSS model discussed in \sect{sec:eloss}) also describe the measurements.  
\begin{figure}[!tbp]
	\centering
	\includegraphics[width=0.5\columnwidth]{D_Alice_pPbWithModels.eps}
	\caption{Nuclear modification factor of prompt D mesons in \pPb collisions at $\snn=5.02$\TeV as a function of transverse momentum as measured with the ALICE detector~\cite{Abelev:2014hha}. The measurements are compared with theoretical calculations including various CNM effects.  
	\label{fig:D_RpAvspt_LHC}
	}
\end{figure}

Preliminary measurements of the prompt D meson production as a function of the multiplicity were performed by ALICE~\cite{Russo:2014iia}. The nuclear modification factor of D mesons was evaluated as a function of the event activity, defined in intervals of multiplicity measured in different rapidity intervals. No event activity dependence is observed within uncertainties. D meson production has also been studied as a function of charged-particle multiplicity. The D meson per-event yields increase as a function of the multiplicity at mid-rapidity. The enhancement of the relative D meson yields is similar to that of \pp collisions at $\s=7$\TeV, described in \sect{sec:pp:HadCorrelations}.  The results in \pp collisions favour the scenarios including the contribution of multiple-parton interactions (MPI), parton-percolation or hydrodynamic effects. In \pPb collisions, the cold nuclear matter effects and the contribution of multiple binary nucleon collisions should also be taken into account. 


\begin{figure}[!t]
	\centering
	\includegraphics[width=0.42\columnwidth]{nonpromptJpsi_RpPbVsy_LHCb_model.eps}
	\includegraphics[width=0.42\columnwidth]{nonpromptJpsi_RfbVspt_LHCb.eps}
	\caption{LHCb measurements of non-prompt $\jpsi$ mesons in \pPb collisions at $\snn=5.02$\TeV~\cite{Aaij:2013zxa}. Left: nuclear modification factor as a function of rapidity, compared to nPDF-based calculations~\cite{delValle:2014wha}. Right: forward to backward rapidity ratio as a function of transverse momentum. 
	\label{fig:NonPromptJpsi_pPb_LHCb}
	}
\end{figure}





\paragraph{Open beauty measurements}


%\subparagraph{$\jpsi$ from beauty decays}
The first measurements of the beauty production cross section in \pA collisions down to $\pt=0$ were carried out by LHCb in \pPb collisions at $\snn=5.02$~TeV~\cite{Aaij:2013zxa}. These results were achieved via the analysis of non-prompt $\jpsi$ mesons at large rapidities, $2<\ylab<4.5$. 
$\jpsi$ mesons were reconstructed by an invariant mass analysis of opposite sign muon pairs. The fraction of $\jpsi$ originated from beauty decays, or non-prompt $\jpsi$ fraction, was evaluated from a fit of the component of the pseudo-proper decay time of the $\jpsi$ along the beam direction. 
The $\rppb$ of non-prompt $\jpsi$ was computed considering as \pp reference an interpolation of the measurements performed in the same rapidity interval at $\s=2.76$, 7 and 8\TeV (see \sect{sec:CNM:ppRef}). \fig{fig:NonPromptJpsi_pPb_LHCb}~(left) reports the $\pt$-integrated $\rppb$ as a function of rapidity, whereas \fig{fig:NonPromptJpsi_pPb_LHCb}~(right) presents the double ratio of the production cross section at positive and negative rapidities, $\rfb$, as a function of the $\jpsi$ transverse momentum. 
%
The $\pt$-integrated $\rppb$ is close to unity in the backward rapidity range, and shows a modest suppression in the forward rapidity region. 
$\rfb$ is compatible with unity within the uncertainties in the measured $\pt$ interval, with values almost systematically smaller than unity.
These results indicate a moderate rapidity asymmetry, and are consistent with the $\rppb$ ones. 
The results are in agreement with LO pQCD calculations including EPS09 or nDSg nuclear PDF parameterisations. 
%

%\subparagraph{B-hadron production} 
%
A preliminary measurement of the production of B mesons  in \pPb collisions at $\snn=5.02$\TeV was carried out by the CMS collaboration~\cite{CMS:2014tfa,Innocenti:2014hha}.
B$^0$, B$^+$ and B$_s^0$ mesons are reconstructed via their decays to $\jpsi + {\rm K}$ or $\phi$ at mid-rapidity for $10<\pt<60\GeVc$. The $\dsdpt$ of B$^0$, B$^+$ and B$_s^0$ are described within uncertainties by FONLL predictions scaled by the number of nucleons in the nucleus. 
B$^+$ $\dsdy$ is also described by FONLL binary scaled calculations, and presents no evidence of rapidity asymmetry within the measurement uncertainties. 
These results suggest that B-hadron production for $\pt>10\GeVc$ is not affected, or mildly, by CNM effects.


%\subparagraph{b jets}
%
Preliminary results of the $\pt$ and $\eta$ differential cross section of $b$-jets in \pPb collisions at $\snn=5.02$\TeV have been reported by CMS at mid-rapidity~\cite{CMS:2014tca}. 
Jets from $b$-quark fragmentation are identified studying the distribution of secondary vertices, typically displaced by several mm for jets of $\pt \sim 100~\GeVc$. 
The measured $b$-jet fraction for $50< \pt^{b-jet}<400\GeVc$ is consistent with PYTHIA simulations with the Z2 tune~\cite{Sjostrand:2006za,Sjostrand:2007gs}. 
The $\pt$- and $\eta$-differential spectra are also described by binary-scaled PYTHIA simulations within uncertainties.
$\rppb$ is computed using PYTHIA as \pp reference and is compatible with unity. 
%The measurement uncertainties do not allow to deduct a possible deviation to the PYTHIA simulations.  Within uncertainties, 
These results conform with the expectations that cold nuclear matter effects are not sizeable at large $\pt$. 


\begin{figure}[!t]
\begin{center}
%
\includegraphics[width=0.45\columnwidth,height=0.35\columnwidth]{./CNM/Figures/HFe_HFm_Correlation_PHENIX_PythiaPowhegComparison_pp.png}
\includegraphics[width=0.475\columnwidth,height=0.35\columnwidth]{./CNM/Figures/HFe_HFm_Correlation_PHENIX_Overlay_pp_dAu.png}

%\subfigure[PHENIX, HFe - HFm $\Delta \phi$, \pp $\s=200$\GeV]{
%	\label{fig:pA:HFeHFmpp}
%	\includegraphics[width=0.45\columnwidth,height=0.35\columnwidth]{./CNM/Figures/HFe_HFm_Correlation_PHENIX_PythiaPowhegComparison_pp.png}
%	} 
%\subfigure[PHENIX, HFe - HFm $\Delta \phi$, \pp and \dAu $\s=200$\GeV]{
%	\label{fig:pA:HFeHFmpA}
%	\includegraphics[width=0.475\columnwidth,height=0.35\columnwidth]{./CNM/Figures/HFe_HFm_Correlation_PHENIX_Overlay_pp_dAu.png}
%	} 
\caption{
Heavy-flavour decay electron  ($\pt>0.5\GeVc$, $|\eta|<0.5$) to heavy-flavour decay muon ($\pt>1\GeVc$, $1.4<\eta<2.1$) $\Delta \phi$ correlations in \pp (left) and \dAu (right) collisions at $\s=200$\GeV~\cite{Adare:2013xlp}. 
The \pp results are compared to POWHEG, PYTHIA and MC\@NLO calculations. 
}
\label{fig:pA:HFeHFm}
\end{center}
\end{figure}


\paragraph{Heavy-flavour azimuthal correlations}

%Heavy-flavour angular correlations bring information on their production mechanisms. 
%The leading-order production processes are associated to back-to-back heavy-quark pairs, whereas the next-to-leading-order production mechanisms produce less correlated heavy-quark pairs. 
As described in \sect{sec:pp:AngleCorrelations}, heavy-flavour particle production inherits the heavy-quark pair correlation, bringing information on the production mechanisms. 
%
Heavy-flavour production in \pA collisions is influenced by initial and/or final state effects. 
The modification of the PDFs or the saturation of the gluon wave function in the nucleus predict a reduction of the overall particle yields.
The CGC formalism also predicts a broadening and suppression of the two-particle away-side azimuthal correlations, more prominent at forward rapidities~\cite{Gelis:2010nm,Marquet:2007vb,Lappi:2012nh}. 
Energy loss or multiple scattering processes in the initial or final state are also expected to cause a depletion of the two-particle correlation away-side yields~\cite{Kang:2011bp}. These effects could also affect heavy-flavour correlations in \pA collisions. 

%
%\subparagraph{Electron to muon correlation}
%{\bf Already discussed in section 4.6 ??}
%
Heavy-flavour decay electron ($\pt>0.5\GeVc$, $|\eta|<0.5$) to heavy-flavour decay muon ($\pt>1\GeVc$, $1.4<\eta<2.1$) $\Delta \phi$ azimuthal correlations have been studied by PHENIX in \pp and \dAu collisions at $\s=200$\GeV~\cite{Adare:2013xlp}. 
They exploit the forward rapidity muon measurements in order to probe the low-$x$ region in the gold nucleus. 
The analysis considers the angular correlations of all sign combinations of electron-muon pairs. The contribution from light-flavour decays and conversions is removed by subtracting the like-sign yield to the unlike-sign yield. 
%The advantage of these correlations is that are free of the background sources that are cancelled in usual analyses, such as resonance decays and Drell-Yan. 
\fig{fig:pA:HFeHFm} presents the electron-muon heavy-flavour decay $\Delta \phi$ correlations. 
Model calculations are compared to data for \pp collisions, see \fig{fig:pA:HFeHFm} (left). Calculations from NLO generators seem to fit better the $\Delta \phi$ distribution than LO simulations.  
%
The corresponding measurement in \dAu collision, see \fig{fig:pA:HFeHFm} (right), shows a reduction of the away-side peak as compared to \pp scaled data, indicating a modification of the charm kinematics due to CNM effects. 
%



%\subparagraph{D meson to charged particles correlation}
Preliminary results of D--hadron azimuthal correlations in \pPb collisions at $\snn=5.02$\TeV were carried out by the ALICE Collaboration~\cite{Bjelogrlic:2014kia}. The measurement uncertainties do not allow a clear  conclusion on a possible modification of heavy-quark azimuthal correlations with respect to pp collisions.


