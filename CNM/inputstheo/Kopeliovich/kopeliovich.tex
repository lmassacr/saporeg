\documentclass[prc,aps,preprintnumbers,amsmath,amssymb]{revtex4}

\usepackage{graphics}
 \newcommand\la{\langle}
 \newcommand\ra{\rangle}
 \newcommand\beq{\begin{equation}}
 \newcommand\noi{\noindent}
 \newcommand\eeq{\end{equation}}
 \newcommand\beqn{\begin{eqnarray}}
 \newcommand\eeqn{\end{eqnarray}}

\begin{document}


\begin{center}
{\bf Effects of coherence in charmonium production off nuclei}


{B.\ Z.~Kopeliovich, I.\ K.~Potashnikova and Iv\'an Schmidt}

{Departamento de F\'{\i}sica,
Universidad T\'ecnica Federico Santa Mar\'{\i}a; and
\\
Centro Cient\'ifico-Tecnol\'ogico de Valpara\'iso;\\
Casilla 110-V, Valpara\'iso, Chile}

\end{center}

Production of a charmonium in $pp$ collisions is characterised by the two essential time scales,
coherence and formation times \cite{kz91}. The former is related to the time uncertainty of $\bar cc$ production by a projectile gluon, which in the target rest frame is, $t_c\sim E_g/4m_c^2$.
The latter corresponds to the time interval taken by the $\bar cc$ to develop the charmonium wave function, It is given by $t_f\sim 2E_g/(M_{\psi'}^2-M_{J/\psi}^2)$ and is much longer than $l_c$. 
In the LHC energy range these time scales at positive rapidities become so long, $l_f>l_c\gg R_A$, that all the production amplitudes from different bound nucleons are in phase. In terms of the dipole description this means that Lorentz time delay "freezes" the $\bar cc$ dipole separation during propagation through the nucleus, what essentially simplifies calculations compared with the path-integral technique, which is required at lower energies \cite{kz91,kst2,kth-psi}.

The charmonium production amplitude in a $pA$ collision with impact parameter $b$ has the form \cite{kth-psi,nontrivial,psi-lhc},
\beqn
R_{pA}&=&{1\over A}\int d^2b\int\limits_{-\infty}^{\infty}dz\,
\rho_A(b,z)
\left|S_{pA}(b,z)\right|^2,
\label{120}\\
S_{pA}(b,z)&=&\int d^2r_T\,W_{\bar cc}(r_T)\,
\exp\left[-{1\over2}\sigma_{\bar ccg}(r_T)T_-(b,z)
-{1\over2}\sigma_{\bar cc}(r_T)T_+(b,z)\right].
\label{140}
\eeqn
Here $W_{\bar cc}(r_T)\propto K_0(m_c r_T)\,r_T^2\,\Psi_{f}(r_T)$ is the distribution function for the dipole size $r_T$; $K_0(m_c r_T)$ describes the $r_T$-distribution of the $\bar cc$ dipole in the projectile gluon; $\Psi_f(r_T)$ is the light-cone wave function of the final charmonium; one factor $r_T$ comes from the color exchange transition $(\bar cc)_8\to(\bar cc)_1$ amplitude, anther factor $r_T$ originates either from radiation of a gluon (color-singlet model for $\psi$), or from the wave function of a $P$-wave state ($\chi$). 
The three-body ($g\bar cc$) dipole cross section
$\sigma_{\bar ccg}(r_T)={9\over4}\sigma_{\bar cc}(r_T/2)-{1\over8}\sigma_{\bar cc}(r_T)$ is responsible for the $g\to\bar cc$ transition and its nuclear shadowing. The thickness functions are defined as, 
$T_-(b,z)=\int_{-\infty}^z dz'\rho_A(b,z')$;~ $T_+(b,z)=T_A(b)-T_-(b,z)$, and $T_A(b)=T_-(b,\infty)$. 

The phenomenological dipole cross section at small $x$ is rather well known from DIS data from HERA, so the magnitude of nuclear suppression Eq.~(\ref{120}) can be predicted in a parameter-free way. 
The effects of gluon shadowing, not included in (\ref{140}), is expected to be weak  \cite{nontrivial,psi-lhc}, as was predicted in \cite{kst2,cronin} and was well confirmed by the recent measurements of the Cronin effect at LHC.

The magnitude of nuclear suppression of $J/\psi$ predicted in \cite{psi-lhc} seems to be too strong compared with the recent measurements at forward rapidities by the ALICE experiment. However, the data could be considerably affected by the feed-down from $\psi^\prime$ decays,
which have been predicted in \cite{kz91} to be significantly enhanced by nuclear effects, as is confirmed by the recent measurement in the CMS experiment. Correspondingly the feed-down
contribution should be considerably larger compared with the previous low-energy measurements.

Notice that the nuclear effects for charmonium production cannot be simply extrapolated from $pA$ to $AA$ collisions \cite{nontrivial}. The latter case includes new effects of double color filtering and a boosted saturation scale, as is explained in detail in \cite{nontrivial}.

{\bf Acknowledgments: }
This work was supported in part
by Fondecyt (Chile) grants 1130543, 1130549, 1100287,
and by ECOS-Conicyt grant No. C12E04.




\begin{thebibliography}{00}

\bibitem{kz91}
  B.~Z.~Kopeliovich and B.~G.~Zakharov,
  %``Quantum effects and color transparency in charmonium photoproduction on
  %nuclei,''
  Phys.\ Rev.\  D {\bf 44}, 3466 (1991).

 \bibitem{kst2} B.~Z.~Kopeliovich, A.~Schafer and A.~V.~Tarasov,
  %``Nonperturbative effects in gluon radiation and photoproduction of quark pairs,''
  Phys.\ Rev.\ D {\bf 62}, 054022 (2000).
  
\bibitem{kth-psi}
  B.~Z.~Kopeliovich, A.~V.~Tarasov and J.~H\"ufner,
  %``Coherence phenomena in charmonium production off nuclei at the energies  of
  %RHIC and LHC,''
  Nucl.\ Phys.\  A {\bf 696} (2001) 669.

\bibitem{nontrivial}
  B.~Z.~Kopeliovich, I.~K.~Potashnikova, H.~J.~Pirner and I.~Schmidt,
 %``Heavy quarkonium production: Nontrivial transition from pA to AA collisions,''
  Phys.\ Rev.\  {\bf C83 } (2011)  014912.

\bibitem{psi-lhc} 
  B.~Z.~Kopeliovich, I.~K.~Potashnikova and I.~Schmidt,
  %``Nuclear suppression of J/Psi: from RHIC to the LHC,''
  Nucl.\ Phys.\ A {\bf 864}, 203 (2011).
  
 \bibitem{cronin} 
  B.~Z.~Kopeliovich, J.~Nemchik, A.~Schafer and A.~V.~Tarasov,
  %``Cronin effect in hadron production off nuclei,''
  Phys.\ Rev.\ Lett.\  {\bf 88}, 232303 (2002).


 \end{thebibliography}


\end{document}


