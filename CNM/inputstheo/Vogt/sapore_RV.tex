%%%%%%%%%%%%%%%%%%%%%%% file template.tex %%%%%%%%%%%%%%%%%%%%%%%%%
%
% This is a template file for The European Physical Journal
%
% Copy it to a new file with a new name and use it as the basis
% for your article
%
%%%%%%%%%%%%%%%%%%%%%%%% Springer-Verlag %%%%%%%%%%%%%%%%%%%%%%%%%%
%
%
\documentclass[epj]{svjour}
% Remove option referee for final version
%
% Remove any % below to load the required packages
%\usepackage{latexsym}
\usepackage{graphics}
% etc
%
\begin{document}
%
\title{Cold Matter Effects on Quarkonium Production at NLO}
%\subtitle{Do you have a subtitle?\\ If so, write it here}
\author{R. Vogt\inst{1}\inst{2}% etc
% \thanks is optional - remove next line if not needed
%\thanks{\emph{Present address:} Insert the address here if needed}%
}                     % Do not remove
%
%\offprints{}          % Insert a name or remove this line
%
\institute{Physics Division, Lawrence Livermore National Laboratory, 
Livermore, CA
94551, USA \and Physics Department, University of California, Davis, CA 95616,
USA}
%
\date{Received: date / Revised version: date}
% The correct dates will be entered by Springer
%
%\abstract{
%Insert your abstract here.
%
%\PACS{
%      {PACS-key}{discribing text of that key}   \and
%      {PACS-key}{discribing text of that key}
%     } % end of PACS codes
%} %end of abstract
%
\maketitle
%
%\usepackage{graphics}
% etc
%
%\begin{document}

\section{Introduction}
\label{intro}

The predictions for $J/\psi$ suppression, considering only NLO modifications
of the parton densities in the nucleus (shadowing), 
are described in this section.  
There are other possible cold matter effects on $J/\psi$ production in
addition to shadowing: breakup
of the quarkonium state due to inelastic interactions with nucleons 
(absorption) or produced hadrons (comovers) and energy loss in cold matter.   
Since the quarkonium
absorption cross section decreases with center-of-mass energy, we can expect
that shadowing is the most important cold matter effect at midrapidity,
see Refs.~\cite{Lourenco:2008sk,McGlinchey:2012bp}.  
Here we show results for the rapidity dependence of shadowing on $J/\psi$
and $\Upsilon$ production at $\sqrt{s_{_{NN}}} = 5$ TeV
$p+$Pb collisions, neglecting absorption.

The results are
obtained in the color evaporation model (CEM) at next-to-leading order in the
total cross section.
In the CEM, the quarkonium 
production cross section is some fraction, $F_C$, of 
all $Q \overline Q$ pairs below the $H \overline H$ threshold where $H$ is
the lowest mass heavy-flavor hadron,
\begin{eqnarray}
\sigma_C^{\rm CEM}(s)  =  F_C \sum_{i,j} 
\int_{4m^2}^{4m_H^2} ds
\int dx_1 \, dx_2~ f_i^p(x_1,\mu_F^2)~ f_j^p(x_2,\mu_F^2)~ 
\hat\sigma_{ij}(\hat{s},\mu_F^2, \mu_R^2) \, 
\, , \label{sigtil}
\end{eqnarray} 
where $ij = q \overline q$ or $gg$ and $\hat\sigma_{ij}(\hat s)$ is the
$ij\rightarrow Q\overline Q$ subprocess cross section.    
The normalization factor $F_C$ is fit 
to the forward (integrated over $x_F > 0$) 
$J/\psi$ cross section data on only $p$, Be, Li,
C, and Si targets.  In this way, uncertainties due to 
ignoring any cold nuclear matter effects which are on the order of a few percent
in light targets are avoided.  The fits are restricted to the forward cross 
sections only.

The same values of the central charm quark
mass and scale parameters are employed as those 
found for open charm, $m_c = 1.27 \pm 0.09$~GeV/$c^2$,
$\mu_F/m_c = 2.10 ^{+2.55}_{-0.85}$, and $\mu_R/m_c = 1.60 ^{+0.11}_{-0.12}$ 
\cite{Nelson:2012bc}.
The normalization $F_C$ is obtained for the central set,
$(m_c,\mu_F/m_c, \mu_R/m_c) = (1.27 \, {\rm GeV}, 2.1,1.6)$.  
The calculations for the extent of the mass and scale uncertainties are
multiplied by the same value of $F_C$ to
obtain the extent of the $J/\psi$ uncertainty band \cite{Nelson:2012bc}.
We calculate $\Upsilon$ production in the same manner, with the central
result obtained for $(m_b,\mu_F/m_b, \mu_R/m_b) = (4.65 \, {\rm GeV}, 1.4,1.1)$
\cite{Nelson_inprog}.  
All the 
calculations are NLO in the total cross section and assume that the intrinsic
$k_T$ broadening is the same in $p+p$ as in $p+$Pb.

\begin{figure*}[htpb]
\begin{center}
\begin{tabular}{cc}
%\vspace*{-0.25in}
\resizebox{0.495\textwidth}{!}{\includegraphics{psiy_nlo_lo_shad_5.ps}} &
\resizebox{0.495\textwidth}{!}{\includegraphics{psipt_for_nlo_shad_5.ps}} \\
\resizebox{0.495\textwidth}{!}{\includegraphics{upsy_nlo_lo_shad_5.ps}} &
\resizebox{0.495\textwidth}{!}{\includegraphics{upspt_for_nlo_shad_5.ps}} 
\end{tabular}
\end{center}
\caption[]{The nuclear modification factor $R_{p{\rm Pb}}$ for $J/\psi$ (upper)
and $\Upsilon$ (lower) production calculated using the EPS09 modifications.  
The results are presented as a function of rapidity (left) and $p_T$ (right).
The dashed red histogram shows the EPS09 NLO uncertainties while the dot-dashed
blue histogram shows the dependence on mass and scale.  The magenta curves
show the LO modification and the corresponding uncertainty band as a function
of rapidity only.  The NLO $J/\psi$ results
were originally shown in Ref.~\protect\cite{Albacete:2013ei}.
}
\label{fig:JpsiUpsypt}
\end{figure*}

The mass and scale uncertainties are calculated 
based on results using the one standard deviation uncertainties on
the quark mass and scale parameters.  If the central, upper and lower limits
of $\mu_{R,F}/m$ are denoted as $C$, $H$, and $L$ respectively, then the seven
sets corresponding to the scale uncertainty are  $\{(\mu_F/m,\mu_F/m)\}$ =
$\{$$(C,C)$, $(H,H)$, $(L,L)$, $(C,L)$, $(L,C)$, $(C,H)$, $(H,C)$$\}$.    
The uncertainty band can be obtained for the best fit sets by
adding the uncertainties from the mass and scale variations in 
quadrature. The envelope containing the resulting curves,
\begin{eqnarray}
\sigma_{\rm max} & = & \sigma_{\rm cent}
+ \sqrt{(\sigma_{\mu ,{\rm max}} - \sigma_{\rm cent})^2
+ (\sigma_{m, {\rm max}} - \sigma_{\rm cent})^2} \, \, , \label{sigmax} \\
\sigma_{\rm min} & = & \sigma_{\rm cent}
- \sqrt{(\sigma_{\mu ,{\rm min}} - \sigma_{\rm cent})^2
+ (\sigma_{m, {\rm min}} - \sigma_{\rm cent})^2} \, \, , \label{sigmin}
\end{eqnarray}
defines the uncertainty. 
The EPS09 NLO band is obtained by calculating the deviations 
from the central value
for the 15 parameter variations on either side of the central set and adding
them in quadrature.  With the new uncertainties on the charm cross section,
the band obtained with the mass and scale variation is narrower than that with
the EPS09 NLO variations.

The upper left-hand side of Fig.~\ref{fig:JpsiUpsypt} shows the uncertainty
in the shadowing effect on $J/\psi$ due to the variations in the 30 EPS09 NLO
sets \cite{Eskola:2009uj}
(red) as well as those due to the mass and scale uncertainties
obtained in the fit to the total charm cross section (blue) calculated with
the EPS09 NLO central set.  The uncertainty band calculated in the CEM 
at LO with the EPS09 LO sets is shown for comparison.  
It is clear that the LO results, represented by the smooth magenta curves in 
Fig.~\ref{fig:JpsiUpsypt}, exhibit a larger shadowing effect.  This difference
between the LO results,
also shown in Ref.~\cite{Vogt:2010aa}, and the NLO calculations
arises because the LO and NLO gluon shadowing parameterizations 
differ significantly at low $x$ \cite{Eskola:2009uj}.  

In principle the
shadowing results should be the same for LO and NLO.  Unfortunately, however, 
the gluon modifications, particularly at low $x$ and moderate $Q^2$, are not
yet well enough constrained.
The lower left panel shows the same calculation for $\Upsilon$ production.
Here the difference between the LO and NLO calculations is reduced because the
mass scale, as well as the range of $x$ values probed, is larger.  
Differences in LO results relative to e.g. the color singlet
model arise less from the production mechanism than from the different mass
and scale values assumed.

The right-hand side of the figure shows the $p_T$ dependence of the effect
at forward rapidity for $J/\psi$ (upper) and $\Upsilon$ (lower).  
There is no LO comparison here
because the $p_T$ dependence cannot be calculated in the LO CEM.  The effect
is rather mild and increases slowly with $p_T$.  There is little difference
between the $J/\psi$ and $\Upsilon$ results for $R_{p{\rm Pb}}(p_T)$ because,
for $p_T$ above a few GeV, the $p_T$ scale is dominant.

%

This work was performed under the auspices of the U.S.\
Department of Energy by Lawrence Livermore National Laboratory under
Contract DE-AC52-07NA27344 and  supported in part by the JET collaboration.
%
% BibTeX users please use
% \bibliographystyle{}
% \bibliography{}
\end{document}


@article{Lourenco:2008sk,
      author         = "Lourenco, Carlos and Vogt, Ramona and Woehri, Hermine K.",
      title          = "{Energy dependence of J/psi absorption in proton-nucleus
                        collisions}",
      journal        = "JHEP",
      volume         = "0902",
      pages          = "014",
      doi            = "10.1088/1126-6708/2009/02/014",
      year           = "2009",
      eprint         = "0901.3054",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ph",
      reportNumber   = "CERN-PH-EP-2008-019",
      SLACcitation   = "%%CITATION = ARXIV:0901.3054;%%",
}

@article{McGlinchey:2012bp,
      author         = "McGlinchey, D.C. and Frawley, A.D. and Vogt, R.",
      title          = "{Impact parameter dependence of the nuclear modification
                        of $J/\psi$ production in $d+$Au collisions at
                        $\sqrt{S_{NN}} = 200$ GeV}",
      journal        = "Phys.Rev.",
      number         = "5",
      volume         = "C87",
      pages          = "054910",
      doi            = "10.1103/PhysRevC.87.054910",
      year           = "2013",
      eprint         = "1208.2667",
      archivePrefix  = "arXiv",
      primaryClass   = "nucl-th",
      SLACcitation   = "%%CITATION = ARXIV:1208.2667;%%",
}

@article{Nelson:2012bc,
      author         = "Nelson, R.E. and Vogt, R. and Frawley, A.D.",
      title          = "{Narrowing the uncertainty on the total charm cross
                        section and its effect on the J/\psi\ cross section}",
      journal        = "Phys.Rev.",
      volume         = "C87",
      pages          = "014908",
      doi            = "10.1103/PhysRevC.87.014908",
      year           = "2013",
      eprint         = "1210.4610",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ph",
      SLACcitation   = "%%CITATION = ARXIV:1210.4610;%%",
}

@article{Nelson_inprog,
      author         = "Nelson, R.E. and Vogt, R. and Frawley, A.D.",
      title          = "{}",
      journal        = "in progress",
      volume         = "",
      pages          = "",
      doi            = "",
      year           = "",
      eprint         = "",
      archivePrefix  = "",
      primaryClass   = "",
      SLACcitation   = "%%CITATION = ARXIV:1210.4610;%%",
}

@article{Eskola:2009uj,
      author         = "Eskola, K.J. and Paukkunen, H. and Salgado, C.A.",
      title          = "{EPS09: A New Generation of NLO and LO Nuclear Parton
                        Distribution Functions}",
      journal        = "JHEP",
      volume         = "0904",
      pages          = "065",
      doi            = "10.1088/1126-6708/2009/04/065",
      year           = "2009",
      eprint         = "0902.4154",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ph",
      SLACcitation   = "%%CITATION = ARXIV:0902.4154;%%",
}

@article{Vogt:2010aa,
      author         = "Vogt, R.",
      title          = "{Cold Nuclear Matter Effects on $J/\psi$ and $\Upsilon$
                        Production at the LHC}",
      journal        = "Phys.Rev.",
      volume         = "C81",
      pages          = "044903",
      doi            = "10.1103/PhysRevC.81.044903",
      year           = "2010",
      eprint         = "1003.3497",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ph",
      reportNumber   = "LLNL-JRNL-414274",
      SLACcitation   = "%%CITATION = ARXIV:1003.3497;%%",
}

@article{Albacete:2013ei,
      author         = "Albacete, J.L. and Armesto, N. and Baier, R. and
                        Barnafoldi, G.G. and Barrette, J. and others",
      title          = "{Predictions for $p+$Pb Collisions at sqrt s_NN = 5 TeV}",
      journal        = "Int.J.Mod.Phys.",
      volume         = "E22",
      pages          = "1330007",
      doi            = "10.1142/S0218301313300075",
      year           = "2013",
      eprint         = "1301.3395",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ph",
      reportNumber   = "LLNL-JRNL-609944",
      SLACcitation   = "%%CITATION = ARXIV:1301.3395;%%",
}

%\end{document}

% end of file template.tex

