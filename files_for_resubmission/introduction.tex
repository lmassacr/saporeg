%!TEX root = ../SaporeGravis.tex

\section{Introduction}
\label{sec:introduction}


Heavy-flavour hadrons, containing open or hidden charm and beauty flavour, are among the most important tools
for the study of Quantum Chromodynamics (QCD) in high-energy hadronic collisions, from the 
production mechanisms in proton--proton collisions and their modification in proton--nucleus collisions
to the investigation of the properties of the hot and dense strongly-interacting Quark-Gluon Plasma (QGP)
in nucleus--nucleus collisions.


%pp
Heavy-flavour production in pp collisions provides important tests of our understanding of various aspects
of QCD. The heavy-quark mass acts as a long distance cut-off so that the partonic hard scattering process can be
calculated in the framework of perturbative QCD down to low transverse momenta (\pt). When the heavy-quark
pair forms a quarkonium bound state, this process is non-perturbative as it involves long distances and soft
momentum scales. Therefore, the detailed study of heavy-flavour production and the comparison to experimental data
provides an important testing ground for both perturbative and non-perturbative aspects of QCD calculations. 

%CNM
In nucleus--nucleus collisions, open and hidden heavy-flavour production constitutes a sensitive probe 
of the hot strongly-interacting medium, because hard scattering processes take place in the early stage of the collision
on a time-scale that is in general shorter than the QGP thermalisation time.
Disentangling the medium-induced effects and relating them to its properties requires an accurate study of the so-called cold nuclear matter
(CNM) effects, which modify the production of heavy quarks in nuclear collisions with respect to proton--proton collisions.
CNM effects, which can be measured in proton--nucleus interactions, include: the modification of the effective partonic luminosity in nuclei (which can be described using nuclear-modified parton densities),
due to saturation of the parton kinematics phase space; the multiple scattering of partons in the nucleus before and after the hard scattering;
the absorption or break-up of quarkonium states, and the interaction with other particles produced in the collision (denoted 
as comovers). 
 
 %UPC
The nuclear modification of the parton distribution functions can also be studied, in a very clean environment, using 
quarkonium
photo-production in ultra-peripheral nucleus--nucleus collisions, 
in which a photon from the coherent electromagnetic field of an accelerated nucleus interacts with the coherent gluon field of
the other nucleus or with the gluon field of a single nucleon in the other nucleus.

%OHF
During their propagation through the QGP produced in high-energy 
nucleus--nucleus collisions, heavy quarks interact with the constituents of this medium
 and lose a part of their momentum, thus being able to reveal some of the QGP properties.
QCD energy loss is expected to occur
via both inelastic (radiative energy loss, via medium-induced gluon radiation) and elastic (collisional energy loss)
processes. Energy loss is expected to depend on the parton colour-charge and mass. Therefore, charm and beauty quarks
provide important tools to investigate the energy loss mechanisms, in addition to the QGP properties.
Furthermore, low-\pt heavy quarks could participate, through their interactions with the medium,
in the collective expansion of the system and possibly reach thermal equilibrium with its constituents.

%ONIA
In nucleus--nucleus collisions, 
quarkonium production is expected to be significantly suppressed 
as a consequence of the 
 colour screening of
the force that binds the \ccbar (\bbbar) state. 
In this
scenario, quarkonium suppression should occur sequentially, according to the
binding energy of each state. 
As a consequence, the in-medium dissociation
probability of these states are expected to provide an estimate of the initial
temperature reached in the collisions. 
At high centre-of-mass energy, a new production mechanism 
could be at work in the case of charmonium:
the
abundance of $c$ and $\overline{c}$ quarks might lead to charmonium
production by (re)combination of these quarks. 
An observation of the recombination of heavy quarks would therefore directly
point to the
existence of a deconfined QGP.
%The recombination of heavy quarks offers us a direct observation of the deconfined medium in heavy-ion collisions.
%This effect is expected to be negligible for the case of beauty,
%because of the low production yield of $b\overline b$ pairs.


The first run of the Large Hadron Collider (LHC), from 2009 to 2013, has provided a wealth of measurements
in pp collisions with unprecedented centre-of-mass energies $\sqrt s$ from 2.76 to 8~TeV, in p--Pb collisions at $\snn=5.02$~TeV
per nucleon--nucleon interaction, in Pb--Pb collisions at $\snn= 2.76$~TeV, as well as in photon-induced collisions.
In the case of heavy-ion collisions, with respect to the experimental programmes at SPS and RHIC, the LHC programme has 
not only extended by more than one order of magnitude the range of explored collision energies, but it has also largely
enriched the studies of heavy-flavour production, with a multitude of new observables and improved 
precision. Both these aspects were made possible by the energy increase, on the one hand, and by the 
excellent performance of the LHC and the experiments, on the other hand. 


%SG
This report results from the activity of the SaporeGravis network\footnote{\url{https://twiki.cern.ch/twiki/bin/view/ReteQuarkonii/SaporeGravis}}
of the I3 Hadron Physics programme of the European Union $7^{\rm th}$ FP.
The network was structured in working groups, that are reflected in the structure of this review, and it focused on supporting and strengthening the interactions between the experimental and theoretical communities. 
This goal was, in particular, pursued by organising
two large workshops, in Nantes (France)\footnote{\url{https://indico.cern.ch/event/247609}}
in December 2013 and in Padova (Italy)\footnote{\url{https://indico.cern.ch/event/305164}} in 
December 2014.

The report is structured in eight sections. 
Sections~\ref{pp section}, \ref{Cold nuclear matter effects}, \ref{OHF}, \ref{sec:quarkonia} and \ref{sec:UPC} review, respectively: heavy-flavour and quarkonium production in pp collisions, the cold nuclear matter 
effects on heavy-flavour and quarkonium production in proton--nucleus collisions, the QGP effects on open heavy-flavour 
production in nucleus--nucleus collisions, the QGP effects on quarkonium production in nucleus--nucleus collisions,
and the production of charmonium in  photon-induced collisions. \sect{sec:upgrade}
presents an outlook of future heavy-flavour studies with 
the LHC and RHIC detector upgrades and with new experiments. A short summary concludes the report in \sect{sec:summary}.

