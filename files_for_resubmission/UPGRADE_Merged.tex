
\newpage
\section{Upgrade programmes and planned experiments}
\label{sec:upgrade}

\subsection{Introduction} 

As seen in the previous \sects, and also summarised in the next one,
alongside the great progress in understanding the physics of heavy quarks
in proton--proton and heavy-ion collisions, a lot of questions emerged too.
Those, as well as the quest for a quantitative description of the hot deconfined 
quark-gluon matter, call for upgrades in existing experiments and also for new ones, 
in which the potential of heavy quarks in answering those questions is fully exploited.
Below, we discuss the ongoing efforts and the possibilities for new experiments.


\subsection{Collider experiments}
\subsubsection{The LHC upgrade programme}

The  LHC roadmap foresees three long shut-downs (LS) of the machine in order to perform major upgrades.
The objective of \LSOne, recently completed, was the preparation of the machine for 6.5--7\TeV operation in 2015 \cite{Bordry:2013} reaching (close to) the design energy and the nominal peak luminosity of $\lumi_{\pp}=10^{34}~{\rm cm}^{-2}{\rm s}^{-1}$ or even higher. %by a factor of 2 ?
For heavy ions, interaction rates of 10-15 kHz are expected in \pb in \RunTwo.
The goal of the heavy-ion programme for \RunTwo is to collect about a factor
10 more statistics for \pb collisions with respect to \RunOne, while the new energy of
\snn $\simeq$ 5\TeV will push the frontier of high-energy-density quark-gluon matter.

\LSTwo, scheduled for 2018--2019, will be mainly devoted to a major upgrade of the injectors as well as interventions performed on the LHC itself aiming at increasing its instantaneous luminosity in \pp and heavy-ion running modes to  $\lumi_{\pp}=3\cdot 10^{34}~{\rm cm}^{-2}{\rm s}^{-1}$  and  $\lumi_{\rm PbPb}=6\cdot10^{27}~{\rm cm}^{-2}{\rm s}^{-1}$, respectively.
After \LSTwo, the interaction rate in \pb collisions is foreseen to be 50~kHz.
After \LSThree, envisaged in 2023--2024, the LHC peak luminosity is expected to reach  $\lumi_{\pp}=$5--7$\cdot10^{34}~{\rm cm}^{-2}{\rm s}^{-1}$ levelled down from higher luminosities~\cite{Rossi:2012} (HL-LHC).
The LHC heavy-ion programme is currently planned to extend to Run~4 (2026--2028).

The four major LHC experiments, ALICE, ATLAS, CMS and LHCb, have rich detector upgrade programmes to fully exploit  the accelerator upgrades. Three different phases, corresponding to the three LHC long shut-downs towards the HL-LHC, are planned.
Heavy-flavour and quarkonium physics, both in \pp and \pb collisions, either drive or strongly benefit from the upgrade programmes.

ALICE is the dedicated heavy-ion experiment, whose  strengths, unique at the LHC, are measurements at low \pt and involving a wide range of identified hadrons, and access to forward rapidities ($y\sim 4$) in muon decay channels. This implies primarily minimum-bias (or centrality-selected) collisions, as trigger selectivity for low-\pt observables is obviously weak.

The ATLAS and CMS experiments are general-purpose experiments designed primarily for the investigations of \pp collisions, the Higgs boson discovery \cite{Chatrchyan201230,Aad20121} being the major achievement of \RunOne for this physics programme. 
Both detectors have demonstrated very good performance in heavy-ion collisions too, including measurements devoted to heavy-quarks, as seen in the previous \sects.
The upgrade programmes of both ATLAS and CMS detectors will extend the studies of Higgs and of physics beyond the standard model with improved detector performances to match the LHC luminosity increases. This will benefit the ATLAS and CMS heavy-ion physics programme as well which is focused on higher \pt, complementary to the ALICE programme.

The LHCb experiment is the dedicated experiment for the studies of $b$-quark physics in \pp collisions. The measurements performed by LHCb, for both charm and beauty hadrons, in \pp and \pPb collisions are of a large variety and unique quality. The upgrades of the LHCb detector retain the focus on heavy-flavour studies, which will be performed in \pp and \pPb collisions, as well as in a fixed-target configuration. 


\paragraph{The ALICE experiment}
The ALICE Collaboration consolidated and completed the installation of current detectors during \LSOne with the aim to accumulate 1~\nbinv of \pb collisions during \RunTwo corresponding to about 10 times the \RunOne integrated luminosity.
In parallel, the ALICE Collaboration pursues a major effort to upgrade the apparatus, in particular to improve the tracking precision and to enable the read-out of all interactions at 50~kHz, with the goal to accumulate 10~\nbinv of \pb collisions after \LSTwo. A low-B field (0.2 T) run to collect 3~\nbinv is also envisaged. 
The implementation of this upgrade programme~\cite{Abelevetal:2014cna,ALICE_MFT_LoI}, foreseen in \LSTwo, includes: 
a new low-material Inner Tracking System \cite{ALICE_ITS_TDR} with a forward rapidity extension (MFT \cite{ALICE_MFT_TDR}) to add vertexing capabilities to the current Muon Spectrometer; 
the replacement of the Time Projection Chamber (TPC) wire chambers with gas electron multiplier (GEM) readout;
a new readout electronics for most of the detectors and an updated trigger system;  a new set of forward trigger detectors~\cite{ALICEUpgrade_TDR} and a new integrated online--offline system.

The new Inner Tracking System~\cite{ALICE_ITS_TDR}, covering mid-rapidity ($|\eta|<1.3$), 
consists of seven concentric layers.
For the forward region ($2.5<\eta<3.6$) muon tracker (MFT), 5 detection planes are envisaged.
Both systems are composed of CMOS Monolithic Active Pixel Sensors with a pixel cell size of about $20\times 30$~$\mu$m. 
At mid-rapidity, the total material budget per layer is 0.3\% and 0.8\% of $X_0$ for the three inner and four outer layers, respectively.
It is 0.6\% of $X_0$ per detection plane at forward rapidity~\cite{ALICE_MFT_TDR}.
These low material budget, high granularity detectors, in conjunction with the reduction of the beam pipe diameter in the centre of the ALICE detector from the present value of 58~mm to 36~mm, leads to a significantly improved measurement of the track impact parameter (distance of closest approach to the primary vertex). 
It reaches 40~$\mu$m at \pt~$\simeq$ 0.5~\GeVc at mid-rapidity and 90~$\mu$m at \pt~$\simeq$ 1~\GeVc at forward rapidity.

Thanks to this improved performance and the high statistics, a large number of new measurements become possible and the existing measurements will be repeated with improved performance.
In the charm sector, at mid-rapidity the \pt coverage will be extended towards zero \pt  and the uncertainties will be significantly reduced for the D and {\ensuremath{\mathrm{D}_{s}} meson nuclear modification factor and \vtwo. %(\ensuremath{p_{\mathrm{T}}^{\mathrm{min}}} = 0).
The \ensuremath{\Lambda_c} baryon reconstruction in its three-prong decay (p, K and $\pi$) will become possible down to \pt~=~2~\GeVc, allowing the measurement of baryon/meson ratio (\ensuremath{\Lambda_c}/D) crucial for the study of thermalisation and hadronisation of charm quarks in the medium.
Two-particle correlation studies with a D meson as ``trigger" particles (see \sect{OHF}) as well as a measurement of D-jet fragmentation function will be performed.
At forward rapidity the separation of open charm and open beauty production cross sections can be performed via the semi-muonic and \jpsi decay channels.


\begin{figure}[t]
\begin{minipage}{8cm}
\begin{center}
\includegraphics[width=0.99\textwidth]{2013-Oct-24-D0BJpsiRAAprompt_TDR_logo}
\end{center}
\end{minipage}
\hfill
\begin{minipage}{8cm}
\begin{center}
\includegraphics[width=0.90\textwidth]{ITSupgrade_HFv2_combined}
\end{center}
\end{minipage}
\caption{Estimated performance of open heavy flavour nuclear modification factor
(left) and elliptic flow (right) with the ALICE upgrade.
The \pt dependence of the two observables is assumed based on current measurements and model predictions~\protect\cite{ALICE_ITS_TDR}.}
\label{fig:ALICE_Beauty_v2}
\end{figure}

The ALICE detector upgrade opens up the possibility to fully reconstruct B$^+$ meson (B$^+ \rightarrow \overline{\mathrm{D}^0}\pi^+$) down to \pt~=~2~\GeVc and the $\Lambda_b$ baryon ($\Lambda_b\rightarrow \Lambda_c^+\pi^-$) down to \pt~=~7~\GeVc.
The thermalisation of $b$-quarks in the medium will be studied via the measurement of the elliptic flow in the semi-leptonic as well as \jpsi or D meson decay channels, both at mid- and forward rapidities.
\fig{fig:ALICE_Beauty_v2} gives an example of the expected performance of open heavy flavour nuclear modification factor and elliptic flow measurements~\cite{ALICE_ITS_TDR}.


\begin{figure}[h]
\begin{minipage}{8cm}
\begin{center}
\includegraphics[width=\textwidth]{v2_jpsi_AliUp}
\end{center}
\end{minipage}
\hfill
\begin{minipage}{8cm}
\begin{center}
\includegraphics[width=\textwidth]{ALICE_Psi2S_Psi_Ratio}
\end{center}
\end{minipage}
\caption{Left: estimated statistical uncertainties of \vtwo measurement of \jpsi with the ALICE upgrade~\cite{Abelevetal:2014cna}. Right: estimated performance of the measurement of ratio of the nuclear modification factor of \psiP and \jpsi~\protect\cite{ALICE_MFT_LoI} compared to two charmonium production model calculations~\protect\cite{Andronic:2009sv,Zhao:2011cv}.}
\label{fig:ALICE_Jpsi_v2}
\end{figure}

The ALICE detector upgrade will lead to a significant improvement of the (prompt) \jpsi measurement, both at mid- and  forward rapidities. The expected performance of the \vtwo measurement is illustrated in \fig{fig:ALICE_Jpsi_v2} (left). At forward rapidity, the measurement of \jpsi polarization in \pb collisions will become possible, as well as precision measurements of the production of \ups states.

The measurement of the \psiP meson, combined with the \jpsi measurement, offers an important tool to discriminate between different charmonium production models.
The \psiP measurement is a challenge in heavy-ion experiments, in particular at low \pt. % at midrapidity.
At forward rapidity, the addition of the MFT will allow a precise measurement of \psiP down to zero \pt even in the most central \pb collisions.
At mid-rapidity the measurement remains very challenging, but a significant result is expected with the full statistics of the ALICE data after the upgrade (\fig{fig:ALICE_Jpsi_v2} right).

Comparable detection performances at mid and forward rapidities will place the ALICE experiment in the position of studying the heavy flavour QGP probes as a function of rapidity.
This will help imposing tighter experimental constraints to theoretical models.

The physics programme for ultra peripheral \AAcoll collisions (UPC, see \sect{sec:UPC}) in ALICE for \RunThree and Run~4 will
have the advantage of increased luminosity by a factor of 10 (100) with respect to \RunTwo (\RunOne).
%But with such luminosities, collision pile-up may bring some difficulties in applying the exclusivity condition.
%Preliminary studies have been performed and conclude  that it will be possible to deal with problems originating from collision pile-up. 
The large increase in statistics could as well allow the detail study of processes with small cross section
like the coherent production of \ups or the production of \etac in $\gamma\gamma$ collisions.



\paragraph{The ATLAS experiment}
During \LSOne, ATLAS achieved the installation of the Insertable B-Layer (IBL)~\cite{ATLAS:TDR:19}, which is an additional fourth pixel layer, placed closer to the beam pipe at an average radius of 33~mm.
It will add redundancy to the inner tracking system, 
leading to improved tracking robustness.
This new layer provides improved pointing resolution of the inner tracker for \pt as low as 1~\GeVc and a pseudo-proper decay length resolution improved by about 30\% compared to \RunOne, leading to an improved $b$-quark tagging performance~\cite{ATLAS:Btagging}.
The inner tracker will be completely replaced during the \LSThree~\cite{ATLAS:LoI:Phase2} 
in order to cope with the high-luminosity after \LSThree of the LHC. %Run3 radiation damage?
This new tracker, composed of silicon pixel and strip layers, will have capabilities equivalent to the current tracker (with the IBL).

 During \LSTwo, ATLAS envisages the installation of new Muon Small Wheels and more selective (``topological") Level-1 trigger criteria ~\cite{ATLAS:LoI:Phase1,ATLAS:TDR:23}, which carry the potential to improve the dimuon acceptance at low \pt. 
The cavern background leads to fake triggers in the forward muon spectrometer, with adverse impact on its physics capability.
In order to be able to handle the high luminosity, it is proposed to replace the first end-cap station by the New Small Wheel (NSW) covering the rapidity range $1.2<|\eta|<2.4$.
The NSW will be integrated into the Level-1 trigger, improving the background rejection.
Two technologies, MicroMegas detectors and small-strip Thin Gap chambers, will be used in order to have both a good position resolution ($<100~\mu$m) and a fast trigger function. 
A longer upgrade plan of the muon end-caps, consisting in the extension of the muon reconstruction coverage to higher rapidities, is under study. 
One of the possible scenarios consists in the addition of a  warm toroid at small angles combined with new muon chambers at high rapidity.
The extension would cover the rapidity range $2.5<|y|<4.0$ and should have a \pt resolution of the order of 15--40\% for \pt ranging from 10 to 100~\GeVc.

The high luminosity of \RunTwo and \RunThree imposes to rise the muon \pt trigger threshold. In order to partially limit this increase, a new Level-1 topological trigger algorithm has been implemented. 
A  \pt threshold of the order of 15~\GeVc at the \jpsi mass which will increase to 30~\GeVc for higher luminosities is foreseen.
The ATLAS quarkonia and heavy flavour programmes will therefore concentrate on the high-\pt range (\pt $\gtrsim$ 30~\GeVc).
 
 
\paragraph{The CMS experiment}
Two phases compose the  CMS experiment upgrade programme.
The first one, spread over \LSOne and \LSTwo, involves consolidation of the current detectors.
The upgrade activities in \LSOne and \LSTwo are focused on the inner pixel detector, the hadron calorimeter, the forward muon systems, and the Level-1 trigger~\cite{CMS_L1_Trigger_TDR, CMS_Pixel_TDR, CMS_HCAL_TDR}.
The entire pixel detector will be replaced during the 2016 -- 2017 yearly shut-down.
The new device adds a fourth detection layer for redundancy in tracking and leads to an improved fake-track rejection.
The pointing resolution will be improved thanks to the reduced material budget and by moving the first detection layer closer to the interaction point, which will substantially improve the $b$-quark tagging capability.
The upgrade in the hadron calorimeter Level-1 trigger is motivated by the heavy-ion programme; the significantly-improved selectivity for high-\pt jets opens up 
precision measurements of $b$-tagged jets in Pb--Pb collisions. An illustration of the expected performance is given in \fig{fig:CMS_bJets} for the doubly-tagged $b$-jet asymmetry parameter $A_J$~\cite{CMS:2013gga}.

\begin{figure}[t]
\begin{center}
\includegraphics[width=0.5\textwidth]{CMS_bJets}
\end{center}
\caption{Estimated performance of the measurement in CMS of the doubly-tagged $b$-jet asymmetry $A_J$ distribution in $|\eta| < 2$~\cite{CMS:2013gga}.}
\label{fig:CMS_bJets}
\end{figure}

The muon system will be completed during \LSTwo by adding a fourth end-cap layer for $1.2<|\eta |<1.8$ and by improving the read-out granularity at mid-rapidity. 
During \LSOne, Level-1 hardware trigger algorithms was upgraded, resulting in an improved muon trigger selectivity~\cite{CMS:2013gga}. 
The impact on the HI physics programme will be a better non-prompt \jpsi extraction and an improved dimuon mass resolution. 

The second phase of the CMS upgrades~\cite{CMS:Phase2} planned to be completed during \LSThree, leads to the readiness of the experiment for physics at the HL-LHC.
This includes the extension of the inner tracking system to $|\eta|<4$ with triggering capability and additional muon redundancy with a possible extension of the muon system to cover $|\eta|<4$. 
It will result in an improved mass resolution (gain of 1.2--1.5) with an improved quarkonia triggering performance. 
High-statistics measurements of prompt and non-prompt \jpsi, \psiP
and \ups states will become available~\cite{CMS:2013gga}.

With regard to UPC (see \sect{sec:UPC}), thanks to the increase of statistics
expected in \RunThree, and pending detail performance studies, CMS would be able to carry out systematic studies for the various \ups states, as well as detailed studies on UPC dijets produced in photon-nucleus collisions.

\paragraph{The LHCb experiment}
The upgrade programme of the LHCb Collaboration \cite{Bediaga:1443882} has two major facets:
i) the replacement of all front-end electronics\footnote{The replacement of front-end electronics implies, for some of the LHCb detectors, like the silicon trackers, the replacement of the active elements.}, which will enable continuous detector readout at 40~MHz, followed by a full software trigger \cite{CERN-LHCC-2014-016};
ii) detector upgrades designed for operation at a luminosity increased by a factor of 5 compared to current conditions (levelled nominal luminosity will be 2$\cdot10^{33}$~cm$^{-2}{\rm s}^{-1}$ in pp collisions). 
This comprises the replacement of the VELO silicon vertex detector \cite{Collaboration:1624070}, %41 mil. 55x55 $\mu$m$^2$ pixels
new tracker systems before and after the dipole magnet \cite{Collaboration:1647400},
and major upgrades for the systems performing particle identification: the RICH, the calorimeter and the muon system \cite{Collaboration:1624074}.

The upgraded LHCb detector components will be installed during \LSTwo and is envisaged to collect a pp data sample of at least 50~\ensuremath{\text{~fb}^\text{$-$1}}. This will significantly enhance the unique physics capability of LHCb for heavy-flavour measurements also in \pPb collisions.
The focus is on rare observables in connection to physics beyond the standard model, but the high-precision measurements of production cross sections for quarkonia and open charm hadrons is a direct bonus.




\subsubsection{The RHIC programme}
\noindent 
The current plans~\cite{Akiba:2015jwa} envisage measurement at RHIC up to mid-2020's, followed by eRHIC. 
During the 2014 run, thanks to the full implementation of 3D stochastic cooling, RHIC achieved, in \AuAu collisions at \snn~=~200~\GeV, an average stored luminosity of $\lumi=5\cdot10^{27}$~cm$^{-2}{\rm s}^{-1}$ reaching 25 times the design value. The ongoing measurements focus on heavy flavour probes of QGP exploiting the newly-installed silicon vertex detectors in both PHENIX and STAR experiments. These campaigns will extend up to 2016, when the electron cooling of RHIC is expected to enter in operation. The 2015 run modes are scheduled to be \pp, {p--Au}\xspace and possibly  {p--Al}\xspace collisions at \snn=100\GeV.
The RHIC luminosity upgrade plan is to operate the collider in \AuAu collisions at \snn~=~200~\GeV at an average stored luminosity of $\lumi=10^{28}$~cm$^{-2}{\rm s}^{-1}$~\cite{RHIC_Projections}.
The second phase (2018--2019) of the Beam Energy Scan (BES-II), spanning  \snn~=~7 -20~\GeV, opens up the potential of heavy flavour measurements at lowest to-date collider energies.


\paragraph{The sPHENIX project}
The PHENIX Collaboration has a radical upgrade plan consisting on replacing the existing PHENIX central detectors, which have small acceptance and lack hadronic calorimetry, with a compact calorimeter and 1.5~T superconducting magnetic solenoid~\cite{Adare:2015kwa}. %\cite{Aidala:2012nz}.
Full azimuthal calorimeter coverage, both electromagnetic and hadronic, will be available in $|\eta|<1$. 
\begin{figure}[bt]
\begin{minipage}{8cm}
\begin{center}
\includegraphics[width=0.95\textwidth]{sPHENIX_RAA_Upsilon_Npart}
\end{center}
\end{minipage}
\hfill
\begin{minipage}{8cm}
\begin{center}
\includegraphics[width=0.9\textwidth]{sPHENIX_RAA_Upsilon_pt}
\end{center}
\end{minipage}
\caption{Estimated performance (quoted are the statistical uncertainties) of the measurement of the \ups  states \raa using sPHENIX in \AuAu collisions at \snn~=~200~\GeV~\protect\cite{Aidala:2012nz}.}
\label{fig:sPHENIX_RAA_UPSILON}
\end{figure}
The sPHENIX detector will be capable of identifying heavy-quark jets and separating \ups states in their dielectron decay channel with a  mass resolution better than 100~{\ensuremath{\mathrm{MeV}/c^2}.
The current PHENIX detectors will be removed during the 2016 shut-down and sPHENIX detectors will be  installed during the 2020 shut-down of RHIC.
The sPHENIX running plan consists of two years of data taking (2021--2022) in \AuAu, \dAu and \pp collisions at \snn~=~200~\GeV.

The inner tracking system will be composed by the currently existing Silicon Vertex Detector (VTX) with three additional silicon layers at larger radii to improve the momentum resolution and the track-finding capability. 
The reconstruction efficiency will be as high as 97\% for \pt$>2$~\GeVc, with a momentum resolution  of the order of 1--1.5\% (depending on \pt).
The pointing resolution will be better than 30~$\mu$m for \pt$>3$~~\GeVc.
The electrons from the \ups decays  are identified using a combination of the electromagnetic calorimeter and the inner hadron calorimeter with a pion rejection power better than 100 at a 95\% electron efficiency.

Exploiting this very good performance, sPHENIX will be able to measure the suppression pattern of the three \ups states.
The high RHIC luminosity and the sPHENIX data acquisition bandwidth (10 kHz) will give to sPHENIX the opportunity  to record 10$^{11}$ \AuAu collisions, leading to an unprecedented precision of \ups measurements, see \fig{fig:sPHENIX_RAA_UPSILON}.

Thanks to the combination of its high-precision inner tracking  and calorimetry systems, sPHENIX will be able to  perform $b$-quark jet tagging by requiring the presence of charged tracks within the jet with a large distance of closest approach to the primary vertex.
Simulations show that for jet \pt~=~20~\GeVc, a $b$-jet purity of 50\% will be reached with an efficiency of the order of 40--50\%~\cite{Aidala:2012nz}, which will allow to extract $b$-jet \raa down to \pt~=~20~\GeVc in central \AuAu collisions. 
Tagging of a $c$-quark jet using the same technique is challenging due to the shorter $c$-hadron lifetime.
Nevertheless, $c$-jet tagging performance is under study by associating fully reconstructed D meson with reconstructed jets in the calorimeter.
Those heavy-quark jet measurements will be an excellent test of in-medium parton energy loss mechanisms and will give some insights on the fragmentation functions of heavy quarks.


\paragraph{STAR experiment}

The recently-installed Heavy Flavour Tracker (HFT) and Muon Telescope Detector (MTD) 
will allow  significantly-improved measurements for heavy flavour observables already 
in the 2016 run \cite{STAR:2015bur}, as illustrated for the $\mathrm{D}^0$ meson elliptic
flow and the \ups \raa in \fig{fig:STAR_perf}.
Among the observables expected to become accessible in this run are $\Lambda_c$ baryon 
and open beauty meson production.


\begin{figure}[bt]
\begin{minipage}{8cm}
\begin{center}
\includegraphics[width=0.97\textwidth]{STAR_HFT-D0_v2projection}
\end{center}
\end{minipage}
\hfill
\begin{minipage}{8cm}
\begin{center}
\includegraphics[width=0.99\textwidth]{STAR_RAA_Y_projections}
\end{center}
\end{minipage}
\caption{Estimated performance (quoted are the statistical uncertainties) of the STAR measurement of the $\mathrm{D}^0$ meson elliptic flow (left panel) and the \ups  states \raa (left panel) in \AuAu collisions at \snn~=~200~\GeV~\protect\cite{STAR:2013bur,STAR:2015bur}.}
\label{fig:STAR_perf}
\end{figure}

The next stages of the STAR upgrade programme \cite{STAR_Decadal} include
on a short term (with focus on BES-II, 2018--2019) an upgrade of the
inner part of the TPC readout chambers as well as a new Event Plane
Detector (EPD) covering the rapidity range $1.5<|y|<5$.
The TPC upgrade will improve the tracking and PID performances and extend
the TPC coverage from $|y|<1$ to $|y|<1.7$.
The long term part of the upgrade (foreseen for the 2021--2022 heavy-ion
programme and the  2025+ eRHIC programme) includes upgrades on the HFT for
faster readout and a forward rapidity system with tracking and
calorimetry. The focus will be on measurements relevant for QGP in \AAcoll
collisions, CNM effects in \pA collisions and for the spin programme at
RHIC \cite{Akiba:2015jwa}. All heavy-flavour observables will receive
significant improvements in precision in these measurements.


\subsection{The fixed target experiments} % 
Fixed-target  experiments using hadron beams have played a major role in quarkonium physics, starting with
the co-discovery  in 1974 of the \jpsi~\cite{Aubert:1974js} at BNL with a 30 GeV proton beam on a Be target, the discovery of
the $\Upsilon$~\cite{Herb:1977ek} with 400 GeV protons on Cu and Pt targets and the first observation of $h_c$~\cite{Armstrong:1992ae} at Fermilab
with antiprotons on an internal hydrogen jet target. In addition, fixed-target experiments
have revealed, through high-precision quarkonium studies, many novel and mostly unexpected features of quark and gluon
dynamics, which included the anomalous suppression of \jpsi~\cite{Abreu:2000ni}
in \pb collisions at SPS, the strong non-factorising nuclear suppression of \jpsi hadroproduction at high
$x_F$~\cite{Hoyer:1990us} and the large-$x_F$ production of \jpsi pairs~\cite{Badier:1982ae}.

A few fixed-target projects in connection with heavy-flavour and quarkonium are being discussed in our community at the SPS, Fermilab, FAIR and the LHC. These are reported below.

\subsubsection{Low energy projects at SPS, Fermilab and FAIR}
Using a 120~\GeV proton beam extracted from the Fermilab Main Injector, the Fermilab E-906/SeaQuest experiment~\cite{Reimer:2001bt}, 
which is part of a series of fixed target Drell-Yan experiments done at Fermilab, 
aims to examine the modifications to the antiquark structure of the proton from nuclear binding and to better quantify the energy loss of a coloured parton (quark) travelling 
through cold, strongly-interacting matter. In the context of this review, one should stress that their muon spectrometer covering 
dimuon mass from roughly 3 to 7~\ensuremath{\mathrm{GeV}/c^2} also allows one to perform \jpsi and \psiP cross section 
measurements with a good accuracy. 

The COMPASS Collaboration has recently started to look at Drell-Yan measurements using a 190 GeV pion beam~\cite{Quintans:2011zz} with the aim of measuring
single-transverse spin asymmetries and of measuring the quark Sivers functions~\cite{Sivers:1989cc}. Data taken during tests in 2009 have revealed that, with the same set-up, 
they can also measure, with a good accuracy, pion-induced \jpsi (probably also  \psiP) production cross sections at \snn~=~18.8\GeV on nuclear targets.
 
Another experiment at SPS, NA61/SHINE also has plans to move ahead to charm production in the context of heavy-ion physics. Their upgrade relies on the installation 
of a new silicon vertex detector~\cite{Ali:2013yva} which would allow for precise track reconstructions and, in turn, for \Dzero production studies in \PbPb collisions 
at \snn~=~8.6 and 17.1\GeV.

Finally, the FAIR project, presently under construction at GSI Darmstadt, has a nucleus--nucleus collisions 
programme devoted to the study of baryon-dominated matter at high densities~\cite{Friman:2011zz}.
The dedicated Compressed Baryonic Matter (CBM) experiment is designed with the initial priority on the study 
of open charm and charmonium close to production threshold in \AuAu collisions around 25 GeV-per-nucleon beam 
energy (\snn~$\simeq$~7\GeV). The current baseline of the FAIR project envisages collisions with Au beams only 
up to 10~\GeV per nucleon (SIS-100), implying that the charm sector of CBM cannot be covered in nucleus--nucleus collisions. 
Production in proton-nucleus collisions will be studied, while the possible addition of a second ring will bring the accelerator to the initially-designed energy (SIS-300).


\subsubsection{Plans for fixed-target experiments using the LHC beams}


\begin{table}[!ht]
\begin{center}
\caption{Expected yields (assuming no nuclear effects) and luminosities obtained for a 7 (2.76) TeV proton (Pb) beam extracted by means of bent crystal (upper part) and obtained with an internal gas target (lower part).}
\footnotesize
{\begin{tabular}{c  c c c c c c c c c c }
  \hline
  Beam & Flux & Target & \snn & Thickness & $\rho$ & $A$ & $\cal{L}$ & $\int{\cal{L}}$ &${\rm Br}_{\ell\ell} \left.\frac{\dd N_{\jpsi}}{\dd y}\right|_{y=0} $ &${\rm Br}_{\ell\ell} \left.\frac{\dd N_{\Upsilon}}{\dd y}\right|_{y=0} $  \\ 
   & (s$^{-1}$) &  & (GeV) &  (cm)&(g\,cm$^{-3}$) &  & ($\mu$b$^{-1}$s$^{-1}$) & (pb$^{-1}$\,y$^{-1})$ & (y$^{-1}$)& (y$^{-1}$)\\ \hline
  p & $5 \times 10^8$ &Liquid H & 115 & 100 & 0.068 & 1 & 2000 & 20000 & $4.0 \times 10^8$ & $8.0 \times 10^5$  \\
  p & $5 \times 10^8$ &Liquid D & 115 & 100 & 0.16 & 2 & 2400 & 24000 & $9.6 \times 10^8$ & $1.9 \times 10^6$\\
  p & $5 \times 10^8$ &Pb & 115 & 1  & 11.35 & 207 & 16 & 160 & $6.7 \times 10^8$ & $1.3 \times 10^6$ \\ \hline 
  Pb & $2 \times 10^5$ &Liquid H & 72 & 100 & 0.068 & 1 & 0.8 & 0.8 & $3.4 \times 10^6$ & $6.9 \times 10^3$\\
  Pb & $2 \times 10^5$ &Liquid D & 72 & 100 & 0.16 & 2 & 1 & 1 & $8.0 \times 10^6$ & $1.6 \times 10^4$\\
  Pb & $2 \times 10^5$ &Pb & 72 & 1  & 11.35 & 207 & 0.007 & 0.007 & $5.7 \times 10^6$ & $1.1 \times 10^4$ \\ \hline \\ \hline
  Beam & Flux & Target & \snn & Usable gas zone & Pressure &  $A$ & $\cal{L}$ & $\int{\cal{L}}$ &${\rm Br}_{\ell\ell} \left.\frac{\dd N_{\jpsi}}{\dd y}\right|_{y=0} $ &${\rm Br}_{\ell\ell} \left.\frac{\dd N_{\Upsilon}}{\dd y}\right|_{y=0} $  \\
       & (s$^{-1}$)&  &  (GeV) & (cm) & (Bar) &  & ($\mu$b$^{-1}$s$^{-1}$) & (pb$^{-1}$\,y$^{-1})$  & (y$^{-1}$)& (y$^{-1}$) \\ \hline
    p  &  $3 \times 10^{18}$&perfect gas & 115 & 100 & $10^{-9}$ & $A$& 10 & 100 &  $2 \times 10^6 \times A$& $4 \times 10^3\times A$\\ \hline
    Pb &  $5 \times 10^{14}$&perfect gas & 72 & 100 & $10^{-9}$ & $A$ & 0.001 & 0.001 & $4.25 \times 10^3 \times A$& $8.6  \times A$\\ 
  \hline
\end{tabular}}
\end{center}
\label{tab:lumi-after}
\end{table}


Historically, the first proposal to perform fixed-target experiments with the LHC beams  dates back to the early nineties
along with the LHB proposal (see \eg \cite{Costantini:1993rr}) to perform flavour physics studies using the expected $10^{10}$ B mesons produced per year
using an extracted beam with a flux of more than  $10^8$ protons per second obtained with a bent-crystal 
positioned in the halo of the beam. This idea was revived in the mid 2000's~\cite{Uggerhoj:2005xz} 
and it is now being investigated at the LHC along with the smart collimator solution proposed by 
the (L)UA9 Collaboration\footnote{\url{http://home.web.cern.ch/about/experiments/ua9}.}.

More generally, a beam of 7\TeV protons colliding on fixed targets results in a centre-of-mass
energy close to 115\GeV, in a range where few systems have been studied at a limited luminosity.  
With the 2.76\TeV Pb beam, \snn amounts to 72\GeV, approximately
half way between the top \AuAu and \CuCu energy at RHIC and the typical energies studied at the SPS.
As discussed in \cis{Brodsky:2012vg,Lansberg:2012kf,Lansberg:2012wj,Rakotozafindrabe:2012ei}, colliding the LHC proton and heavy-ion
beams on fixed targets offer a remarkably wide range of physics opportunities. 
The fixed-target mode with TeV beams has four critical advantages:
i) very large luminosities, 
ii) an easy access over the full target-rapidity domain,
iii) the target versatility and 
iv) the target polarisation.
This respectively allows for: 
i) decisive statistical precision for many processes,
ii) the first experiment covering the whole negative $x_F$ domain up to $- 1$,
iii) an ambitious spin programme relying on the study of single transverse spin asymmetries and
iv)  a unique opportunity to
study in detail the nuclear matter versus the hot and dense matter formed in heavy-ion collisions, including the formation of the quark-gluon plasma down to the target rapidities.

\paragraph{SMOG -- the first step} A first -- probably decisive -- step towards such a project has been made by the LHCb Collaboration using SMOG, 
 a system designed to perform imaging of the beam profiles towards luminosity determination~\cite{FerroLuzzi:2005em}.
SMOG consists in the injection of a gas (Ne until now) in the VErtex LOcator of LHCb; this also allows to record fixed-target collisions.
During test beams, data have been recorded in {p--Ne} collisions at \snn~=~87~\GeV and {Pb--Ne} at \snn~=~54.4~\GeV. 
The current limited  statistics -- due to the limited gas pressure and the short run durations -- has for now only allowed for strange-hadron reconstruction. 
A handful of \jpsi and charmed mesons might be extracted from these data. In any case, they have illustrated 
that a detector like LHCb has a very good coverage for the fixed-target mode and proved that this system can be used beyond its primary goal and offers new physics opportunities.
LHCb plans in taking more data using SMOG during \RunTwo. The goal is to accumulate about 0.5~\nbinv of  {Pb--Ne} collisions with the aim of studying \jpsi and \Dzero productions.
Let us stress here that, thanks of the boost between the cms and laboratory frame, 
the rapidity shift between them is 4.8 with the 7~\TeV proton beam. Hence,
a detector covering $\eta_{\rm lab} \in [1,5]$ allows for measurements  in essentially the whole backward hemisphere, \ie $y_{\rm cms}\leq 0$ or $x_F \leq 0$.


\paragraph{A Fixed-Target ExpeRiment at the LHC, AFTER@LHC}
With a dedicated set-up and run schedule (see below), \pp and \pA collisions can be studied, during the $10^7$ s LHC proton run,
with luminosities three orders of magnitude larger than at RHIC. \ensuremath{\mathrm{Pb-A}} collisions can be studied, during the $10^6$~s LHC Pb run,  
at a luminosity comparable to that of RHIC and the LHC over the full range of the target-rapidity domain with a large
variety of nuclei. Quarkonium production, open heavy-flavour hadrons
and prompt photons in \pA collisions can thus be investigated~\cite{Brodsky:2012vg,Lansberg:2012kf} with statistics 
previously unheard of (see \tab{tab:lumi-after}) and in the backward region, $x_F < 0$, which is essentially uncharted.
This would certainly complement the studies discussed in \sect{Cold nuclear matter effects}.
In complement to conventional nuclear targets made of Pb, Au, W, Cu, etc., high precision QCD measurements (including some of those discussed in \sect{pp section}) can also obviously be performed in \pp and p--d collisions with hydrogen and deuterium targets.
Finally, looking at ultra-relativistic nucleus--nucleus collisions from the rest frame of one of the colliding nuclei offers the opportunity to study in detail its remnants in collisions where the QGP can be formed. 
Thanks to the use of the recent ultra-granular calorimetry technology,
studies of direct  photons, \chic\ and even \chib\ production  in heavy-ion collisions
-- two measurements not available in any other experimental configuration -- can be envisioned (see~\ci{Arleo:2012dxa} for a similar idea at SPS energies).



To substantiate these claims, we have gathered in \tab{tab:lumi-after} a set of key quantities for two scenarios -- a beam extracted/splitted with a bent crystal with a dense target and a gas target intercepting the full LHC flux --,  
such as the cms energy, the flux through the target, its length, its density/pressure, the instantaneous and yearly luminosities as well as the \jpsi and \ups yields close to $y=0$.
Another possibility, which consists in positioning a 500~$\mu$m thick lead ribbon in the halo of the proton or lead LHC beams, would lead to instantaneous luminosities of 100~mb$^{-1}$s$^{-1}$ and 2.2~mb$^{-1}$s$^{-1}$, respectively~\cite{Kurepin:2011zz}.



