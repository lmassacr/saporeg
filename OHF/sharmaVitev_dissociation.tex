% !TEX root = ../SaporeGravis.tex

Heavy flavour dynamics in dense QCD matter critically depends on the time  scales 
involved in the underlying reaction. Two of these timescales, the formation time 
of the QGP  $\tau_0$ and its transverse size $L_{QGP}$,  
can be related to the nuclear geometry, the QGP expansion,  and the bulk particle 
properties.  The formation time  $\tau_{\rm form}$ of heavy mesons and quarkonia, on the other hand, 
can be evaluated from the virtuality of the  heavy  quark $Q$  decay  into 
D, B mesons~\cite{Adil:2006ra,Sharma:2009hn} or the time for the \QQbar pair to 
expand to the size of the \jpsi or  
$\Upsilon $ wave function~\cite{Sharma:2012dy}.  For a $\pi^0$ with an energy of 10\GeV, 
$\tau_{\rm form} \sim 25$~fm $\gg L_{QGP}$ affords a relatively simple interpretation
of light hadron  quenching in terms of radiative and collisional parton-level energy loss~\cite{Kang:2014xsa}. 
On the other hand, for D, B, \jpsi and $\Upsilon(1{\rm S})$, 
one obtains $ \tau_{\rm form} \sim$~1.6, 0.4, 3.3 and 1.4~fm $\ll L_{QGP}$.
Such short formation times necessitate understanding of heavy meson and quarkonium 
propagation and dissociation in strongly interacting matter.

The Gulassy-Levai-Vitev (GLV) reaction operator formalism
was developed for calculating the interactions of parton systems
as  they pass through a dense  strongly-interacting medium.  It was  generalised to the dissociation 
of mesons (quark-antiquark binaries), as long as the momentum exchanges from the medium 
$\mu = g T$  can resolve their internal structure.  The dissociation probability  and dissociation time
\begin{equation}
P_d(\pt,m_Q,t) = 1 -   \left| \int \dd^{2}{\vec \Delta k} \dd x \,
\psi_{f}^* (\Delta {\vec k},x)\psi_{0}(\Delta {\vec k}, x) \right|^{2}  \;,  \qquad  \frac{1}{\langle \tau_{\rm diss}(\pt, t) \rangle} = 
\frac{\partial}{\partial t} \ln  P_d(\pt,m_Q,t) \; , 
\end{equation}
can be obtained from the overlap between the  medium-broadened time-evolved and  vacuum  initial
meson wave functions, $\psi_f$ and  $\psi_0$, respectively. Here, $\psi_f$ has the resummed collisional 
interactions in the QGP. Let us denote by
\begin{equation}
\label{convent1}
f^{Q}({p}_{T},t)= \frac{\dd\sigma^Q(t)}{\dd y \dd^2\pt} \;,  
 \;   f^{Q}({p}_{T},t=0) = 
\frac{\dd \sigma^Q_{PQCD}}{\dd y \dd^2\pt} \;, \qquad      f^{H}({p}_{T},t)= \frac{\dd\sigma^H(t)}{\dd y\dd^2\pt} \;,
 \;   f^{H}({p}_{T},t=0) = 0 \; ,   
\end{equation}
the double differential production cross sections for the heavy quarks and
hadrons.  Initial conditions are also specified  above, in particular the heavy quark distribution is given by 
the perturbative QCD \cquark and \bquark quark cross section. Energy loss in the partonic state can be 
implemented as quenched initial conditions~\cite{Sharma:2009hn,Sharma:2012dy}. 
 Including the loss and 
gain terms one obtains:
\begin{eqnarray}
\label{rateq1}
\partial_t f^{Q}({p}_{\rm T},t)&=& 
-  \frac{1}{\langle \tau_{\rm form}(\pt, t) \rangle} f^{Q}({p}_{\rm T},t) 
 + \, \frac{1}{\langle 
\tau_{\rm diss}(\pt/\bar{x}, t) \rangle}
\int_0^1 \dd x \,  \frac{1}{x^2} \phi_{Q/H}(x)  
f^{H}({p}_{\rm T}/x,t) \;, \qquad \\
\partial_t f^{H}({p}_{\rm T},t)&=& 
-  \frac{1}{\langle \tau_{\rm diss}(\pt, t) \rangle} f^{H}({p}_{\rm T},t) 
 +\, \frac{1}{\langle 
\tau_{\rm form}(\pt/\bar{z}, t) \rangle}
\int_0^1 \dd z \,  \frac{1}{z^2} D_{H/Q}(z) 
 f^{Q}({p}_{\rm T}/z,t) \;. \qquad 
\label{rateq2}
\end{eqnarray}
In \eqs~(\ref{rateq1}) and (\ref{rateq2})   $\phi_{Q/H}(x)$  and  $D_{H/Q}(z)$  are the distribution function of heavy quarks in 
a heavy meson and the fragmentation function of a heavy quark into a heavy mesons, respectively, and  $\bar{z}$ and $\bar{x}$ are
typical fragmentation and dissociation momentum  fractions. It was checked that  
in the absence of a medium, $\tau_{\rm diss}(\pt, t) \rightarrow \infty$,
so that the pQCD spectrum of heavy hadrons from vacuum jet fragmentation are recovered. Details for the rate equation relevant to quarkonium formation and dissociation are given in~\ci{Sharma:2012dy}. 
 Solving the above equations in the limit $t \rightarrow \infty$ in the absence and presence of a medium
allows to evaluate the nuclear modification factor  for heavy-flavour  mesons.

