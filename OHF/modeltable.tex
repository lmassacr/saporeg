% !TEX root = ../SaporeGravis.tex


The theoretical models described in the previous sections are compared in \tab{tab:HFmodels} in terms of their ``ingredients'' for heavy-quark production,
medium modelling, quark--medium interactions, and heavy-quark hadronisation.



In this section we compile a comparison of model calculations with heavy-flavour \raa and \vtwo measurements by the RHIC and LHC experiments.

\fig{fig:DmesonALICEcompTheo} shows the comparison for D mesons in \pb collisions at $\snn = 2.76~\TeV$, measured by the ALICE Collaboration~\cite{ALICE:2012ab,Abelev:2013lca}.
The left panels show \raa in the centrality class 0--20\%, the right panels show \vtwo in the centrality class 30--50\%. The models that include only collisional energy loss are shown
in the upper panels. These models provide in general a good description of \vtwo. The original version of the POWLANG model does not exhibit a clear maximum in \vtwo like the other models,
which could be due to the fact that it does not include, in such a version, hadronisation via recombination.
The latter has been recently introduced in the POWLANG model 
and the additional flow inherited by the D mesons from the light quarks moves the calculations to higher values of \vtwo.
In the TAMU model the decrease of \vtwo towards high \pt is faster than in the other models, which reflects a moderate coupling with the medium, also seen in the 
rise of \raa of D mesons at large \pt. 
% suggesting a smaller effect from path-length dependence of energy loss. 
In this range, some of the other models over-suppress the \raa and one observes large discrepancies between them, which mostly originate from the medium description as well as from the transport coefficients adopted in each model. At low \pt the models (UrQMD, BAMPS) that do not include PDF shadowing give a \raa value larger than observed in the data.
The models that include both radiative and collisional energy loss are shown in the central panels.  All these models provide a good description of \raa, but most of them 
underestimate the maximum of \vtwo observed in data. This could be due to the fact that the inclusion of radiative process reduces the weight of collisional process, which 
are more effective in building up the azimuthal anisotropy. In addition, some of these models (Djordjevic \etal, WHDG, Vitev \etal) do not include a fluid-dynamical medium (for this reason,
the Djordjevic \etal and Vitev \etal models do not provide a calculation for \vtwo), and none of them implements the detailed balance reaction which is mandatory
to reach thermalisation and then undergo the full drift from the medium.
The POWLANG model with l-QCD based transport coefficient and the AdS/CFT predictions are plotted in the lower panels. POWLANG provides a good description of \raa, 
while, for what concerns \vtwo, the results depend crucially on the way hadronisation is described, recombination scenarios leading to a larger elliptic flow (although still smaller than the experimental data in the accessible \pt range).
AdS/CFT, on the other hand, over-predicts the suppression in the full \pt range explored.

\fig{fig:DzeroSTARcompTheo} shows the comparison for the \Dzero meson \raa in \AuAu collisions at $\snn=200$\GeV, measured by the STAR Collaboration~\cite{Adamczyk:2014uip}.
The models that include collisional interactions in an expanding fluid-dynamical medium (TAMU, BAMPS, Duke, MC@$_s$HQ, POWLANG) describe qualitatively the shape of \raa in the interval 0--3\GeVc, with a rise, a 
maximum at 1.5\GeVc with $\raa>1$, and a decrease. In these models, this shape is the effect of radial flow on light and charm quarks. The TAMU model also includes flow in the hadronic phase.
It can be noted that these models predict a similar bump also at LHC energy (left panels of \fig{fig:DmesonALICEcompTheo}): the bump reaches $\raa>1$ for the models that do not include
PDF shadowing, while it stays below $\raa=0.8$ for the models that include it. 
The present ALICE data for $\pt>2$\GeVc do not allow to draw a strong conclusion. However, the preliminary ALICE data reaching down to 1\GeVc in the centrality class 0--7.5\%~\cite{Grelli:2012yv} do not favour models
that predict a bump with $\raa>1$.

The comparisons with measurements of heavy-flavour decay leptons at RHIC and LHC are shown in ~\figs~\ref{fig:hfeRHICcompTheo} and~\ref{fig:hfmuALICEcompTheo}, respectively.
The \raa and \vtwo of heavy-flavour decay electrons in \AuAu collisions at top RHIC energy, measured by PHENIX~\cite{Adare:2010de} and STAR~\cite{Abelev:2006db}, are well described by all model calculations.
Note that in some of the models the quark--medium coupling (medium density or temperature or interaction cross section) is tuned to describe the \raa of pions (Djordjevic \etal, WHDG, Vitev \etal) or electrons (BAMPS) at RHIC.
The \raa of heavy-flavour decay muons at forward rapidity ($2.5<y<4$), measured by ALICE in central \pb collisions at LHC~\cite{Abelev:2012qh}, is well described by most of the models. 
The BAMPS model tends to over-suppress this \raa, as observed also for the high-\pt \raa of D mesons at RHIC and LHC. The MC@$_s$HQ model describes the data better when 
radiative energy loss is not included.
In general, it can be noted that the differences between the various model predictions are less pronounced in the case of heavy-flavour decay lepton observables 
than in the case of D mesons. This is due to the fact that the former include a \pt-dependent contribution of charm and beauty decays. In addition, the decay kinematics 
shifts the lepton spectra towards low momentum, reducing the impact on \raa of effects like PDF shadowing, radial flow and recombination.

In \fig{fig:DnonPromptJpsicompTheo} we compile the model calculations for the centrality dependence of the nuclear modification factors of D mesons in the interval $8<\pt<16$\GeVc and non-prompt \jpsi mesons 
in the interval $6.5<\pt<30$\GeVc, in \pb collisions at $\snn = 2.76$~\TeV. All models predict the \raa of D mesons to be lower by about 0.2--0.3 units than that of non-prompt \jpsi. 
This difference arises from the mass-dependence of quark--medium interactions. The available published data, from the first limited-statistics \pb run at LHC (in 2010), are reported in the figure: note that the D meson \raa 
measured by the ALICE experiment~\cite{ALICE:2012ab} corresponds to the interval 6--12\GeVc (slightly lower than that of the calculations), while the 
non-prompt \jpsi \raa measured by the CMS experiment~\cite{Chatrchyan:2012np}  corresponds to the large centrality classes 0--20\% and 20--100\%. Due to the large uncertainties and the large centrality 
intervals, the data do not allow for a clear conclusion on the comparison with models. The preliminary ALICE~\cite{Bruna:2014pfa} and CMS~\cite{CMS:2012wba} measurements 
using the higher-statistics 2011 \PbPb sample are well-described by the model calculations.
The effect of the heavy-quark mass on the nuclear modification factor is illustrated in \fig{fig:nonPromptJpsiMassEffect},
where the \raa of non-prompt J/$\psi$ is obtained in the Djordjevic {\it et al.}, MC@$_s$HQ and TAMU
models using the $c$-quark mass value for the calculation of the 
in-medium interactions of $b$ quarks. In this case, substantially-lower values of \raa are obtained.


Finally, in \fig{fig:bJetsCMScompTheo} the nuclear modification factor of \bquark-tagged jets measured by the CMS Collaboration in minimum-bias \pb collisions at  $\snn = 2.76$~\TeV is compared with the model described at the end of Section~\ref{sec:pQCDEloss}, including radiative and collisional energy loss. The calculation is shown for three values of the quark--medium coupling parameter $g^{\rm med}$~\cite{Huang:2013vaa}. A precise measurement of this observable in future LHC runs should allow to constrain this parameter to the 10\% level. In addition, an extension of the measurement to transverse momenta lower than 50\GeVc should allow to observe the reduction of suppression due to the mass-dependence of energy loss.

In summary, the comparison of model calculations with currently available data from RHIC and LHC allows for the following considerations:
\begin{itemize}
\item the D meson \vtwo measurements at LHC are best described by the models that include
collisional interactions within a fluid-dynamical expanding medium, as well as hadronisation via recombination;
\item however, theoretical predictions of the \raa of D mesons from these models are scattered, both at RHIC and LHC, which leaves room for theoretical improvement in the future before reliable conclusions can be drawn;
\item on the contrary, the models that include radiative and collisional energy loss provide a good description of the D meson \raa,
but they under-estimate the value of \vtwo at LHC;
\item the models that include collisional energy loss in a fluid-dynamical expanding medium, hence radial flow, exhibit a bump in the low-\pt D meson \raa, 
which is qualitatively consistent with the RHIC data;
\item these models predict a bump also at LHC energy, the size of which depends strongly on the nuclear modification of the PDFs (shadowing); the current data
at LHC are not precise enough to be conclusive in this respect;
\item most of the models can describe within uncertainties the measurements of \raa and \vtwo for heavy-flavour decay electrons at RHIC (in some models,
the quark--medium coupling is tuned to describe these data) and of \raa for heavy-flavour decay muons at LHC;
\item all models predict that the \raa of non-prompt \jpsi from B decays is larger than that of D mesons by about 0.2--0.3 units 
for the \pt region ($\sim 10$\GeVc) for which preliminary data from the LHC experiments exist. 
\end{itemize}


\begin{landscape}
\begin{table}[t]
\caption{Comparative overview of the models for heavy-quark energy loss or transport in the medium described in the previous sections.}
\centering
%\begin{small}
\begin{tabular}{cccccc}
\hline
{\it Model} & {\it Heavy-quark} & {\it Medium modelling} & {\it Quark--medium} & {\it Heavy-quark} & \it Tuning of medium-coupling \\
 & \it production & & \it interactions & \it hadronisation & \it (or density) parameter(s) \\
\hline
{\bf Djordjevic \etal}    & FONLL & Glauber model  & rad. + coll. energy loss & fragmentation & Medium temperature  \\
   \cite{Djordjevic:2009cr,Djordjevic:2008iz,Djordjevic:2006tw,Djordjevic:2011dd,Djordjevic:2013xoa} &  no PDF shadowing & nuclear overlap & finite magnetic mass & & fixed separately \\
   & & no fl. dyn. evolution&  & &  at RHIC and LHC \\
\hline
{\bf WHDG} & FONLL & Glauber model  & rad. + coll. energy loss & fragmentation & RHIC \\
   \cite{Wicks:2005gt,Wicks:2007am} & no PDF shadowing & nuclear overlap & & & (then scaled with ${\rm d}N_{\rm ch}/{\rm d}\eta$)\\
   &  & no fl. dyn. evolution &  &   &  \\
\hline
{\bf Vitev et al.} & non-zero-mass VFNS & Glauber model  & radiative energy loss & fragmentation & RHIC \\
   \cite{Adil:2006ra,Sharma:2009hn} & no PDF shadowing & nuclear overlap & in-medium meson dissociation & & (then scaled with ${\rm d}N_{\rm ch}/{\rm d}\eta$) \\
  & & ideal fl. dyn. 1+1d  & & & \\
  & & Bjorken expansion & & & \\

\hline
{\bf AdS/CFT (HG)} & FONLL & Glauber model  & AdS/CFT drag & fragmentation & RHIC \\
   \cite{Horowitz:2007su,Horowitz:2011wm} &  no PDF shadowing & nuclear overlap & & & (then scaled with ${\rm d}N_{\rm ch}/{\rm d}\eta$) \\
  &  &  no fl. dyn. evolution &  &  &  \\
 \hline
{\bf POWLANG} & POWHEG (NLO) & 2+1d expansion &  transport with Langevin eq. & fragmentation & assume pQCD (or l-QCD  \\
   \cite{Beraudo:2014iva,Beraudo:2014boa,Alberico:2013bza,Alberico:2011zy,Beraudo:2009pe} &  EPS09 (NLO)  & with viscous  & collisional energy loss & recombination & $U$ potential) \\
   & PDF shadowing &  fl. dyn. evolution &  &  & \\
 \hline 
{\bf MC@$_s$HQ+EPOS2} & FONLL & 3+1d expansion &  transport with Boltzmann eq.  &  fragmentation & QGP transport coefficient \\
   \cite{Gossiaux:2008jv,Gossiaux:2009mk,Nahrgang:2013saa} & EPS09 (LO)  & (EPOS model) & rad. + coll. energy loss & recombination & fixed at LHC, slightly\\
   & PDF shadowing & F&  & & adapted for RHIC \\
 \hline 
{\bf BAMPS} & MC@NLO & 3+1d expansion &  transport with Boltzmann eq. &  fragmentation & RHIC \\
   \cite{Uphoff:2010sh,Uphoff:2011ad,Uphoff:2013rka,Uphoff:2014hza} &  no PDF shadowing & parton cascade & rad. + coll. energy loss & & (then scaled with ${\rm d}N_{\rm ch}/{\rm d}\eta$)\\
 \hline 
{\bf TAMU} & FONLL & 2+1d expansion &  transport with Langevin eq.  &  fragmentation & assume l-QCD \\
   \cite{He:2011yi,He:2012df,He:2014cla} & EPS09 (NLO)  & ideal fl. dyn. & collisional energy loss &  recombination & $U$ potential  \\
   & PDF shadowing & & diffusion in hadronic phase  & & \\
 \hline 
{\bf UrQMD} & PYTHIA & 3+1d expansion &  transport with Langevin eq.  &  fragmentation &  assume l-QCD  \\
   \cite{Lang:2012cx,Lang:2013cca,Lang:2013wya} &  no PDF shadowing & ideal fl. dyn. & collisional energy loss &  recombination & $U$ potential \\
 \hline 
{\bf Duke} & PYTHIA & 2+1d expansion &  transport with Langevin eq.  &  fragmentation & QGP transport coefficient \\
   \cite{Cao:2011et,Cao:2013ita} &  EPS09 (LO)  & viscous fl. dyn. & rad. + coll. energy loss &  recombination & fixed at RHIC and LHC \\
   &  PDF shadowing &  &  & & (same value) \\
 \hline 
\end{tabular}
%\end{small}
\label{tab:HFmodels}
\end{table}
\end{landscape}



\begin{figure}[!h]
 \centering
 \includegraphics[width=0.48\textwidth]{comp_Dmeson_Raa_vs_pt_PbPb2760_0_20_coll}
 \includegraphics[width=0.48\textwidth]{comp_Dmeson_v2_vs_pt_PbPb2760_30_50_coll}
 
 \includegraphics[width=0.48\textwidth]{comp_Dmeson_Raa_vs_pt_PbPb2760_0_20_radColl}
 \includegraphics[width=0.48\textwidth]{comp_Dmeson_v2_vs_pt_PbPb2760_30_50_radColl}
 
 \includegraphics[width=0.48\textwidth]{comp_Dmeson_Raa_vs_pt_PbPb2760_0_20_others}
 \includegraphics[width=0.48\textwidth]{comp_Dmeson_v2_vs_pt_PbPb2760_30_50_others}
 \caption{Left: nuclear modification factor as a function of transverse momentum of averaged prompt D mesons in the 0--20\% most central \pb collisions at \snn=2.76~TeV~\cite{ALICE:2012ab} (the filled box at $\raa=1$ is the systematic uncertainty on the normalisation). Right: \vtwo as a function of transverse momentum of D mesons in the 30--50\% centrality \pb collisions at \snn=2.76~TeV~\cite{Abelev:2013lca} (the filled boxes are the systematic uncertainties on the feed-down subtraction). The results are obtained as an average of the \Dzero, \Dplus and \Dstarplus measurements. The results are compared to model calculations implementing collisional energy loss (top panels), collisional and radiative energy loss (middle panels) and to models which cannot be ascribed to the previous categories (bottom panels).}
 \label{fig:DmesonALICEcompTheo}
\end{figure}

\begin{figure}
 \centering
 \includegraphics[width=0.48\textwidth]{comp_Dmeson_Raa_vs_pt_AuAu200_0_10_coll}
 \includegraphics[width=0.48\textwidth]{comp_Dmeson_Raa_vs_pt_AuAu200_0_10_radColl}
 
 \includegraphics[width=0.48\textwidth]{comp_Dmeson_Raa_vs_pt_AuAu200_0_10_others}
 \phantom{\includegraphics[width=0.48\textwidth]{comp_Dmeson_Raa_vs_pt_AuAu200_0_10_coll}}
 \caption{Nuclear modification factor as a function of transverse momentum of \Dzero mesons in the 0--10\% most central \AuAu collisions at \snn=200~GeV~\cite{Adamczyk:2014uip}. The filled boxes at $\raa=1$ are, from left to right, the systematic uncertainties on the normalisation of \AuAu and \pp data. The results are compared to model calculations implementing collisional energy loss (top left), collisional and radiative energy loss (top right) and to models which cannot be ascribed to the previous categories (bottom left).}
 \label{fig:DzeroSTARcompTheo}
\end{figure}


\begin{figure}
 \centering
 \includegraphics[width=0.48\textwidth]{comp_hfe_Raa_vs_pt_AuAu200_0_10_coll}
 \includegraphics[width=0.48\textwidth]{comp_hfe_v2_vs_pt_AuAu200_0_100_coll}
 
 \includegraphics[width=0.48\textwidth]{comp_hfe_Raa_vs_pt_AuAu200_0_10_radColl}
 \includegraphics[width=0.48\textwidth]{comp_hfe_v2_vs_pt_AuAu200_0_100_radColl}
 \caption{Left: nuclear modification factor as a function of transverse momentum of heavy flavour electrons in the 0--5\%~\cite{Abelev:2006db} and 0--10\%~\cite{Adare:2010de} most central \AuAu collisions at \snn=200~GeV. The dashed band (filled box) at $\raa=1$ is the normalisation uncertainty for STAR (PHENIX) data. Right: \vtwo as a function of transverse momentum of heavy flavour electrons in \AuAu collisions at \snn=200~GeV~\cite{Adare:2010de}. The results are compared to model calculations implementing collisional energy loss (top panels) and collisional and radiative energy loss (bottom panels).}
 \label{fig:hfeRHICcompTheo}
\end{figure}

\begin{figure}
 \centering
 \includegraphics[width=0.48\textwidth]{comp_hfmu_Raa_vs_pt_PbPb2760_0_10_coll}
 \includegraphics[width=0.48\textwidth]{comp_hfmu_Raa_vs_pt_PbPb2760_0_10_radColl}
 \caption{Nuclear modification factor as a function of transverse momentum of heavy flavour muons with $2.5<y<4$ measured in the 0--10\% most central \pb collisions at \snn=2.76~TeV~\cite{Abelev:2012qh}. The filled box at $\raa=1$ is the systematic uncertainty on the normalisation. The results are compared to model calculations implementing collisional energy loss (left) and collisional and radiative energy loss (right).}
 \label{fig:hfmuALICEcompTheo}
\end{figure}

%\begin{figure}
% \centering
% \includegraphics[width=0.48\textwidth]{comp_pi0_Raa_vs_pt_PbPb2760_0_5_radColl}
% \caption{Nuclear modification factor as a function of transverse momentum of neutral pions measured in the 0--5\% most central \pb collisions at \snn=2.76~TeV~\cite{Abelev:2014ypa}. The filled box at $\raa=1$ is the systematic uncertainty on the normalisation. The results are compared to model calculations implementing collisional and radiative energy loss. {\bf SHALL WE DROP THIS IF IT HAS ONLY 1-2 MODELS??}}
% \label{fig:pi0ALICEcompTheo}
%\end{figure}

\begin{figure}
 \centering
 \includegraphics[width=0.48\textwidth]{comp_D_nonPromptJpsi_Raa_vs_centr_PbPb2760_coll}
 \includegraphics[width=0.48\textwidth]{comp_D_nonPromptJpsi_Raa_vs_centr_PbPb2760_radColl}
 \caption{Nuclear modification factor as a function of the number of participants of averaged prompt D mesons~\cite{ALICE:2012ab} and non-prompt \jpsi~\cite{Chatrchyan:2012np} measured in \pb collisions at \snn=2.76~TeV compared to model calculations implementing collisional (left) and collisional and radiative energy loss (right). The filled box at $\raa=1$ is the systematic uncertainty on the normalisation of non-prompt \jpsi data.  Note that: a) model predictions refer to ALICE and CMS preliminary data, in a slightly different \pt and rapidity range; b) the point at low \Npart for the non-prompt \jpsi \raa refers to a very large centrality interval (20--100\%).}
%\caption{Model calculations for the nuclear modification factor as a function of the number of participants of D mesons and non-prompt \jpsi in \pb collisions at \snn=2.76~TeV for models implementing collisional only (left panel) and collisional and radiative (right panel) energy loss for heavy quarks.}
 \label{fig:DnonPromptJpsicompTheo}
\end{figure}

\begin{figure}
 \centering
 \includegraphics[width=0.48\textwidth]{comp_nonPromptJpsi_Raa_vs_centr_PbPb2760_cmass}
 \caption{Quark mass dependence of energy loss. The nuclear modification factor of non-prompt \jpsi~\cite{Chatrchyan:2012np} measured in \pb collisions at \snn=2.76~TeV is compared to model calculations obtained in the same way as in \fig{fig:DnonPromptJpsicompTheo} and assuming that the \bquark quark has the mass of the \cquark quark.}
%\caption{Model calculations for the nuclear modification factor as a function of the number of participants of D mesons and non-prompt \jpsi in \pb collisions at \snn=2.76~TeV for models implementing collisional only (left panel) and collisional and radiative (right panel) energy loss for heavy quarks.}
 \label{fig:nonPromptJpsiMassEffect}
\end{figure}


\begin{figure}
 \centering
 \includegraphics[width=0.48\textwidth]{comp_bjets_Raa_vs_pt_PbPb2760_0_100}
 \caption{Nuclear modification factor as a function of transverse momentum of \bquark-jets measured in \pb collisions at \snn=2.76~TeV~\cite{Chatrchyan:2013exa} compared to model calculations. For each value of $g^{\rm med}$, the upper curve is the calculation with the $b$-quark mass and the lower curve is the massless case. The filled box at $\raa=1$ is the systematic uncertainty on the normalisation.}
%\caption{Model calculations for the nuclear modification factor as a function of the number of participants of D mesons and non-prompt \jpsi in \pb collisions at \snn=2.76~TeV for models implementing collisional only (left panel) and collisional and radiative (right panel) energy loss for heavy quarks.}
 \label{fig:bJetsCMScompTheo}
\end{figure}

\clearpage



%\textbf{Djordjevic}
%In Figure~\ref{RaaCentral}, we concentrate on most central collisions and calculate \raa momentum dependence for different probes at these collisions. Specifically, we compare our predictions with experimental data for neutral pions at RHIC and charged hadrons, D mesons and non-prompt \jpsi at LHC. We observe an excellent agreement for light flavour probes and D mesons, and a good agreement for non-prompt \jpsi. In Figure~\ref{RaaVsCent}, we test the predictions against the non-central data, where we fix the momentum region of the probes, and explore how \raa depends on the number of participants. The predictions are generated for the same set of probes as in Fig.~\ref{RaaCentral}, and we also observe a good agreement with the experimental data. 

%%%%%%%%%%%%%%%%%%%%%%%%% Fig. 1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{figure}
% \centering
%% \includegraphics[width=7in,height=1.9in,clip=5,angle=0]{PiHDJPsiRaa}
%%\epsfig{file=PiHDJPsiRaa.eps,width=7in,height=1.9in,clip=5,angle=0}
%%\vspace*{-0.4cm}
%\caption{{\bf Theory vs. experimental data for momentum dependence of light and heavy flavour $R_{AA}$  at RHIC and LHC.} The first panel compares theoretical predictions with experimental data for momentum dependence of $\pi^0$ $R_{AA}$ at 200 GeV central Au+Au collisions at RHIC, where full triangles correspond to Adare:2012wg data~\cite{Adare:2008qa} and open triangles correspond to STAR data~\cite{Abelev:2009wx}. The second, third and fourth panel compares theoretical predictions with experimental data for momentum dependence of, respectively, $h^\pm$ (full triangles correspond to ALICE~\cite{Abelev:2012hxa} and open triangles to CMS~\cite{CMS:2012aa} data), D meson~\cite{Grelli:2012yv} and non-prompt $J/\psi$~\cite{Jo:2013zsa} $R_{AA}$ at 2.76 TeV central Pb+Pb collisions at LHC.  For charged hadrons, full triangles correspond to ALICE data while open triangles correspond to CMS data.}
%\label{RaaCentral}
%%\end{minipage}
%%\end{minipage}
%\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%% Fig. 1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{figure}
%% \includegraphics[width=7in,height=1.9in,clip=5,angle=0]{Raa_vs_cent}
%%\epsfig{file=Raa_vs_cent.eps,width=7in,height=1.9in,clip=5,angle=0}
%%\vspace*{-0.4cm}
%\caption{{\bf Theory vs. experimental data for participant dependence of light and heavy flavour $R_{AA}$  at RHIC and LHC.} The first panel compares theoretical predictions with experimental data for participant dependence of $\pi^0$ $R_{AA}$~\cite{Adare:2012wg} at 200 GeV Au+Au collisions at RHIC, where $\pi^0$ momentum is larger than 7 GeV. The second, third and fourth panel compares theoretical predictions with experimental data for participant dependence of, respectively, $h^\pm$~\cite{Abelev:2012hxa}, D meson~\cite{ALICE:2012ab} and non-prompt $J/\psi$~\cite{Chatrchyan:2012np} $R_{AA}$ at 2.76 TeV Pb+Pb collisions at LHC.  The jet momentum range for the second panel is 6-12 GeV, for the third panel is 8-16 GeV and for the fourth panel is 6.5-30 GeV. Figure adopted from~\cite{Djordjevic:2014tka}. }
%\label{RaaVsCent}
%%\end{minipage}
%%\end{minipage}
%\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\textbf{MC@$_s$HQ}

%\begin{figure}[h]
%\begin{minipage}[t]{0.49\textwidth}
%\centering
%%\includegraphics[width=1.0\textwidth]{RAAcorrected.eps}%eps
%\end{minipage}
%\hfill
%\begin{minipage}[t]{0.49\textwidth}
%\centering
%%\includegraphics[width=1.0\textwidth]{v2_LHCnewa.eps}%eps
%\end{minipage}
%\caption{Nuclear modification factor $R_{\rm AA}$ (left) and elliptic flow \vtwo (right) of D mesons at LHC in comparison to data from the ALICE collaboration \cite{Grelli:2012yv,Abelev:2013lca}. }
%\label{fig:mcatshq_lhc}
%\end{figure}

%In Fig.~\ref{fig:DmesonALICEcompTheo} the \raa and the elliptic flow \vtwo of D mesons are  shown for the two energy loss mechanisms discussed above 
% PB Gossiaux's contribution
%, the purely collisional and the collisional plus radiative and LPM mechanisms. The $K$-factors are tuned such that the \raa for higher transverse momenta is in reasonable agreement with the experimental data. One observes that the purely collisional energy loss mechanism captures the rising trend towards higher \pt better than the one including radiative energy loss.
%At transverse momentum $\pt<7$\GeVc the calculations are significantly above the data. This is due to the lack of shadowing in the initialisation and the strong radial flow developped in the EPOS fluid-dynamical medium. 
%The \vtwo of D mesons compares well with the experimental data for both energy loss mechanisms. At lower \pt the purely collisional scenario yields a larger \vtwo than the collisional plus radiative and LPM, while this picture is reversed at larger \pt where the elliptic flow is more dominated by energy loss. 



%\textbf{BAMPS}
%The top and middle panels of Figure~\ref{fig:DmesonALICEcompTheo} depict the curves for two scenarios: i) elastic only but with $K=3.5$ (this describes the RHIC heavy flavour electron data well), ii) elastic and radiative processes with LPM parameter $X=0.2$, which is tuned to the $D$ meson $R_{AA}$ at LHC.

%The elliptic flow at LHC cannot be explained by elastic and radiative processes with $X=0.2$, although the \raa is described with this parameter. In contrast, the curve with only binary collisions scaled with $K=3.5$ describes both the \vtwo and \raa.

%The mechanisms responsible for the two observables are very different. For the \raa, the energy loss is the important quantity, while \vtwo is most effectively build up by a large transport cross section.
%Although the energy loss of both scenarios is on the same order, the transport cross section of scenario i) is a factor 2-3 larger. This leads to a very similar \raa, but a large \vtwo for the case with only binary interactions and $K=3.5$.


%\textbf{AdS/CFT}
%The curves labeled (BLAH) in FIG (BLAH) represent the heavy flavour suppression predicted based on the application of the AdS/CFT drag energy loss to RHIC and LHC data. 

%The predictions at LHC are not inconsistent with the current $B$ meson suppression measurement from CMS \cite{Chatrchyan:2012np}.  However, the $D$ meson prediction has about a factor of 5 greater suppression than that measured by ALICE \cite{ALICE:2012ab}.  There are two major experimental caveats: first, the significant enhancement of $e^\pm$ from open heavy flavour in preliminary \dAu measurements reported by PHENIX \cite{Tarafdar:2012ef} might indicate significant cold nuclear matter effects not taken into account in this calculation and second, while the $B$ meson suppression prediction is consistent with the current measurement from CMS, it is likely that future measurements will be more discriminating.  %although the measurement current 
