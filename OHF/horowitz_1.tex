The anti-de-Sitter/conformal field theory (AdS/CFT) correspondence~\cite{CasalderreySolana:2011us,DeWolfe:2013cua} is a conjectured dual between field theories in $n$ dimensions and string theories in $n+1$ dimensions (times some compact manifold).  The correspondence is most well understood between $\mathcal{N} = 4$ super Yang-Mills (SYM) and Type IIB string theory; these two theories are generally considered exact duals of one another.  The calculational advantage provided by the conjecture is that there is generally speaking an inverse relationship between the strength of the coupling in the dual theories: when the field theory is weakly-coupled the string theory is strongly-coupled, and vice-versa.  The advantage for QCD physics accessible at current colliders is clear: the temperatures reached at RHIC and LHC are at most a few times $\Lambda_{\mathrm{QCD}}$; it is therefore reasonable to expect that much of the dynamics in these collisions is dominated by QCD physics that is strongly-coupled and hence theoretically accessible only via the lattice, which is then generally restricted to imaginary time correlators, or via the methods of AdS/CFT.  The leading order contribution to string theory calculations (corresponding to a very strong coupling limit in the field theory) comes from classical gravity; one uses the usual tools of Einsteinian General Relativity.  Although much research is focused on finding dual string theories ever closer to QCD, no one has yet found an exact dual; nevertheless, one hopes to gain nontrivial insight into QCD physics by investigating the relevant physics from known AdS/CFT duals.  An obvious limitation of the use of AdS/CFT is that it is difficult to quantify the corrections one expects when going from the dual field theory in which a derivation is performed to actual QCD.

The main thrust of open heavy flavour suppression research that uses the AdS/CFT correspondence assumes that all couplings are strong, regardless of scale (calculations for light quarks with all couplings assumed strong and calculations for which some couplings are strong and some are weak have also been performed; see~\cite{CasalderreySolana:2011us,DeWolfe:2013cua} and references therein for a review).  For reasons soon to be seen, the result is known as ``heavy quark drag'';
%
%There are two main thrusts of open heavy flavour research that exploit the AdS/CFT correspondence: ``heavy quark drag'' and ``$\hat{q}$.'' %(there is also a significant effort in the direction of light flavour energy loss, heavy quark potentials, and low-$p_T$ phenomena).  
%In the first case, ``heavy quark drag,'' all couplings are assumed strong and the entire process of probe quark and thermal medium is modeled in the string theory; 
see~\fig{horowitz:f:setup} %(\subref{horowitz:f:hanging}) 
for a picture of the setup.  The heavy quark is modelled as a string with one endpoint near (or, for an infinitely massive quark, at) the boundary of the AdS space; the string hangs down in the fifth dimension of the spacetime towards a black hole horizon (the Hawking temperature of the black hole is equal to the temperature of the Yang-Mills plasma).  As the string endpoint near the boundary moves, momentum flows down the string; this momentum is lost to the thermal plasma.  For a heavy quark moving with constant velocity $v$ in $\mathcal{N} = 4$ SYM, one finds~\cite{Herzog:2006gh,Gubser:2006bz}
\begin{equation}
	\label{horowitz:e:adsdrag}
	\frac{\dd p}{\dd t} = -\frac{\pi\sqrt{\lambda}}{2}T_{SYM}^2\frac{v}{\sqrt{1-v^2}} \;\; \Longrightarrow \;\; \frac{\dd p}{\dd t} = -\mu_Q\,p,
\end{equation}
where $\mu_Q = \pi\sqrt{\lambda}T_{SYM}^2/2m_Q$, and the energy loss reduces to a simple drag relationship in the limit of a very heavy quark, for which corrections to the usual dispersion relation $p/m_Q \, = \, v/\sqrt{1-v^2}$ are small.  

As the string is dragged, an induced black hole horizon forms in the induced metric on the worldsheet of the string.  This horizon emits Hawking radiation that is dual in the field theory to the influence of the thermal fluctuations of the plasma on the motion of the heavy quark.  The diffusion coefficients have been derived in the large mass, constant motion limit~\cite{Gubser:2006nz,Son:2009vu} as
\begin{equation}
	\label{horowitz:e:adsdiffusion}
	\kappa_{\rm T} = \pi\sqrt{\lambda}\gamma^{1/2}T_{SYM}^3,\;\;\kappa_{\rm L} = \pi\sqrt{\lambda}\gamma^{5/2}T_{SYM}^3.
\end{equation}
Note that for $v\,\ne\,0$ the above diffusion coefficients deviate from the usual ones found via the fluctuation-dissipation theorem, which, for the $\mu_Q$ of \eq{horowitz:e:adsdrag}, yields $\kappa_{\rm T}\,=\,\kappa_{\rm L}\,=\,\pi\sqrt{\lambda}\gamma \, T_{SYM}$.  The entire setup breaks down at a characteristic ``speed limit,'' $\gamma_{crit}^{SL} = (1 + 2 \, m_Q / \sqrt{\lambda}\,T_{SYM})^2 \, \approx 4\,m_Q^2 / \lambda \, T^2$, which corresponds to the velocity at which the induced horizon on the worldsheet moves above the string endpoint (equivalently, if an electric field maintains the constant velocity of the heavy quark, at the critical velocity the field strength is so large that it begins to pair-produce heavy quarks)~\cite{Gubser:2006nz}.  Above the critical velocity, it no longer makes sense to treat the heavy quark as heavy, and one must resort to light flavour energy loss methods in AdS/CFT. 

%In the ``$\hat{q}$'' case, only the $\hat{q}$ of the BDMPS approach, which encapsulates the interaction of the probe quark with the thermal medium, is calculated using AdS/CFT; the heavy quark loses energy via weakly-coupled pQCD radiation.  In this approach one computes the expectation value of a light-like Wilson line

% In one common set of coordinates, the five dimensional background has metric 
% \begin{equation}
% 	\label{horowitz:e:metric}
% 	ds^2 = \frac{L^2}{u^2}\left[-f(u)dt^2 + d\mathbf{x}^2 + \frac{du^2}{f(u)}  \right],
% \end{equation}
% where $f(u) \, = \, 1 - (u/u_h)^4$ is the blackening factor and $L$ is the curvature radius for the AdS space.  The boundary of the AdS space is at $u\,=\,0$ and the $4D$ slice of spacetime is identical to vacuum Minkowski space.  

% The heavy quark is modeled as a string hanging down

\begin{figure}
	\centering
    \includegraphics[width=2.15in]{horowitzhangingstring}
\caption{Schema for the heavy quark drag calculation~\cite{DeWolfe:2013cua}.}
\label{horowitz:f:setup}
\end{figure}

% \begin{figure}[!htbp]
% 	\centering
% \begin{subfigure}[b]{.1in}
%     \captionsetup{skip=-15pt,margin=-10pt}
%     \caption{}
%     \label{horowitz:f:hanging}
% \end{subfigure}
% \begin{subfigure}[b]{2.15in}
%     \centering
%     \includegraphics[width=2.15in]{horowitzhangingstring.pdf}
% \end{subfigure}\quad
% \begin{subfigure}[b]{.15in}
%     \captionsetup{skip=-15pt,margin=-20pt}
%     \caption{}
%     \label{horowitz:f:qhat}
% \end{subfigure}
% \begin{subfigure}[b]{2.15in}
%     \centering
%     \includegraphics[width=2.15in]{horowitzwilsonqhat.pdf}
% \end{subfigure}
% \vspace{5pt}
% \caption{(\subref{horowitz:f:hanging}) Setup for the heavy quark drag calculation.  (\subref{horowitz:f:qhat}) Setup for the $\hat{q}$ calculation.}
% \label{horowitz:f:setup}
% \end{figure}

%Write your contribution here. Example of reference here \cite{Andronic:2013zim}.
