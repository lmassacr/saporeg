%!TEX root = ../SaporeGravis.tex


As mentioned in the introduction to this chapter, the azimuthal anisotropy of particle production in heavy-ion collisions is measured using the Fourier expansion of the 
azimuthal angle ($\phi$) and the \pt-dependent particle distribution $\dd^2 N / \dd\pt \dd\phi$. 
The second coefficient, \vtwo or elliptic flow, which is the dominant component of the anisotropy in non-central nucleus--nucleus collisions, 
is measured using these three
methods: event plane (EP)~\cite{Poskanzer:1998yz}, scalar product (SP)~\cite{Adler:2002pu} and multi-particle cumulants~\cite{Bilandzic:2010jr}.
%In the EP method, the reaction plane is estimated from the measurement of the bulk particles in the event and the azimuthal angle is provided with respect to it.
%In the SP method, a flow coefficient is calculated by the mean cosine of the difference in angle of two flow vectors, each weighted by the length of the vector itself.
%Finally, the multi-particle cumulants uses the formalism of generating functions in order to express the genuine multi-particle correlations, thus allowing to subtract non-flow effects from flow measurements order by order.
In the following, an overview of the elliptic flow measurements for heavy-flavour particles is presented: the published measurements at RHIC use heavy-flavour decay electrons (\sect{sec:v2leptons}); the published measurements at LHC use D mesons (\sect{sec:v2Dmesons}).

%In order to determine the second harmonic coefficient \vtwo, the $Q$-vector is defined from the azimuthal distribution of particles:
 
%\noindent\begin{minipage}{.5\linewidth}
%\begin{equation}
%\label{eq:QVectorFormula}
 %\vec{Q}= \left(\frac{\sum^{N _i=1} w_i \cos 2\phi_i}{\sum^{N _i=1} w_i \sin 2\phi_i}\right)\,,
%\end{equation}
%\end{minipage}%
%\begin{minipage}{.5\linewidth}
%\begin{equation}
%\label{eq:EventPlaneAngle}
%\psi_n = \frac{1}{2} \arctan \left(\frac{Q_y}{Q_x}\right)\,.
%\end{equation}
%\end{minipage}

%The azimuthal angle of the $Q$-vector (Eq.~\ref{eq:EventPlaneAngle}) is called event plane angle and it can be used to estimate the second harmonic symmetry plane $\Psi_2$. 

%In the event plane method, particles yields are directly measured in $90^\circ$-wide intervals of the difference between the azimuthal angle of the particle and the event plane angle. The measured anisotropy is then corrected for the event plane resolution that depends on the method used to compute the event plane itself, on the multiplicity and on the \vtwo of the particles used to compute the Q vector~\cite{Poskanzer:1998yz}. 
%The scalar product method is instead based on the correlation between particles in two different pseudo rapidity regions, in order to suppress the contribution of short distance correlations that might enhance the elliptic flow estimate~\cite{Adler:2002pu}.
%The multi-particle cumulants method is based on the azimuthal distribution of the different particles in particular on the azimuthal correlation between 2 or 4 particles, exploiting the unique relationship between \vtwo and the so-called Q-cumulants~\cite{Bilandzic:2010jr}. The usage of two or four particle correlation gives a different estimate of the elliptic flow ($\vtwo\rm{ \{2\} }$ and $\vtwo\rm{ \{4\}}$) because the two methods have different sensitivity to event-by-event flow fluctuations and non-flow effect (correlations related to jets, resonances decay, \dots). While event-by-event flow fluctuations give on average no clear bias on the flow extraction, the non-flow correlations tend to increase the measured flow present in the event. The usage of four particles correlations or the introduction of a pseudo rapidity ($\eta$) gap between the Q vector measurement and the particles of interest allows to reduce those correlations.

\subsubsection{Inclusive measurements with electrons}
\label{sec:v2leptons}

The measurement of the production of heavy-flavour decay electrons has been presented in \sect{sec:HFelectrons} .   
In order to determine the heavy-flavour decay electron \vtwo, the starting point is the measurement of \vtwo for inclusive electrons.  Inclusive electrons include, mainly, the so-called photonic (or background) electrons (from photon conversion in the detector material and internal conversions in the Dalitz decays of light mesons), a possible contamination from hadrons, and heavy-flavour decay electrons. Exploiting the additivity of \vtwo, the heavy-flavour decay electron \vtwo is obtained by subtracting from the inclusive electron \vtwo the \vtwo of photonic electrons and hadrons, weighted by the corresponding contributions to the inclusive yield.

\begin{figure}[t]
\centering
\includegraphics[width=0.46\textwidth]{fig_PHENIX_HFe_minBias}
\includegraphics[width=0.53\textwidth]{fig_PHENIX_HFe_62GeV}

%\begin{picture}(0,0)
%\put(128,109){\colorbox{white}{\textbf{\footnotesize \! Heavy flavour \!}}}
%\end{picture}
%\vspace{-4mm}
\caption{Heavy-flavour decay electron \vtwo measured by the PHENIX Collaboration in \AuAu collisions at RHIC. Left: measurement in minimum-bias collisions as a function of \pt at $\snn = 200$\GeV~\cite{Adare:2010de}. Right:  measurements at $\snn=62.4$~and~200\GeV in the 20--40\% centrality class for the interval $1.3<\pt<2.5$\GeVc, compared with the $\pi^0$ \vtwo~\cite{Adare:2014rly}.}
\label{fig:HFePHENIX}
\end{figure}

\begin{figure}[t]
\begin{center}
%\includegraphics[width=0.46\textwidth]{fig_STAR_HFe_v2Methods}
\includegraphics[width=0.46\textwidth]{fig_STAR_HFe_sqrts}
\end{center}
\caption{Heavy-flavour decay electron \vtwo measured by the STAR Collaboration in \AuAu collisions (0--60\% centrality class) at centre-of-mass energies 39, 62.4 and 200\GeV~\cite{Adamczyk:2014yew}.}
\label{fig:HFeSTAR}
\end{figure}


The PHENIX Collaboration measured the heavy-flavour decay electron \vtwo in \AuAu collisions at $\snn = 62.4$ and 200\GeV using the event plane method~\cite{Adare:2010de, Adare:2014rly}. 
Electrons were detected at mid-rapidity $|\eta| < 0.35$ in the interval $0.5 < \pt < 5$\GeVc. 
The event plane was instead determined using charged particles at forward rapidity $3.0 < |\eta| < 3.9$.
This large $\eta$-gap is expected to reduce the non-flow effects (like auto-correlations) in the \vtwo measurement. 
\fig{fig:HFePHENIX}~(left) shows the heavy-flavour decay electron \vtwo for minimum-bias events (without any selection on centrality)~\cite{Adare:2010de}. 
\vtwo is larger than zero in the interval $0.5<\pt<2.5$\GeVc, with a maximum value of about 0.1 at \pt of about 1.5\GeVc. Towards larger \pt the data suggest a decreasing trend, although the statistical uncertainties prevent a firm conclusion.  The study of the centrality dependence of \vtwo (not shown) indicates a maximum effect in the two semi-peripheral centrality classes (20--40\% and 40--60\%), for which the initial spatial anisotropy is largest~\cite{Adare:2010de}. 
The central value of the heavy-flavour electron \vtwo in \AuAu collisions at $\snn = 62.4$\GeV~\cite{Adare:2014rly} is significantly lower than at 200\GeV
(see \fig{fig:HFePHENIX}~(right)). However, the statistical and systematic uncertainties are sizeable and do not allow to conclude firmly on the energy dependence of \vtwo.
In \fig{fig:HFePHENIX}~(right) the measurements for heavy-flavour decay electrons with $1.3<\pt<2.5$\GeVc are compared with those for neutral pions with the same \pt: the pions exhibit a larger \vtwo than the electrons; however, this comparison should be taken with care, because the \pt of the heavy-flavour mesons is significantly larger than that of their decay electrons.

%The results on the $R_{\rm{AA}}$ at this energy show no evidence of suppression, even if with large statistical uncertainties, mainly due to the lack of $pp$ reference data at the same energy. Also the $\vtwo$ measurement shows a milder effect: for heavy-flavour electrons of $1.3 < p_{T} < 2.5$\GeVc the $\vtwo$ at $\sqrt{s_{\rm{NN}}} = 62.4\GeV$ is about a factor of two lower than at $\sqrt{s_{\rm{NN}}} = 200\GeV$ for the centrality class 20--40$\%$ (\fig{fig:HFePHENIX} right). Also in this case the results don't allow to draw any strong conclusion due to the large statistical and systematic uncertainties~\cite{Adare:2014rly}. 
 
The STAR Collaboration measured the heavy-flavour decay electron \vtwo in \AuAu collisions at $\snn = 39, ~62.4$ and 200\GeV~\cite{Adamczyk:2014yew}. 
The two-particle cumulant method was used to measure the elliptic flow for the two lower collision energies. The event plane, and both two- and four-particle cumulant methods were used 
at $\snn = 200$\GeV.
%\fig{fig:HFeSTAR}~(left) shows the results on the heavy-flavour decay electron \vtwo at $\snn = 200$\GeV for minimum-bias triggers and triggers with the electromagnetic calorimeter. 
%The measurements based on minimum-bias triggers show a \vtwo larger than zero for $\pt>0.3$\GeVc, compatible with the measurement by the PHENIX Collaboration in the same centrality class.  An increase of the \vtwo for $\pt > 4$\GeVc is observed, that is probably due to jet-like correlations. The black line in the \fig{fig:HFeSTAR}~(left) shows the jet-like correlation expected in \AuAu collisions based on an estimate of non-flow correlations from measurements in \pp collisions scaled by the particle multiplicity in \AuAu. Those correlations can explain the rise of $\vtwo\{2\}$ and $\vtwo\text{\{EP\}}$ with \pt as observed in the data and it is likely that they are dominating the \vtwo measurement for $\pt > 4$\GeVc.
\fig{fig:HFeSTAR} shows the \vtwo measured with two-particle cumulants at the three centre-of-mass energies. 
At $\snn = 200$\GeV the measurement shows a \vtwo larger than zero for $\pt>0.3$\GeVc, compatible with the measurement by the PHENIX Collaboration in the same centrality class (see comparison in~\cite{Adamczyk:2014yew}).  
At  $\snn = 39$ and 62.4\GeV, the $\vtwo\{2\}$ values are consistent with zero within uncertainties.

Preliminary results by the ALICE Collaboration on the elliptic flow of heavy-flavour decay electrons at central rapidity ($|y|<0.6$) and of heavy-flavour decay muons at forward rapidity ($2.5<y<4$)
in \pb collisions at the LHC show a \vtwo significantly larger than zero in both rapidity regions and with central values similar to those measured at top RHIC energy~\cite{Bailhache:2014fia}.

\begin{figure}[!ht]
\begin{center}
\includegraphics[width=0.48\textwidth]{fig_ALICE_Dmes_3050}
\includegraphics[width=0.50\textwidth]{fig_ALICE_Dmes_threeCentr}
\end{center}
\caption{D meson elliptic flow measured by the ALICE Collaboration in \pb collisions at $\snn=2.76$\TeV~\cite{Abelev:2013lca,Abelev:2014ipa}. Left: average of the \vtwo values for $\Dzero$, $\Dplus$ and $\Dstarplus$ mesons in the centrality class 30--50\% as a function of \pt, compared with the \vtwo of charged particles. Right: centrality dependence of the $\Dzero$ meson \vtwo for three \pt intervals.}
\label{fig:DmesALICE}
%\end{figure}
%\begin{figure}[t]
\begin{center}
\includegraphics[width=0.45\textwidth]{fig_ALICE_Dmes_RaavsEP}
\end{center}
\caption{D meson \raa in the direction of the event plane and in the direction orthogonal to the event plane, measured by the ALICE Collaboration in \pb 
collisions (centrality class 30--50\%) at $\snn=2.76$\TeV~\cite{Abelev:2013lca,Abelev:2014ipa}. }
\label{fig:DmesRaavsEPALICE}
\end{figure}



\subsubsection{{\rm D} meson measurements}
\label{sec:v2Dmesons}


The ALICE Collaboration measured the \vtwo of prompt D mesons in \pb collisions at $\snn=2.76$\TeV~\cite{Abelev:2013lca,Abelev:2014ipa}.  
The D mesons ($\Dzero$, $\Dplus$ and $\Dstarplus$) were measured in $|y| < 0.8$ and $2 < \pt < 16$\GeVc 
using their hadronic decay channels, and exploiting the separation of a few hundred $\mu$m of the decay vertex from the interaction vertex 
to reduce the combinatorial background.
The measurement of D meson \vtwo was carried out using the event plane, the scalar product and the two-particle cumulant methods.

\fig{fig:DmesALICE}~(left) shows the average of  the \vtwo measurements for $\Dzero$, $\Dplus$ and $\Dstarplus$ in the centrality class 30--50\% as a function of \pt~\cite{Abelev:2013lca}. The measurement shows a \vtwo larger than zero in the interval $2 < \pt< 6$\GeVc with a $5.7\sigma$ significance. 
In the same figure, the \vtwo of charged particles for the same centrality class is reported for comparison: the magnitude of \vtwo is similar for charmed and light-flavour hadrons. 
\fig{fig:DmesALICE}~(right) shows the dependence on collision centrality of the $\Dzero$ meson \vtwo for three \pt intervals.
An increasing trend of \vtwo towards more peripheral collisions is observed, as expected due to the increasing initial spatial anisotropy. 
%The D mesons \vtwo is also compared to the light flavour one for the same centrality classes reported in \fig{fig:DmesALICE} (right) and the two show a similar trend as a function of centrality, though the large statistical uncertainties that affect the D mesons measurement.

%In ALICE, the measurement of the D mesons $\vtwo$ is also performed using the scalar product and the particle cumulants methods~\cite{Abelev:2014ipa}. For these methods D mesons candidates were reconstructed in bins of $\eta$  and \pt and the independent particle correlators were computed for each bin, obtaining a distribution of \vtwo as a function of mass of the D meson. The \vtwo of the signal is then disentagled from the  one of background with a simultaneous fit of the invariant mass yield and on the \vtwo vs mass distribution, considering the additivity property of the elliptic flow quantity. The results of the three methods and consistent within statistical uncertainties for all three mesons species~\cite{Abelev:2014ipa}.
%The \vtwo measurements with the Event Plane and the Scalar Product methods have been performed both with and without introducing an $\eta$-gap between the D mesons and the $\vec{Q}$ reconstruction in order to reduce non-flow effects. Results are compatible within statical uncertainties indicating that the bias due to non-flow correlations is within the statistical precision of the measurements~\cite{Abelev:2014ipa}. 

As discussed at the beginning of this Section, the azimuthal dependence of the nuclear modification factor \raa can provide insight into the path length dependence of heavy-quark energy loss.
The nuclear modification factor of $\Dzero$ mesons in \pb collisions (30--50\% centrality class) was measured by the ALICE Collaboration in the direction of the event plane (in-plane)
and in the direction orthogonal to the event plane (out-of-plane)~\cite{Abelev:2014ipa}. The results, shown in \fig{fig:DmesRaavsEPALICE}, exhibit a larger high-\pt suppression 
in the out-of-plane direction, where the average path length in the medium is expected to be larger. It is worth noting that the difference between the values of \raa in-plane
and \raa out-of-plane is equivalent to the observation of $\vtwo > 0$, because the three observables are directly correlated. 

