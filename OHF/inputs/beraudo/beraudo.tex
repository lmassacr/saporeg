%\documentclass{appolb}
\documentclass{article}
%\usepackage[margin=0.3in]{geometry}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage[margin=2.3cm]{geometry}
\textheight=23cm
\textwidth=17cm
%\hoffset=-2cm
%\voffset=-1cm


%\usepackage[dvips]{graphicx} 
%\usepackage{graphics}
%\usepackage{graphicx}
%\DeclareGraphicsExtensions{.eps,.ps,.pdf,.png,.jpeg}
% epsfig package included for placing EPS figures in the text
%------------------------------------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                %
%    BEGINNING OF TEXT                           %
%                                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
% \eqsec  % uncomment this line to get equations numbered by (sec.num)
\title{My contribution}

\date{}

\author{A. Beraudo} 


\maketitle
\section{Models of HF-QGP interaction}\label{sec:interaction}
The final ambitious goal of the heavy-flavour experimental programs in $AA$ collisions, besides showing differences in the data with respect to the $pp$ benchmark suggesting the formation of a hot/dense medium, should be to characterize the properties of the produced matter, in particular getting access to the transport coefficients of the Quark Gluon Plasma. Theoretical calculations based (mostly) on numerical implementations of the relativistic Langevin equation and encoding the interaction of the heavy quarks with the plasma into a few transport coefficients provide the tools to achieve this goal: through a comparison of the experimental data with the numerical outcomes obtained with different choices of the transport coefficients it should be possible, in principle, to put tight constraints on the values of the latter. This would be the analogous of the way of extracting information on $\eta/s$ through the comparison of soft-particle spectra with hydrodynamic predictions. A even more intriguing challenge for a theorist would be to derive the heavy-flavour transport coefficients through a first principle QCD calculation, implement them into some numerical code describing the propagation of the quarks in the plasma and compare the results with the experimental data. This would require focusing on observable at the same time experimentally accessible (now or in the near future) and for which theoretical calculations are based on solid grounds.   

First principle non perturbative results for the transport coefficients can be obtained, although within a limited kinematic domain and with sizable systematic uncertainties, from lattice QCD (l-QCD) calculations. Here we briefly summarize the setup employed to extract the momentum diffusion coefficient $\kappa$~\cite{CasalderreySolana:2006rq}, which has to be considered valid in the non-relativistic limit (for this calculation heavy quarks on the lattice are taken as static color sources) in which the Langevin equation reduces to
\begin{equation}
\frac{dp^i}{dt}=-\eta_D p^i+ \xi^i(t),\quad{\rm with}\quad \langle\xi^i(t)\xi^j(t')\rangle\!=\!\delta^{ij}\delta(t-t')\kappa.
\end{equation}
Hence, in the $p\!\to\!0$ limit, $\kappa$ is given by the Fourier transform of the following force-force correlator 
\begin{equation}
\kappa=\frac{1}{3}\int_{-\infty}^{+\infty}\!\!\!dt\langle\xi^i(t)\xi^i(0)\rangle_{\rm HQ}
\approx\frac{1}{3}\int_{-\infty}^{+\infty}\!\!\!dt{\langle F^i(t)F^i(0)\rangle_{\rm HQ}}\equiv\frac{1}{3}\int_{-\infty}^{+\infty}\!\!\!dt D^>(t)=\frac{1}{3}D^>(\omega\!=\!0),
\end{equation}
where the expectation value is taken over an ensemble of states containing, besides thermal light partons, a static ($M\!=\!\infty$) heavy quark.
In a thermal bath correlators are related by the Kubo-Martin-Schwinger condition, entailing for the spectra function ${\sigma(\omega)}\!\equiv\!D^>(\omega)\!-\!D^<(\omega)={(1-e^{-\beta\omega})D^>(\omega)}$, so that
\begin{equation}
{\kappa}\equiv\lim_{\omega\to 0}\frac{{D^>(\omega)}}{3}=\lim_{\omega\to 0}\frac{1}{3}{\frac{\sigma(\omega)}{1-e^{-\beta\omega}}}\underset{\omega\to 0}{\sim}\frac{1}{3}\frac{T}{{\omega}}{\sigma(\omega)}.\label{eq:kappa}
\end{equation}
In the static limit magnetic effects vanish and the force felt by the heavy quark can only be due to the chromo-electric field
\begin{equation}
{\bf F}(t)=g\int \!d{\bf x} \,Q^\dagger(t,{\bf x})t^aQ(t,{\bf x})\,{\bf E}^a(t,{\bf x}).
\end{equation}
Eq.~(\ref{eq:kappa}) shows how $\kappa$ depends on the low-frequency behaviour of the spectral density $\sigma(\omega)$ of the electric-field correlator in the presence of a heavy quark; the latter can be evaluated on the lattice for imaginary times $t\!=\!-i\tau$~\cite{CaronHuot:2009uh}: 
\begin{equation}
{D_E(\tau)}=-\frac{\langle{\rm Re\,Tr}[U(\beta,\tau)gE^i(\tau,{\bf 0})U(\tau,0)gE^i(0,{\bf 0})]\rangle}{\langle{\rm Re\,Tr}[U(\beta,0)]\rangle}.
\end{equation}
In the above the expectation value is now taken over a thermal ensemble of states of gluons and light quarks, with the Wilson lines $U(\tau_1,\tau_2)$ reflecting the presence of a static heavy quark. As it's always the case when attempting to get information on real-time quantities from l-QCD simulations, the major difficulty consists in reconstructing the spectral density $\sigma(\omega)$ from the inversion of
\begin{equation}
{D_E(\tau)}=\int_0^{+\infty}\frac{d\omega}{2\pi}\frac{\cosh(\tau-\beta/2)}{\sinh(\beta\omega/2)}{\sigma(\omega)},
\end{equation}
where one knows the correlator $D_E(\tau)$ for a limited set of times $\tau_i\!\in\!(0,\beta)$. Lattice results for the heavy quark diffusion coefficient are currently available for the case of a pure $SU(3)$ gluon plasma~\cite{Banerjee:2012zj,Francis:2011gc}. In our transport calculations, depending on the temperature, we employed the values $\kappa/T^3\!\equiv\!\overline{\kappa}\!\approx\!2.5-4$ obtained in \cite{Francis:2011gc}, which the authors are currently trying to extrapolate to the continuum (i.e. zero lattice-spacing) limit. 

Being derived in the static $M\!=\!\infty$ limit and lacking any information on their possible momentum dependence, the above results for $\kappa$ have to be taken with some grain of salt when facing the present experimental data (mostly referring to charm at not so small $p_T$); however they could represent a really solid theoretical benchmark when beauty measurements, for which $M\!\gg\!T$, at low $p_T$ will become available. Bearing in mind the above caveats and neglecting any possible momentum dependence of $\kappa$, we have in any case implemented the above l-QCD transport coefficients (the friction coefficient $\eta_D=\kappa/2ET$ being fixed by the Einstein relation) into our POWLANG code~\cite{Alberico:2013bza} and used them to provide predictions for $D$-mesons, heavy-flavour electrons and $J/\psi$'s from B decays. One can estimate what the above results would entail for the average heavy-quark energy-loss: 
\begin{equation}
\langle{dE}/{dx}\rangle=\langle{dp}/{dt}\rangle=-\eta_D\,p=-(\kappa/2ET)\,p=-(\overline{\kappa}T^2/2)\,v.
\end{equation}   
Numerically this would imply a stopping power $\langle-dE/dx\rangle\approx\overline{\kappa}\cdot 0.4\cdot v$ GeV/fm at $T=400$ MeV and  $\langle-dE/dx\rangle\approx\overline{\kappa}\cdot 0.1\cdot v$ GeV/fm at $T=200$ MeV.


\section{Models of HF evolution in URHIC}
The starting point of our POWLANG setup~\cite{Alberico:2013bza} is the generation of the $Q\overline{Q}$ pairs in elementary nucleon-nucleon collisions. For this purpose the POWHEG-BOX package~\cite{Frixione:2007nw,Alioli:2010xd} is employed: the latter deals with the initial production in the hard pQCD event (evaluated at NLO accuracy), interfacing it to PYTHIA 6.4~\cite{Sjostrand:2006za} to simulate the associated initial and final-state radiation and other effects, like e.g. the intrinsic-$k_T$ broadening. In the $AA$ case EPS09 nuclear corrections~\cite{Eskola:2009uj} are applied to the PDFs and the $Q\overline{Q}$ pairs are distributed in the transverse plane according to the local density of binary collisions given by the geometric Glauber model. Furthermore a further $k_T$ broadening depending on the crossed nuclear matter thickness is introduced, as described in~\cite{Alberico:2011zy}. Both in the $pp$ benchmark and in the $AA$ case (at the decoupling from the fireball) hadronization is modeled through independent fragmentation of the heavy quarks, with in-vacuum fragmentation functions tuned by the FONLL authors~\cite{Cacciari:2005rk}.
Concerning the modeling of the fireball evolution, the latter is taken from the output of the (2+1)D viscous hydrodynamic code of Ref.~\cite{Romatschke:2007mq}. At each time step the update of the heavy-quark momentum according to the Langevin equation is performed in the local rest frame of the fluid, boosting then back the results into the laboratory frame.
In setting the transport coefficients entering into the Langevin equation the approach adopted in~\cite{Alberico:2013bza} was to derive the momentum broadening $\kappa_{T/L}(p)$ and to fix the friction coefficient $\eta_D(p)$ so to satisfy the Einstein relation. For the former two different set of values were explored: the ones from a weak-coupling calculations, accounting for 2-to-2 soft (with hard Thermal Loop resummation of medium effects) and hard collisions with gluons and light quarks, and the ones provided by the lattice QCD calculations described in Sec.~\ref{sec:interaction}.

The major limitation of the lattice QCD approach, providing in principle a non perturbative results, is the absence of any information on the momentum dependence of $\kappa$. We decided to take it as a constant. On the contrary in the weak-coupling calculation the longitudinal momentum broadening coefficient $\kappa_L(p)$, although starting from a much lower value than the l-QCD one, displays a steep rise with the heavy-quark momentum, which for high enough energy makes it overshoot the lattice-QCD result, taken as constant. Experimental data on the $R_{AA}$ of $D$ mesons and heavy flavour electrons seem to favor a somehow intermediate scenario.

So far in POWLANG hadronization was modeled as occurring in the vacuum, neglecting the possibility of recombination of the heavy quarks with light thermal partons from the medium. Hence no modification of the heavy flavour hadrochemistry was considered, charm and beauty going into hadrons with the same fragmentation fractions as in the vacuum. Notice that the recombination with light thermal quarks would make the final charmed hadrons inherit part of the flow of the medium, moving present POWLANG results closer to the experimental data. Preliminary results show that this is actually the case, in particular for what concerns the elliptic flow of $D$ mesons measured by ALICE and their $R_{AA}$ at low $p_T$ measured by STAR and displaying a peak that one would like to interpreted as coming from radial flow. 
        

\bibliographystyle{ieeetr}
\bibliography{biblio}

\end{document}
