%\documentclass{appolb}
\documentclass{article}
%\usepackage[margin=0.3in]{geometry}

\usepackage[margin=2.3cm]{geometry}
\textheight=23cm
\textwidth=17cm
%\hoffset=-2cm
%\voffset=-1cm


\usepackage[dvips]{graphicx} 
%\usepackage{graphics}
%\usepackage{graphicx}
%\DeclareGraphicsExtensions{.eps,.ps,.pdf,.png,.jpeg}
% epsfig package included for placing EPS figures in the text
%------------------------------------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                %
%    BEGINNING OF TEXT                           %
%                                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
% \eqsec  % uncomment this line to get equations numbered by (sec.num)
\title{$T$-Matrix Approach to Heavy-Quark Interactions in the QGP}

\date{\today}

\author{Ralf Rapp {\it et al.}} 


\maketitle

The thermodynamic $T$-matrix approach is a first-principles framework to 
(self-consistently) compute one- and two-body correlations in hot and dense matter. 
It has been widely applied to, e.g., electromagnetic plasmas~\cite{Redmer:1997}
and the nuclear many-body problem~\cite{Jeukenne:1976uy,Brockmann:1990cn}. Its 
main assumption is that the basic two-body interaction can be cast into the form 
of a potential, $V(t)$, with the 4-momentum transfer approximated as  
$t=q^2=q_0^2-\vec q^2 \simeq -\vec q^2$. This relation is satisfied for charm and 
bottom quarks ($Q$=$c$,$b$) in a quark-gluon plasma (QGP) up to temperatures of 
2-3\,$T_c$, since their large masses imply $q_0^2\simeq (\vec q^2/2m_Q)^2\ll \vec q^2$ 
with typical momentum transfers of $\vec q^2 \sim T^2$. Therefore, the $T$-matrix 
formalism is a promising framework to treat
the non-perturbative physics of heavy-quark (HQ) interactions in the 
QGP~\cite{Mannarelli:2005pz,Cabrera:2006wh,Riek:2010fk}. 
It can be applied to both hidden and open heavy-flavor states, and provides
a comprehensive treatment of bound and scattering states~\cite{Riek:2010fk}. It 
can be systematically constrained by lattice data~\cite{Riek:2010py}, and 
implemented to calculate heavy-flavor observables in heavy-ion 
collisions~\cite{vanHees:2007me,He:2011qa}.      
 
The potential approximation allows to reduce the 4-dimensional Bethe-Salpeter 
into a 3-dimensional Lippmann-Schwinger equation, schematically given by 
\begin{equation}
T(s,t) = V_{ab}(t) + \int d^3k  \ V(t') \ G_2(s,k) \ T(s,t'') \    
\end{equation}
where $G_2$ denotes the in-medium 2-particle propagator. Using the well-known
Cornell potential in vacuum, heavy quarkonium spectroscopy and heavy-light meson
masses can be reproduced, while relativistic corrections (magnetic interactions)
are important to recover the perturbative limit in high-energy HQ 
scattering~\cite{Riek:2010fk}. 

We here focus on HQ interactions with light partons relevant to heavy-flavor diffusion
in the QGP. The pertinent transport coefficients for a heavy quark of 3-momentum $p$ are 
schematically given by  
\begin{equation}
\gamma_Q(p)  =  \sum\limits_{i=q,\bar{q},g} \int \frac{d^3k}{(2\pi)^3 2\omega_k} \ 
\frac{d^3p'}{(2\pi)^32\omega_{p'}} \ f^i(\omega_k) \ \delta(E_i-E_f) \ 
{\cal M}_{Qi}(s,t) \left( 1-\vec p \cdot \vec p'/p^2 \right)
\end{equation} 
for the relaxation rate and analogous expressions for momentum diffusion~\cite{Rapp:2009my}. 
The invariant HQ-parton scattering amplitude is directly proportional to the $T$-matrix. An 
important question is how the HQ potential $V$ is modified in medium. This is currently an 
open question. As limiting cases the HQ free ($F$) and internal ($U$) energies computed in 
lattice-QCD (lQCD) have been employed~\cite{Kaczmarek:2007pb}. The internal energy produces 
a markedly stronger interaction, and, when 
employed in the $T$-matrix approach, generally leads to better agreement with other lattice 
results (e.g., quarkonium correlators, HQ susceptibility, etc.~\cite{Riek:2010py}). The 
resulting $c$-quark relaxation rates, including scattering off thermal $u$-, $d$-, $s$-quarks 
and gluons, are enhanced over their perturbatively calculated counterparts by up 
to a factor of $\sim$6 at low momenta and temperatures close to $T_{\rm c}$, 
cf.~Fig.~\ref{fig_c-tmat} (a similar enhancement is found for $b$ quarks, although the 
absolute magnitude of the relaxation rate is smaller than for $c$ quarks by about a 
factor of $m_b/m_c\simeq3$.) The enhancement is mostly caused by resonant $D$-meson and 
di-quark states which emerge in the color-singlet and anti-triplet channels as $T_c$ is 
approached from above. These states naturally provide HQ coalescence processes 
in the hadronization transition, i.e., the same interaction that drives nonperturbative 
diffusion also induces hadron formation. The resummations in the $T$-matrix, together with 
the confining interaction in the potential, play a critical role in this framework. At 
high momenta, both confining and resummation effects become much weaker and the diffusion 
coefficients approach the perturbative (color-Coulomb) results, although at $p_c\simeq5$\,GeV, 
the enhancement is still about a factor of 2. With increasing temperature, the 
color screening of the lQCD-based interaction potentials leads to an increase in the 
(temperature-scaled) spatial diffusion coefficient, $D_s(2\pi T) = 2\pi T^2/(m_Q\gamma_Q)$, 
see right panel of Fig.~\ref{fig_c-tmat}.     
\begin{figure}[!ht]
\includegraphics[width=0.5\columnwidth]{gamma_c.eps}
\includegraphics[width=0.52\columnwidth]{DsvsT.eps}
\caption{Left: Charm-quark friction coefficient, $A=\gamma_c$, as a function of 3-momentum
in the QGP from nonpertubative $T$-matrix scattering amplitudes off thermal light and strange 
quarks~\cite{Riek:2010fk}, as well as gluons~\cite{Huggins:2012dj}; the curves correspond to 
temperatures $T$=1.2, 1.5 and 2\,$T_c$ (bottom to top); figure taken from 
Ref.~\cite{Huggins:2012dj}. Right: Spatial heavy-flavor diffusion coefficient (defined via the 
relaxation rate at zero momentum) for the $T$-matrix aproach in the QGP using the $U$ (lower 
red band) or $F$ potential (upper green band)~\cite{Riek:2010fk}, or pQCD with $\alpha_s$=0.4 
(dash-dotted line), in hadronic matter (dashed line)~\cite{He:2011yi}, and from quenched 
lattice QCD~\cite{Ding:2011hr,Banerjee:2011ra} (data points); figure taken from 
Ref.~\cite{He:2012df}.}
\label{fig_c-tmat}
\end{figure}

After coalescence into open-charm mesons, our approach also accounts for the diffusion of 
heavy-flavor mesons in the hadronic phase. Pertinent transport coefficients have been worked 
out in Ref.~\cite{He:2011yi}, based on effective $D$-meson scattering amplitudes off light 
hadrons as available from the literature. These include $\pi$, $K$, $\eta$, $\rho$, $\omega$, 
as well as nucleons and $\Delta$(1232)'s and their anti-particles. The combined effect of 
these scatterings is appreciable, leading to a diffusion coefficient comparable to the 
$T$-matrix calculations in the QGP close to $T_{\rm c}$. As first pointed out in 
Refs.~\cite{He:2011yi,He:2012df}, this suggests a minimum of the ($T$-scaled) 
heavy-flavor diffusion coefficient via a smooth transition through $T_{\rm pc}$, as to 
be expected for a cross-over transition, see right panel of Fig.~\ref{fig_c-tmat}.

\bibliographystyle{ieeetr}
\bibliography{biblio}

\end{document}
