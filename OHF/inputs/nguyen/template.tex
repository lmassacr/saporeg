%\documentclass{appolb}
\documentclass{article}
%\usepackage[margin=0.3in]{geometry}

\usepackage[margin=2.3cm]{geometry}
\textheight=23cm
\textwidth=17cm
%\hoffset=-2cm
%\voffset=-1cm


%\usepackage[dvips]{graphicx} 
%\usepackage{graphics}
\usepackage{graphicx}
 \usepackage{cite}
%\DeclareGraphicsExtensions{.eps,.ps,.pdf,.png,.jpeg}
% epsfig package included for placing EPS figures in the text
%------------------------------------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                %
%    BEGINNING OF TEXT                           %
%                                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
% \eqsec  % uncomment this line to get equations numbered by (sec.num)
\title{B meson measurements and b-tagged jets}

\date{}

\author{Matthew Nguyen} 


\maketitle

% Many signatures of supersymmetry: Bs and 3rd generation squarks

Due to the relatively large mass of the bottom quark, measurements of open beauty production permit the study of the mass dependence of parton energy loss through comparison to light and charm hadron measurements.  Open beauty also serves as a reference for measurements of the bottomonia states, for which, in contrast to the charmonia states, $q\bar{q}$ regeneration is not expected to play a role even at LHC energies.  Fig.~\ref{bottomXsec} shows the evolution of the total $b\bar{b}$ cross-section (per unit rapidity) from SPS to LHC energies.  At 7 TeV, the total $b\bar{b}$ cross section, measured by LHCb to be $284 \pm 20 \pm 49 \mu$b~\cite{Aaij:2010gn} (compatible with a measurement by ALICE~\cite{Abelev:2012gx}), is comparable to the total charm cross section at RHIC.  This large production rate enables precision measurement of various aspects of bottom quark production, and indeed the four large LHC experiments were designed to take advantage of this.  Bottom quark measurements are an essential component for many aspects of the LHC program, such as the search for 3rd generation supersymmetric partners. The recent addition of silicon vertex trackers to STAR and PHENIX similarly provide the capacity for open beauty measurements.

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.48\textwidth]{fig7_radcorr.pdf}
\caption[Compilation of total bottom cross section measurements]{\label{bottomXsec}
A compilation of $b\bar{b}$ cross section measurements in $pp$ and $p\bar{p}$ collisions, plotted vs. $\sqrt{s}$~\cite{Abelev:2012gx}.  Measurements from ALICE, UA1~\cite{Albajar:1990zu}, CDF~\cite{Abe:1995dv} and PHENIX~\cite{Adare:2009ic} are shown, and compared to FONLL calculations~\cite{Cacciari:2003uh, Cacciari:2012ny}.
}
\end{center}
\end{figure}

The detection and identification of $b$ hadrons usually exploits their long life times, with $c\tau$ values of about 500 $\mu m$.   Precise charged particle tracking and vertexing are of tantamount importance, with the required resolution of the track impact parameter in the transverse plane being on the order of 100 $\mu$m. Most decay channels proceed as a $b$ $\rightarrow$ $c$ hadron cascade, giving rise to a topology which contains both a secondary and a tertiary decay vertex.  Lepton identification is often exploited in bottom measurements, as the semi-leptonic branching ratio is about 20\%, taking into account both decay vertices.  A counterexample to the cascading decay topology is the $J/\psi + X$ decay mode.  Such decays can be measured inclusively by decomposing the $J/\psi$ yield into its prompt and non-prompt components, using a lifetime fit.  Measurements of non-prompt $J/\psi$ in $pp$
collisions are available from all four LHC experiments~\cite{Khachatryan:2010yr, Aad:2011sp, Abelev:2012gx, Aaij:2011jh}.
%These measurements have been performed at mid-rapidity in the dielectron channel by ALICE and in the dimuon channel by CMS~\cite{Khachatryan:2010yr} and ATLAS~\cite{Aad:2011sp}.  
Figure~\ref{bottomPPmeas} shows the $p_T$ dependence of the double differential cross-section from CMS. 
For $p_T$ on the order of the $b$-quark mass NLO and resummation effects become important such that predictions from FONLL~\cite{Cacciari:2003uh, Cacciari:2012ny} provide a good description of the data, whereas those from PYTHIA do not.  %The fraction of $J/\psi$ from feed-down of b-hadron decays was found to grow approximately logarithmically with $p_T$ from about 10\% at 2 GeV/c to more than 60\% above 30 GeV/c.  
%LHCb has performed the same measurement in the dimuon channel at forward rapidity ($2 < y < 5$) and shown that the $b$-fraction decreases with rapidity.  
Exclusive measurements have also been performed at the LHC in the $B^{+/-} \rightarrow J/\psi + K^{+/-}$, $B^{0} \rightarrow J/\psi + K^*(892)$ and $B^0_s \rightarrow J/\psi + \phi$ channels~\cite{ATLAS:2013cia,Aaij:2013noa,Aaij:2012jd,Khachatryan:2011mk,Chatrchyan:2011pw,Chatrchyan:2011vh}.  

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.48\textwidth]{d2sigdptdy_nonprompt_16y24_inone_new.pdf}
\includegraphics[width=0.48\textwidth]{fig_05a.pdf}
\caption[stuff]{\label{bottomPPmeas}
Left:  The double differential cross-section of non-prompt $J/\psi$ as a function of $p_T$ from CMS~\cite{Khachatryan:2010yr}.  Right:  The ratio of the b-jet cross-section from ATLAS~\cite{ATLAS:2011ac} to theoretical predictions using POWHEG~\cite{Alioli:2010xd}.
}
\end{center}
\end{figure}

For many cases, such as the reconstruction of the Higgs boson via its $b\bar{b}$ decay, it is desirable to measure the entire energy of the $b$-quark, using reconstructed jets as a proxy.  The fragmentation of the $b$ quark is relatively hard compared to that of lighter flavors, with the b-hadron taking about 70\% of the parton momentum on average~\cite{DELPHI:2011aa}.  For the typical jet $p_T$ of about 100 GeV/c at the LHC, this gives rise to a secondary vertex displacement of several mm.  Identification of jets from bottom quark fragmentation or ``b-tagging'' can be achieved by direct reconstruction of displaced vertices, although the efficiency for doing so is limited.  Higher efficiency can be achieved by looking for large impact parameter tracks inside jets, or by a combination of the two methods, which are collectively known as lifetime tagging.  Leptons inside jets can be also be used for b-tagging, but, due to the branching fraction, are usually only used as a calibration of the lifetime methods.  A review of b-tagging methodologies is available from CMS~\cite{Chatrchyan:2012jua}.
Both ATLAS and CMS have performed measurements of the b-jet cross section~\cite{ATLAS:2011ac, Chatrchyan:2012dk}.  Theoretical comparisons can be made to models which calculate fully exclusive final states, which can be achieved by matching NLO calculations to parton showers~\cite{Nason:2004rx}. Figure~\ref{bottomPPmeas} shows ratio of the b-jet cross section measurement by ATLAS, in several bins of rapidity, to calculations from POWHEG~\cite{Alioli:2010xd} (matched to PYTHIA), which is found to reproduce the data.  Measurements from both lifetime- and lepton-based tagging methods are shown.

Run 1 of the LHC has provided the first direct bottom measurements in heavy ions at collider energies.  From the 2010 dataset, CMS measured non-prompt  $J/\psi$ for $6.5 < p_T < 30$ GeV/c in $|y| < 2.4$.  The $R_{AA}$ in the 20\% most central collisions was measured to be $0.37 \pm 0.08 {\rm(stat.)} \pm 0.02 {\rm(syst.)}$.  Preliminary measurements from the larger 2011 dataset explore the $p_T$ dependence of $R_{AA}$~\cite{CMS:2012wba}.  The $R_{AA}$ values for non-prompt $J/\psi$ tend to be larger than those measured for D mesons by ALICE~\cite{ALICE:2012ab}.  This observation cannot, however, be directly interpreted as a difference in bottom and charm quark energy loss.  A direct comparison of B and D mesons requires taking the $B \rightarrow J/\psi$ kinematics into account.   Since $R_{AA}$ decreases with $p_T$, the B meson $R_{AA}$ is actually larger than that of non-prompt $J\psi$ at the same $p_T$, which would accentuate the difference in D and B suppression factors.  The effect of decay kinematics can be removed by exclusive B meson measurements (preliminary measurements in pPb from CMS are already available~\cite{HIN14004}), however, this is not the only important effect.  The shape of the parent quark $p_T$ spectrum is significantly different for D and B mesons, as the spectrum turns over at a value of $p_T$ which is determined by the mass (this can be seen in the left panel of Fig.~\ref{bottomPPmeas}).  This effect, as well as the somewhat harder fragmentation of $b$ quarks than $c$ quarks, will tend to produce a difference in the observed $R_{AA}$, independent of the parton energy loss.  Realistic model calculations for both $b$ and $c$ quarks are required to determine to what extent the observed difference in $R_{AA}$ corresponds to a flavor dependence of their parton energy loss.  Measurements in pPb collisions, which allow to constrain cold nuclear effects, have been carried out by LHCb~\cite{Aaij:2013zxa}.


\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.47\textwidth]{NonPromptJpsi_RAA.pdf}
\includegraphics[width=0.5\textwidth]{raaVsCent.pdf}
\caption[Non-prompt and b-jet $R_{AA}$ from PbPb collisions]{\label{fig:bottomPbPbmeas}
Left:  Non-prompt $J/\psi$ $R_{AA}$ measured in two centrality bins from CMS~\cite{Chatrchyan:2012np}.  The uncertainty of 6\% on the $pp$ luminosity is shown as a grey box.
Right:  $R_{AA}$ of $b$ jets, as a function of $N_{\rm part}$ from CMS~\cite{Chatrchyan:2013exa}, for two jet $p_T$ selections as indicated in the legend.  Systematic uncertainties are shown as filled boxes, except the  $T_{AA}$ uncertainties, depicted as open boxes. The luminosity uncertainty is represented by the green band.
}
\end{center}
\end{figure}

With respect to $b$ hadrons, measurements of fully reconstructed $b$ jets provide a complimentary handle on parton energy loss, %(albeit at typically larger values of $p_T$)
 as the reconstructed jet energy is closely related to that of the $b$ quark.  Assuming that the quark hadronizes outside the medium, to first approximation the jet energy represents the sum of the parton energy after its interaction with the medium, as well as any transferred energy which remains inside the jet cone.  CMS has performed a measurement of $b$ jets in PbPb collisions by direct reconstruction of displaced vertices associated to the jets~\cite{Chatrchyan:2012np}.  Despite the large underlying PbPb event, a light jet rejection factor of about 100:1 can still be achieved in central PbPb events.  The $R_{AA}$ of b jets as function of the number of participants is shown in Fig.~\ref{fig:bottomPbPbmeas} (right), for two different ranges of jet $p_T$.  The observed suppression, which reaches a value of about 0.4 in central collisions, does not show any significant difference compared to a similar measurement of the inclusive jet $R_{AA}$~\cite{CMS:2012rba}, to within the sizable systematic uncertainties.  While quark mass effects may not play a role at such large values of $p_T$, the difference in energy loss between quarks and gluons should manifest itself as a difference in $R_{AA}$ for $b$ jets and inclusive jets, as the latter is dominated by gluon jets up to very large $p_T$.  It should be noted, however, that not all jets associated to a $b$-hadron correspond to primary $b$ quarks.  At LHC energies, a significant component of $b$ jets are produced by splitting of gluons into $b$-$\bar{b}$ pairs~\cite{Banfi:2007gu}.  
 %This mechanism turns out to be numerically dominant over the LO production mode at LHC energies due to the large abundance of gluons.
   For splittings which occur at low virtuality, most of the medium path-length is likely to be traversed by the parent gluon, as opposed to the $b$ quarks.  The implications of heavy flavor production via gluon splitting on measurements of jets, as well as on those of hadrons, needs to be carefully considered in energy loss models.  The gluon splitting contribution can be minimized by selecting hard fragments, although this is complicated by the fact that the $b$-hadron kinematics are not fully measured.  An alternative is to select back-to-back b-tagged jets, a configuration in which the gluon splitting contribution is negligible.  The dijet asymmetry of $b$ jets can then be compared to that of inclusive jets, a measurement which should be  feasible with the luminosity expected from the upcoming LHC Run 2. 
 







%\begin{figure}[htb]
%\begin{center}
%\includegraphics[width=0.48\textwidth]{bFraction_pt.pdf}
%\caption[Non-prompt $J/\psi$ $R_{AA}$ and $J//psi$ b-fraction]{\label{fig:NonPromptJpsi}
%Non-prompt $J//\psi$ $R_{AA}$ in two bins of centrality from the CMS Collaboration. 
%}
%\end{center}
%\end{figure}




\clearpage

\bibliographystyle{ieeetr}
\bibliography{biblio}

\end{document}