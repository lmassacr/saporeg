%!TEX root = ../SaporeGravis.tex

The LHC \RunOne has provided a wealth of measurements of heavy-flavour production in heavy-ion collisions,
which have extended and complemented the results from the RHIC programme. The main observations and 
their present interpretation are summarized in the following.

{\it High-$\pt$ region} (above 5--10~GeV/$c$): in this region, heavy-flavour measurements are expected to provide information 
mainly on the 
properties of in-medium energy loss.
\begin{itemize}
\item The $\raa$ measurements show a strong suppression with respect to binary scaling in central nucleus--nucleus collisions for D mesons, heavy-flavour decay leptons and J$/\psi$ from B decays.
The suppression of D mesons and heavy-flavour decay leptons is similar, within uncertainties, at RHIC and LHC energies.
Given that a suppression is not observed in proton(deuteron)--nucleus collisions, the effect in nucleus--nucleus collisions can be attributed to in-medium energy loss.
\item The suppression of D mesons with average $\pt$ of about 10~GeV/$c$ is stronger than that of J/$\psi$ decaying from B mesons with similar average $\pt$. This observation, still based on preliminary results, is consistent with the expectation of lower energy loss for heavier quarks and it is described by model calculations that implement radiative and collisional energy loss with this feature.
\item The suppression of D mesons and pions is consistent within uncertainties at both RHIC and LHC. While there is no experimental evidence of the colour-charge dependence of energy loss, model calculations indicate that similar $\raa$ values can result from the combined effect of colour-charge dependent energy loss and the softer $\pt$ distribution and fragmentation function of gluons with respect to $c$ quarks.
\item At very high-$\pt$ (above 100~GeV/$c$), a similar $\raa$ is observed for $b$-tagged jets and inclusive jets. This observation is consistent with a negligible effect of the heavy quark mass at these scales. 
\end{itemize}

{\it Low-$\pt$ region} (below 5--10~GeV/$c$): in this region, heavy-flavour measurements 
are expected to provide information on the total production yields (and the role of initial-state effects)
and on heavy-quark in-medium dynamics (participation to collective expansion, in-medium hadronisation effects).
\begin{itemize}
\item The measurements of electrons (in particular) and D mesons at RHIC show that the total production of charm quarks
is consistent with binary scaling within uncertainties of about 30--40\%. The available measurements at LHC do not extend 
to sufficiently-small $\pt$ to provide an estimate of the total yields.
\item The D meson $\raa$ at RHIC energy shows a pronounced maximum at $\pt$ of about 1--2~GeV/$c$ (where $\raa$ becomes larger than unity). This feature is not observed in the measurements at LHC energy. Model calculations including collisional (elastic) interaction processes in an expanding medium and a contribution of hadronization via in-medium quark recombination, as well as initial-state 
gluon shadowing, describe qualitatively the behaviour observed at both energies. In these models the bump at RHIC is due to radial flow 
and the effect on $\raa$ at LHC is strongly reduced because of the harder $\pt$ distributions and of the effect of gluon shadowing.
\item A positive elliptic flow $v_2$ is measured in non-central collisions for D mesons at LHC and heavy-flavour decay leptons 
at RHIC and LHC. The D meson $v_2$ at LHC is comparable to that of light-flavour hadrons (within uncertainties of about 30\%).
These measurements indicate that the interaction with the medium constituents transfers information about the azimuthal anisotropy of the system to charm quarks.
The $v_2$ measurements are best described by the models that include collisional interactions within a 
fluid-dynamical expanding medium, as well as hadronisation via recombination.
\end{itemize}

The main open questions in light of these observations are:
\begin{itemize}
\item Does the total charm and beauty production follow binary scaling or is there a significant gluon shadowing effect? This requires a precise measurement of charm and beauty production down to zero $\pt$, in proton--proton, proton--nucleus and nucleus--nucleus collisions.
\item Can there be an experimental evidence of the colour-charge dependence of energy loss? This requires a precise comparison of D mesons and pions in the intermediate $\pt$ region, at both RHIC and LHC.
\item Is the difference in the nuclear modification factor of charm and beauty hadrons consistent with the 
quark mass dependent mechanisms of energy loss? Can it provide further insight on these mechanisms (for example, the gluon
radiation angular distribution)? This requires a precise measurement of D and B meson (or J/$\psi$ from B) $\raa$ over a wide $\pt$ range and as a function of collision centrality. This will also be mandatory in order to extract the precise path-length
dependence of energy loss, which cannot be extracted from the actual data.
\item Does the positive elliptic flow observed for D mesons and heavy-flavour decay leptons result from the charm quark interactions
in the expanding medium? Are charm quarks thermalised in the medium? Is there a contribution (dominant?) inherited from light quarks
via the recombination process? What is the contribution from the path length dependence of energy loss? This requires precise measurements of the elliptic flow and of the higher order flow coefficients of charm and beauty hadrons over a wide $\pt$ interval, and their comparison with light-flavour hadrons.
\item What is the role of in-medium hadronisation and of radial flow for heavy quarks? This requires measurements of $\raa$ and $v_2$
of heavy flavour hadrons with different quark composition and different masses, namely D, ${\rm D}_s$, B, ${\rm B}_s$, $\Lambda_c$, $\Xi_c$, 
$\Lambda_b$.     
\item What is the relevance of radiative and collisional processes in heavy quark energy loss? What is the path length dependence of the two types of processes? This requires precise simultaneous measurements of the $\raa$ and $v_2$ and their comparison with model calculations. Heavy-quark correlations are also regarded as a promising tool 
in this context. 
\end{itemize}
The outlook for addressing these open questions with the future experimental programmes at RHIC and LHC is discussed in \sect{sec:upgrade}.

From the theoretical point of view,
a wide range of models, also with somewhat different ``ingredients", can describe most of the available data, at least qualitatively. The main challenges in the theory sector is thus to connect the data with the fundamental properties of the QGP and of the theory of the strong interaction. For this purpose, it is important to identify the features of the  quark--medium interaction that are needed for an optimal description of all aspects of the data and to reach a uniform treatment of the ``external inputs" in the models (e.g. using state-of-the-art pQCD baseline, fragmentation functions and fluid-dynamical medium description, and fixing transport coefficients on those that will be ultimately obtained from lattice calculations for finite \pt).
