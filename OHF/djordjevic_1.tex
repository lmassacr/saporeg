% !TEX root = ../SaporeGravis.tex

The radiative processes, which are neglected in the model described above, are taken into account in other approaches.
%Jet quenching~\cite{Bjorken:1982tu} is a powerful tool to explore the QCD medium created in heavy-ion collisions at RHIC and LHC~\cite{Brambilla:2004wf,Gyulassy:2001zv,d'Enterria:2010zz} and mapping the medium properties requires a systematic comparison of experimental measurements with jet quenching predictions. %Since one of the most important ingredients in jet suppression is jet energy loss, it is evident that reliable predictions of jet suppression require reliable calculations of jet energy loss in QCD medium. 
%With this goal in mind, 
Djordjevic \etal developed a state-of-the-art dynamical energy loss formalism, which \textit{i)} is applicable for both light and heavy partons, \textit{ii)} computes both radiative~\cite{Djordjevic:2009cr,Djordjevic:2008iz} and collisional~\cite{Djordjevic:2006tw} energy loss in the \emph{same} theoretical framework, \textit{iii)} takes into account recoil of the medium constituents, \ie the fact that medium partons are moving (\ie dynamical) particles,  \textit{iv)} includes realistic finite size effects, \ie the fact that the partons are produced inside the medium and that the medium has finite size. Recently, the formalism was also extended to include \textit{v)} finite magnetic mass effects~\cite{Djordjevic:2011dd}  and \textit{vi)} running coupling (momentum dependence of $\alpha_s$)~\cite{Djordjevic:2013xoa}.

% {\bf Energy loss framework.}
In this formalism, radiative and collisional energy losses are calculated for an optically thin dilute QCD medium. Consequently, both collisional and radiative energy losses are computed to the leading order. That is, for collisional energy loss, the loss is calculated for one collisional interaction with the medium, while for radiative energy loss, the loss is calculated for one interaction with the medium accompanied by the emission of one gluon. 
%To introduce the finite size of the medium one starts from the approach 
%described in \cite{Zakharov:2002ik} and follows the procedure used in 
%\cite{Djordjevic:2006tw}. That is, the medium is assumed to extend for a length $L$ from the jet production point, and the collisional interaction occurs  after a distance $l<L$, inside the medium.     
The medium is described as a thermalised QGP~\cite{Kapusta:2006pm,bellac2000thermal} at temperature $T$ and zero baryon density, with $n_f$ effective 
massless quark flavours in equilibrium with the gluons. The Feynman diagrams contributing to the collisional and  the radiative quark energy loss are presented in \cis{Djordjevic:2006tw,Djordjevic:2009cr}. A 
full account of the calculation is presented in \ci{Djordjevic:2006tw} for collisional energy loss, and in~\ci{Djordjevic:2009cr} for radiative energy loss. Since the expression for collisional energy loss is lengthy, it will not be presented here, while the expression for radiative energy loss is given by 
\begin{equation}
\frac{\Delta E_{\mathrm{dyn}}}{E}=\int \dd x \dd^2k\, x \frac{\dd^3N^g}{\dd x \dd^2k}  
\end{equation}
with the radiation spectrum 
\begin{eqnarray}
\frac{\dd^3N^g}{\dd x \dd^2k}  
= \frac{C_R \alpha_s}{\pi}\,\frac{L}{\lambda_\mathrm{dyn}}  
    \int \frac{\dd^2q}{x \pi^2} \, v_\mathrm{dyn}(\bq)
\,\left(1-\frac{\sin{\left(\frac{(\bk{+}\bq)^2+\chi}{x E^+} \, L\right)}} 
    {\frac{(\bk{+}\bq)^2+\chi}{x E^+}\, L}\right) 
    \frac{2(\bk{+}\bq)}{(\bk{+}\bq)^2{+}\chi}
    \left(\frac{(\bk{+}\bq)}{(\bk{+}\bq)^2{+}\chi}
    - \frac{\bk}{\bk^2{+}\chi}
    \right),
    \label{DeltaEDyn}
\end{eqnarray}
where $\bq$ and $\bk$ are respectively the momentum of the radiated gluon and the momentum of the 
exchanged virtual gluon with a parton in the medium, with both $\bq$ and $\bk$ transverse to the jet direction.
Here $\lambda_\mathrm{dyn}^{-1} \equiv C_2(G) \alpha_s T$  -- where $C_2(G)=3$ is the gluon quadratic Casimir invariant --  defines the ``dynamical mean free path''~\cite{Djordjevic:2008iz}, $\alpha_s$ is the strong coupling constant, and $C_R=4/3$ is the Casimir factor. Further, $v_\mathrm{dyn}(\bq)=\frac{\mu_E^2}{\bq^2 (\bq^2{+}\mu_E^2)}$ is the effective potential. 
$\chi$ is defined as $m_Q^2 x^2 + m_g^2$, where $m_Q$ is the heavy-quark mass, $x$ is the longitudinal momentum 
fraction of the heavy quark carried away by the emitted gluon and 
$m_g=\frac{\mu_E}{\sqrt{2}}$ is the effective mass for gluons with 
hard momenta $k\gtrsim T$ and $\mu_E$ is the Debye mass. It can be noted that the $C_R$ term encodes the colour charge dependence of energy loss (for radiative energy loss
off a gluon $C_R$ is 3 instead of $4/3$).
The $\chi$ term encodes the quark mass dependence of energy loss, which is reduced for increasing values of $m_Q/(\bk+\bq)$.

% {\bf Dynamical \textit{vs.} static energy loss.}
Note that this dynamical energy loss presents an extension of the well-known static DGLV~\cite{Gyulassy:2000er,Djordjevic:2003zk} energy loss formalism to the dynamical QCD medium. 
The connection between dynamical and static energy losses was discussed in~\cis{Djordjevic:2009cr,Djordjevic:2008iz}. That is, static energy loss can be obtained from the above dynamical energy loss expression by replacing the dynamical mean-free path and effective potential  by equivalent expressions for a static QCD medium: 
$v_\mathrm{dyn}(\bq)\rightarrow v_\mathrm{stat}(\bq)=\frac{\mu_E^2}{(\bq^2{+}\mu_E^2)^2}$
and $\lambda_\mathrm{dyn}^{-1}\rightarrow \lambda_\mathrm{stat}^{-1}= 6 \frac{1.202}{\pi^2} \frac{1{+}\frac{n_f}{4}}{1{+}\frac{n_f}{6}} \lambda_\mathrm{dyn}^{-1}$.
%, \ie $\lambda_\mathrm{dyn}$ should be replaced by $\lambda_\mathrm{stat}=6 \frac{1.202}{\pi^2} \frac{1{+}\frac{n_f}{4}}{1{+}\frac{n_f}{6}} \lambda_\mathrm{dyn}$, while  $v_\mathrm{dyn}(\bq)$ should be replaced by $v_\mathrm{stat}(\bq)=\frac{\mu_E^2}{(\bq^2{+}\mu_E^2)^2}$.
Note that the static DGLV formalism was also used in the WHDG model~\cite{Wicks:2005gt,Wicks:2007am},
as well as for the quark energy loss calculation by  Vitev \etal (see \sect{sec:sharmaVitev_dissociation}).


%{\bf Finite magnetic mass.}
The dynamical energy loss formalism was further extended to the case of finite magnetic mass, since various non-perturbative approaches suggest a non-zero magnetic mass at RHIC and LHC collision energies (see \eg~\cis{Maezawa:2010vj,Maezawa:2008kh,Nakamura:2003pu,Hart:2000ha,Bak:2007fk}). The finite magnetic mass is introduced through generalised sum-rules~\cite{Djordjevic:2011dd}. 
The main effect of the inclusion of finite magnetic mass turns out to be the modification of effective cross section $v_\mathrm{dyn}(\bq)$ in \eq{DeltaEDyn} to $v(\bq)=\frac{\mu_E^2-\mu_M^2}{(\bq^2+\mu_M^2) (\bq^2{+}\mu_E^2)}$, where $\mu_M$ is the magnetic mass.
In \fig{fig:eloss_djordjevic}, the fractional energy loss $\frac{\Delta E}{E}$ corresponding to the 
full model described above is shown, for a path length $L=5~{\rm fm}$ and an effective constant temperature of $T=304\MeV$.
For charm quarks, radiative energy loss starts to dominate for $\pt> 10$\GeVc,
while this transition happens for $\pt>25$\GeVc for beauty quark.
The comparison of radiative energy loss for the two quark species clearly illustrates the dead cone effect, as well as its disappearance when $\pt\gg m_Q$.
\begin{figure}[tb]
\centering
\includegraphics[width=0.48\textwidth]{eloss_djordjevic}
\caption{Fractional energy loss (\eq{DeltaEDyn}) evaluated for collisional and radiative processes and 
for charm and beauty quarks, at $T=304~{\rm MeV}$.}
\label{fig:eloss_djordjevic}
\end{figure}


In \cite{Huang:2013vaa}, a calculation for the $b$-jet production in \AAcoll was performed following 
very similar ingredients for the energy loss. The medium-induced gluon spectrum in the soft gluon approximation was evaluated as in~\cite{Vitev:2007ve} in a medium which incorporates Glauber geometry and Bjorken expansion. With the QGP-induced distribution of gluons $\frac{\dd^3 N^g}{\dd x \dd^2k}$ -- of the type of \eq{DeltaEDyn} -- and the related $\frac{\dd^2 N^g}{\dd \omega \dd r}$ ($\omega$ is the energy and $r$ is the angle) of gluons at hand, the fraction $f$ of the in-medium  parton 
shower energy that is contained in the jet cone of radius $R$ was evaluated as:  
\begin{equation}
 f(R,\omega^{\rm coll})_{(s)}= \frac{\int_0^{R} \dd r
\int_{\omega^{\rm coll}}^E  \dd\omega \,
\frac{ \omega \dd^2N^{g}_{(s)}} {\dd\omega  \dd r }}
{\int_0^{{R}^{\infty}} \dd r \int_{0}^E  \dd\omega \,
\frac{ \omega \dd^2N^{g}_{(s)}}{\dd \omega \dd r} } \; .
\label{fractionbjet}
\end{equation}
In \eq{fractionbjet} $f(R,0)_{(s)} $ takes into account  medium-induced 
parton splitting effects. On the other hand $f(R^\infty, \omega^{\rm coll})_{(s)}  
= \Delta E^{\rm coll} / E $ is the energy dissipated by the medium-induced 
parton shower into the QGP due to collisional processes.  
$\Delta E^{\rm coll}$ is evaluated as in Ref.~\cite{Neufeld:2011yh,Neufeld:2014yaa} and helps to 
solve for $\omega^{\rm coll}$. Then, for any $R$, \eq{fractionbjet} allows to treat the radiative and collisional energy loss effects on the same footing.  
Writing down explicitly the phase space Jacobian
$|J(\epsilon)|_{(s)} = 1/\left(1 - [1-f(R,\omega^{\rm coll})_{(s)}]  \epsilon \right)$ for the case of $b$-jets the cross section
per elementary nucleon-nucleon collision writes: 
\begin{equation}
\frac{1}{\langle  N_{\rm bin}  \rangle}
 \frac{\dd^2 \sigma_{AA}^{b-{\rm jet}}(R)}{\dd y \dd\pt} =\sum_{(s)}
\int_{0}^1  \! \dd\epsilon \;  
\frac{ P_{(s)}(\epsilon) }{  \left(1 - [1-f(R,\omega^{\rm coll})_{(s)}]  \epsilon \right)  } 
\frac{\dd^2\sigma^{\rm CNM,LO+PS}_{(s)} \left(|J(\epsilon)|_{(s)} \pt \right)} {\dd y \dd\pt}.
\label{incl}
\end{equation}
Here, the sum runs over the set of final states (s). 
$\dd^2\sigma^{\rm CNM,LO+PS} / {\dd y \dd\pt} $ includes cold nuclear matter effects. 

The same group recently published predictions for photon-tagged and B-meson-tagged $b$-jet production at \linebreak LHC~\cite{Huang:2015mva}.
