% !TEX root = ../SaporeGravis.tex



% \title{$T$-Matrix Approach to Heavy-Quark Interactions in the QGP}
The thermodynamic $T$-matrix approach is a first-principles framework to 
self-consistently compute one- and two-body correlations in hot and dense matter. 
It has been widely applied to, \eg, electromagnetic plasmas~\cite{Redmer:1997}
and the nuclear many-body problem~\cite{Jeukenne:1976uy,Brockmann:1990cn}. Its 
main assumption is that the basic two-body interaction can be cast into the form 
of a potential, $V(t)$, with the 4-momentum transfer approximated as  
$t=q^2=q_0^2-\vec q^{\,2}\simeq -\vec q^{\,2}$. This relation is satisfied for charm and 
beauty quarks ($Q = c,b$) in a QGP up to temperatures of 
2-3\,$T_{\rm c}$, since their large masses imply 
$q_0^2\simeq (\vec q^{\,2}/2m_Q)^2\ll \vec q^{\,2}$ with typical momentum transfers 
of $\vec q^{\, 2} \sim T^2$. Therefore, the $T$-matrix formalism is a promising framework 
to treat the non-perturbative physics of heavy-quark (HQ) interactions in the 
QGP~\cite{Mannarelli:2005pz,Cabrera:2006wh,Riek:2010fk}. 
It can be applied to both hidden and open heavy-flavour states, and provides
a comprehensive treatment of bound and scattering states~\cite{Riek:2010fk}. It 
can be systematically constrained by lattice data~\cite{Riek:2010py}, and 
implemented to calculate heavy-flavour observables in heavy-ion 
collisions~\cite{vanHees:2007me,He:2011qa}.      
 
The potential approximation allows to reduce the 4-dimensional Bethe-Salpeter 
into a 3-dimensional Lipp\-mann-Schwinger equation, schematically given by 
\begin{equation}
T(s,t) = V(t) + \int \dd^3k  \ V(t') \ G_2(s,k) \ T(s,t'') \   , 
\end{equation}
where $G_2$ denotes the in-medium 2-particle propagator. Using the well-known
Cornell potential in vacuum~\cite{Eichten:1978tg}, heavy quarkonium spectroscopy and heavy-light meson
masses can be reproduced, while relativistic corrections (magnetic interactions)
allow to recover perturbative results in the high-energy limit for HQ 
scattering~\cite{Riek:2010fk}. 

The pertinent transport coefficients for a heavy quark of momentum $\vec p$ are 
given by  
\begin{equation}
A_Q(p)  =  \frac{1}{2\omega_Q(p)(2\pi)^9} \sum\limits_{j=q,\bar{q},g} 
\int \frac{\dd^3k}{2\omega_k}  \frac{\dd^3k'}{2\omega_{k'}} 
\frac{\dd^3p'}{2\omega_Q(p')}  f^j(\omega_k) \ \delta^{(4)}(P_i-P_f) \ 
|{\cal M}_{Qj}(s,t)|^2 \left( 1-\frac{\vec p \cdot \vec p\,'}{\vec p^{\,2}} \right)
\end{equation} 
for the friction coefficient (or relaxation rate) and analogous expressions for momentum diffusion~\cite{Rapp:2009my}. 
The invariant HQ-parton scattering amplitude, ${\cal M}_{Qj}$, is directly proportional to 
the $T$-matrix. An important ingredient is how the HQ potential $V$ is modified in medium. 
This is currently an open question. As limiting cases the HQ free ($F$) and internal ($U$) 
energies computed in lattice-QCD (l-QCD) have been employed~\cite{Kaczmarek:2007pb}. The 
internal energy produces a markedly stronger interaction, and, when employed in the 
$T$-matrix approach, generally leads to better agreement with other quantities computed on
the lattice (\eg, quarkonium correlators, HQ susceptibility, etc.~\cite{Riek:2010py}). The 
resulting \cquark-quark relaxation rates, including scattering off thermal \uquark, \dquark, \squark quarks 
and gluons, are enhanced over their perturbatively calculated counterparts by up 
to a factor of $\sim$6 at low momenta and temperatures close to $T_{\rm c}$, cf.~left panel 
of \fig{fig_A}. A similar enhancement is found for \bquark quarks, although the 
absolute magnitude of the relaxation rate is smaller than for \cquark quarks by about a 
factor of $m_b/m_c\simeq3$, cf.~right panel of \fig{fig_A}. 
The non-perturbative enhancement is mostly caused by resonant D/B-meson and 
di-quark states which emerge in the colour-singlet and anti-triplet channels as $T_{\rm c}$ is 
approached from above. These states naturally provide for HQ coalescence processes 
in the hadronisation transition, \ie, the same interaction that drives non-perturbative 
diffusion also induces hadron formation. The resummations in the $T$-matrix, together with 
the confining interaction in the potential, play a critical role in this framework. At 
high momenta, both confining and resummation effects become much weaker and the diffusion 
coefficients approach the perturbative (colour-Coulomb) results, although at $p\simeq5$~GeV, 
the enhancement is still about a factor of 2 for charm quarks. With increasing temperature, the 
colour screening of the l-QCD-based interaction potentials leads to an increase in the 
(temperature-scaled) spatial diffusion coefficient, $D_s(2\pi T) = 2\pi T^2/(m_Q\,A_Q)$, 
see \fig{fig_Ds}.

\begin{figure}[t]
\includegraphics[width=0.5\columnwidth]{gamma_c}
\includegraphics[width=0.5\columnwidth]{gamma_b}
\caption{Left: Charm-quark friction coefficient, $A_c$, as a function of momentum
in the QGP from non-pertubative $T$-matrix scattering amplitudes off thermal light and strange 
quarks~\cite{Riek:2010fk}, as well as gluons~\cite{Huggins:2012dj}; the curves correspond to 
temperatures $T$=1.2, 1.5 and 2\,$T_{\rm c}$ (bottom to top); 
Right: Same as left panel but for bottom quarks.
Figures are taken from~\ci{Huggins:2012dj}.
}
\label{fig_A}
\end{figure}

After coalescence into open-charm mesons, the approach also accounts for the diffusion of 
heavy-flavour mesons in the hadronic phase. Pertinent transport coefficients have been worked 
out in~\cite{He:2011yi}, based on effective D-meson scattering amplitudes off light 
hadrons as available from the literature. These include $\pi$, K, $\eta$, $\rho$, $\omega$, 
as well as nucleons and $\Delta$(1232) and their anti-particles. The combined effect of 
these scatterings is appreciable, leading to a hadronic diffusion coefficient comparable 
to the $T$-matrix calculations in the QGP close to $T_{\rm c}$. As first pointed out in~\cite{He:2011yi,He:2012df}, this suggests a minimum of the ($T$-scaled) heavy-flavour 
diffusion coefficient via a smooth transition through the pseudo-critical region, as to 
be expected for a cross-over transition  (see \fig{fig_Ds}).

\begin{figure}
\begin{center}
\includegraphics[width=0.7\columnwidth]{DsvsT}
\end{center}
\vspace{-0.5cm}
\caption{
Spatial heavy-flavour diffusion coefficient (defined via the
relaxation rate at zero momentum) for the $T$-matrix aproach in the QGP using the $U$ (lower
red band) or $F$ potential (upper green band)~\cite{Riek:2010fk}, or pQCD with $\alpha_s$=0.4
(dash-dotted line), in hadronic matter (dashed line)~\cite{He:2011yi}, and from quenched
lattice QCD~\cite{Ding:2011hr,Banerjee:2011ra} (data points); figure taken from~\cite{He:2012df}.}
\label{fig_Ds}
\end{figure}
