#!/usr/bin/perl

#use strict;

@user = getpwuid($<);
@userfirstname = split(" ",$user[6]);
print "============================================================\n";
print ">>> Hi $userfirstname[0],  \n";
print ">>> Thanks for using this script for merging the tex files of your writeup,\n";

if ($#ARGV lt 1) {
  print "============================================================\n";
  print ">>> $userfirstname[0], remember that this script needs 2-3 arguments: \n";
  print ">>> The main tex file name, the merged main tex file name \n";
  print ">>> in case you want to run latexdiff with a previous merged main tex file, also its name \n";
  print ">>> Exiting form script execution!\n";
  exit;
}

my $maintexfile      = $ARGV[0];
my $mergedtexfile    = $ARGV[1];
my $mergedtexfileold = $ARGV[2];

print "============================================================\n";
print ">>> The main tex file name is $maintexfile \n";
print ">>>  the basename of the merged main tex file name is $mergedtexfile \n";
print ">>>  the merged old main tex file name is $mergedtexfileold \n";


#Checking if file exists
if (-e $maintexfile) {
  print "============================================================\n";
  print ">>> $maintexfile exists \n";
} else {
  print "============================================================\n";
  print ">>> $maintexfile does not exist \n";
  exit;
}



#Checking local directory
my $localdirectory = `pwd`;
chop($localdirectory);

my $datestring = `date`;
my @datesepstring = split(" ",$datestring);
my $suffixdate = $datesepstring[2].$datesepstring[1].$datesepstring[5];

# Generation of the output tex file and of the file with the list of merged files
my $filelist = $localdirectory."/MergedOutputs_filelist_".$suffixdate.".txt";
unlink($filelist);
print "Opening $filelist \n";
open(MERGINGMACROFILE,">$filelist") || die "$filelist was not created \n";
my $outfile = $localdirectory."/".$mergedtexfile."_Merged".$suffixdate.".tex";
unlink($outfile);
print "Opening $outfile \n";
open(OUTMACROFILE,">$outfile") || die "$outfile was not created \n";

# Reading the main file
print "Reading $maintexfile \n";
open(MAINFILE,"<$maintexfile")  || die "$maintexfile was not read \n";
print MERGINGMACROFILE "$maintexfile \n";
# Get the input line by line
while(<MAINFILE>){

    chomp($_);
    my $row = $_;

    my $firstchar = substr($row,0,1);
    # if first character is "%" do nothing
    if( !($firstchar=~/\%/) ) {

	# if line starts with "\input" then get the tex filename and read it on its turn
    if($row=~/input\{/){
     
#	    print $row;
	    my @fieldsFirst = split(/\{/, $row);
	    my @fieldsSecond = split(/\}/,$fieldsFirst[1]);
	    my $nfilename = $fieldsSecond[0];
	    print "Processing subfile named $nfilename \n";
	    print MERGINGMACROFILE "$nfilename \n";
	    print OUTMACROFILE "\n\n";
	    
	    # read the subfilenames
	    open(FILEN,"<$nfilename") || die "$nfilename was not read \n";
	    while(my $rownfile=<FILEN>){
		chomp($rownfile);
		my $firstcharnfile = substr($rownfile,0,1);
		if( !($ffirstcharnfile=~/\%/) ) {
#		print "$rownfile\n";
		    print OUTMACROFILE "$rownfile \n";
		}
	    }
	    close(FILEN);

	} else{
	    print OUTMACROFILE "$row\n";
	}
    }
}
close(MAINFILE);

close(MERGINGMACROFILE);
close(OUTMACROFILE);


#
# Checking if old merged file exists
#
if (-e $mergedtexfileold) {
  print "============================================================\n";
  print ">>> $mergedtexfileold exists \n";
} else {
  print "============================================================\n";
  print ">>> $mergedtexfileold does not exist \n";
  print "============================================================\n";
  print ">>> script finished successfully \n";
  print ">>> merged the tex files of $maintexfile \n";
  print "============================================================\n";
  exit;
}
#
# If exists, then run the latexdiff of the files
#
my @oldfileentries = split(".",$mergedtexfileold);
my $oldfilebasename = $oldfileentries[0];

my $outfileDiffBase = "Diff_".$mergedtexfile."_".$suffixdate;
my $outfileDiff = $localdirectory."/".$outfileDiffBase.".tex";
unlink($outfileDiff);
print "Opening $outfileDiff \n";
open(OUTMACROFILEDIF,">$outfileDiff") || die "$outfileDiff was not created \n";

print "Creating the latexdiff file $outfileDiff \n";
`latexdiff $mergedtexfileold $maintexfile > $outfileDiff`;
print "Runing pdflatex on $outfileDiffBase \n";
`pdflatex $outfileDiffBase`;
print "Runing bibtex on $outfileDiffBase \n";
`bibtex $outfileDiffBase`;
print "Re-runing pdflatex on $outfileDiffBase \n";
`pdflatex $outfileDiffBase`;
`pdflatex $outfileDiffBase`;

my $outdir = $localdirectory."/".$suffixdate;
print "Creating output directory $outdir and moving all stuff there \n";
`mkdir $outdir`;
`mv $filelist $outdir`;
`mv $outfile $outdir`;
`mv $outfileDiffBase* $outdir`;

print "============================================================\n";
print ">>> script finished successfully \n";
print ">>> merged the tex files of $maintexfile \n";
print ">>> run the latexdiff and created the pdf file vs $mergedtexfileold \n";
print ">>> all moved to subdirectory $suffixdate \n";
print "============================================================\n";
