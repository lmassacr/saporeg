

//
//  PlotFigsCNM.C
//  
//
//
//


//starting point macro to plots the figures for the review
//To be updated with more functionalities (such as function to add theory curves)
//the default plotting of data points is black and white, and support the plotting of statistical, uncorrelated and correlated systematic errors
//(symmetric or asymmetric)
//there is a list of colors and markers to use in priority if need to plot several data points


#include <stdio.h>
#include "TStyle.h"
#include "TH1.h"
#include "TH2.h"
#include "TH2F.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TLine.h"
#include "TPad.h"
#include "TF1.h"
#include "TGraphAsymmErrors.h"


void SetLegendStyle(TLegend *leg);
void SetGraphStyle(TGraphAsymmErrors *gr, Int_t ibin);
void SetGraphStyleCorrelatedUnc(TGraphAsymmErrors *gr, Int_t ibin);
void SetGraphStyleUncorrelatedUnc(TGraphAsymmErrors *gr, Int_t ibin);
void SetGraphStyleFullyCorrelatedUnc(TGraphAsymmErrors *gr, Int_t ibin);
void SetGraphStylePoint(TGraphAsymmErrors *gr, Int_t ibin, Bool_t isCopy=false);
TH2F *hframe(Int_t nbinsx=40, Double_t xmin=0.,Double_t xmax=40.,Int_t nbinsy=100,Double_t ymin=0.,Double_t ymax=2.0);
TCanvas * canvasSinglePlot();
TCanvas * canvasDoublePlot();
TCanvas * canvasTriplePlot();
void CreateHistoFromDataPoints_PHENIX_Bjpsi_RpA(TGraphAsymmErrors* &hstat, TGraphAsymmErrors* &hsyst, TGraphAsymmErrors *&hsystCorr, TGraphAsymmErrors *&hsystFullCorr);
void CreateHistoFromDataPoints_PHENIX_Cjpsi_RpA(TGraphAsymmErrors* &hstat, TGraphAsymmErrors* &hsyst, TGraphAsymmErrors *&hsystCorr, TGraphAsymmErrors *&hsystFullCorr);
void CreateHistoFromDataPoints_PHENIX_Fjpsi_RpA(TGraphAsymmErrors* &hstat, TGraphAsymmErrors* &hsyst, TGraphAsymmErrors *&hsystCorr, TGraphAsymmErrors *&hsystFullCorr);
void CreateHistoFromDataPoints_PHENIX_BmuHF_RpA(TGraphAsymmErrors* &hstat, TGraphAsymmErrors* &hsyst, TGraphAsymmErrors *&hsystCorr, TGraphAsymmErrors *&hsystFullCorr);
void CreateHistoFromDataPoints_PHENIX_CeHF_RpA(TGraphAsymmErrors* &hstat, TGraphAsymmErrors* &hsyst, TGraphAsymmErrors *&hsystCorr, TGraphAsymmErrors *&hsystFullCorr);
void CreateHistoFromDataPoints_PHENIX_FmuHF_RpA(TGraphAsymmErrors* &hstat, TGraphAsymmErrors* &hsyst, TGraphAsymmErrors *&hsystCorr, TGraphAsymmErrors *&hsystFullCorr);

void SetStyle(Bool_t graypalette=kFALSE);
//void CreateHistoFromDataPoints_PHENIX(TGraphAsymmErrors* &hstat, TGraphAsymmErrors* &hsyst, TGraphAsymmErrors* &hsystCorr);
//void CreateHistoFromDataPoints_STAR(TGraphAsymmErrors* &hstat, TGraphAsymmErrors* &hsyst, TGraphAsymmErrors* &hsystCorr);


// Preferred colors and markers
const Int_t fillColors[] = {kGray+1,  kRed-8, kBlue-9, kGreen-8, kMagenta-9, kOrange-9,kCyan-8,kYellow-7}; // for syst bands
const Int_t colors[]     = {kBlack, kRed+1 , kBlue+1, kGreen+3, kMagenta+1, kOrange-1,kCyan+2,kYellow+2};
const Int_t markers[]    = {kFullCircle, kFullSquare,kOpenCircle,kOpenSquare,kOpenDiamond,kOpenCross,kFullCross,kFullDiamond,kFullStar,kOpenStar};


void Plot_Charm_RHIC_RpA() {   
  
  // Set the default style
  SetStyle();
  
  // Prepare Figure, please stick to the default canvas size(s) unless absolutely necessary in your case
  // Rectangular
  TCanvas *cfig;
  //cfig = canvasSinglePlot();
  //cfig = canvasDoublePlot();
  cfig = canvasTriplePlot();

  //log scale if needed
  // cfig->SetLogy();
    
    
  // Define your frame for 1st histogram 
  Int_t nbinsx1=90, nbinsy1=30; 
  Double_t xmin1=0.0, xmax1=9.0, ymin1=0.0, ymax1=3.0;
  TH2F * hframe1 = hframe(nbinsx1,xmin1,xmax1,nbinsy1,ymin1,ymax1);

  // Define your frame for 2nd histogram 
  Int_t nbinsx2=100, nbinsy2=30; 
  Double_t xmin2=0.0, xmax2=10.0, ymin2=0.0, ymax2=3.0;
  TH2F * hframe2 = hframe(nbinsx2,xmin2,xmax2,nbinsy2,ymin2,ymax2);
    
  // Define your frame for 3rd histogram 
  Int_t nbinsx3=90, nbinsy3=30; 
  Double_t xmin3=0.0, xmax3=9.0, ymin3=0.0, ymax3=3.0;
  TH2F * hframe3 = hframe(nbinsx3,xmin3,xmax3,nbinsy3,ymin3,ymax3);
    
  // Set titles
  // === List of commonly used X and Y axis: ===
  // pt invariant yields
  /* const char *  texPtX="#it{p}_{T} (GeV/#it{c})";
     const char *  texPtY="1/#it{N}_{ev} 1/(2#pi#it{p}_{T}) d#it{N}/(d#it{p}_{T}d#it{y}) ((GeV/#it{c})^{-2})";
     // mt invariant yields
     const char *  texMtX="#it{m}_{T} (GeV/#it{c^2})";
     const char *  texMtY="1/#it{N}_{ev} 1/(2#pi#it{m}_{T}) d#it{N}/(d#it{m}_{T}d#it{y}) ((GeV/#it{c^2})^{-2}) ";
     // Invariant mass with decay products K and pi
     const char *  texMassX="#it{M}_{K#pi} (GeV/#it{c}^2)";
     const char *  texMassY="d#it{N}/(d#it{M}_{K#pi}";
     // <pt>, npart
     const char * texMeanPt    = "#LT#it{p}_{T}#GT (GeV/#it{c})";
    const char * texMeanNpart = "#LT#it{N}_{part}#GT"; */
  
  const char *texX1 = "#it{p}_{T} (GeV/#it{c})";
  const char *texY1 = "Nuclear modification factor";
  hframe1->SetXTitle(texX1);
  hframe1->SetYTitle(texY1);

  const char *texX2 = "#it{p}_{T} (GeV/#it{c})";
  const char *texY2 = "R_{dAu}";
  hframe2->SetXTitle(texX2);
  //hframe2->SetYTitle(texY2);

  const char *texX3 = "#it{p}_{T} (GeV/#it{c})";
  const char *texY3 = "R_{dAu}";
  hframe3->SetXTitle(texX3);
  //hframe3->SetYTitle(texY3);

  // Data point style
  Int_t imarker=0, icolor=0;
  
  // PHENIX points Backward (-2.2 < y < -1.2) Jpsi
  imarker = 1;
  icolor = 1;
  
  TGraphAsymmErrors* hstat_PHENIX_Bjpsi_RpA, *hsyst_PHENIX_Bjpsi_RpA, *hsystCorr_PHENIX_Bjpsi_RpA, *hsystFullCorr_PHENIX_Bjpsi_RpA;
  CreateHistoFromDataPoints_PHENIX_Bjpsi_RpA(hstat_PHENIX_Bjpsi_RpA,hsyst_PHENIX_Bjpsi_RpA,hsystCorr_PHENIX_Bjpsi_RpA,hsystFullCorr_PHENIX_Bjpsi_RpA);
  SetGraphStylePoint(hstat_PHENIX_Bjpsi_RpA,icolor);
  SetGraphStyleUncorrelatedUnc(hsystCorr_PHENIX_Bjpsi_RpA,icolor);
  SetGraphStyleFullyCorrelatedUnc(hsystFullCorr_PHENIX_Bjpsi_RpA,icolor);

  // PHENIX points Central (|y| < 0.35) Jpsi
  imarker = 1;
  icolor = 1;
  
  TGraphAsymmErrors* hstat_PHENIX_Cjpsi_RpA, *hsyst_PHENIX_Cjpsi_RpA, *hsystCorr_PHENIX_Cjpsi_RpA, *hsystFullCorr_PHENIX_Cjpsi_RpA;
  CreateHistoFromDataPoints_PHENIX_Cjpsi_RpA(hstat_PHENIX_Cjpsi_RpA,hsyst_PHENIX_Cjpsi_RpA,hsystCorr_PHENIX_Cjpsi_RpA,hsystFullCorr_PHENIX_Cjpsi_RpA);
  SetGraphStylePoint(hstat_PHENIX_Cjpsi_RpA,icolor);
  //SetGraphStyleUncorrelatedUnc(hsyst_PHENIX_Cjpsi_RpA,icolor);
  SetGraphStyleUncorrelatedUnc(hsystCorr_PHENIX_Cjpsi_RpA,icolor);
  SetGraphStyleFullyCorrelatedUnc(hsystFullCorr_PHENIX_Cjpsi_RpA,icolor);

  // PHENIX points Forward (1.2 < y < 2.2) Jpsi
  imarker = 1;
  icolor = 1;
  
  TGraphAsymmErrors* hstat_PHENIX_Fjpsi_RpA, *hsyst_PHENIX_Fjpsi_RpA, *hsystCorr_PHENIX_Fjpsi_RpA, *hsystFullCorr_PHENIX_Fjpsi_RpA;
  CreateHistoFromDataPoints_PHENIX_Fjpsi_RpA(hstat_PHENIX_Fjpsi_RpA,hsyst_PHENIX_Fjpsi_RpA,hsystCorr_PHENIX_Fjpsi_RpA,hsystFullCorr_PHENIX_Fjpsi_RpA);
  SetGraphStylePoint(hstat_PHENIX_Fjpsi_RpA,icolor);
  //  SetGraphStyleUncorrelatedUnc(hsyst_PHENIX_Fjpsi_RpA,icolor);
  SetGraphStyleUncorrelatedUnc(hsystCorr_PHENIX_Fjpsi_RpA,icolor);
  SetGraphStyleFullyCorrelatedUnc(hsystFullCorr_PHENIX_Fjpsi_RpA,icolor);

  // PHENIX points Backward (-2.0 < y < -1.4) mu HF
  imarker = 2;
  icolor = 2;
  
  TGraphAsymmErrors* hstat_PHENIX_BmuHF_RpA, *hsyst_PHENIX_BmuHF_RpA, *hsystCorr_PHENIX_BmuHF_RpA, *hsystFullCorr_PHENIX_BmuHF_RpA;
  CreateHistoFromDataPoints_PHENIX_BmuHF_RpA(hstat_PHENIX_BmuHF_RpA,hsyst_PHENIX_BmuHF_RpA,hsystCorr_PHENIX_BmuHF_RpA,hsystFullCorr_PHENIX_BmuHF_RpA);
  SetGraphStylePoint(hstat_PHENIX_BmuHF_RpA,icolor);
  //  SetGraphStyleUncorrelatedUnc(hsyst_PHENIX_BmuHF_RpA,icolor);
  SetGraphStyleUncorrelatedUnc(hsystCorr_PHENIX_BmuHF_RpA,icolor);
  SetGraphStyleFullyCorrelatedUnc(hsystFullCorr_PHENIX_BmuHF_RpA,icolor);

  // PHENIX points Backward (|y| < 0.35) mu HF
  imarker = 2;
  icolor = 2;
  
  TGraphAsymmErrors* hstat_PHENIX_CeHF_RpA, *hsyst_PHENIX_CeHF_RpA, *hsystCorr_PHENIX_CeHF_RpA, *hsystFullCorr_PHENIX_CeHF_RpA;
  CreateHistoFromDataPoints_PHENIX_CeHF_RpA(hstat_PHENIX_CeHF_RpA,hsyst_PHENIX_CeHF_RpA,hsystCorr_PHENIX_CeHF_RpA,hsystFullCorr_PHENIX_CeHF_RpA);
  SetGraphStylePoint(hstat_PHENIX_CeHF_RpA,icolor);
  //  SetGraphStyleUncorrelatedUnc(hsyst_PHENIX_CeHF_RpA,icolor);
  SetGraphStyleUncorrelatedUnc(hsystCorr_PHENIX_CeHF_RpA,icolor);
  SetGraphStyleFullyCorrelatedUnc(hsystFullCorr_PHENIX_CeHF_RpA,icolor);

  // PHENIX points Forward (1.4 < y < 2.0) mu HF
  imarker = 2;
  icolor = 2;
  
  TGraphAsymmErrors* hstat_PHENIX_FmuHF_RpA, *hsyst_PHENIX_FmuHF_RpA, *hsystCorr_PHENIX_FmuHF_RpA, *hsystFullCorr_PHENIX_FmuHF_RpA;
  CreateHistoFromDataPoints_PHENIX_FmuHF_RpA(hstat_PHENIX_FmuHF_RpA,hsyst_PHENIX_FmuHF_RpA,hsystCorr_PHENIX_FmuHF_RpA,hsystFullCorr_PHENIX_FmuHF_RpA);
  SetGraphStylePoint(hstat_PHENIX_FmuHF_RpA,icolor);
  //SetGraphStyleUncorrelatedUnc(hsyst_PHENIX_FmuHF_RpA,icolor);
  SetGraphStyleUncorrelatedUnc(hsystCorr_PHENIX_FmuHF_RpA,icolor);
  SetGraphStyleFullyCorrelatedUnc(hsystFullCorr_PHENIX_FmuHF_RpA,icolor);

  // You should always specify the colliding system
  // NOTATION: pp, p-Pb, Pb-Pb.
  // Don't forget to use #sqrt{s_{NN}} for p-Pb and Pb-Pb
  
  // You can change the position of the text box by varying X and Y coordinates, by default 5.0 and 600 for this histo
  //TLatex * text = new TLatex (0.3*xmax,0.9*ymax,"Colliding system and energy");
  // You can change the position of the text box by varying X and Y coordinates, by default 0.65 and 55 for this histo
  //TLatex * text2 = new TLatex (0.3*xmax,0.825*ymax,"Histogram title");
  //text2->SetTextSizePixels(24);

  // Line
  TLine * line1 = new TLine(0.0,1.0,9.0,1.0);
  line1->SetLineColor(kBlack);
  line1->SetLineStyle(2);
  line1->SetLineWidth(1);
  TLine * line2 = new TLine(0.0,1.0,10.0,1.0);
  line2->SetLineColor(kBlack);
  line2->SetLineStyle(2);
  line2->SetLineWidth(1);
    
  //Legend, if needed
  //TLegend * leg1 = new TLegend(0.2,0.77,0.5,0.9,"d-Au #sqrt{s_{NN}} = 200 GeV (centrality 0-20%) ");
  TLegend * leg1 = new TLegend(0.2,0.77,0.5,0.9);
  SetLegendStyle(leg1);
  leg1->AddEntry(hstat_PHENIX_BmuHF_RpA, "HF #mu^{#pm} (-2.0<y<-1.4)", "P");
  leg1->AddEntry(hstat_PHENIX_Bjpsi_RpA, "J/#psi (-2.2<y<-1.2)", "P");

  TLegend * leg2 = new TLegend(0.1,0.67,0.5,0.9);
  SetLegendStyle(leg2);
  leg2->AddEntry(hstat_PHENIX_FmuHF_RpA, "HF e^{#pm} (|y|<0.35)", "P" );
  leg2->AddEntry(hstat_PHENIX_Fjpsi_RpA, "J/#psi (|y|<0.35)", "P" );

  TLegend * leg3 = new TLegend(0.1,0.67,0.5,0.9,"PHENIX d-Au #sqrt{s_{NN}} = 200 GeV (centrality 0-20%)");
  SetLegendStyle(leg3);
  leg3->AddEntry(hstat_PHENIX_FmuHF_RpA, "HF #mu^{#pm} (1.4<y<2.0)", "P" );
  leg3->AddEntry(hstat_PHENIX_Fjpsi_RpA, "J/#psi (1.2<y<2.2)", "P" );

  // Now drawing
  
  cfig->cd(1);
  hframe1->Draw();
  hstat_PHENIX_BmuHF_RpA->Draw("P same");
  //hsyst_PHENIX_BmuHF_RpA->Draw("2");
  hsystCorr_PHENIX_BmuHF_RpA->Draw("2");
  hsystFullCorr_PHENIX_BmuHF_RpA->Draw("2");
  hstat_PHENIX_BmuHF_RpA->Draw("P same");
  hstat_PHENIX_Bjpsi_RpA->Draw("P same");
  //hsyst_PHENIX_Bjpsi_RpA->Draw("2");
  hsystCorr_PHENIX_Bjpsi_RpA->Draw("2");
  hsystFullCorr_PHENIX_Bjpsi_RpA->Draw("2");
  hstat_PHENIX_Bjpsi_RpA->Draw("P same");
  leg1->Draw();
  line1->Draw();

  cfig->cd(2);
  hframe2->Draw();
  hstat_PHENIX_CeHF_RpA->Draw("P same");
  //hsyst_PHENIX_CeHF_RpA->Draw("2");
  hsystCorr_PHENIX_CeHF_RpA->Draw("2");
  hsystFullCorr_PHENIX_CeHF_RpA->Draw("2");
  hstat_PHENIX_CeHF_RpA->Draw("P same");
  hstat_PHENIX_Cjpsi_RpA->Draw("P same");
  //hsyst_PHENIX_Cjpsi_RpA->Draw("2");
  hsystCorr_PHENIX_Cjpsi_RpA->Draw("2");
  hsystFullCorr_PHENIX_Cjpsi_RpA->Draw("2");
  hstat_PHENIX_Cjpsi_RpA->Draw("P same");
  leg2->Draw();
  line2->Draw();
    
  cfig->cd(3);
  hframe3->Draw();
  hstat_PHENIX_FmuHF_RpA->Draw("P same");
  //hsyst_PHENIX_FmuHF_RpA->Draw("2");
  hsystCorr_PHENIX_FmuHF_RpA->Draw("2");
  hsystFullCorr_PHENIX_FmuHF_RpA->Draw("2");
  hstat_PHENIX_FmuHF_RpA->Draw("P same");
  hstat_PHENIX_Fjpsi_RpA->Draw("P same");
  //hsyst_PHENIX_Fjpsi_RpA->Draw("2");
  hsystCorr_PHENIX_Fjpsi_RpA->Draw("2");
  hsystFullCorr_PHENIX_Fjpsi_RpA->Draw("2");
  hstat_PHENIX_Fjpsi_RpA->Draw("P same");
  leg3->Draw();
  line1->Draw();
    
  cfig->Print("QQ-5.pdf");

}

//____________________________________________________________
void SetLegendStyle(TLegend *leg)
{
  leg->SetBorderSize(0);
  leg->SetTextSize(gStyle->GetTextSize()*0.8);
  leg->SetLineColor(0);
  leg->SetLineStyle(1);
  leg->SetLineWidth(1);
  leg->SetFillColor(0);
  leg->SetFillStyle(1001);
  return;
}

//____________________________________________________________
void SetGraphStyle(TGraphAsymmErrors *gr, Int_t ibin){
 gr->SetFillStyle(0);
 gr->SetLineColor(colors[ibin]);
 gr->SetLineWidth(2);
 return;
}

//____________________________________________________________
void SetGraphStyleUncorrelatedUnc(TGraphAsymmErrors *gr, Int_t ibin){
 gr->SetFillStyle(0);
 gr->SetLineColor(colors[ibin]);
 gr->SetLineWidth(2);
 return;
}

//____________________________________________________________
void SetGraphStyleCorrelatedUnc(TGraphAsymmErrors *gr, Int_t ibin){
 gr->SetFillStyle(3001);
 gr->SetLineColor(fillColors[ibin]);
 gr->SetFillColor(fillColors[ibin]);
 gr->SetLineWidth(2);
 return;
}

//____________________________________________________________
void SetGraphStyleFullyCorrelatedUnc(TGraphAsymmErrors *gr, Int_t ibin){
 gr->SetFillStyle(1001);
 gr->SetLineColor(colors[ibin]);
 gr->SetFillColor(colors[ibin]);
 gr->SetLineWidth(2);
 return;
}
//____________________________________________________________
void SetGraphStylePoint(TGraphAsymmErrors *gr, Int_t ibin, Bool_t isCopy){
 gr->SetMarkerStyle(markers[ibin]);
 gr->SetMarkerColor(colors[ibin]);
 gr->SetLineColor(colors[ibin]);
 gr->SetLineWidth(2);
 if(isCopy) {
   gr->SetMarkerStyle(markers[ibin]+24);
   gr->SetMarkerColor(kBlack);
 }
 return;
}

//____________________________________________________________
TH2F *hframe(Int_t nbinsx, Double_t xmin,Double_t xmax,Int_t nbinsy,Double_t ymin,Double_t ymax)
{
  TH2F *hFrame = new TH2F("hFrame"," ",nbinsx,xmin,xmax,nbinsy,ymin,ymax);
  hFrame->GetXaxis()->SetTitleSize(0.05);
  hFrame->GetXaxis()->SetTitleOffset(0.9);
  hFrame->GetYaxis()->SetTitleSize(0.05);
  hFrame->GetYaxis()->SetTitleOffset(0.9);
  hFrame->GetYaxis()->SetTitleOffset(1.05);

  return (TH2F*)hFrame;
}

//____________________________________________________________
TCanvas * canvasSinglePlot()
{

  TCanvas *singleCanvas = new TCanvas("singleCanvas","Upsilon RpA vs y",550,550);
  singleCanvas->SetTopMargin(0.085);
  singleCanvas->Range(-3.177966,-0.2350333,25.74153,2.079823);
  singleCanvas->SetBottomMargin(0.1);
  singleCanvas->SetBorderSize(1);
  singleCanvas->SetLeftMargin(0.114);
  singleCanvas->SetRightMargin(0.02564103);
  singleCanvas->SetTopMargin(0.03448276);
  singleCanvas->SetTickx();
  singleCanvas->SetTicky();

  return (TCanvas*)singleCanvas;
}

//____________________________________________________________
TCanvas * canvasDoublePlot()
{
  gStyle->SetPadTopMargin(0.);
  gStyle->SetPadBottomMargin(0.);
  gStyle->SetPadLeftMargin(0.);
  gStyle->SetPadRightMargin(0.);

  TCanvas *doubleCanvas = new TCanvas("doubleCanvas","Upsilon RpA vs y",700,350);
  
  TPad *pad1 = new TPad("pad1","pad1",0,0,0.5,1.,0);
  pad1->SetNumber(1);
  pad1->SetTopMargin(0.05);
  pad1->SetBottomMargin(0.1);
  pad1->SetRightMargin(0.002);
  //pad1->SetRightMargin(0.025);
  pad1->SetLeftMargin(0.135);
  pad1->SetFillColor(0);
  pad1->SetBorderMode(0);
  pad1->SetBorderSize(0);
  pad1->SetTickx(1);
  pad1->SetTicky(1);
  pad1->Draw();
  TPad *pad2 = new TPad("pad2","pad2",0.5,0,1.,1.,0);
  pad2->SetNumber(2);
  pad2->SetTopMargin(0.05);
  pad2->SetBottomMargin(0.1);
  pad2->SetRightMargin(0.025);
  pad2->SetLeftMargin(0.);
  //pad2->SetLeftMargin(0.135);
  pad2->SetFillColor(0);
  pad2->SetBorderMode(0);
  pad2->SetBorderSize(0);
  pad2->SetTickx(1);
  pad2->SetTicky(1);
  pad2->Draw();

  return (TCanvas*)doubleCanvas;
}

//____________________________________________________________
TCanvas * canvasTriplePlot()
{
  gStyle->SetPadTopMargin(0.);
  gStyle->SetPadBottomMargin(0.);
  gStyle->SetPadLeftMargin(0.);
  gStyle->SetPadRightMargin(0.);

  TCanvas *tripleCanvas = new TCanvas("tripleCanvas","Upsilon RpA vs y",1050,525);
  
  TPad *pad1 = new TPad("pad1","pad1",0,0,0.36,1.,0);
  pad1->SetNumber(1);
  pad1->SetTopMargin(0.05);
  pad1->SetBottomMargin(0.1);
  pad1->SetLeftMargin(0.135);
  pad1->SetRightMargin(0.0);
  pad1->SetFillColor(0);
  pad1->SetBorderMode(0);
  pad1->SetBorderSize(0);
  pad1->SetTickx(1);
  pad1->SetTicky(1);
  pad1->Draw();
  TPad *pad2 = new TPad("pad2","pad2",0.36,0,0.675,1.,0);
  pad2->SetNumber(2);
  pad2->SetTopMargin(0.05);
  pad2->SetBottomMargin(0.1);
  pad2->SetLeftMargin(0.002);
  pad2->SetRightMargin(0.0);
  pad2->SetFillColor(0);
  pad2->SetBorderMode(0);
  pad2->SetBorderSize(0);
  pad2->SetTickx(1);
  pad2->SetTicky(1);
  pad2->Draw();
  TPad *pad3 = new TPad("pad3","pad3",0.675,0,1.,1.,0);
  pad3->SetNumber(3);
  pad3->SetTopMargin(0.05);
  pad3->SetBottomMargin(0.1);
  pad3->SetLeftMargin(0.002);
  pad3->SetRightMargin(0.025);
  pad3->SetFillColor(0);
  pad3->SetBorderMode(0);
  pad3->SetBorderSize(0);
  pad3->SetTickx(1);
  pad3->SetTicky(1);
  pad3->Draw();

  return (TCanvas*)tripleCanvas;
}


//____________________________________________________________
void SetStyle(Bool_t graypalette) {
  
  gStyle->Reset("Plain");
  gStyle->SetOptTitle(0);
  gStyle->SetOptStat(0);
  if(graypalette) gStyle->SetPalette(8,0);
  else gStyle->SetPalette(1);
  gStyle->SetCanvasColor(10);
  gStyle->SetCanvasBorderMode(0);
  gStyle->SetFrameLineWidth(1);
  gStyle->SetFrameFillColor(kWhite);
  gStyle->SetPadColor(10);
  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);
  gStyle->SetPadBottomMargin(0.15);
  gStyle->SetPadLeftMargin(0.15);
  gStyle->SetHistLineWidth(1);
  gStyle->SetHistLineColor(kRed);
  gStyle->SetFuncWidth(2);
  gStyle->SetFuncColor(kGreen);
  gStyle->SetLineWidth(2);
  gStyle->SetLabelSize(0.045,"xyz");
  gStyle->SetLabelOffset(0.01,"y");
  gStyle->SetLabelOffset(0.01,"x");
  gStyle->SetLabelColor(kBlack,"xyz");
  gStyle->SetTitleSize(0.05,"xyz");
  gStyle->SetTitleOffset(1.25,"y");
  gStyle->SetTitleOffset(1.2,"x");
  gStyle->SetTitleFillColor(kWhite);
  gStyle->SetTextSizePixels(26);
  gStyle->SetTextFont(42);
 
  gStyle->SetLegendBorderSize(0);
  gStyle->SetLegendFillColor(kWhite);
  gStyle->SetLegendFont(42);  

  return;
}

//____________________________________________________________
//        DATA 
//
//  PHENIX, PRL 109, 242301 (2012)
//  PHENIX, arXiv:1211.4017
//  STAR, Phys. Lett. B 735 (2014) 127-137
//  LHCb, JHEP07 (2014) 094
//  ALICE, Internal Note
//____________________________________________________________

 

void CreateHistoFromDataPoints_PHENIX_Bjpsi_RpA(TGraphAsymmErrors* &hstat, TGraphAsymmErrors* &hsyst, TGraphAsymmErrors *&hsystCorr, TGraphAsymmErrors *&hsystFullCorr){

   // Similar to HepData format (can copy paste values from HepData)
   // Support asymmetric errors
   // Enter the number of points of your histogram, the X value, the lower X error, the upper X error, the Y value, the lower Y error (statistical),
   // the upper Y error (statistical), the lower Y error (uncorrelated systematic), the upper Y error (uncorrelated systematic), the lower Y error (correlated systematic),
   // the upper Y error (correlated systematic)

    int numpoints = 24;
    double xval[] = { 0.125, 0.375, 0.625, 0.875, 1.125, 1.375, 1.625, 1.875, 2.125, 2.375, 2.625, 2.875, 3.125, 3.375, 3.625, 3.875, 4.125, 4.375, 4.625, 4.875, 5.25, 5.75, 6.5, 7.5 };
    double xerrminus[24];
    double xerrplus[24];
    double yval[] = { 0.702, 0.760, 0.840, 0.894, 0.954, 1.008, 1.000, 1.058, 1.058, 1.252, 1.240, 1.249, 1.230, 1.088, 1.406, 1.131, 1.244, 1.209, 1.824, 1.508, 1.182, 1.171, 1.848, 2.079 };
    double ystaterrminus[] = { 0.121, 0.104, 0.105, 0.076, 0.059, 0.066, 0.061, 0.066, 0.062, 0.072, 0.076, 0.083, 0.095, 0.093, 0.130, 0.124, 0.165, 0.186, 0.314, 0.329, 0.330, 0.522, 0.830, 1.364 };
    double ystaterrplus[24];
    double ysysterrminus[24];
    double ysysterrplus[24];
    double ysystcorrerrminus[] = { 0.049, 0.053, 0.058, 0.062, 0.065, 0.069, 0.069, 0.073, 0.073, 0.086, 0.085, 0.086, 0.086, 0.075, 0.097, 0.078, 0.086, 0.084, 0.127, 0.106, 0.082, 0.082, 0.128, 0.146 };
    double ysystcorrerrplus[24];
    for (int i=0; i<numpoints; i++) {
      xerrminus[i] = 0.1;
      xerrplus[i] = 0.1;
      ystaterrplus[i] = ystaterrminus[i];
      ysystcorrerrplus[i] = ysystcorrerrminus[i];
    }

    hstat = new TGraphAsymmErrors(numpoints,xval,yval,xerrminus,xerrplus,ystaterrminus,ystaterrplus);
    hsyst = new TGraphAsymmErrors(numpoints,xval,yval,xerrminus,xerrplus,ysysterrminus,ysysterrplus);
    hsystCorr = new TGraphAsymmErrors(numpoints,xval,yval,xerrminus,xerrplus,ysystcorrerrminus,ysystcorrerrplus);

    double global = 9.0;
    int numpointsfull = 1;
    double xvalfull[] = {8.4};
    double xfullerrminus[] = {0.2};
    double xfullerrplus[] = {0.2};
    double yvalfull[] = {1.0};
    double ysystfullerrminus[] = {global/100.};
    double ysystfullerrplus[] = {global/100.};

    hsystFullCorr = new TGraphAsymmErrors(numpointsfull,xvalfull,yvalfull,xfullerrminus,xfullerrplus,ysystfullerrminus,ysystfullerrplus);

}


void CreateHistoFromDataPoints_PHENIX_Cjpsi_RpA(TGraphAsymmErrors* &hstat, TGraphAsymmErrors* &hsyst, TGraphAsymmErrors *&hsystCorr, TGraphAsymmErrors *&hsystFullCorr){

   // Similar to HepData format (can copy paste values from HepData)
   // Support asymmetric errors
   // Enter the number of points of your histogram, the X value, the lower X error, the upper X error, the Y value, the lower Y error (statistical),
   // the upper Y error (statistical), the lower Y error (uncorrelated systematic), the upper Y error (uncorrelated systematic), the lower Y error (correlated systematic),
   // the upper Y error (correlated systematic)

    int numpoints = 11;
    double xval[] = { 0.25, 0.75, 1.25, 1.75, 2.25, 2.75, 3.25, 3.75, 4.5, 6.0, 8.0 };
    double xerrminus[11];
    double xerrplus[11];
    double yval[] = { 0.69, 0.60, 0.65, 0.73, 0.88, 0.81, 1.10, 1.10, 1.10, 0.59, 1.10 };
    double ystaterrminus[] = { 0.071, 0.040, 0.042, 0.054, 0.088, 0.11, 0.17, 0.24, 0.24, 0.19, 0.54 };
    double ystaterrplus[11];
    double ysysterrminus[11];
    double ysysterrplus[11];
    double ysystcorrerrminus[] = { 0.094, 0.081, 0.088, 0.097, 0.12, 0.11, 0.15, 0.15, 0.15, 0.087, 0.17 };
    double ysystcorrerrplus[11];
    for (int i=0; i<numpoints; i++) {
      xerrminus[i] = 0.1;
      xerrplus[i] = 0.1;
      ystaterrplus[i] = ystaterrminus[i];
      ysystcorrerrplus[i] = ysystcorrerrminus[i];
    }

    hstat = new TGraphAsymmErrors(numpoints,xval,yval,xerrminus,xerrplus,ystaterrminus,ystaterrplus);
    hsyst = new TGraphAsymmErrors(numpoints,xval,yval,xerrminus,xerrplus,ysysterrminus,ysysterrplus);
    hsystCorr = new TGraphAsymmErrors(numpoints,xval,yval,xerrminus,xerrplus,ysystcorrerrminus,ysystcorrerrplus);

    double global = 8.6;
    int numpointsfull = 1;
    double xvalfull[] = {9.4};
    double xfullerrminus[] = {0.2};
    double xfullerrplus[] = {0.2};
    double yvalfull[] = {1.0};
    double ysystfullerrminus[] = {global/100.};
    double ysystfullerrplus[] = {global/100.};

    hsystFullCorr = new TGraphAsymmErrors(numpointsfull,xvalfull,yvalfull,xfullerrminus,xfullerrplus,ysystfullerrminus,ysystfullerrplus);

}


void CreateHistoFromDataPoints_PHENIX_Fjpsi_RpA(TGraphAsymmErrors* &hstat, TGraphAsymmErrors* &hsyst, TGraphAsymmErrors *&hsystCorr, TGraphAsymmErrors *&hsystFullCorr){

   // Similar to HepData format (can copy paste values from HepData)
   // Support asymmetric errors
   // Enter the number of points of your histogram, the X value, the lower X error, the upper X error, the Y value, the lower Y error (statistical),
   // the upper Y error (statistical), the lower Y error (uncorrelated systematic), the upper Y error (uncorrelated systematic), the lower Y error (correlated systematic),
   // the upper Y error (correlated systematic)

    int numpoints = 24;
    double xval[] = { 0.125, 0.375, 0.625, 0.875, 1.125, 1.375, 1.625, 1.875, 2.125, 2.375, 2.625, 2.875, 3.125, 3.375, 3.625, 3.875, 4.125, 4.375, 4.625, 4.875, 5.25, 5.75, 6.5, 7.5 };
    double xerrminus[24];
    double xerrplus[24];
    double yval[] = { 0.566, 0.557, 0.557, 0.563, 0.559, 0.594, 0.634, 0.587, 0.698, 0.691, 0.691, 0.713, 0.812, 0.631, 0.708, 0.772, 0.922, 0.834, 0.953, 0.922, 0.735, 0.997, 0.971, 0.989 };
    double ystaterrminus[] = { 0.054, 0.046, 0.034, 0.031, 0.029, 0.029, 0.031, 0.029, 0.034, 0.037, 0.041, 0.047, 0.056, 0.052, 0.064, 0.081, 0.112, 0.124, 0.163, 0.183, 0.192, 0.396, 0.378, 0.739 };
    double ystaterrplus[24];
    double ysysterrminus[24];
    double ysysterrplus[24];
    double ysystcorrerrminus[]  = { 0.042, 0.042, 0.042, 0.042, 0.042, 0.044, 0.047, 0.044, 0.052, 0.051, 0.052, 0.053, 0.061, 0.047, 0.053, 0.058, 0.069, 0.062, 0.071, 0.070, 0.055, 0.075, 0.073, 0.075 };
    double ysystcorrerrplus[24];
    for (int i=0; i<numpoints; i++) {
      xerrminus[i] = 0.1;
      xerrplus[i] = 0.1;
      ystaterrplus[i] = ystaterrminus[i];
      ysystcorrerrplus[i] = ysystcorrerrminus[i];
    }

    hstat = new TGraphAsymmErrors(numpoints,xval,yval,xerrminus,xerrplus,ystaterrminus,ystaterrplus);
    hsyst = new TGraphAsymmErrors(numpoints,xval,yval,xerrminus,xerrplus,ysysterrminus,ysysterrplus);
    hsystCorr = new TGraphAsymmErrors(numpoints,xval,yval,xerrminus,xerrplus,ysystcorrerrminus,ysystcorrerrplus);

    double global = 9.0;
    int numpointsfull = 1;
    double xvalfull[] = {8.4};
    double xfullerrminus[] = {0.2};
    double xfullerrplus[] = {0.2};
    double yvalfull[] = {1.0};
    double ysystfullerrminus[] = {global/100.};
    double ysystfullerrplus[] = {global/100.};

    hsystFullCorr = new TGraphAsymmErrors(numpointsfull,xvalfull,yvalfull,xfullerrminus,xfullerrplus,ysystfullerrminus,ysystfullerrplus);

}

void CreateHistoFromDataPoints_PHENIX_BmuHF_RpA(TGraphAsymmErrors* &hstat, TGraphAsymmErrors* &hsyst, TGraphAsymmErrors *&hsystCorr, TGraphAsymmErrors *&hsystFullCorr){

   // Similar to HepData format (can copy paste values from HepData)
   // Support asymmetric errors
   // Enter the number of points of your histogram, the X value, the lower X error, the upper X error, the Y value, the lower Y error (statistical),
   // the upper Y error (statistical), the lower Y error (uncorrelated systematic), the upper Y error (uncorrelated systematic), the lower Y error (correlated systematic),
   // the upper Y error (correlated systematic)
 
    int numpoints = 12;
    double xval[] = { 1.125, 1.375, 1.625, 1.875, 2.125, 2.375, 2.625, 2.875, 3.250, 3.750, 4.500, 6.000 };
    double xerrminus[12];
    double xerrplus[12];
    double yval[] = { 1.50505, 1.82439, 1.79303, 1.92385, 1.84845, 1.82618, 1.86446, 1.73719, 1.44191, 1.41892, 1.51455, 0.991064 };
    double ystaterrminus[] = { 0.0580309, 0.073155, 0.0820153, 0.0806596, 0.0997342, 0.128116, 0.169869, 0.192534, 0.154265, 0.260937, 0.329885, 0.32744 };
    double ystaterrplus[12];
    double ysysterrminus[12];
    double ysysterrplus[12];
    double ysystcorrerrminus[] = { 0.279917, 0.342255, 0.337548, 0.384877, 0.369996, 0.398226, 0.395934, 0.407817, 0.337803, 0.355508, 0.382188, 0.245757 };
    double ysystcorrerrplus[12];
    for (int i=0; i<numpoints; i++) {
      xerrminus[i] = 0.1;
      xerrplus[i] = 0.1;
      ystaterrplus[i] = ystaterrminus[i];
      ysystcorrerrplus[i] = ysystcorrerrminus[i];
    }

    hstat = new TGraphAsymmErrors(numpoints,xval,yval,xerrminus,xerrplus,ystaterrminus,ystaterrplus);
    hsyst = new TGraphAsymmErrors(numpoints,xval,yval,xerrminus,xerrplus,ysysterrminus,ysysterrplus);
    hsystCorr = new TGraphAsymmErrors(numpoints,xval,yval,xerrminus,xerrplus,ysystcorrerrminus,ysystcorrerrplus);

    double global = 11.0;
    int numpointsfull = 1;
    double xvalfull[] = {8.8};
    double xfullerrminus[] = {0.2};
    double xfullerrplus[] = {0.2};
    double yvalfull[] = {1.0};
    double ysystfullerrminus[] = {global/100.};
    double ysystfullerrplus[] = {global/100.};

    hsystFullCorr = new TGraphAsymmErrors(numpointsfull,xvalfull,yvalfull,xfullerrminus,xfullerrplus,ysystfullerrminus,ysystfullerrplus);

}

void CreateHistoFromDataPoints_PHENIX_CeHF_RpA(TGraphAsymmErrors* &hstat, TGraphAsymmErrors* &hsyst, TGraphAsymmErrors *&hsystCorr, TGraphAsymmErrors *&hsystFullCorr){

   // Similar to HepData format (can copy paste values from HepData)
   // Support asymmetric errors
   // Enter the number of points of your histogram, the X value, the lower X error, the upper X error, the Y value, the lower Y error (statistical),
   // the upper Y error (statistical), the lower Y error (uncorrelated systematic), the upper Y error (uncorrelated systematic), the lower Y error (correlated systematic),
   // the upper Y error (correlated systematic)
 
    int numpoints = 23;
    double xval[] = { 0.85, 0.95, 1.1, 1.3, 1.5, 1.7, 1.9, 2.1, 2.3, 2.5, 2.7, 2.9, 3.1, 3.3, 3.5, 3.7, 3.9, 4.25, 4.75, 5.5, 6.5, 7.5, 8.5 };
    double xerrminus[] = { 0.05, 0.05, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.25, 0.25, 0.5, 0.5, 0.5, 0.5 };
    double xerrplus[23];
    double yval[] = { 1.51281, 1.27293, 1.2279, 1.43924, 1.37722, 1.39184, 1.41118, 1.41135, 1.40522, 1.42563, 1.42324, 1.47637, 1.46578, 1.40123, 1.44638, 1.26929, 1.39004, 1.30279, 1.29398, 1.07704, 0.932836, 1.00065, 0.653463 };
    double ystaterrminus[] = { 0.282979, 0.220046, 0.152058, 0.23997, 0.261751, 0.0100855, 0.0126361, 0.0156074, 0.0191612, 0.0234, 0.0281478, 0.0341213, 0.0408781, 0.047267, 0.0560901, 0.0619366, 0.0748321, 0.0593097, 0.111913, 0.128413, 0.208142, 0.331864, 0.457035 };
    double ystaterrplus[23];
    double ysysterrminus[23];
    double ysysterrplus[23];
    double ysystcorrerrminus[]= { 0.371849, 0.267562, 0.217433, 0.246477, 0.205262, 0.189864, 0.186996, 0.180906, 0.175434, 0.180734, 0.230789, 0.226528, 0.225741, 0.212048, 0.213296, 0.185985, 0.203581, 0.188585, 0.186905, 0.163552, 0.137712, 0.168447, 0.116371 };
    double ysystcorrerrplus[23];
    for (int i=0; i<numpoints; i++) {
      xerrplus[i] = xerrminus[i];
      ystaterrplus[i] = ystaterrminus[i];
      ysystcorrerrplus[i] = ysystcorrerrminus[i];
    }

    hstat = new TGraphAsymmErrors(numpoints,xval,yval,xerrminus,xerrplus,ystaterrminus,ystaterrplus);
    hsyst = new TGraphAsymmErrors(numpoints,xval,yval,xerrminus,xerrplus,ysysterrminus,ysysterrplus);
    hsystCorr = new TGraphAsymmErrors(numpoints,xval,yval,xerrminus,xerrplus,ysystcorrerrminus,ysystcorrerrplus);

    double global = 11.0;
    int numpointsfull = 1;
    double xvalfull[] = {9.8};
    double xfullerrminus[] = {0.2};
    double xfullerrplus[] = {0.2};
    double yvalfull[] = {1.0};
    double ysystfullerrminus[] = {global/100.};
    double ysystfullerrplus[] = {global/100.};

    hsystFullCorr = new TGraphAsymmErrors(numpointsfull,xvalfull,yvalfull,xfullerrminus,xfullerrplus,ysystfullerrminus,ysystfullerrplus);

}

void CreateHistoFromDataPoints_PHENIX_FmuHF_RpA(TGraphAsymmErrors* &hstat, TGraphAsymmErrors* &hsyst, TGraphAsymmErrors *&hsystCorr, TGraphAsymmErrors *&hsystFullCorr){

   // Similar to HepData format (can copy paste values from HepData)
   // Support asymmetric errors
   // Enter the number of points of your histogram, the X value, the lower X error, the upper X error, the Y value, the lower Y error (statistical),
   // the upper Y error (statistical), the lower Y error (uncorrelated systematic), the upper Y error (uncorrelated systematic), the lower Y error (correlated systematic),
   // the upper Y error (correlated systematic)

    int numpoints = 12;
    double xval[] = { 1.125, 1.375, 1.625, 1.875, 2.125, 2.375, 2.625, 2.875, 3.250, 3.750, 4.500, 6.000 };
    double xerrminus[12];
    double xerrplus[12];
    double yval[] = { 0.539525, 0.670848, 0.692226, 0.837114, 0.856834, 0.840515, 0.921036, 0.781902, 0.777094, 0.917098, 0.914528, 0.503566 };
    double ystaterrminus[] = { 0.0279606, 0.0330657, 0.0373826, 0.0383232, 0.0495966, 0.0627771, 0.0878861, 0.0917625, 0.0859809, 0.1680740, 0.203528, 0.180839 };
    double ystaterrplus[12];
    double ysysterrminus[12];
    double ysysterrplus[12];
    double ysystcorrerrminus[] = { 0.102968, 0.127999, 0.13193, 0.168851, 0.172553, 0.185603, 0.197015, 0.188741, 0.18308, 0.227235, 0.232128, 0.130923 };
    double ysystcorrerrplus[12];
    for (int i=0; i<numpoints; i++) {
      xerrminus[i] = 0.1;
      xerrplus[i] = 0.1;
      ystaterrplus[i] = ystaterrminus[i];
      ysystcorrerrplus[i] = ysystcorrerrminus[i];
    }

    hstat = new TGraphAsymmErrors(numpoints,xval,yval,xerrminus,xerrplus,ystaterrminus,ystaterrplus);
    hsyst = new TGraphAsymmErrors(numpoints,xval,yval,xerrminus,xerrplus,ysysterrminus,ysysterrplus);
    hsystCorr = new TGraphAsymmErrors(numpoints,xval,yval,xerrminus,xerrplus,ysystcorrerrminus,ysystcorrerrplus);

    double global = 11.0;
    int numpointsfull = 1;
    double xvalfull[] = {8.8};
    double xfullerrminus[] = {0.2};
    double xfullerrplus[] = {0.2};
    double yvalfull[] = {1.0};
    double ysystfullerrminus[] = {global/100.};
    double ysystfullerrplus[] = {global/100.};

    hsystFullCorr = new TGraphAsymmErrors(numpointsfull,xvalfull,yvalfull,xfullerrminus,xfullerrplus,ysystfullerrminus,ysystfullerrplus);

}




