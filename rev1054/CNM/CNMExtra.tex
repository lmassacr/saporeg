%!TEX root = ../SaporeGravis.tex

%\subsection{CNM extrapolation to A--A collisions {\bf (Philippe)}} 

It is an important question to know whether cold nuclear matter effects can be simply extrapolated from \pA\ to \AAcoll\ collisions.
%, namely if the product of the nuclear modification factors at forward and backward rapidity in p--A collisions would give the predicted result for A--A collisions at the same energy.
Some of  the CNM effects discussed in \sect{CNM_theory}  can in principle be extrapolated to \AAcoll\ collisions. 
This is the case of  nPDF and coherent energy loss effects, discussed below. Some other approaches, on the contrary, are affected by interference effects between the two nuclei involved in the collision, making delicate an extrapolation to \AAcoll\ collisions.

\paragraph{Nuclear PDF}

Regarding the nPDF effects discussed in \sect{sec:npdf},
it is straightforward to make this comparison at leading order in the colour evaporation model (CEM) where the $\pt$ of the $\QQbar$ pair is zero and the $x_1$ and $x_2$ values are related to the quarkonium rapidity by  \eq{2-1_x-Bjorken}.  
As long as the production cross section obeys the factorisation hypothesis, \eq{cross-section-factorization}, the nuclear modification factors (taken at the same energy) also factorize, \ie the following relation  is exact,
\begin{equation}\label{CNMfactorization}
\raa^{\rm CNM}(y) = \rpa(+y) \cdot \rpa(-y)\ .
\end{equation}

At next-to-leading order in the CEM, however, the assumption of factorization of nPDF effects is less simple to understand because of the large contribution from $2 \rightarrow 2$ diagrams. For such processes, the correlation between the initial momentum fractions $x_1, x_2$ and the rapidity of the  quarkonium state is less straightforward.  
However, the factorisation hypothesis (see \eq{CNMfactorization}) is seen to still hold at NLO, as shown by a calculation using EPS09 NLO central nPDF set at $\snn = 2.76$ TeV in \fig{factpsi} as a function of $y$ (left) and $\pt$ (right)~\cite{RVogtinprogress}.  

%Note, however, that there is a significant difference in the shape of the nPDF ratios as a function of rapidity between the LO and NLO results.  
%This is due to the differences in the EPS09 LO and NLO sets themselves and gives some indication of the uncertainty inherent in the extraction of the nuclear gluon density.

In principle, this factorization hypothesis can also be applied to open heavy flavour.

\begin{figure}[!htp]
        \centering
%        \includegraphics[width=0.45\textwidth]{raa_y_lo.pdf}
        \includegraphics[width=0.45\textwidth]{raa_y_nlo.pdf} 
        \includegraphics[width=0.45\textwidth]{raa_pt.pdf}
        \caption{The $\jpsi$ $\raa$ (red) ratio is compared to the product $\rpa(+y) \cdot \rpa(-y)$ (points) along with the individual \pA\ ratios at forward (dashed) and backward (dot-dashed) rapidity.  
Results are compared for the $y$  (left) and $\pt$  (right) dependencies at NLO, from Ref.~\cite{RVogtinprogress}.}
        \label{factpsi}
\end{figure}

\paragraph{Multiple scattering and energy loss}

Let us first discuss how predictions can be extrapolated in the coherent energy loss model. In a generic A--B collision both incoming partons, respectively from the `projectile' nucleus A and the `target' nucleus B, might suffer multiple scattering in the nucleus B and A, respectively. Consequently, gluon radiation off both partons can interfere with that of the final state particle (here, the compact colour octet $\QQbar$ pair), making a priori difficult the calculation of the medium-induced gluon spectrum in the collision of two heavy ions.

However, it was shown in~\cite{Arleo:2014oha} that the gluon radiation induced by rescattering in nuclei A and B occurs in distinct regions of phase space (see \fig{fig-rap-regions}). As a consequence, the energy loss induced by the presence of each nucleus can be combined in a probabilistic manner, making a rather straightforward extrapolation of the model predictions from \pA\ to \AAcoll\ collisions. Remarkably, it is possible to show that the quarkonium suppression in \AAcoll\ collisions follows the factorisation hypothesis (see \eq{CNMfactorization}).
However, since the energy loss effects do not scale with the momentum fraction $x_2$, the data-driven extrapolation of \pPb\ data at $\snn=5$~TeV to \PbPb\ data at $\snn=2.76$~TeV, discussed below, and which assumes nPDF effects only is not expected to hold~\cite{Arleo:2014oha}.


%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[ht]
\centering
\includegraphics[width=11cm]{sketch_cohel}
\caption{Sketch of the rapidity regions populated by medium-induced radiation in an A--B collision in the coherent energy loss model. The `target' B and `projectile' A move with respectively negative and positive rapidities.}
\label{fig-rap-regions}
\end{figure} 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


The model by Sharma and Vitev can also easily be generalized to \AAcoll\ reactions where both incoming and outgoing partons undergo elastic, inelastic and coherent soft interactions in the large nuclei. 
%
In contrast, the Kopeliovich, Potashnikova and Schmidt approach for charmonium production cannot be simply extrapolated from  \pA\ to \AAcoll\ collisions~\cite{Kopeliovich:2010nw}, because nucleus-nucleus collisions include new effects of double colour filtering and a boosted saturation scale, as explained in detail in~\cite{Kopeliovich:2010nw}. 

\paragraph{Data-driven extrapolation}

At RHIC, the \dAu collisions are performed with symmetrical beam energies, so that $y_{\rm CM/lab} = 0$, and at the same nucleon-nucleon centre-of-mass energy than for heavy-ion collisions. 
The direct comparison of \dAu data to heavy-ion data is then easier. 
In this context, the PHENIX experiment has evaluated the \jpsi breakup (\ie absorption) cross section by fitting \rdau as a function of the rapidity, and also as a function of the average number of binary collisions (\Ncoll), and by assuming different shadowing scenarios (EKS and NDSG)~\cite{Adare:2007gn}. 
The two shadowing scenarios with their resulting breakup cross section were applied to \jpsi \raa, both for \CuCu and \AuAu collisions.
Moreover, an alternative data-driven method~\cite{GranierdeCassagnac:2007aj} was applied to PHENIX data~\cite{Adare:2007gn}. 
This method assumes that all cold nuclear matter effects are parametrized with a modification factor consisting of a function of the radial position in the nucleus.

A more recent investigation of RHIC data by Ferreiro et al.~\cite{Ferreiro:2009ur} showed how the use of $2 \to 2$ partonic process instead of the usual $2 \to 1$ can imply a different value of the absorption cross section~\cite{Rakotozafindrabe:2010su}, since the anti-shadowing peak is systematically shifted towards larger rapidities in \dAu. 
The other noticeable consequence is that \rdau versus $y$ is not symmetric anymore around $y \approx 0$. 
This implies that the CNM effects in \raa at RHIC will also show a rapidity dependence, with less suppression from CNM effects at mid rapidity than at forward rapidity, in the same direction as the one exhibited by the \AuAu and \CuCu data from PHENIX (see extensive comparisons in~\cite{Ferreiro:2009ur}). 
This is quite important since this shape of \raa at RHIC was also considered as a possible hint for hot in-medium recombination effects, while it might come from CNM effects only.

At LHC, the \pPb\ results can not be easily compared to \pb collisions.
Indeed, the nucleon-nucleon centre-of-mass energies are not the same (5.02 versus 2.76 TeV) and moreover the p and the Pb beam energies per nucleon are different, leading to a rapidity shift of the centre-of-mass frame with respect to the lab frame.
But assuming factorisation and \eq{CNMfactorization}, a data-driven extrapolation of \pA\ data to \AAcoll\ can be performed.

In a given detector acceptance (at fixed $y_{\rm lab}$), the ratio of $x_2$ values probed in a given process in \pb and \pPb collisions is  
\begin{eqnarray}\label{CNMextrapolation}
\frac{x_2^{\rm PbPb}}{x_2^{\rm pPb}} = \frac{\sqrt{s_{\rm NN}^{\rm pPb}}}{\sqrt{s_{\rm NN}^{\rm PbPb}}} \exp(-y_{\rm CM/lab}^{\rm pPb}) \, .
\end{eqnarray}
At the LHC, the rapidity shift is $y_{\rm CM/lab}^{\rm pPb} = 0.465$. In \RunOne conditions, one has $\sqrt{s_{\rm NN}^{\rm PbPb}} = 2.76$~TeV and $\sqrt{s_{\rm NN}^{\rm pPb}} = 5.02$~TeV and the ratio is $\frac{x_2^{\rm PbPb}}{x_2^{\rm pPb}} = 8\ {\rm TeV} / 7\ {\rm TeV}\simeq 1.14$. 
The typical momentum fraction ranges involved in \pPb collisions are shown in \fig{Fig-LHCx_range}.

This data-driven extrapolation of \pA\ collisions to \AAcoll\ collisions applied by the ALICE collaboration to $\jpsi$ production lead to~\cite{Abelev:2013yxa}: $[\rppb(2<y<3.5) \cdot \rppb(-4.5<y<-3)]^{\jpsi} = 0.75 \pm 0.10 \pm 0.12$, the first uncertainty being the quadratic combination of statistical and uncorrelated systematic uncertainties and the second one the linear combination of correlated uncertainties.
The application of this result to the interpretation of \PbPb data is discussed in \sect{sec:other_ref}.

%\begin{figure}[!htp]
%        \centering
%        \includegraphics[width=0.7\columnwidth]{LHCxBjorken2.pdf}
%        \caption{Nucleus $x$-Bjorken ($x_2$) ranges involve by $D$ mesons at mid rapidity ($|y_{\rm lab}| < 0.9$) and $\jpsi$ at forward rapidity ($2.5 < |y_{\rm lab}| < 4$) with $\pt < 10$~GeV/$c$ in \pPb collisions at $\snn=5.02$~TeV and \pb collisions at $\snn=2.76$ TeV.}
%        \label{Fig-LHCx_range2}  
%\end{figure}

%\paragraph{Conclusion}

In summary, according to the theoretical and data driven extrapolation approaches, one can conclude that there are non negligible CNM effects on \AAcoll\ results at the LHC (up to 50\% at low \pt).
A \pt dependence of \jpsi \rppb factorization will be presented in \sect{sec:npdf_aa}.



%Call first reference \cite{Andronic:2013zim}. Cite the first figure \ref{Fig1}. \\
%
%\begin{figure}[!htp]
%	\centering
%	\includegraphics[width=0.2\columnwidth]{LogosaporeGravis.jpg}
%	\caption{Caption of Figure 1.}
%	\label{Fig1}
%\end{figure}
