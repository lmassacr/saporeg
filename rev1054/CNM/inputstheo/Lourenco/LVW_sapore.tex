\documentclass[epj]{svjour}
\usepackage{graphics}
\usepackage{graphicx}
%
\begin{document}
%
\title{Energy Dependence of the $J/\psi$ Absorption Cross Section}
\author{C. Louren\c{c}o\inst{1}, R. Vogt{\inst{2}\inst{3}}, H. W\"{o}hri\inst{1}}
\institute{CERN, Geneva, Switzerland \and
Physics Division, Lawrence Livermore National Laboratory, Livermore, CA 94551, USA \and 
Physics Department, University of California, Davis, CA 95616, USA}
%
\date{Received: date / Revised version: date}
\maketitle

\section{Introduction}
\label{intro}

In proton-nucleus collisions, the charmonium production cross sections increase 
less than linearly with the number of binary nucleon-nucleon collisions.  This
``normal nuclear absorption'' needs to be well understood so that a
robust baseline reference can be established, with respect to which we
can clearly and unambiguously identify the signals of ``new physics''
specific to the high density QCD medium. In particular, it is important to
understand the energy dependence, if any, of the normal nuclear absorption.
In Ref.~\cite{Lourenco:2008sk}, the available fixed-target data were studied
to try and discern any energy dependence of the normal absorption, both with
and without considering nuclear modifications of the parton distributions (nPDFs).  

\begin{figure}[h!]
\begin{center}
\includegraphics[width=0.37\textwidth]{psiAdep_sps_3}
\includegraphics[width=0.42\textwidth]{sigmaAbs_sqrts_yCMS0_CTEQ6L-EKS98-power-law-phenix}
\end{center}
\caption{Top: Illustration of the interplay between nuclear modifications of the 
parton densities and final-state charmonium absorption~\protect\cite{Lourenco:2008sk}.
Bottom: Energy dependence of $\sigma_{\rm abs}^{J/\psi}$ at midrapidity,
% for power law (dashed), exponential (solid) and linear (dotted)
% approximations to $\sigma_{\rm abs}^{J/\psi}(y=0,\sqrt{s_{NN}})$ 
using EKS98-CTEQ61L NPDFs~\protect\cite{Eskola:1998iy,Eskola:1998df,
Pumplin:2002vw,Stump:2003yu}.}
\label{fig:LVWfig1}
\end{figure}

The absorption cross section, $\sigma_{\rm abs}^{J/\psi}$,
was traditionally assumed to be independent of the production kinematics
until measurements covering broad phase space regions
showed clear dependences of the nuclear effects on $x_F$ and $p_T$.  
It was further assumed to be independent of collision center-of-mass energy, 
$\sqrt{s_{NN}}$, simultaneously neglecting any nuclear effects on the
parton distributions. However, $J/\psi$ production is sensitive to
the gluon distribution in the nucleus and the fixed-target measurements
probe parton momentum fractions, $x$, in the antishadowing region.
This effect enhances the $J/\psi$ production rate at midrapidity and 
a larger absorption cross section is required to match the data, as shown 
in Fig.~\ref{fig:LVWfig1}-top
for $pA$ collisions at $E_{\rm lab}=400$~GeV, employing the color evaporation 
model~\cite{Gavai:1994in} to calculate the production cross sections and using 
the EKS98-MRST nPDFs~\cite{Eskola:1998iy,Eskola:1998df,Martin:2001es}.

If one focuses on the behavior of $J/\psi$ production at $x_F \approx 0$, the
absorption cross section is found to depend on $\sqrt{s_{NN}}$, essentially 
independent of the chosen parameterization of the nuclear parton 
densities~\cite{Lourenco:2008sk},
%This general result is robust although the details depend on the shadowing 
%parameterization, the proton parton densities chosen, and the details of the 
%production model.  For example, if the shadowing parameterization chosen 
%exhibits little antishadowing at moderate momentum fractions, $x \sim 1$, the 
%difference between the extracted cross section with and without shadowing is 
%small~\cite{Lourenco:2008sk}.  
as shown by the lines in Fig.~\ref{fig:LVWfig1}-bottom, 
determined~\cite{Lourenco:2008sk} from measurements by 
NA3~\cite{Badier:1983dg}, 
NA50~\cite{Alessandro:2006jt,Alessandro:2003pc}, E866~\cite{Leitch:1999ea} and 
HERA-B~\cite{Abt:2008ya}. 
% The vertical dotted line indicates the energy of the Pb+Pb and In+In collisions 
% at the CERN SPS.  The PHENIX point \protect\cite{} is also shown.
%
Subsequent measurements by NA60~\cite{Scomparin:2009tg} and
PHENIX~\cite{daSilva:2009yy}, respectively at $\sqrt{s_{NN}} = 16.8$~GeV
and $\sqrt{s_{NN}} = 200$~GeV, confirmed the decreasing trend of 
$\sigma_{\rm abs}(y_{\rm cms}=0)$ with energy, as shown in 
Fig.~\ref{fig:LVWfig1}-bottom.

Away from midrapidity, the extracted $\sigma_{\rm abs}^{J/\psi}$ grows with $x_F$, 
as shown in Fig.~\ref{fig:LVWfig2}, indicating that other mechanisms, such as 
initial-state energy loss, suppress $J/\psi$ production in the forward region 
($x_F > 0.25$), in addition to absorption and shadowing. 
It seems that the rise starts closer to $x_F = 0$ for lower collision 
energies~\cite{Brambilla:2010cs}.
More recent analyses~\cite{McGlinchey:2012bp}, using 
EPS09~\cite{Eskola:2009uj}, 
are in general agreement with the results of Ref.~\cite{Lourenco:2008sk}.

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.45\textwidth]{sigAbs_xF_EKS98} 
\end{center}
\caption{The $x_F$ dependence of $\sigma_{\rm abs}^{J/\psi}$,
determined~\protect\cite{Lourenco:2008sk} from fixed-target 
measurements~\protect\cite{Scomparin:2009tg,Badier:1983dg,Alessandro:2003pi,
Alessandro:2006jt,Leitch:1999ea,Abt:2008ya} and using the 
EKS98 nPDFs~\protect\cite{Eskola:1998iy,Eskola:1998df}.}
\label{fig:LVWfig2}
\end{figure}
%

The work of RV was performed under the auspices of the U.S.\
Department of Energy by Lawrence Livermore National Laboratory under
Contract DE-AC52-07NA27344 and  supported in part by the JET collaboration.
%
%\bibliographystyle{my_unsrt}
\bibliographystyle{}
\bibliography{LVW_sapore}

\end{document}
