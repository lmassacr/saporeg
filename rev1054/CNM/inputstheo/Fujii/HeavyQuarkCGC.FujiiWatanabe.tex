\documentclass{article}

\textwidth=16.5cm \oddsidemargin=0cm
\textheight=23cm \topmargin=0cm


\usepackage[final]{graphics}
\usepackage{amsmath}
\usepackage{amsfonts,amsbsy}
\usepackage[psamsfonts]{amssymb}
\usepackage{graphicx}
\usepackage{color}
\usepackage{setspace}
\usepackage{cite}

\begin{document}

\section{CGC approach to heavy quark production in pA}

We compute heavy quark production cross section in high energy pA collisions
in the Color Glass Condensate (CGC) framework\cite{FujiiWatanabe},
which is given at the leading order (LO) 
in the strong coupling constant $\alpha_s$,
but includes multiple-scattering effects 
on the gluons and heavy quarks by the dense target~\cite{QuarkFromCGC}.
It is expressed in terms of hard matrix elements, 
2-point gluon function in the dilute projectile and 
multi-point gluon functions in the dense target, 
which breaks the $k_\perp$-factorization~\cite{Fujii:2005vj}.
The energy dependence in this approach
is incorporated through the gluon functions
which obey the nonlinear $x$-evolution equation
leading to the gluon saturation phenomenon.


In the large-$N_c$ approximation,
the multi-point functions reduce to a product of two dipole
amplitudes in the fundamental representation 
and the evolution equation for the dipole has a closed form,
called the Balitsky-Kovchegov (BK) equation.
The BK equation with running coupling corrections (rcBK) is today 
widely exploited for phenomenological studies of
saturation, and its numerical solution for $x<x_0=0.01$
is constrained with HERA DIS data and has been applied 
to hadronic reactions successfully~\cite{AAMQSetc}.
Nuclear dependence is taken into account 
here in the initial condition for the rcBK equation 
by setting larger initial saturation scales, $Q_{sA}^2(x_0)$, 
depending on the nuclear thickness.


We evaluate heavy quark production applying the CGC framework
in the large-$N_c$ approximation with 
the numerical solution of the rcBK equation~\cite{FujiiWatanabe}.
In hadronization processes, we use the color evaporation model (CEM) for
$J/psi$ ($\Upsilon$) and the vacuum fragmentation function for $D$ meson
production, presuming that the hadronization occurs 
outside the target.




We reproduced the rapidity dependence of the nuclear 
modification factor $R_\text{pA}(y)$ of
J/$\psi$ at RHIC by setting $Q_{sA}^2(x_0)=(4 - 6) Q_{pA}^2(x_0)$, 
and predicted a stronger suppression at the LHC,
which reflects the stronger saturation effects at the smaller
$x$ values~(Fig.\ref{fig:HQ-Quarkonium-RpA-y}).  
Quarkonium suppression in this framework also 
includes the multiple scattering effects 
on the quark pair traversing the dense target.
The LHC data for $R_\text{pA}(y)$ of $J/\psi$
shows weaker suppression than our value.
On the other hand, 
our result of $R_\text{pA}(p_T)$ for $D$ meson at mid rapidity is 
found to be compatible with the LHC data.


The CGC expression for the heavy quark production is derived at LO 
in the eikonal approximation for the clor sources.
Then the NLO extension should be investigated to be consistent with the
use of the rcBK equation.
Furthermore, for quarkonium production,
color channel dependence of the hadronization process 
will be important and brings in a new multi-point function,
which is simply ignore in CEM.




\begin{figure}[tbp]
\begin{center}
\resizebox*{!}{5.5cm}
{\includegraphics[angle=-90]{Fig-CGC-RpA.eps}}
\end{center}
\caption{ Nuclear modification factor 
$R_\text{pA}(y)$ for $D$ and J/$\psi$  
in pA collisions at $\sqrt{s}=5.02$ TeV.
The bands indicate the uncertainties from the variations $m_c=1.2 - 1.5$ GeV 
and $Q_{s0,A}^2= (4-6) Q_{s0,p}^2$.
}
\label{fig:HQ-Quarkonium-RpA-y}
\end{figure}






\thebibliography{*}
%\cite{Fujii:2013gxa}
%\bibitem{Fujii:2013gxa} 
\bibitem{FujiiWatanabe} 
  H.~Fujii and K.~Watanabe,
  %``Heavy quark pair production in high energy pA collisions: Quarkonium,''
  Nucl.\ Phys.\ A {\bf 915}, 1 (2013)
  [arXiv:1304.2221 [hep-ph]];
  %%CITATION = ARXIV:1304.2221;%%
  %14 citations counted in INSPIRE as of 21 Jun 2014
%\cite{Fujii:2013yja}
%\bibitem{Fujii:2013yja} 
%  H.~Fujii and K.~Watanabe,
  %``Heavy quark pair production in high energy pA collisions: Open heavy flavors,''
  Nucl.\ Phys.\ A {\bf 920}, 78 (2013)
  [arXiv:1308.1258 [hep-ph]].
  %%CITATION = ARXIV:1308.1258;%%
  %9 citations counted in INSPIRE as of 21 Jun 2014



%\cite{Blaizot:2004wv}
%\bibitem{Blaizot:2004wv} 
\bibitem{QuarkFromCGC} 
  J.~P.~Blaizot, F.~Gelis and R.~Venugopalan,
  %``High-energy pA collisions in the color glass condensate approach. 2. Quark production,''
  Nucl.\ Phys.\ A {\bf 743}, 57 (2004)
  [hep-ph/0402257].
  %%CITATION = HEP-PH/0402257;%%
  %143 citations counted in INSPIRE as of 21 Jun 2014
%\cite{Fujii:2005vj}
\bibitem{Fujii:2005vj} 
  H.~Fujii, F.~Gelis and R.~Venugopalan,
  %``Quantitative study of the violation of k-perpendicular-factorization in hadroproduction of quarks at collider energies,''
  Phys.\ Rev.\ Lett.\  {\bf 95}, 162002 (2005)
  [hep-ph/0504047];
  %%CITATION = HEP-PH/0504047;%%
  %43 citations counted in INSPIRE as of 21 Jun 2014
%\cite{Fujii:2006ab}
%\bibitem{Fujii:2006ab} 
%  H.~Fujii, F.~Gelis and R.~Venugopalan,
  %``Quark pair production in high energy pA collisions: General features,''
  Nucl.\ Phys.\ A {\bf 780}, 146 (2006)
  [hep-ph/0603099].
  %%CITATION = HEP-PH/0603099;%%
  %40 citations counted in INSPIRE as of 21 Jun 2014




%\cite{Albacete:2009fh}
%\bibitem{Albacete:2009fh} 
\bibitem{AAMQSetc} 
J.~L.~Albacete et al.
%J.~L.~Albacete, N.~Armesto, J.~G.~Milhano and C.~A.~Salgado,
  %``Non-linear QCD meets data: A Global analysis of lepton-proton scattering with running coupling BK evolution,''
  Phys.\ Rev.\ D {\bf 80}, 034031 (2009)
  [arXiv:0902.1112 [hep-ph]];
  %%CITATION = ARXIV:0902.1112;%%
  %115 citations counted in INSPIRE as of 21 Jun 2014
%\cite{Albacete:2010sy}
%\bibitem{Albacete:2010sy} 
%  J.~L.~Albacete, N.~Armesto, J.~G.~Milhano, P.~Quiroga-Arias and C.~A.~Salgado,
  %``AAMQS: A non-linear QCD analysis of new HERA data at small-x including heavy quarks,''
  Eur.\ Phys.\ J.\ C {\bf 71}, 1705 (2011)
  [arXiv:1012.4408 [hep-ph]].
  %%CITATION = ARXIV:1012.4408;%%
  %81 citations counted in INSPIRE as of 21 Jun 2014
%\cite{Albacete:2012xq}
%\bibitem{Albacete:2012xq} 
  J.~L.~Albacete, A.~Dumitru, H.~Fujii and Y.~Nara,
  %``CGC predictions for p+Pb collisions at the LHC,''
  Nucl.\ Phys.\ A {\bf 897}, 1 (2013)
  [arXiv:1209.2001 [hep-ph]].
  %%CITATION = ARXIV:1209.2001;%%
  %46 citations counted in INSPIRE as of 21 Jun 2014
%\cite{Lappi:2013zma}
%\bibitem{Lappi:2013zma} 
  T.~Lappi and H.~Mntysaari,
  %``Single inclusive particle production at high energy from HERA data to proton-nucleus collisions,''
  Phys.\ Rev.\ D {\bf 88}, 114020 (2013)
  [arXiv:1309.6963 [hep-ph]].
  %%CITATION = ARXIV:1309.6963;%%
  %13 citations counted in INSPIRE as of 22 Jun 2014



\end{document}
