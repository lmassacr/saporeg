%!TEX root = CNM.tex

%\subsection{Heavy-flavours in \pA\ collisions}

Open and hidden heavy flavour production constitutes a very sensitive probe of medium effects because heavy quarks are produced in hard processes in the early stage of the nucleus--nucleus collision.
Open and hidden heavy-flavour production can be affected by the following CNM effects:
\begin{itemize}
\item {\it Modification of the effective partonic luminosity} in colliding nuclei, with respect to colliding protons. This effect is due to the different dynamics of partons within free nucleons (\eg protons) with respect to partons in nucleons that are bound in a nucleus, mainly as a consequence of the larger resulting density of partons per unit area in the plane transverse to the beam direction. 
The type and size of these effects depend on Bjorken-$x$ and on the scale of the parton--parton interaction $Q^2$ (the square of the momentum transfer). The largest effect occurs at small $x$ and small $Q^2$
and it goes under the generic name of {\it parton saturation}: the partonic phase space becomes saturated, as a consequence of the rapid increase of the PDFs at small $x$ and $Q^2$, 
and the merging of small-$x$ partons results in an effective reduction of the parton luminosity available for scattering processes. 
This phenomenology is often described by introducing additional terms in 
the parton density evolution equations (be it in $Q^2$ or in $x$) that are non-linear (namely, quadratic) in the parton densities (\eg marging of two gluons to a single, larger-$x$, gluon).
In the next two items, we describe two theoretical approaches for the description of this modification of the parton dynamics.
\item In the collinear factorisation approach to perturbative QCD calculations the nuclear effects on the parton dynamics are described in terms of nuclear-modified PDFs (hereafter indicated as nPDF for brevity). Quite schematically three regimes can be identified for the nPDF to PDF ratio of parton flavour $i$, $R_i$, depending on the values of the momentum fraction $x$ carried by the parton: a depletion ($R_i<1$) --~often referred to as \emph{shadowing}~-- at small $x \lesssim 10^{-2}$, a possible enhancement $R_i>1$ (\emph{anti-shadowing}) at intermediate values of $x$ ($10^{-2} \lesssim x \lesssim 10^{-1}$), and the EMC effect, a depletion taking place at large $x$ ($x \gtrsim 10^{-1}$). In general, these effects are largest for small values of $Q^2$ and tend to vanish when $Q^2$ becomes very large.
The nuclear-modified PDFs are parametrised through $x$- and $Q^2$-dependent 
$R_i$ factors that multiply the standard proton PDFs. 
The $R_i$-factor sets are determined from a global fit analyses of lepton--nucleus and proton--nucleus data (see \sect{sec:npdf}).
\item Another approach to describe the physics of parton saturation at small $x$ is the Colour Glass Condensate (CGC) theoretical framework. Unlike the nPDF approach, which uses DGLAP linear evolution equations, the CGC framework is based on the Balitsky-Kovchegov or JIMWLK non-linear evolution equations (see \sect{sec:saturation}). 
\item {\it Multiple scattering of partons} in the nucleus before and/or after the hard scattering, leading to parton energy loss (either radiative or collisional) and transverse momentum broadening (known as the Cronin effect). In most approaches (see \sect{sec:eloss}) it is characterized by the transport coefficient of cold nuclear matter, $\hat{q}$.
\item Final-state inelastic interaction, or {\it nuclear absorption}, of \QQbar bound states when passing through the nucleus.  
%and not the open heavy flavour rate\footnote{The fraction of heavy quark pairs leading to  quarkonium production is small, of the order of 1\%.}. 
The important parameter of these calculations is the ``absorption'' (or break-up)  cross section \sabs, namely the inelastic cross section of a heavy-quarkonium state with a nucleon. 
\item On top of the above genuine CNM effects, the large set of particles (partons or hadrons) produced in \pA\ collisions at high energy may be responsible for a modification of open heavy flavour or quarkonium production. It is still highly-debated whether this set of particles could form a ``medium" with some degree of collectivity. If this was the case, this medium could impart a flow to heavy-flavour hadrons.
Moreover, heavy quarkonia can be dissociated by  {\it comovers}, \ie, the partons or hadrons produced in the collision in the vicinity of the heavy-quarkonium state (see \sect{sec:absorption}).
%The dissociation by these so-called {\it comovers} is expected to be large in heavy-ion collisions and possibly in \pA\ collisions at high energy, \eg, at LHC.
\end{itemize}
%The three first effects are sometime refered as initial state effects, while the last one, which concerns only quarkonia (and then marginally open heavy flavours~\footnote{Assuming that the fraction of heavy quark pairs giving quarkonia is small $\approx$ 1 to 2 \%.}), is called a final state effect.

%With the convention 1 = p-beam and 2 = nucleon in the nucleus, 
Assuming factorisation, and neglecting isospin effects, the hadroproduction cross section of a heavy-quark pair \QQbar is given by
% proportional to the parton distribution functions $f(x,Q^2)$: 
\begin{eqnarray}
\label{cross-section-factorization}
%\sigma_{\pA \to \QQbar}(\snn) = \sum_{i,j} \int dx_1\,dx_2\,f_i^{\rm p}(x_1,\mu_F^2)\,f_j^{\rm p/A}(x_2,\mu_F^2)\,\hat{\sigma}_{ij\to\QQbar}(x_1\ x_2\ s_{\rm NN},\mu_F^2,\mu_R^2) \, ,
\sigma_{{\rm pA}\to \QQbar}(\snn) = A \sum_{i,j} \int \dd x_i\,\dd x_j\,f_i^{\rm N}(x_i,\mu_F^2)\,f_j^{\rm N}(x_j,\mu_F^2)\,\hat{\sigma}_{ij\to\QQbar}(x_i, x_j, \snn, \mu_F^2,\mu_R^2) \, ,
\end{eqnarray}
%where $f_i^{\rm p}$ (respectively, $f_j^{\rm p/A}$) are the proton (respectively, nuclear) parton
where $f_i^{\rm N}$ are the nucleon parton distributions, $i$ ($j$) denotes all possible partons in the proton (nucleus) carrying a fraction $x_i$ ($x_j$) of the nucleon momentum, %also called $x$-Bjorken variable, 
$\hat{\sigma}_{ij\to\QQbar}$ is the partonic cross section, $\snn$ is the nucleon--nucleon centre-of-mass energy of the collision, and $\mu_F$ ($\mu_R$) is the factorization (renormalization) scale of the process.
In high energy hadron collisions (especially at RHIC and LHC), heavy-quarks are mainly produced by gluon fusion~\cite{Mangano:1997ri}.

For a $2 \to 1$ partonic process giving a particle of mass $m$, at leading order there is a direct correspondence between the momentum fractions and the rapidity $y$ of the outgoing particle in the nucleon--nucleon centre-of-mass (CM) frame,
\begin{eqnarray}
\label{2-1_x-Bjorken}
x_1 = \frac{m}{\snn}\exp(y) & \text{and} & x_2 = \frac{m}{\snn}\exp(-y)\,,
\end{eqnarray}
where we have indicated as $x_2$ the smallest of the two $x$ values probed in the colliding nucleons.
For a $2 \to 2$ partonic process, the extra degree of freedom coming from the transverse momentum results 
%are more degrees of freedom in the kinematics resulting
 in a less direct correspondence leading to the following useful relations
%\begin{eqnarray}
%\label{2-2_x-Bjorken}
%x_2\ = \ \frac{x_1\ m_{\rm T}\ \snn \exp(-y) - m^2}{\snn\ [x_1 \snn - m_{\rm T} \exp(y)]}, & &
%\end{eqnarray}
%From those expressions, we can derive the following useful relations for heavy-flavour probes
\begin{eqnarray}
\label{OHF_x-Bjorken}
\text{open heavy-flavour (D and B mesons...)} & & x_2 \approx \frac{2 \mt}{\snn}\exp(-y), \\
\label{QQ_x-Bjorken}
\text{quarkonia ($\jpsi$, $\Upsilon$...)} & & x_2 \approx \frac{\mt+\pt}{\snn}\exp(-y).
\end{eqnarray}
where $\mt = \sqrt{m^2+\pt^2}$ is the transverse mass of the outgoing particle of mass $m$, transverse momentum $\pt$ and rapidity $\ycm$ in the centre-of-mass frame.
So, the typical resolution scale should be �of the order� of the transverse mass of the particle produced.
%Note that at leading order, the CEM involves $2\to 1$ partonic subprocesses for quarkonium production, while the LO CSM  --~which requires the emission of a perturbative gluon for direct1S quarkonium production~-- 

The typical range for the momentum fractions probed is therefore a function of both the acceptance of the detector (rapidity coverage), and the nature of the particles produced and their associated energy scale.
Moreover, assuming different underlying partonic production processes can end up in average values of $x$ that can differ from one another.

% type of particle produced, via $\mt$. 
%So, the quantification of nPDF required to study particle production on a wide rapidity and mass ranges in p--A.
%The saturation predicted by CGC is a similar effect and can be reached in the same way.
%Multiple scaterring of in-coming parton through the nucleus before hard scattering leads to a modification of transverse momentum ($\pt$) spectra: a broadening of $\pt$ distribution for Cronin effect and a $\pt$ suppression for energy loss.
%As for nPDF, this initial state effect is expected to be the same for same $Q^2$, \ie for particles very closed in mass.
%The nuclear absorption of quarkonium states depends on formation time (\ie energy) and nucleus size ($A$). 

%Different probes and observables are sensitive to various CNM effects.
%In order to disentangle these effects for a reliable nucleus--nucleus data interpretation, \pA\ collisions must be performed at the same energy (\snn) and in the same kinematical configuration than the one of nucleus--nucleus collisions with a large acceptance, for wide variety of particles and over the full $\pt$ spectrum.

Studies of \pA\ collisions since 1980 were first performed on fixed-target experiments at SPS, Tevatron and HERA, and more recently at colliders, RHIC and LHC.
Current available data are summarised in \tab{Tab-pA_data_collider} for collider experiments and in \tab{Tab-pA_data_fixedtarget} for fixed-target experiments.
This section is focused on the most recent results from the RHIC and LHC experiments, and their theoretical interpretation.

\begin{table}[!t]
\centering
\caption{Available \pA\ data in collider: the probes, the colliding system, $\snn$, the kinematic range (with $\ycm$ the rapidity in the centre-of-mass frame), the observables (as a function of variables)are given as well as the references.}
\label{Tab-pA_data_collider}
\begin{tabular}{p{0.12\textwidth}p{0.10\textwidth}p{0.07\textwidth}p{0.21\textwidth}p{0.30\textwidth}p{0.10\textwidth}}
\hline
Probes & Colliding system & \snn (TeV) & \ycm & Observables (variables) & Ref. \\
\hline
& & & PHENIX & & \\
\hline
\hfe & \dAu & 0.2 & $|\ycm|<0.35$ & \rdau(\pt,\Ncoll), $\langle\pt^2\rangle$ & \cite{Adare:2012yxa} \\
\hfm & & & $1.4<|\ycm|<2$ & \rdau(\Ncoll,\pt) & \cite{Adare:2013lkk} \\
\bbbar & & & $|\ycm|<0.5$ & $\sigma(\ycm)$ & \cite{Adare:2014iwg} \\
$e^\pm,\mu^\pm$ & & & $|\ycm|<0.5$ \& $1.4<\ycm<2.1$ & $\Delta\phi$, $J_{\rm dAu}$ & \cite{Adare:2013xlp} \\
\jpsi & & & $-2.2<\ycm<2.4$ & \rdau, \rcp(\Ncoll,\ycm,$x_2$,\xf,\pt), $\alpha$ & \cite{Adler:2005ph,Adare:2007gn,Adare:2010fn} \\
& & & $-2.2<\ycm<2.2$ & \rdau(\pt,\ycm,\Ncoll), $\langle\pt^2\rangle$ & \cite{Adare:2012qf} \\
\jpsi, \psiP, \chic & & & $|\ycm|<0.35$ & \rdau(\Ncoll), double ratio & \cite{Adare:2013ezl} \\
\ups & & & $1.2<|\ycm|<2.2$ & \rdau(\ycm,$x_2$,\xf), $\alpha$ & \cite{Adare:2012bv} \\
\hline
& & & STAR & & \\
\hline
\Dzero, \hfe & \dAu & 0.2 & $|\ycm|<1$ & yield(\ycm,\pt) & \cite{Adams:2004fc} \\
\ups & & & $|\ycm|<1$ & $\sigma$, \rdau(\ycm,\xf), $\alpha$ & \cite{Adamczyk:2013poh} \\
\hline
& & & ALICE & & \\
\hline
$D$ & \pPb & 5.02 & $-0.96<\ycm<0.04$ & $\sigma$, \rppb(\pt,\ycm) & \cite{Abelev:2014hha} \\
\jpsi & & & $-4.96<\ycm<-2.96$ \& $2.03<\ycm<3.53$ & $\sigma$, \rppb(\ycm), \rfb & \cite{Abelev:2013yxa} \\
\jpsi, \psiP & & & & $\sigma$, \rppb(\ycm,\pt), double ratio & \cite{Abelev:2014zpa} \\ 
\jpsi & & & \& $-1.37<\ycm<0.43$ & $\sigma$(\ycm,\pt), \rppb(\ycm,\pt), [\rppb(+\ycm)$\cdot$\rppb(-\ycm)] (\pt) & \cite{Adam:2015iga} \\ 
\upsa, \upsb & & & & $\sigma$, \rppb(\ycm), \rfb, ratio & \cite{Abelev:2014oea} \\
\hline
& & & CMS & & \\
\hline
\upsn & \pPb & 5.02 & $|\ycm|<1.93$ & double ratio ($E_{\rm T}^{\eta>4},N_{\rm tracks}^{|\eta|<2.4})$ & \cite{Chatrchyan:2013nza} \\
\hline
& & & LHCb & & \\
\hline
\jpsi (from B) & \pPb & 5.02 & $-5.0<\ycm<-2.5$ \& $1.5<y<4.0$ & $\sigma(\pt,\ycm)$, \rppb(\ycm), \rfb(\ycm,\pt) & \cite{Aaij:2013zxa} \\
\upsn & & & & $\sigma(\ycm)$, ratio(\ycm), \rppb(\ycm), \rfb & \cite{Aaij:2014mza} \\
\hline
\end{tabular}
\end{table}

\begin{table}[!htp]
\centering
\caption{Available \pA\ data in fixed target: the probes, the target, $\snn$, the kinematic range (with $\ycm$ the rapidity in the centre-of-mass frame), the observables (as a function of variables) are given as well as the references. 
The flag ($\dagger$) means that a cut on $|\cos\theta_{\rm CS}|<0.5$ is applied in the analysis, where $\theta_{\rm CS}$ is the decay muon angle in the Collins-Soper frame.
Feynman-$x$ variable $\xf=\frac{2p_{\rm L,CM}}{\sqrt{s_{\rm NN}}}$, where $p_{\rm L,CM}$ is the longitudinal momentum of the partonic system in the CM frame, is connected to the momentum fraction variables by $\xf\approx x_1-x_2$, in the limit $p_{\rm T} \ll p$.}
\label{Tab-pA_data_fixedtarget}
\begin{tabular}{p{0.13\textwidth}p{0.14\textwidth}p{0.10\textwidth}p{0.19\textwidth}p{0.23\textwidth}p{0.09\textwidth}}
\hline
Probes & Target & \snn (GeV) & \ycm (or \ylab or \xf) & Observables (variables) & Ref. \\
\hline
& & & NA3 & & \\
\hline
\jpsi & H$_2$, Pt & 16.8 - 27.4 & $0<\xf<0.9$ & $\sigma(\xf,\pt)$ & \cite{Badier:1983dg,Badier:1985ri} \\
\hline
& & & NA38 & & \\
\hline
\jpsi, \psiP & Cu, U & 19.4 & $2.8<\ylab<4.1$ & $\sigma(E_{\rm T},A)$, $\langle \pt^{(2)}\rangle(\epsilon)$, ratio($\epsilon,E_{\rm T},A,L)$ & \cite{Baglin:1991gy,Baglin:1994ui,Baglin:1991vb} \\
\jpsi, \psiP, \ccbar & W & 19.4 & $3<\ylab<4$ & ratio($\epsilon$), $\sigma_{\ccbar}(p_{\rm lab})$ & \cite{Lourenco:1993mr} \\
\jpsi, \psiP & C, Al, Cu, W & 29.1 & $-0.4<\ycm<0.6$ ($\dagger$) & $\sigma(A)$ and ratio$(A)$ & \cite{Abreu:1998ee} \\
\jpsi, \psiP, DY & O, S & 19.4 (29.1) & $0(-0.4)<\ycm<1(0.6)$ & $\sigma(A,L)$, ratio($A,L$) & \cite{Abreu:1999nn} \\
\hline
& & & NA38/NA50 & & \\
\hline
\ccbar & Al, Cu, Ag, W & 29.1 & $-0.52<\ycm<0.48$ ($\dagger$) & $\sigma_{\ccbar}$ & \cite{Abreu:2000nj} \\
\hline
& & & NA50 & & \\
\hline
\jpsi, \psiP, DY & Be, Al, Cu, Ag, W & 29.1 & $-0.4<\ycm<0.6$ & $\sigma(A)$, ratio$(A,E_{\rm T},L)$, $\sabs$ & \cite{Alessandro:2003pi,Alessandro:2006jt} \\
\jpsi, \psiP & & & $-0.1<\xf<0.1$ ($\dagger$) & $\sigma(A,L)$, \sabs($\xf$) & \cite{Alessandro:2003pc} \\
\ups, DY & & & $-0.5<\ycm<0.5$ ($\dagger$) & $\sigma(A)$, $\langle\pt^2\rangle(L)$, $\langle\pt\rangle$ & \cite{Alessandro:2006dc} \\
\hline
& & & NA60 & & \\
\hline
\jpsi & Be, Al, Cu, In, W, Pb, U & 17.3, 27.5 & $3.2<\ylab<3.7$ ($\dagger$) & $\sigma$, \sabs, ratio($L$), $\alpha(\xf,x_2)$ & \cite{Arnaldi:2010ky} \\
\hline
& & & E772 & & \\
\hline
\jpsi,\psiP & H$_2$, C, Ca, W & 38.8 & $0.1<\xf<0.7$ & ratio($A$,\xf,\pt), $\alpha(\xf,x_2,\pt)$ & \cite{Alde:1990wa} \\
\ups & & & $-0.15<\xf<0.5$ & $\sigma(\pt,\xf)$, ratio($A$), $\alpha(\xf,x_2,\pt)$ & \cite{Alde:1991sw} \\
\hline
& & & E789 & & \\
\hline
\Dzero & Be, Au & 38.8 & $0<\xf<0.08$ & $\sigma(\pt)$, $\alpha(\xf,\pt)$, ratio & \cite{Leitch:1994vc} \\
\bbbar & & & $0<\xf<0.1$ & $\sigma(\xf,\pt)$ & \cite{Jansen:1994bz} \\
\jpsi & Be, Cu & 38.8 & $0.3<\xf<0.95$ & $\sigma(\xf)$, $\alpha(\xf)$ & \cite{Kowitt:1993ns} \\
\jpsi,\psiP & Be, Au & & $-0.03<\xf<0.15$ & $\sigma(\pt,\xf,y)$, ratio(\pt,\xf) & \cite{Schub:1995pu} \\
\jpsi & Be, C, W & & $-0.1<\xf<0.1$ & $\alpha$ (\xf,$x_{\rm target}$,\pt) & \cite{Leitch:1995yc} \\
\hline
& & & E866/NuSea & & \\
\hline
\jpsi, \psiP & Be, Fe, W & 38.8 & $-0.1<\xf<0.93$ & $\alpha$ (\pt,\xf) & \cite{Leitch:1999ea} \\
\jpsi & Cu & & $0.3<\xf<0.9$ & $\lambda_\theta(\pt,\xf)$ & \cite{Chang:2003rz} \\
\upsn, DY &  & & $0<\xf<0.6$ & $\lambda_\theta(\pt,\xf)$ & \cite{Brown:2000bz} \\
\hline
& & & HERA-B & & \\
\hline
D & C, Ti, W & 41.6 & $-0.15<\xf<0.05$ & $\sigma(\xf,\pt^2)$ & \cite{Abt:2007zg} \\
\bbbar, \jpsi & & & $-0.35<\xf<0.15$ & $\sigma$, ratio & \cite{Abt:2005qs} \\
\bbbar & & & $-0.3<\xf<0.15$ & $\sigma$ & \cite{Abt:2006xa} \\
\bbbar & C, Ti & & $-0.25<\xf<0.15$ & $\sigma$ & \cite{Abt:2002rd} \\ 
\jpsi & C, Ti, W & & $-0.225<\xf<0.075$ & $\sigma(A,\ycm)$ & \cite{Abt:2005qr} \\
& & & $-0.34<\xf<0.14$ & $\langle\pt^2\rangle(A)$, $\alpha(\pt,\xf)$ & \cite{Abt:2008ya} \\
& C, W & & $-0.34<\xf<0.14$ & $\lambda_\theta,\lambda_\phi,\lambda_{\theta\phi}(\pt,\xf)$ & \cite{Abt:2009nu}\\
\jpsi, \psiP & C, Ti, W & & $-0.35<\xf<0.1$ & ratio$(\xf,\pt,A)$, $\alpha^\prime$-$\alpha$(\xf) & \cite{Abt:2006va} \\
\jpsi, \chic & & & & ratio(\xf,\pt) & \cite{Abt:2008ed} \\
\ups & C, Ti, W & & $-0.6<\xf<0.15$ & $\sigma(\ycm)$ & \cite{Abt:2006wc} \\
\hline
\end{tabular}
\end{table}

In LHC \RunOne p--Pb collisions, protons have an energy of 4 TeV and the Pb nuclei an energy $Z/A(4 {\rm TeV})=1.58$~TeV ($Z=82$, $A=208$), leading to $\snn=5.02$~TeV and a relative velocity of the CM with respect to the laboratory frame $\beta=0.435$ in the direction of the proton beam.
The rapidity of any particle in the CM frame is thus shifted, $y=\ylab-0.465$.
% where $\ylab=\frac{1}{2}\ln\left(\frac{E+p_{\rm L}}{E-p_{\rm L}}\right)$ is the rapidity measured by the experiment in the laboratory frame ($E$ and $p_{\rm L}$ are the energy and the longitudinal momentum w.r.t. z-axis, respectively).
Applying those experimental conditions to heavy-flavour probes such as D and B mesons and quarkonia, and according to \eq{OHF_x-Bjorken} and~\eq{QQ_x-Bjorken}, leads to a large coverage of $x_2$ from $10^{-5}$ for the D meson at forward rapidity, to 0.5 for 10~GeV/$c$ $\Upsilon$ at backward rapidity, as reported in \fig{Fig-LHCx_range}.

\begin{figure}[!tp]
        \centering
        \includegraphics[width=0.55\columnwidth]{LHCxBjorken.pdf}
        \caption{Accessible $x_2$ and $m_{\rm T}$  range at the LHC ($|\ylab| < 4.5$) in p--Pb collisions at $\snn=5.02$ TeV
%         as a function of the transverse mass of
for         different heavy-flavour probes (D and B mesons, $\jpsi$ and $\Upsilon$) with $0<\pt < 10$~GeV/$c$.}
%The corresponding range for $\jpsi$ production in Pb--Pb collisions at $\snn=2.76$ TeV is also indicated for comparison to current p--Pb range.}
        \label{Fig-LHCx_range}  
\end{figure}

%

%Call first reference \cite{Andronic:2013zim}. Cite the first figure \ref{Fig1}. \\
%
%\begin{figure}[!htp]
%	\centering
%	\includegraphics[width=0.2\columnwidth]{LogosaporeGravis.jpg}
%	\caption{Caption of Figure 1.}
%	\label{Fig1}
%\end{figure}
