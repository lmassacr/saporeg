\begingroup \small 
\contentsline {section}{\numberline {1}Introduction}{2}{section.1}
\contentsline {section}{\numberline {2}$pp$ section}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Production mechanisms of open and closed heavy-flavour in $pp$ collisions}{3}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Open heavy-flavour production (IS)}{3}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Quarkonium-production mechanism (JPL)}{3}{subsubsection.2.1.2}
\contentsline {paragraph}{The Colour-Evaporation Model (CEM)}{3}{section*.2}
\contentsline {paragraph}{The Colour-Singlet Model (CSM)}{3}{section*.3}
\contentsline {paragraph}{The Colour-Octet Mechanism (COM) and NRQCD}{4}{section*.4}
\contentsline {subsection}{\numberline {2.2}Experimental cross-section measurements in $pp$ collisions}{4}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Open charm}{4}{subsubsection.2.2.1}
\contentsline {paragraph}{Charmed mesons}{4}{section*.5}
\contentsline {paragraph}{Charmed baryons}{4}{section*.6}
\contentsline {paragraph}{Leptons}{4}{section*.7}
\contentsline {subsubsection}{\numberline {2.2.2}Open beauty}{4}{subsubsection.2.2.2}
\contentsline {paragraph}{Beauty mesons}{4}{section*.8}
\contentsline {paragraph}{Beauty baryons}{4}{section*.9}
\contentsline {paragraph}{Non-prompt charmonium}{4}{section*.10}
\contentsline {subsubsection}{\numberline {2.2.3}Charmonium}{4}{subsubsection.2.2.3}
\contentsline {paragraph}{S-waves}{4}{section*.11}
\contentsline {paragraph}{P-waves}{5}{section*.12}
\contentsline {subsubsection}{\numberline {2.2.4}Bottomonium}{5}{subsubsection.2.2.4}
\contentsline {paragraph}{S-waves}{5}{section*.13}
\contentsline {paragraph}{P-waves}{5}{section*.14}
\contentsline {subsubsection}{\numberline {2.2.5}B(c)}{5}{subsubsection.2.2.5}
\contentsline {subsubsection}{\numberline {2.2.6}Double charmed baryons}{5}{subsubsection.2.2.6}
\contentsline {subsection}{\numberline {2.3}Prompt-quarkonium polarisation}{5}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}What we understand from cross sections and polarization (and what we do not)}{5}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}New observables in $pp$ collisions}{5}{subsection.2.5}
\contentsline {subsubsection}{\numberline {2.5.1}Correlation with charged particles}{5}{subsubsection.2.5.1}
\contentsline {subsubsection}{\numberline {2.5.2}Angular correlations}{6}{subsubsection.2.5.2}
\contentsline {subsubsection}{\numberline {2.5.3}Associated production (PR)}{7}{subsubsection.2.5.3}
\contentsline {section}{\numberline {3}Cold nuclear matter effects}{9}{section.3}
\contentsline {subsection}{\numberline {3.1}Heavy-flavours in p--A collisions {\bf (Philippe)}}{9}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Theoretical models in p-A and comparison to data {\bf (Fran\c {c}ois)}}{10}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}Nuclear absorption}{10}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}Nuclear PDF}{11}{subsubsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.3}Saturation}{12}{subsubsection.3.2.3}
\contentsline {subsubsection}{\numberline {3.2.4}Multiple scattering and energy loss}{13}{subsubsection.3.2.4}
\contentsline {subsection}{\numberline {3.3}Latest RHIC and LHC results {\bf (Cynthia and Zaida)}}{16}{subsection.3.3}
\contentsline {subsubsection}{\numberline {3.3.1}Open heavy flavour measurements}{16}{subsubsection.3.3.1}
\contentsline {paragraph}{Single lepton}{16}{figure.12}
\contentsline {paragraph}{Dileptons}{16}{section*.16}
\contentsline {paragraph}{D mesons}{16}{figure.14}
\contentsline {paragraph}{Electron-muon correlation}{16}{section*.18}
\contentsline {paragraph}{$\ensuremath {\mathrm {J}/\psi }\xspace $ from beauty decays}{16}{figure.15}
\contentsline {subsubsection}{\numberline {3.3.2}Quarkonium measurements}{16}{subsubsection.3.3.2}
\contentsline {paragraph}{Charmonium}{17}{section*.20}
\contentsline {paragraph}{Bottomonium measurements}{18}{figure.23}
\contentsline {subsection}{\numberline {3.4}CNM extrapolation to A--A collisions {\bf (Philippe)}}{18}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Status and perspectives}{19}{subsection.3.5}
\contentsline {section}{\numberline {4}Open Heavy Flavour}{20}{section.4}
\contentsline {subsection}{\numberline {4.1}Experimental overview: production and nuclear modification factor measurements}{20}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}Inclusive measurements with leptons}{20}{subsubsection.4.1.1}
\contentsline {subsubsection}{\numberline {4.1.2}D meson measurements}{23}{subsubsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.3}Beauty production measurements}{24}{subsubsection.4.1.3}
\contentsline {subsubsection}{\numberline {4.1.4}Comparison of \ensuremath {R_{\mathrm {AA}}}\xspace for charm, beauty and light flavour hadrons}{25}{subsubsection.4.1.4}
\contentsline {subsection}{\numberline {4.2}Experimental overview: azimuthal anisotropy measurements}{25}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}Inclusive measurements with electrons}{25}{subsubsection.4.2.1}
\contentsline {subsubsection}{\numberline {4.2.2}Charmed hadron measurements}{27}{subsubsection.4.2.2}
\contentsline {subsection}{\numberline {4.3}Theoretical overview: heavy flavour interactions in the medium}{27}{subsection.4.3}
\contentsline {subsubsection}{\numberline {4.3.1}Jet energy loss in a finite size dynamical QCD medium}{27}{subsubsection.4.3.1}
\contentsline {subsubsection}{\numberline {4.3.2}Collisional dissociation of heavy mesons and quarkonia in the QGP}{29}{subsubsection.4.3.2}
\contentsline {subsubsection}{\numberline {4.3.3}A pQCD-inspired running $\alpha _s$ energy loss model in MC$@_s$HQ}{29}{subsubsection.4.3.3}
\contentsline {subsubsection}{\numberline {4.3.4}$T$-Matrix Approach to Heavy-Quark Interactions in the QGP}{30}{subsubsection.4.3.4}
\contentsline {subsubsection}{\numberline {4.3.5}lQCD}{31}{subsubsection.4.3.5}
\contentsline {subsubsection}{\numberline {4.3.6}HF Interaction with Medium in AdS/CFT}{33}{subsubsection.4.3.6}
\contentsline {subsection}{\numberline {4.4}Theoretical overview: medium modelling and medium-induced modification of heavy flavour production}{33}{subsection.4.4}
\contentsline {subsubsection}{\numberline {4.4.1}pQCD prediction in static fireball and in hydro}{33}{subsubsection.4.4.1}
\contentsline {subsubsection}{\numberline {4.4.2}Running $\alpha _s$ pQCD inspired in hydro and in BAMPS transport}{34}{subsubsection.4.4.2}
\contentsline {subsubsection}{\numberline {4.4.3}T-matrix approach in fireball and in URQMD transport}{34}{subsubsection.4.4.3}
\contentsline {subsubsection}{\numberline {4.4.4}lQCD in hydro}{35}{subsubsection.4.4.4}
\contentsline {subsubsection}{\numberline {4.4.5}AdS/CFT Calculations Compared to Data}{35}{subsubsection.4.4.5}
\contentsline {subsection}{\numberline {4.5}Heavy flavour correlations in heavy-ion collisions: status and prospects}{36}{subsection.4.5}
\contentsline {section}{\numberline {5}Quarkonia}{40}{section.5}
\contentsline {subsection}{\numberline {5.1}Introduction}{40}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Theory overview}{40}{subsection.5.2}
\contentsline {subsubsection}{\numberline {5.2.1}Sequential Suppression}{40}{subsubsection.5.2.1}
\contentsline {subsubsection}{\numberline {5.2.2}Lattice QCD calculations}{40}{subsubsection.5.2.2}
\contentsline {subsubsection}{\numberline {5.2.3}(Re)generation models}{42}{subsubsection.5.2.3}
\contentsline {subsubsection}{\numberline {5.2.4}Role of nuclear PDF in A-A}{42}{subsubsection.5.2.4}
\contentsline {subsubsection}{\numberline {5.2.5}Transport Approach for In-Medium Quarkonia}{44}{subsubsection.5.2.5}
\contentsline {subsubsection}{\numberline {5.2.6}Quarkonium Production in a Dynamic Transport Approach}{46}{subsubsection.5.2.6}
\contentsline {subsubsection}{\numberline {5.2.7}Non-equilibrium effects on quarkonium suppression}{47}{subsubsection.5.2.7}
\contentsline {subsection}{\numberline {5.3}Experimental overview of RHIC quarkonium results}{49}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}Experimental overview of LHC quarkonium results}{49}{subsection.5.4}
\contentsline {subsubsection}{\numberline {5.4.1}\ensuremath {pp}\xspace as a reference for \ensuremath {R_{\mathrm {AA}}}\xspace }{49}{subsubsection.5.4.1}
\contentsline {subsubsection}{\numberline {5.4.2}\ensuremath {R_{\mathrm {AA}}}\xspace charmonium results at low \ensuremath {p_{\mathrm {T}}}\xspace }{50}{subsubsection.5.4.2}
\contentsline {subsubsection}{\numberline {5.4.3}\ensuremath {R_{\mathrm {AA}}}\xspace charmonium results at high \ensuremath {p_{\mathrm {T}}}\xspace }{50}{subsubsection.5.4.3}
\contentsline {subsubsection}{\numberline {5.4.4}\ensuremath {\mathrm {J}/\psi }\xspace azimuthal distribution}{50}{subsubsection.5.4.4}
\contentsline {subsubsection}{\numberline {5.4.5}Excited charmonium states}{51}{subsubsection.5.4.5}
\contentsline {subsubsection}{\numberline {5.4.6}Bottomonium results from CMS}{51}{subsubsection.5.4.6}
\contentsline {subsubsection}{\numberline {5.4.7}Bottomonium results from ALICE}{51}{subsubsection.5.4.7}
\contentsline {subsubsection}{\numberline {5.4.8}Charmonium vs bottomonium}{51}{subsubsection.5.4.8}
\contentsline {subsection}{\numberline {5.5}Other references for quarkonium behaviour in $\mathrm {A-A}$}{51}{subsection.5.5}
\contentsline {subsubsection}{\numberline {5.5.1}\ensuremath {p\mathrm {-A}}\xspace collisions}{51}{subsubsection.5.5.1}
\contentsline {subsubsection}{\numberline {5.5.2}Open heavy flavour}{51}{subsubsection.5.5.2}
\contentsline {subsection}{\numberline {5.6}Summary}{52}{subsection.5.6}
\contentsline {subsection}{\numberline {5.7}Outlook}{52}{subsection.5.7}
\contentsline {section}{\numberline {6}Future upgrade programs and experiments (10--12 page(s))}{56}{section.6}
\contentsline {subsection}{\numberline {6.1}Introduction (1--2 page(s))}{56}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}The LHC experiments}{56}{subsection.6.2}
\contentsline {subsubsection}{\numberline {6.2.1}ALICE}{56}{subsubsection.6.2.1}
\contentsline {subsubsection}{\numberline {6.2.2}ATLAS}{56}{subsubsection.6.2.2}
\contentsline {subsubsection}{\numberline {6.2.3}CMS}{56}{subsubsection.6.2.3}
\contentsline {subsubsection}{\numberline {6.2.4}LHCb}{56}{subsubsection.6.2.4}
\contentsline {subsection}{\numberline {6.3}The RHIC experiments (2 pages) }{56}{subsection.6.3}
\contentsline {subsubsection}{\numberline {6.3.1}sPHENIX}{56}{subsubsection.6.3.1}
\contentsline {subsubsection}{\numberline {6.3.2}STAR}{56}{subsubsection.6.3.2}
\contentsline {subsection}{\numberline {6.4}The fixed target experiments (2 pages)}{56}{subsection.6.4}
\contentsline {subsection}{\numberline {6.5}Experiment comparaison (1--2 pages))}{56}{subsection.6.5}
\contentsline {section}{\numberline {7}Soft and hard diffraction}{58}{section.7}
\contentsline {section}{\numberline {8}Conclusion}{59}{section.8}
\endgroup 
