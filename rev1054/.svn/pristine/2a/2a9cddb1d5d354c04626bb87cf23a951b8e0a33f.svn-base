
//
//  PlotFigsCNM.C ==> QQ-4.pdf
//  
//
//
//


//starting point macro to plots the figures for the review
//To be updated with more functionalities (such as function to add theory curves)
//the default plotting of data points is black and white, and support the plotting of statistical, uncorrelated and correlated systematic errors
//(symmetric or asymmetric)
//there is a list of colors and markers to use in priority if need to plot several data points

#include "Riostream.h"
#include <stdio.h>
#include "TStyle.h"
#include "TH1.h"
#include "TH2.h"
#include "TH2F.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TPad.h"
#include "TF1.h"
#include "TGraphAsymmErrors.h"
#include "TMath.h"
#include "TLine.h"

void SetLegendStyle(TLegend *leg);
void SetGraphStyle(TGraphAsymmErrors *gr, Int_t ibin);
void SetGraphStyleCorrelatedUnc(TGraphAsymmErrors *gr, Int_t ibin);
void SetGraphStyleFullyCorrelatedUnc(TGraphAsymmErrors *gr, Int_t ibin);
void SetGraphStyleUncorrelatedUnc(TGraphAsymmErrors *gr, Int_t ibin);
void SetGraphStylePoint(TGraphAsymmErrors *gr, Int_t ibin, Bool_t isCopy=false);
void SetGraphStyleLineTheory(TGraphAsymmErrors *gr, Int_t ibin);
TH2F *hframeRaa(TString name ="framefig", Int_t nbinsx=40, Double_t xmin=0.,Double_t xmax=40.,Int_t nbinsy=100,Double_t ymin=0.,Double_t ymax=2.0);
TCanvas * canvasSinglePlot();
TCanvas * canvasDoublePlot();

void SetStyle(Bool_t graypalette=kFALSE);
void CreateHistoFromDataPoints(TGraphAsymmErrors* &hstat, TGraphAsymmErrors* &hsyst, TGraphAsymmErrors* &hsystCorr);

void CreateHistoFromDataPointsLHCb(TGraphAsymmErrors* &hstat, TGraphAsymmErrors* &hsyst, TGraphAsymmErrors* &hsystCorr);
void CreateHistoFromDataPointsALICE(TGraphAsymmErrors* &hstat, TGraphAsymmErrors* &hsyst, TGraphAsymmErrors* &hsystCorr, Float_t xmax, Float_t xmin);
void CreateHistoFromDataPointsRHIC(TGraphAsymmErrors* &hstat, TGraphAsymmErrors* &hsystCorr, TGraphAsymmErrors * &hsystFullCorr, Float_t xmax, Float_t xmin);

void CreateHistoFromComovers(TGraphAsymmErrors *&, Int_t energy);

Int_t GetDataFromFiles(TString fileName, Double_t *&, Double_t *&, Double_t *&, Double_t *&);
Int_t GetDataFromFiles3Col(TString fileName, Double_t *&, Double_t *&, Double_t *&);
Int_t GetDataFromFiles2Col(TString fileName, Double_t *&, Double_t *&);

// Preferred colors and markers
const Int_t fillColors[] = {kGray+1,  kRed-7, kBlue-9, kGreen-8, kMagenta-9, kGray+1,kCyan-8,kYellow-7}; // for syst bands
const Int_t colors[]     = {kBlack, kRed+1 , kBlue+1, kGreen+3, kMagenta+2, kGray-1,kCyan+2,kYellow+2};

//const Int_t markers[]    = {kFullCircle, kFullSquare,kOpenCircle,kOpenSquare,kOpenDiamond,kOpenCross,kFullCross,kFullDiamond,kFullStar,kOpenStar};
const Int_t markers[]    = {kFullCircle, kFullSquare,kFullTriangleUp,kFullDiamond,kFullStar,kOpenCircle,kOpenSquare,kOpenTriangleUp,kOpenDiamond,kOpenStar};
const Int_t colorsTheo[]     = {kBlue+1,  kRed+1, kBlue+1, kGreen+3, kMagenta+2, kGray+1,kCyan+2,kYellow+2}; // for syst bands
const Int_t style[]     = {kSolid, 7, kDotted, kSolid, 9, 6,8,8};//kDashed kDashDotted
// 0:comovers   1:eloss   2:shad1  3:shad2 4:shad2abs 5:gsat 6:kop
const Int_t fillStyle[]     = {kSolid, 3011, 3353, 3335, 3002, 3010,3003,3003};//kDashed kDashDotted
const Int_t fillEdgeLineStyle[]     = {kSolid, kSolid, kSolid, kSolid, kSolid, kSolid,kSolid,kSolid};//kDashed kDashDotted


void PlotPsioverJpsiRHICandLHC2(Bool_t isdoubleCanvas = kTRUE, Bool_t isModel = kTRUE) {
   
  
  // Set the default style
  SetStyle();
  
  // Prepare Figure, please stick to the default canvas size(s) unless absolutely necessary in your case
  // Rectangular
  TCanvas *cfig;
  if(isdoubleCanvas) cfig = canvasDoublePlot();
  else cfig = canvasSinglePlot();

  //log scale if needed
  // cfig->SetLogy();
    
    
  // Define your frame histogram 
  Int_t nbinsx=40, nbinsy=100; 
  Double_t xmin=-5.5, xmax=5, ymin=0, ymax=1.7;
  TH2F * hframe = hframeRaa("LHC",nbinsx,xmin,xmax,nbinsy,ymin,ymax);
  Double_t xminRHIC=0, xmaxRHIC=19;
  TH2F * hframeRHIC = hframeRaa("RHIC",nbinsx,xminRHIC,xmaxRHIC,nbinsy,ymin,ymax);
    

  // Set titles
  // === List of commonly used X and Y axis: ===
  // pt invariant yields
  /* const char *  texPtX="#it{p}_{T} (GeV/#it{c})";
     const char *  texPtY="1/#it{N}_{ev} 1/(2#pi#it{p}_{T}) d#it{N}/(d#it{p}_{T}d#it{y}) ((GeV/#it{c})^{-2})";
     // mt invariant yields
     const char *  texMtX="#it{m}_{T} (GeV/#it{c^2})";
     const char *  texMtY="1/#it{N}_{ev} 1/(2#pi#it{m}_{T}) d#it{N}/(d#it{m}_{T}d#it{y}) ((GeV/#it{c^2})^{-2}) ";
     // Invariant mass with decay products K and pi
     const char *  texMassX="#it{M}_{K#pi} (GeV/#it{c}^2)";
     const char *  texMassY="d#it{N}/(d#it{M}_{K#pi}";
     // <pt>, npart
     const char * texMeanPt    = "#LT#it{p}_{T}#GT (GeV/#it{c})";
    const char * texMeanNpart = "#LT#it{N}_{part}#GT"; */
  
  const char *texX = "#it{y}";//"#it{p}_{T} (GeV/#it{c})" ; //"p_{T} (GeV/#it{c})" ;
  const char *texXRHIC = "N_{coll}" ; //"p_{T} (GeV/#it{c})" ;
  //  const char *texY = "#psi(2S) / J/#psi" ;
  const char *texY = "[#psi(2S)/J/#psi]_{pPb} / [#psi(2S)/J/#psi]_{pp}" ;
  hframe->SetXTitle(texX);
  hframe->SetYTitle(texY);
  
  hframeRHIC->SetXTitle(texXRHIC);
  hframeRHIC->SetYTitle(texY);

  // Draw your histos here:
  //TH1* hstat, *hsyst, *hsystCorr;
  TGraphAsymmErrors* hstatALICE, *hsystALICE, *hsystPartCorrALICE, *hsystFullCorrALICE;
  TGraphAsymmErrors* hstatRHIC, *hsystCorrRHIC, *hsystFullCorrRHIC;
  
  Int_t icolor = 0;

  CreateHistoFromDataPointsALICE(hstatALICE,hsystALICE,hsystPartCorrALICE,xmax,xmin);

  CreateHistoFromDataPointsRHIC(hstatRHIC,hsystCorrRHIC,hsystFullCorrRHIC,xmaxRHIC,xminRHIC);

  icolor = 0;
  SetGraphStylePoint(hstatRHIC,icolor);
  SetGraphStyleUncorrelatedUnc(hsystCorrRHIC,icolor);
  SetGraphStyleFullyCorrelatedUnc(hsystFullCorrRHIC,icolor);

  icolor=1;
  SetGraphStylePoint(hstatALICE,icolor);
  SetGraphStyleUncorrelatedUnc(hsystALICE,icolor);
  SetGraphStyleFullyCorrelatedUnc(hsystPartCorrALICE,icolor);

  //*****************************
  // Theory data points
  //*****************************

  //  SetGraphStyleCorrelatedUnc(hsystCorr,icolor);

  // You should always specify the colliding system
  // NOTATION: pp, p-Pb, Pb-Pb.
  // Don't forget to use #sqrt{s_{NN}} for p-Pb and Pb-Pb
  
  TString textLine1 = "p-Pb #sqrt{s_{NN}}=5.02 TeV";
  TString textLine2 = "";

  TString textLineRHIC = "d-Au #sqrt{s_{NN}}=200 GeV";

  // You can change the position of the text box by varying X and Y coordinates, by default 5.0 and 600 for this histo
  TLatex * text = new TLatex (xmin+0.3*(xmax-xmin),ymin+0.9*(ymax-ymin),textLine1.Data());
  text->SetTextSizePixels(20);
  // You can change the position of the text box by varying X and Y coordinates, by default 0.65 and 55 for this histo
  TLatex * text2 = new TLatex (xmin+0.3*(xmax-xmin),ymin+0.825*(ymax-ymin),textLine2.Data());
  text2->SetTextSizePixels(22);

  TLatex * textRHIC = new TLatex (xminRHIC+0.3*(xmaxRHIC-xminRHIC),ymin+0.9*(ymax-ymin),textLineRHIC.Data());
  textRHIC->SetTextSizePixels(20);
    
  //Legend, if needed
  TLegend * leg = new TLegend(0.2,0.72,0.6,0.82);
  SetLegendStyle(leg);
  //  leg->AddEntry(hstat,     "Data point, stat error",   "LPE");
  // leg->AddEntry(hsyst,     "syst error (Uncorrelated)",  "F");
  //leg->AddEntry(hsystCorr, "syst error (Correlated)",    "F" );
  //  leg->AddEntry(hstatALICE,     "ALICE, inclusive J/#psi #rightarrow #mu^{+}+#mu^{-}, #it{p}_{T} < 15 GeV/c",   "LPE");
  leg->AddEntry(hstatALICE,     "ALICE, inclusive J/#psi and #psi(2S)",   "P");

  TLegend * legRHIC = new TLegend(0.2,0.2,0.6,0.3);
  SetLegendStyle(legRHIC);
  legRHIC->AddEntry(hstatRHIC,     "PHENIX, inclusive J/#psi and #psi(2S)",  "P");

  TLine *lineLHC = new TLine(xmin,1.,xmax,1);
  lineLHC->SetLineStyle(2);
  lineLHC->SetLineWidth(1);

  TLine *lineRHIC = new TLine(xminRHIC,1.,xmaxRHIC,1);
  lineRHIC->SetLineStyle(2);
  lineRHIC->SetLineWidth(1);
  
  //*****************************
  // Theory data points
  //*****************************
  double x1 = 0.2, x2 = x1+0.6;
  TLegend * legTheory = new TLegend(x1,0.15,x2,0.25);
  SetLegendStyle(legTheory);
  
  
  TGraphAsymmErrors *hComRHIC, *hComLHC;  
  CreateHistoFromComovers(hComRHIC,0);//Ferreiro
  CreateHistoFromComovers(hComLHC,1);//Arleo, Peigne

  Int_t iStyle = 0;
  SetGraphStyleLineTheory(hComRHIC,iStyle);
  SetGraphStyleLineTheory(hComLHC,iStyle);

  legTheory->AddEntry(hComRHIC,"COMOV - Ferreiro","L");

  // Now drawing
  
  if(isdoubleCanvas){
    cfig->cd(1);
    hframeRHIC->Draw();
    textRHIC->Draw();

    if (isModel) hComRHIC->Draw("L"); 

    hsystCorrRHIC->Draw("2");
    hsystFullCorrRHIC->Draw("2");
    hstatRHIC->Draw("P same");

    legRHIC->Draw("same");
    lineRHIC->Draw("same");


    cfig->cd(2);
    hframe->Draw();
    text->Draw();
    text2->Draw();

    if (isModel) hComLHC->Draw("F"); 
    
    hsystPartCorrALICE->Draw("2");
    hstatALICE->Draw("P same");


    hsystALICE->Draw("2");


   
    leg->Draw();
    lineLHC->Draw("same");

    legTheory->Draw("same");


  } else {
    hframe->Draw();
    hframeRHIC->Draw();
    textRHIC->Draw();
    
    hsystCorrRHIC->Draw("2");
    hsystFullCorrRHIC->Draw("2");
    hstatRHIC->Draw("P same");
    
    legRHIC->Draw("same");
    lineRHIC->Draw("same");
  }

  
  //  cfig->Print("psioverjpsiRHICLHC.jpg");
  // cfig->Print("QQ-4.jpg");
  cfig->Print("QQ-4.pdf");
    
    
}

//____________________________________________________________
void SetLegendStyle(TLegend *leg)
{
  leg->SetBorderSize(0);
  leg->SetTextSize(gStyle->GetTextSize()*0.8);
  leg->SetLineColor(0);
  leg->SetLineStyle(1);
  leg->SetLineWidth(1);
  leg->SetFillColor(0);
  leg->SetFillStyle(1001);
  return;
}

//____________________________________________________________
void SetGraphStyle(TGraphAsymmErrors *gr, Int_t ibin){
 gr->SetFillStyle(0);
 gr->SetLineColor(colors[ibin]);
 gr->SetLineWidth(2);
 return;
}

//____________________________________________________________
void SetGraphStyleUncorrelatedUnc(TGraphAsymmErrors *gr, Int_t ibin){
 gr->SetFillStyle(0);
 gr->SetLineColor(colors[ibin]);
 gr->SetLineWidth(2);
 return;
}

//____________________________________________________________
void SetGraphStyleCorrelatedUnc(TGraphAsymmErrors *gr, Int_t ibin){
 gr->SetFillStyle(3001);
 gr->SetLineColor(fillColors[ibin]);
 gr->SetFillColor(fillColors[ibin]);
 gr->SetLineWidth(2);
 return;
}

//____________________________________________________________
void SetGraphStyleFullyCorrelatedUnc(TGraphAsymmErrors *gr, Int_t ibin){
 gr->SetFillStyle(1001);
 gr->SetLineColor(fillColors[ibin]);
 gr->SetFillColor(colors[ibin]);
 gr->SetLineWidth(2);
 return;
}


//____________________________________________________________
void SetGraphStylePoint(TGraphAsymmErrors *gr, Int_t ibin, Bool_t isCopy){
 gr->SetMarkerStyle(markers[ibin]);
 gr->SetMarkerColor(colors[ibin]);
 gr->SetLineColor(colors[ibin]);
 gr->SetLineWidth(2);
 if(isCopy) {
   gr->SetMarkerStyle(markers[ibin]+24);
   gr->SetMarkerColor(kBlack);
 }
 return;
}

//____________________________________________________________
TH2F *hframeRaa(TString name, Int_t nbinsx, Double_t xmin,Double_t xmax,Int_t nbinsy,Double_t ymin,Double_t ymax)
{
  TH2F *hraaFrame = new TH2F(name," R_{AB}(c) vs p_{T} (no Eloss hypothesis); p_{t} (GeV/c) ; R_{AA} prompt D (|y|<0.5)",nbinsx,xmin,xmax,nbinsy,ymin,ymax);
  hraaFrame->GetXaxis()->SetTitleSize(0.05);
  hraaFrame->GetXaxis()->SetTitleOffset(1.1);
  hraaFrame->GetYaxis()->SetTitleSize(0.05);
  hraaFrame->GetYaxis()->SetTitleOffset(1.4);


  return (TH2F*)hraaFrame;
}

//____________________________________________________________
TCanvas * canvasSinglePlot()
{

  TCanvas *singleCanvas = new TCanvas("singleCanvas","RAB vs pt, plot all",550,550);
  singleCanvas->SetTopMargin(0.085);
  singleCanvas->Range(-3.177966,-0.2350333,25.74153,2.079823);
  singleCanvas->SetBottomMargin(0.1);
  singleCanvas->SetBorderSize(1);
  singleCanvas->SetLeftMargin(0.114);
  singleCanvas->SetRightMargin(0.02564103);
  singleCanvas->SetTopMargin(0.03448276);
  singleCanvas->SetTickx();
  singleCanvas->SetTicky();

  return (TCanvas*)singleCanvas;
}

//____________________________________________________________
TCanvas * canvasDoublePlot()
{
  gStyle->SetPadTopMargin(0.);
  gStyle->SetPadBottomMargin(0.);
  gStyle->SetPadLeftMargin(0.);
  gStyle->SetPadRightMargin(0.);

  TCanvas *doubleCanvas = new TCanvas("doubleCanvas","RAB vs pt, plot all",700,350);

  
  TPad *pad1 = new TPad("pad1","pad1",0,0,0.525,1.,0);
  pad1->SetNumber(1);
  pad1->SetTopMargin(0.05);
  pad1->SetBottomMargin(0.12);
  pad1->SetRightMargin(0.002);
  pad1->SetLeftMargin(0.17);
  pad1->SetFillColor(0);
  pad1->SetBorderMode(0);
  pad1->SetBorderSize(0);
  pad1->SetTickx(1);
  pad1->SetTicky(1);
  pad1->Draw();
  TPad *pad2 = new TPad("pad2","pad2",0.525,0,1.,1.,0);
  pad2->SetNumber(2);
  pad2->SetTopMargin(0.05);
  pad2->SetBottomMargin(0.12);
  pad2->SetRightMargin(0.025);
  pad2->SetLeftMargin(0.);
  pad2->SetFillColor(0);
  pad2->SetBorderMode(0);
  pad2->SetBorderSize(0);
  pad2->SetTickx(1);
  pad2->SetTicky(1);
  pad2->Draw();

  return (TCanvas*)doubleCanvas;
}



//____________________________________________________________
void SetStyle(Bool_t graypalette) {
  
  gStyle->Reset("Plain");
  gStyle->SetOptTitle(0);
  gStyle->SetOptStat(0);
  if(graypalette) gStyle->SetPalette(8,0);
  else gStyle->SetPalette(1);
  gStyle->SetCanvasColor(10);
  gStyle->SetCanvasBorderMode(0);
  gStyle->SetFrameLineWidth(1);
  gStyle->SetFrameFillColor(kWhite);
  gStyle->SetPadColor(10);
  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);
  gStyle->SetPadBottomMargin(0.15);
  gStyle->SetPadLeftMargin(0.15);
  gStyle->SetHistLineWidth(1);
  gStyle->SetHistLineColor(kRed);
  gStyle->SetFuncWidth(2);
  gStyle->SetFuncColor(kGreen);
  gStyle->SetLineWidth(2);
  gStyle->SetLabelSize(0.045,"xyz");
  gStyle->SetLabelOffset(0.01,"y");
  gStyle->SetLabelOffset(0.01,"x");
  gStyle->SetLabelColor(kBlack,"xyz");
  gStyle->SetTitleSize(0.05,"xyz");
  gStyle->SetTitleOffset(1.25,"y");
  gStyle->SetTitleOffset(1.2,"x");
  gStyle->SetTitleFillColor(kWhite);
  gStyle->SetTextSizePixels(24);
  gStyle->SetTextFont(42);
 
  gStyle->SetLegendBorderSize(0);
  gStyle->SetLegendFillColor(kWhite);
  gStyle->SetLegendFont(42);  

  return;
}


 

void CreateHistoFromDataPoints(TGraphAsymmErrors* &hstat, TGraphAsymmErrors* &hsyst, TGraphAsymmErrors *&hsystCorr){

   // Similar to HepData format (can copy paste values from HepData)
   // Support asymmetric errors
   // Enter the number of points of your histogram, the X value, the lower X error, the upper X error, the Y value, the lower Y error (statistical),
   // the upper Y error (statistical), the lower Y error (uncorrelated systematic), the upper Y error (uncorrelated systematic), the lower Y error (correlated systematic),
   // the upper Y error (correlated systematic)
 
    int numpoints = 5;
    double xval[] = { 5.0, 15.0, 25.0, 40.0, 65.0 };
    double xerrminus[] = { 5.0, 5.0, 5.0, 10.0, 15.0 };
    double xerrplus[] = { 5.0, 5.0, 5.0, 10.0, 15.0 };
    double yval[] = { 0.556, 0.517, 0.594, 0.496, 0.681 };
    double ystaterrminus[] = { 0.0841486779456457, 0.07516648189186455, 0.10332473082471592, 0.07463243262818116, 0.1040096149401583 };
    double ystaterrplus[] = { 0.0841486779456457, 0.07516648189186455, 0.10332473082471592, 0.07463243262818116, 0.1040096149401583 };
    double ysysterrminus[] = { 0.0841486779456457, 0.07516648189186455, 0.10332473082471592, 0.07463243262818116, 0.1040096149401583 };
    double ysysterrplus[] = { 0.0841486779456457, 0.07516648189186455, 0.10332473082471592, 0.07463243262818116, 0.1040096149401583 };
    double ysystcorrerrminus[] = { 0.0841486779456457, 0.07516648189186455, 0.10332473082471592, 0.07463243262818116, 0.1040096149401583 };
    double ysystcorrerrplus[] = { 0.0841486779456457, 0.07516648189186455, 0.10332473082471592, 0.07463243262818116, 0.1040096149401583 };

    hstat = new TGraphAsymmErrors(numpoints,xval,yval,xerrminus,xerrplus,ystaterrminus,ystaterrplus);
    hsyst = new TGraphAsymmErrors(numpoints,xval,yval,xerrminus,xerrplus,ysysterrminus,ysysterrplus);
    hsystCorr = new TGraphAsymmErrors(numpoints,xval,yval,xerrminus,xerrplus,ysystcorrerrminus,ysystcorrerrplus);

}

void CreateHistoFromDataPointsLHCb(TGraphAsymmErrors* &hstat, TGraphAsymmErrors* &hsyst, TGraphAsymmErrors *&hsystCorr){

  //LHCb provides syst. unc. and unc. from pp data -> combined quadratically to syst. unc.
  //no correlated unc. with rapidity in the paper
  Double_t syst1 = TMath::Sqrt( TMath::Power(0.05,2.) + TMath::Power(0.05,2.) );
  Double_t syst2 = TMath::Sqrt( TMath::Power(0.02,2.) + TMath::Power(0.03,2.) );

    int numpoints = 2;
    double xval[] = { -3.25, 3.25 };
    double xerrminus[] = { 0.75, 0.75 };
    double xerrplus[] = {  0.75, 0.75 };
    double yval[] = { 0.93, 0.62 };
    double ystaterrminus[] = { 0.03, 0.01 };
    double ystaterrplus[] = { 0.03, 0.01 };
    double ysysterrminus[] = { syst1, syst2 };
    double ysysterrplus[] = { syst1, syst2 };
    double ysystcorrerrminus[] = { 0.0, 0.0 };
    double ysystcorrerrplus[] = { 0.0, 0.0 };

    hstat = new TGraphAsymmErrors(numpoints,xval,yval,xerrminus,xerrplus,ystaterrminus,ystaterrplus);
    hsyst = new TGraphAsymmErrors(numpoints,xval,yval,xerrminus,xerrplus,ysysterrminus,ysysterrplus);
    hsystCorr = 0;//new TGraphAsymmErrors(numpoints,xval,yval,xerrminus,xerrplus,ysystcorrerrminus,ysystcorrerrplus); 

}


void CreateHistoFromDataPointsALICE(TGraphAsymmErrors* &hstat, TGraphAsymmErrors* &hsyst, TGraphAsymmErrors *&hsystCorr, Float_t xmax, Float_t xmin){

  //  TGraphAsymmErrors *hstat, *hsyst, *hsystCorr;

  //from http://arxiv.org/pdf/1405.3796.pdf
  int const numpoints = 2;
  double xval[] = { -3.71, 2.78 };
  double xerrminus[] = { 0.75, 0.75 };
  double xerrplus[numpoints];
  double xerr[] = {0.1, 0.1};
  double yval[] = { 0.5200, 0.6895};
  double_t ystaterrminus[] = { 0.0893, 0.0949 };
  double_t ystaterrplus[numpoints] = {};
  //uncorrelated
  double_t ysysterrminus[] = {0.0508, 0.0679};
  double_t ysysterrplus[numpoints] = {};
  //correlated
  double xvalcorr[] = { xmax, xmax };
  double_t ysystcorrerrminus[] = {0.055, 0.0730 };
  double_t ysystcorrerrplus[numpoints] = {};
  
  for ( Int_t ind = 0; ind < numpoints; ind++ ) {
    xerrplus[ind] = xerrminus[ind];
    ystaterrplus[ind] = ystaterrminus[ind];
    ysysterrplus[ind] = ysysterrminus[ind];
    ysystcorrerrplus[ind] = ysystcorrerrminus[ind];
  }

  hstat = new TGraphAsymmErrors(numpoints,xval,yval,xerrminus,xerrplus,ystaterrminus,ystaterrplus);
  hsyst = new TGraphAsymmErrors(numpoints,xval,yval,xerr,xerr,ysysterrminus,ysysterrplus);
  //  hsystCorr = new TGraphAsymmErrors(numpoints,xval,yval,xerr,xerr,ysystcorrerrminus,ysystcorrerrplus);

  //check global (in percent) is constant vs rapidty
  Double_t global = 0;
  for ( Int_t ind = 0; ind < numpoints; ind++ ){
    global = ysystcorrerrplus[ind] / yval[ind] * 100 ;
    cout << "rapidity bin= "<< ind <<" global="<< Form("%2.1f",global)<<endl;
  }
  
  int numpointsfull = 1;
  double   width = (xmax - xmin ) * 0.015;
  double xvalfull[] = {xmax-width};
  double xfullerrminus[] = {width};
  double xfullerrplus[] = {width};
  double yvalfull[] = {1.0};
  double ysystfullerrminus[] = {global/100.};
  double ysystfullerrplus[] = {global/100.};

  hsystCorr = new TGraphAsymmErrors(numpointsfull,xvalfull,yvalfull,xfullerrminus,xfullerrplus,ysystfullerrminus,ysystfullerrplus);

  //calculate the deviation from the integrated case
  //total uncertainty is considered

  for ( Int_t ind = 0; ind < numpoints; ind++ ){
    Double_t fullerr = TMath::Power(ysysterrminus[ind],2.) + TMath::Power(ystaterrminus[ind] ,2.) + TMath::Power(ysystcorrerrminus[ind],2.);
    fullerr = TMath::Sqrt(fullerr);
    Double_t dev = 1 - yval[ind];
    Double_t sigma_dev = dev / fullerr; 
    cout<<Form("psi(2S)/Jpsi = %.2f+/-%.2f+/-%.2f+/-%.2f \n Deviation from 1 for %s = %.2f",yval[ind],ystaterrminus[ind],ysysterrminus[ind],ysystcorrerrminus[ind],(ind)?"forward":"backward",sigma_dev)<<endl;
  }
}

void CreateHistoFromDataPointsRHIC(TGraphAsymmErrors* &hstat, TGraphAsymmErrors *&hsystCorr, TGraphAsymmErrors *&hsystFullCorr, Float_t xmax, Float_t xmin){

  /*http://www.phenix.bnl.gov/phenix/WWW/info/data/ppg151_data.html
    
integrated value psi(2S)/Jpsi 0.68     +/-0.14   +0.13 -0.08   +/-0.16  

    Ncoll     tau [fm/c]    psi'/(J/psi)        type A           type B           type C
    15.061      0.041           0.23            +/-0.20         +0.15 -0.02       +/-0.06
    10.248      0.037           0.69            +/-0.26         +0.12 -0.08       +/-0.16
    6.579      0.031           0.96            +/-0.32         +0.13 -0.11       +/-0.23
    3.198      0.023           1.18            +/-0.36         +0.13 -0.15       +/-0.28
  */
  double yvalint = 0.68;
  double ystatint = 0.14;
  double ysystint = 0.13;
  double ysystcorrint = 0.16;

  int numpoints = 4;
  double xval[] = { 3.198, 6.579, 10.248, 15.061};
  double xerrminus[] = {0, 0, 0, 0};
  double xerrplus[] =  {0, 0, 0, 0};
  double yval[] = {1.18, 0.96, 0.69, 0.23 };
  //these are stat + uncorr syst added in quadrature
  double_t ystaterrminus[] = {0.36, 0.32, 0.26, 0.20};
  double_t ystaterrplus[] = {0.36, 0.32, 0.26, 0.20};
  //correlated point to point
  double_t ysystcorrerrminus[] = {0.15, 0.11, 0.08, 0.02};
  double_t ysystcorrerrplus[] = {0.13,0.13,0.12,0.15};
  //fully correlated
  double_t ysystfullycorrerrminus[] = {0.28, 0.23, 0.16, 0.06};
  //  double_t ysystfullycorrerrplus[] = {0.28, 0.23, 0.16, 0.06};
 
  //need to give a witdh to correlated uncertainties
  double width = 0.25;
  for ( Int_t i = 0; i < numpoints; i++ ){
    xerrminus[i] = width;
    xerrplus[i] = xerrminus[i];
  }

  //check global (in percent) is constant vs Ncoll
  Double_t global = 0;
  cout<<"=====Please check global uncertainty in percent (should be constant with Ncoll) "<<endl;
  for ( Int_t ind = 0; ind < numpoints; ind++ ){
    global = ysystfullycorrerrminus[ind] / yval[ind] * 100 ;
    cout << "Ncoll bin="<<ind <<" global="<<Form("%2.1f",global)<<endl;
  }

  //since it is not constant, one takes as written in the paper fig. 4
  global = 24;

  int numpointsfull = 1;
  width = (xmax - xmin ) * 0.015;
  double xvalfull[] = {xmax-width};
  double xfullerrminus[] = {width};
  double xfullerrplus[] = {width};
  double yvalfull[] = {1.0};
  double ysystfullerrminus[] = {global/100.};
  double ysystfullerrplus[] = {global/100.};

  hstat = new TGraphAsymmErrors(numpoints,xval,yval,xerrminus,xerrplus,ystaterrminus,ystaterrplus);
  hsystCorr = new TGraphAsymmErrors(numpoints,xval,yval,xerrminus,xerrplus,ysystcorrerrminus,ysystcorrerrplus);
  hsystFullCorr = new TGraphAsymmErrors(numpointsfull,xvalfull,yvalfull,xfullerrminus,xfullerrplus,ysystfullerrminus,ysystfullerrplus);
  

  //calculate the deviation from 0 for the last point
  //total uncertainty on last point
  Int_t index = 3;
  Double_t fullerr = TMath::Power(ysystfullerrplus[index],2.) + TMath::Power(ystaterrplus[index] ,2.) + TMath::Power(ysystcorrerrplus[index],2.);
  
  fullerr = TMath::Sqrt(fullerr);
  
  Double_t dev = 1 - yval[index];
  Double_t sigma_dev = dev / fullerr; 
  cout<<Form("Deviation from 1 for Ncoll = %.2f = %.2f",xval[index],sigma_dev)<<endl;

  //integrated value
  fullerr = TMath::Power(ystatint,2.) + TMath::Power(ysystint,2.) + TMath::Power(ysystcorrint,2.);
  fullerr = TMath::Sqrt(fullerr);
  
  dev =  1 - yvalint;
  sigma_dev = dev / fullerr;
  cout<<Form("Deviation from 1 for integrated value %.2f => %.2f",yvalint,sigma_dev)<<endl;


}


void CreateHistoFromComovers(TGraphAsymmErrors *&gr, Int_t energy) {

  cout<<Form("==== Ferreiro for %s ====",energy?"LHC":"RHIC")<<endl;

  TString fileName = "input/theory/Ferreiro/resultsFig1.txt";
  if (energy == 1) fileName = "input/theory/Ferreiro/resultsFig3.txt";
  Double_t *xval, *rpa;
  Double_t *xvalmin, *xvalmax;
  Int_t numpoints = 0;
  if (energy == 0 ) numpoints = GetDataFromFiles2Col(fileName,xval,rpa);
  else numpoints = GetDataFromFiles3Col(fileName,xvalmin,xvalmax,rpa);

  if (energy == 0) {
    xvalmin = new Double_t[numpoints];
    xvalmax = new Double_t[numpoints];
    for (Int_t ibin = 0; ibin < numpoints; ibin++ ) {
      xvalmin[ibin] = xvalmax[ibin] = 0;
    }
  }
  else{
    xval = new Double_t[numpoints];
    //central values
    for (Int_t ibin = 0; ibin < numpoints; ibin++ ) {
      xval[ibin] = (xvalmin[ibin] + xvalmax[ibin])/2.;
      xvalmin[ibin] = xval[ibin]-xvalmin[ibin];
      xvalmax[ibin] = xvalmax[ibin]-xval[ibin];
    }
  }

  gr = new TGraphAsymmErrors(numpoints,xval,rpa,xvalmin,xvalmax,0,0);
  gr->SetName(Form("comovers-%s",energy?"LHC":"RHIC"));
  cout<<gr->GetName()<<endl; gr->Print();
  cout<<"==== Ferreiro done ===="<<endl;

  return;
}


Int_t GetDataFromFiles(TString fileName, Double_t *&xVal, Double_t *&yVal, Double_t *&yValMin, Double_t *&yValMax){

  ifstream myfile;
  myfile.open( fileName.Data() );

  Int_t nMax = 200, nCol = 4;

  //  Double_t x, y, ymin, ymax
  xVal = new Double_t[nMax]; 
  yVal = new Double_t[nMax];
  yValMin = new Double_t[nMax];
  yValMax = new Double_t[nMax];

  if ( !myfile || myfile.bad() ) {
    cout<< "E-No file "<<fileName<<" found!"<<endl;
    return 0;
  }
  
  Int_t nLines = 0;
  Bool_t isOk = kFALSE;
  char line[256];

  TArrayD xArray(nCol);

  while ( !myfile.eof() ) {

    if ( nLines > nMax ) break;
    
    isOk = kFALSE;
    char c = myfile.peek();
    TString temp(c);
    if ( temp.Contains("#") ) {
      myfile.getline( line , 256);
      cout<<"Remove line ="<<line<<endl;
      continue;
    }
    xArray.Reset();

    Int_t indArray = 0;

    for (Int_t i = 0; i < nCol; i++ ) {
      myfile >> temp;
      if ( temp.IsFloat() ) {
	isOk = kTRUE;
	xArray[indArray] = temp.Atof();
	//	cout<<xArray[indArray]<<"\t";
	indArray++;
      }
      else {
	cout<<"Warning: a non-float was read and that could affect the reading of the data table "<<temp<<endl;
      }
    }
    //    cout<<endl;
    
    if ( isOk ) {
      //change this according to nCol and data tables
      //here the values are x ycentral ymin ymax
      xVal[nLines] = xArray[0];
      yVal[nLines] = xArray[1];
      yValMin[nLines] = xArray[1]-xArray[2];
      yValMax[nLines] = xArray[3]-xArray[1];
      nLines++;
    }
  }

  cout<<nLines<<endl<<"y "<<endl;
  for (Int_t ind = 0; ind < nLines; ind++) {
    cout<<Form("%.3f, ",xVal[ind]);
  }
  cout<<endl<<"rpa "<<endl;
  for (Int_t ind = 0; ind < nLines; ind++) {
    cout<<Form("%.3f, ",yVal[ind]);
  }
  cout<<endl<<"rpamin "<<endl;
  for (Int_t ind = 0; ind < nLines; ind++) {
    cout<<Form("%.3f, ",yValMin[ind]);
  }
 cout<<endl<<"rpamax "<<endl;
  for (Int_t ind = 0; ind < nLines; ind++) {
    cout<<Form("%.3f, ",yValMax[ind]);
  }
  cout<<endl;
  myfile.close();

  return nLines;
}

Int_t GetDataFromFiles3Col(TString fileName, Double_t *&xValMin, Double_t *&xValMax, Double_t *&yVal){

  ifstream myfile;
  myfile.open( fileName.Data() );

  Int_t nMax = 200, nCol = 3;

  //  Double_t x, y, ymin, ymax
  xValMin = new Double_t[nMax]; 
  xValMax = new Double_t[nMax];
  yVal = new Double_t[nMax];

  if ( !myfile || myfile.bad() ) {
    cout<< "E-No file "<<fileName<<" found!"<<endl;
    return 0;
  }
  
  Int_t nLines = 0;
  Bool_t isOk = kFALSE;
  char line[256];

  TArrayD xArray(nCol);

  while ( !myfile.eof() ) {

    if ( nLines > nMax ) break;
    
    isOk = kFALSE;
    char c = myfile.peek();
    TString temp(c);
    if ( temp.Contains("#") ) {
      myfile.getline( line , 256);
      cout<<"Remove line ="<<line<<endl;
      continue;
    }
    xArray.Reset();

    Int_t indArray = 0;

    for (Int_t i = 0; i < nCol; i++ ) {
      myfile >> temp;
      if ( temp.IsFloat() ) {
	isOk = kTRUE;
	xArray[indArray] = temp.Atof();
	//	cout<<xArray[indArray]<<"\t";
	indArray++;
      }
      else {
	cout<<"Warning: a non-float was read and that could affect the reading of the data table "<<temp<<endl;
      }
    }
    //    cout<<endl;
    
    if ( isOk ) {
      //change this according to nCol and data tables
      //here the values are x ycentral ymin ymax
      xValMin[nLines] = xArray[0];
      xValMax[nLines] = xArray[1];
      yVal[nLines] = xArray[2];
      nLines++;
    }
  }

  cout<<nLines<<endl<<"xvalmin "<<endl;
  for (Int_t ind = 0; ind < nLines; ind++) {
    cout<<Form("%.3f, ",xValMin[ind]);
  }
  cout<<endl<<"xvalmax "<<endl;
  for (Int_t ind = 0; ind < nLines; ind++) {
    cout<<Form("%.3f, ",xValMax[ind]);
  }
  cout<<endl<<"yVal "<<endl;
  for (Int_t ind = 0; ind < nLines; ind++) {
    cout<<Form("%.3f, ",yVal[ind]);
  }
  cout<<endl;
  myfile.close();

  return nLines;
}

Int_t GetDataFromFiles2Col(TString fileName, Double_t *&xVal, Double_t *&yVal){

  ifstream myfile;
  myfile.open( fileName.Data() );

  Int_t nMax = 200, nCol = 2;

  //  Double_t x, y, ymin, ymax
  xVal = new Double_t[nMax]; 
  yVal = new Double_t[nMax];

  if ( !myfile || myfile.bad() ) {
    cout<< "E-No file "<<fileName<<" found!"<<endl;
    return 0;
  }
  
  Int_t nLines = 0;
  Bool_t isOk = kFALSE;
  char line[256];

  TArrayD xArray(nCol);

  while ( !myfile.eof() ) {

    if ( nLines > nMax ) break;
    
    isOk = kFALSE;
    char c = myfile.peek();
    TString temp(c);
    if ( temp.Contains("#") ) {
      myfile.getline( line , 256);
      cout<<"Remove line ="<<line<<endl;
      continue;
    }
    xArray.Reset();

    Int_t indArray = 0;

    for (Int_t i = 0; i < nCol; i++ ) {
      myfile >> temp;
      if ( temp.IsFloat() ) {
	isOk = kTRUE;
	xArray[indArray] = temp.Atof();
	//	cout<<xArray[indArray]<<"\t";
	indArray++;
      }
      else {
	cout<<"Warning: a non-float was read and that could affect the reading of the data table "<<temp<<endl;
      }
    }
    //    cout<<endl;
    
    if ( isOk ) {
      //change this according to nCol and data tables
      //here the values are x ycentral ymin ymax
      xVal[nLines] = xArray[0];
      yVal[nLines] = xArray[1];
      nLines++;
    }
  }

  cout<<nLines<<endl<<"Ncoll "<<endl;
  for (Int_t ind = 0; ind < nLines; ind++) {
    cout<<Form("%.3f, ",xVal[ind]);
  }
  cout<<endl<<"rpa "<<endl;
  for (Int_t ind = 0; ind < nLines; ind++) {
    cout<<Form("%.3f, ",yVal[ind]);
  }
  cout<<endl;

  myfile.close();

  return nLines;
}

//____________________________________________________________
void SetGraphStyleLineTheory(TGraphAsymmErrors *gr, Int_t ibin){

  //  gr->SetFillStyle(fillStyle[ibin]);
  //  gr->SetFillColor(fillColors[ibin]);

  gr->SetLineStyle(style[ibin]);
  gr->SetLineWidth(2);
  gr->SetLineColor(colorsTheo[ibin]);
  
 return;
}
