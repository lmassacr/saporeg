%\documentclass{appolb}
\documentclass{article}
%\usepackage[margin=0.3in]{geometry}

\usepackage[margin=2.3cm]{geometry}
\textheight=23cm
\textwidth=17cm
%\hoffset=-2cm
%\voffset=-1cm


%\usepackage[dvips]{graphicx} 
%\usepackage{graphics}
\usepackage{graphicx}
%\DeclareGraphicsExtensions{.eps,.ps,.pdf,.png,.jpeg}
% epsfig package included for placing EPS figures in the text
%------------------------------------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                %
%    BEGINNING OF TEXT                           %
%                                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
%\eqsec  % uncomment this line to get equations numbered by (sec.num)
\title{Collisional dissociation of heavy mesons and quarkonia in the QGP}

\date{June 15, 2014}

\author{Rishi Sharma, Ivan Vitev}


\maketitle 


Heavy flavor dynamics in dense QCD matter critically depends on the time  scales 
involved in the underlying reaction. Two of these timescales, the formation time 
of the QGP  $\tau_0$ and its transverse size $L_{QGP}$,  
can be related to the nuclear geometry, the QGP expansion,  and the bulk particle 
properties.  The formation time  $\tau_{\rm form}$ of heavy mesons and quarkonia, on the other hand, 
can be evaluated form the virtuality of the  heavy  quark $Q$  decay  into 
$D-$, $B-$mesons \cite{Adil:2006ra,Sharma:2009hn} or the time for the $Q\bar{Q}$ pair to 
expand to the size of the $J/\psi$ or  
$\Upsilon $ wave function~\cite{Sharma:2012dy}.  For a 10 GeV $\pi^0$, 
$\tau_{\rm form} \sim 25$~fm $\ll L_{QGP}$ affords a relatively simple interpretation
of light hadron  quenching in terms of radiative and collisional parton-level energy loss~\cite{Kang:2014xsa}. 
On the other hand, for $D, \; B,\; J/\psi,\;   \Upsilon(1S)$, $\cdots$
one obtains $ \tau_{\rm form} \sim$~1.6~fm, 0.4~fm, 3.3~fm, 1.4~fm $\cdots $ $\ll L_{QGP}$.
Such short formation times necessitate understanding of heavy meson and quarkonium 
propagation and dissociation in strongly interacting matter.

The Gulassy-Levai-Vitev (GLV) reaction operator formalism
was developed for calculating the interactions of parton systems
as  they pass through a dense  strongly-interacting medium.  It was  generalized to the dissociation 
of mesons (quark-antiquark binaries), as long as the momentum exchanges from the medium 
$\mu = g T$  can resolve their internal structure.  The dissociation probability  and dissociation time
\begin{equation}
P_d(p_T,m_Q,t) = 1 -   \left| \int d^{2}{\bf \Delta k} dx \,
\psi_{f}^* (\Delta {\bf k},x)\psi_{0}(\Delta {\bf k}, x) \right|^{2}  \;,  \qquad  \frac{1}{\langle \tau_{\rm diss}(p_T, t) \rangle} = 
\frac{\partial}{\partial t} \ln  P_d(p_T,m_Q,t) \; , 
\end{equation}
can be obtained form the overlap between the  medium-broadened time-evolved and  vacuum  initial
meson wave functions, $\psi_f$ and  $\psi_0$ respectively. Here, $\psi_f$ has the resummed collisional 
interactions in the QGP. Let us denote by
\begin{equation}
\label{convent1}
f^{Q}({p}_{T},t)= \frac{d\sigma^Q(t)}{dy d^2p_T} \;,  
 \;   f^{Q}({p}_{T},t=0) = 
\frac{d \sigma^Q_{PQCD}}{dy d^2p_T} \;, \qquad      f^{H}({p}_{T},t)= \frac{d\sigma^H(t)}{dyd^2p_T} \;,
 \;   f^{H}({p}_{T},t=0) = 0 \; ,   
\end{equation}
the double differential cross sections for the heavy quarks and
hadrons.  Initial conditions are also specified  above, in particular the heavy quark distribution is given by 
the perturbative $c$-  and $b$-quark jet cross section. Energy loss in the partonic state can be 
implemented as quenched initial conditions~\cite{Sharma:2009hn,Sharma:2012dy}. 
The fragmentation fraction of $b$-quarks into $B_c$-mesons is 
very small. In our work it is neglected and the rate 
equations that describe the competition between $b$- and $c$-quark
fragmentation and $D$- and $B$-meson dissociation decouple
for different heavy quark flavors.  Including the loss and 
gain terms we obtain:
\begin{eqnarray}
\label{rateq1}
\partial_t f^{Q}({p}_{T},t)&=& 
-  \frac{1}{\langle \tau_{\rm form}(p_T, t) \rangle} f^{Q}({p}_{T},t) 
 + \, \frac{1}{\langle 
\tau_{\rm diss}(p_T/\bar{x}, t) \rangle}
\int_0^1 dx \,  \frac{1}{x^2} \phi_{Q/H}(x)  
f^{H}({p}_{T}/x,t) \;, \qquad \\
\partial_t f^{H}({p}_{T},t)&=& 
-  \frac{1}{\langle \tau_{\rm diss}(p_T, t) \rangle} f^{H}({p}_{T},t) 
 +\, \frac{1}{\langle 
\tau_{\rm form}(p_T/\bar{z}, t) \rangle}
\int_0^1 dz \,  \frac{1}{z^2} D_{H/Q}(z) 
 f^{Q}({p}_{T}/z,t) \;. \qquad 
\label{rateq2}
\end{eqnarray}
In Eqs.~(\ref{rateq1}) and (\ref{rateq2})   $\phi_{Q/H}(x)$  and  $D_{H/Q}(z)$  are the distribution function of heavy quarks in 
a heavy meson and the fragmentation function of a heavy quark into a heavy mesons, respectively, and   $\bar{z}$ and $\bar{x}$ are
typical fragmentation and dissociation momentum  fractions.  We have checked that  
in the absence of a medium, $\tau_{\rm diss}(p_T, t) \rightarrow \infty$,
we recover the pQCD spectrum of heavy hadrons from vacuum 
jet fragmentation.  Details for the rate equation relevant to quarkonium formation and dissociation are given in~\cite{Sharma:2012dy}. 
 Solving the above equations in the limit $t \rightarrow \infty$ in the absence and presence of a medium
allows us to to evaluate the nuclear modification factor  for heavy mesons and quarkonia in heavy ion reactions.





\bibliographystyle{ieeetr}
\bibliography{biblio}

\end{document}