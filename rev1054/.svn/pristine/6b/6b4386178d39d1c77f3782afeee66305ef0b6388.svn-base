

\subsubsection{Quarkonium measurements}
\label{sec:CNM:Onia}

Quarkonia are mainly measured via their leptonic decay channels. 
In the PHENIX experiment, the Ring Imaging Cherenkov
 associated with the Electromagnetic Calorimeter (EMCAL) allows one to identify 
electrons at mid-rapidity ($|y| < 0.35$). In this rapidity range where the EMCAL 
can reconstruct the photons, \chic can also be measured from its decay channel 
to \jpsi and photon. At backward and forward rapidity ($1.2 < |y| < 2.2$),
 two muon spectrometers allow the reconstruction of quarkonia via their muonic 
decay channel. In the STAR experiment, quarkonia are reconstructed at mid-rapidity 
($|y| < 1$) thanks to the electron identification and momentum measurements from 
the TPC. In the ALICE experiment, a TPC at mid-rapidity ($|\ylab| < 0.9$)
 is used for electron reconstruction and identification and a spectrometer at 
forward rapidity for muon reconstruction ($2.5 < \ylab < 4$). The LHCb
 experiment is a forward spectrometer that allows for the quarkonium measurement 
via their muonic decay channel for $2 < \ylab < 4.5$. In the CMS experiment, 
quarkonia are reconstructed in a large range around mid-rapidity  ($|\ylab| < 2.4$) 
via the muonic decay channel. In LHCb, CMS and  in ALICE at mid-rapidity, the 
separation of prompt \jpsi from inclusive \jpsi exploits the long lifetime of 
$b$ hadrons, with $c\tau$ value of about 500 $\mu$m, using the good 
resolution of the vertex detector. 

\paragraph{Charmonium}

The nuclear modification factor for inclusive and/or prompt \jpsi has been measured for a large range in rapidity 
and is shown in \fig{fig:fig01} for RHIC (left) and LHC (right). It should be emphasized that there 
are no \pp\ measurements at \snn=5.02~TeV at the LHC and the \pp\ cross section interpolation 
procedure described in \sect{sec:CNM:ppRef} results into additional uncertainties. 
\begin{figure}[!tbp]
	\centering
	\includegraphics[width=0.8\columnwidth]{QQ-1.pdf}
	\caption{Left: rapidity dependence of \rdau for inclusive \jpsi in PHENIX~\cite{Adare:2010fn}. 
The error bars represent the uncorrelated uncertainties (statistical and systematic), the open boxes the 
point-to-point correlated systematic uncertainties and the box at unity the correlated one. Right: rapidity 
dependence of \rppb for inclusive \jpsi in ALICE~\cite{Abelev:2013yxa,Adam:2015iga} and prompt \jpsi 
in LHCb~\cite{Aaij:2013zxa}. The error bars represent the statistical uncertainties while the open boxes 
the systematic uncertainties. Other uncertainties are displayed similarly to PHENIX. }
        \label{fig:fig01}  
\end{figure}

The measurements from 
PHENIX~\cite{Adare:2010fn} in \dAu collisions at \snn~=~200~\GeV cover four units of rapidity. 
The \jpsi is suppressed with respect to binary-scaled \pp collisions in the full rapidity range with a suppression that can 
reach more than 40\% at $y=2.3$. 
Inclusive \jpsi includes a contribution from prompt \jpsi (direct \jpsi and excited charmonium states, 
\chic and $\rm \psi(2S)$) and a contribution from decays of B mesons. At RHIC energy, the contribution from B-meson 
decays to the inclusive yield is expected to be small, of the order of $3\%$~\cite{Cacciari:2005rk}, 
but has not been measured so far in \dAu collisions. 
The contribution from excited states such as \chic and $\rm \psi(2S)$ has been measured at 
mid-rapidity~\cite{Adare:2013ezl} and is discussed 
later in this section. While the inclusive \jpsi \rdau for $ |y| < 0.9$ is found to be 
$0.77\pm0.02\rm{(stat)}\pm0.16\rm{(syst)}$, the correction from \chic and 
$\rm \psi(2S)$ amounts to $5\%$ and leads to a feed-down corrected \jpsi \rdau of 
$0.81\pm0.12\rm{(stat)}\pm0.23\rm{(syst)}$. 

At the LHC, the results for inclusive \jpsi from ALICE~\cite{Abelev:2013yxa,Adam:2015iga} and 
for prompt \jpsi from LHCb~\cite{Aaij:2013zxa} show a larger suppression of the \jpsi production with respect 
to the binary-scaled \jpsi production in \pp collisions at forward rapidity (40\% at $\ycm=3.5$). 
In the backward rapidity region the nuclear modification factor is 
slightly suppressed (prompt \jpsi from LHCb) or enhanced (inclusive \jpsi from ALICE) but within the uncertainties 
compatible with unity. 
%In~\cite{ALICE:2013spa}, a comparison of the inclusive \jpsi measurements shows a good agreement 
%between the two experiments. 
LHCb has also measured the production of \jpsi from B mesons~\cite{Aaij:2013zxa}: they contribute to the 
inclusive \jpsi yield by 8\% at $-4 < \ycm < -2.5$ and 12\% at $1.5<\ycm<4$ with an increase towards mid-rapidity 
and high \pt region. At $\pt>8$ GeV/c, the fraction of B mesons 
can reach up to 26\% in the backward rapidity region covered by LHCb. 
In addition, the nuclear modification factor for \jpsi from B mesons is above 0.8 when 
integrated over \pt, as shown in \fig{fig:NonPromptJpsi_pPb_LHCb}, 
At low \pt, a small effect from B mesons on inclusive \jpsi measurements is therefore expected at LHC energy and 
this is confirmed by the comparison of prompt to inclusive \jpsi that shows a good agreement as seen in the 
right panel of \fig{fig:fig01}.

The models based on nuclear PDFs~\cite{Vogt:2010aa,Ferreiro:2008wc,Ferreiro:2013pua} (CEM EPS09 NLO, EXT EKS98 LO and EXT EPS09 LO), 
gluon saturation~\cite{Fujii:2013gxa} (SAT), multiple scattering and energy loss~\cite{Arleo:2010rb,Kopeliovich:2011zz} (COH.ELOSS and KPS) 
described in \sect{CNM_theory} are also shown in \fig{fig:fig01}. 
The uncertainty from the nuclear PDFs on the gluon distribution 
function is large as discussed in \sect{sec:npdf} and is shown by the uncertainty band of the corresponding calculations. 
The models based on nPDFs overestimate the data at RHIC in particular 
at backward rapidity, the anti-shadowing region. At forward rapidity, a strong shadowing with the EPS09 NLO nPDFs 
parameterization is favoured by the RHIC data. By including a \jpsi absorption cross section, 
$\sigma_{\rm abs}^{\jpsi}=4.2$ mb, the calculation 
from EXT EKS98 LO ABS that uses EKS98 LO nPDFs can describe the \rdau measured at RHIC in the full rapidity range. 
In the latter calculations, since the behaviour of EKS98 is very close to the one of the central set of EPS09 LO, 
the theoretical curves are expected to be similar to those of EPS09 LO nPDFs.  
%At the LHC, the formation time of the quarkonium state in the nucleus rest frame is larger than the size 
%of the nucleus and 
%the \jpsi state is not fully formed when crossing the nucleus: the fully formed \jpsi is therefore not expected 
%to suffer final-state inelastic interaction in the nucleus and no absorption effect is added in the calculations. 
At the LHC, while the backward rapidity data is well described by the nPDF models, a strong shadowing is 
favoured by the data at forward rapidity. Both EPS09 LO and the lower uncertainty band of EPS09 NLO 
parameterizations provide such a strong shadowing. 
In the COH.ELOSS approach, the rapidity dependence of the nuclear modification factor is well described both at RHIC and LHC energies. 
In the KPS model, the rapidity dependence of the RHIC data is correctly described but the calculations are systematically lower than the measurements. 
At LHC energy, the KPS model overestimates the \jpsi suppression over the full rapidity range. 
Finally, the SAT model is not valid for the full rapidity 
range, see \sect{sec:saturation}. While it describes correctly the data for $y>0.5$ at $\snn=200$~\GeV and the mid-rapidity data at 
$\snn=5.02$~\TeV, it overestimates the \jpsi suppression at forward rapidity at $\snn=5.02$~\TeV. 

\begin{figure}[!tbp]%!htp
	\centering
	\includegraphics[width=0.5\columnwidth]{QQ-1-x2.pdf}
	\caption{$x_2 = \frac{m}{\snn}\exp(-y)$ dependence of \rpa for inclusive J/$\psi$ 
in PHENIX~\cite{Adare:2010fn} and ALICE~\cite{Abelev:2013yxa,Adam:2015iga} and for prompt \jpsi in LHCb~\cite{Aaij:2013zxa}. 
The uncertainties are described in \fig{fig:fig01}.} 
        \label{fig:fig01x2}  
\end{figure}

%as shown also inn~\cite{Adler:2005ph}, PHENIX and low energy data as a function of 
It is interesting to check whether a simple scaling exists on $\jpsi$ suppression between RHIC and LHC. The effects of nPDF or saturation are expected to scale with the momentum fraction $x_2$, independently of the centre-of-mass energy of the collision. It is also the case of nuclear absorption, since the $\jpsi$ formation time is proportional to the Lorentz factor $\gamma$, which is uniquely related to $x_2$, $\gamma = m / (2 m_p\ x_2)$, assuming $2\to 1$ kinematics for the production process.
In order to test the possible  $x_2$ scaling expected in the case of 
nPDF and nuclear absorption theoretical approches, the data from RHIC and 
LHC~\cite{Adare:2010fn,Abelev:2013yxa,Adam:2015iga,Aaij:2013zxa} of \fig{fig:fig01} are shown together 
in \fig{fig:fig01x2} as a function of $x_2 = \frac{m}{\snn}\exp(-y)$ where the low $x_2$ values correspond to 
forward rapidity data. Note that \eq{QQ_x-Bjorken} for the calculation of $x_2$, 
which refers to a $2 \to 2$ partonic process, can not be used since the \meanpt values for all the data points 
have not been measured. While at $x_2 < 10^{-2}$, the nuclear modification factors are compatible at RHIC and 
LHC energy within uncertainties, the data presents some tension with a $x_2$ scaling at large $x_2$.   

The transverse momentum distribution of the nuclear modification factor is shown for different rapidity ranges 
in \fig{fig:fig02} for RHIC (left) and LHC (right) energies. At \snn~=~200~\GeV, the \jpsi \rdau is 
suppressed at low \pt and increases with \pt for the full rapidity range. The mid- and forward rapidity results 
show a similar behaviour: \rdau 
increases gradually with \pt and is consistent with unity at $\pt\gtrsim4$\GeVc. At backward rapidity, \rdau 
increases rapidly to reach 1 at $\pt \approx 1.5$\GeVc and is above unity for $\pt > 2.5$\GeVc. At \snn~=~5.02~\TeV, 
a similar shape and amplitude is observed for \rppb at mid and forward rapidity: in that case 
it is consistent with unity at $\pt\gtrsim5$\GeVc. The backward rapidity results are consistent throughout the full 
\pt interval with unity. 

\begin{figure}[!t]
	\centering
	\includegraphics[width=0.95\columnwidth]{QQ-2.pdf}
	\caption{Left: transverse momentum dependence of \rdau for three rapidity ranges for inclusive \jpsi in PHENIX~\cite{Adare:2012qf}. Right: transverse momentum dependence of \rppb for inclusive \jpsi in ALICE~\cite{Adam:2015iga}. The uncertainties are the same as described in \fig{fig:fig01}.}
        \label{fig:fig02}
\end{figure}

In addition to the aforementioned models, the calculations based on the energy loss approach from~\cite{Sharma:2012dy} (ELOSS), 
valid for $y \geq 0$ and $\pt > 3$ GeV/c, are also compared to the data. Among these models, only the COH.ELOSS and SAT model includes 
effects from initial- or final-state multiple scattering that may lead to a \pt broadening. The \pt dependence of \rpa is correctly described by the 
CEM EPS09 NLO model except at backward rapidity and $\snn=200$~\GeV. The model based on EXT EKS98 LO ABS 
with an absorption cross section of $4.2$ mb describes the mid and forward rapidity results at $\snn=200$~\GeV 
but not the \pt dependence at backward rapidity. A good agreement is reached at $\snn=5.02$~\TeV with CEM EPS09 NLO and EXT EPS09 LO calculations. 
The ELOSS model describes correctly the \pt dependence at mid and forward 
rapidity at both energies. The COH.ELOSS calculations describe correctly the data with however a steeper \pt dependence at forward rapidity 
and $\snn=5.02$~TeV. Finally the SAT model gives a good description of the data at mid-rapidity at $\snn=5.02$~TeV but 
does not describe the \pt dependence at forward rapidity at $\snn=$200~\GeV and overestimates the suppression at forward rapidity at $\snn=5.02$~TeV. 

It is also worth mentioning that the ratio \rfb of the nuclear modification factors for a rapidity range 
symmetric with respect to $\ycm \sim 0$ has also been extracted as a function of rapidity and \pt 
at $\snn=5.02$~TeV~\cite{Aaij:2013zxa,Abelev:2013yxa}. Despite the reduction of statistics (since the rapidity 
range is limited), the \pp cross section and its associated systematics cancels out in the ratio and results on \rfb 
provides additional constraints to the models.

The dependence of the \jpsi suppression has also been measured in \dAu as a function of the centrality 
of the collision in PHENIX~\cite{Adare:2010fn,Adare:2012qf}. The centrality of the \dAu collision 
is determined thanks to the total energy deposited in the  beam-beam counter (BBC) located in the nucleus direction. 
A larger suppression is observed in central (0--20\%) as compared 
to peripheral (60--88\%) collisions. In order to study the centrality dependence of the nuclear effect, the 
nuclear modification factor between central and peripheral collisions, $R_{\rm CP}$, has been measured. 
\fig{fig:fig03} 
shows \rcp as a function of \rdau: the forward rapidity measurements correspond to the lowest \rcp values. 
The nuclear effect has been parametrized by three functional forms (exponential, linear or quadratic) that depend 
on the density-weighted longitudinal thickness through the nucleus 
$\Lambda(r_\mathrm{T})=\frac{1}{\rho_0}\int \dd z\rho(z,r_\mathrm{T})$. Here $\rho_0$ is the density in the 
centre of the nucleus and $r_\mathrm{T}$ the transverse radial position of the nucleon-nucleon collision relative to the centre 
of the nucleus. While the effect from nuclear absorption is expected to follow an exponential dependence, other models 
like nPDF assume a linear form to describe the centrality dependence of the nuclear effect. While at 
backward and mid-rapidity the data can not discriminate between the functional forms, the forward rapidity data 
suggest that the dependence on $\Lambda(r_\mathrm{T})$ is nonlinear and closer to quadratic.
%\begin{itemize}
%\item Exponential: $M(r_T)=e^{-a\Lambda(r_T)}$
%\item Linear: $M(r_T)=e^{-a\Lambda(r_T)}$
%\item Quadratic: $M(r_T)=e^{-a\Lambda(r_T)}$
%\end{itemize}
\begin{figure}[!tbp]
	\centering
	\includegraphics[width=0.4\columnwidth]{QQ-3.png}
	\caption{\rcp as a function of \rdau for inclusive \jpsi in PHENIX~\cite{Adare:2010fn}. The curves are constraint lines for three geometric dependencies of the nuclear modification. The ellipses represent a one standard deviation contour for the systematic uncertainties.}
        \label{fig:fig03}
\end{figure}

The centrality dependence of the \jpsi nuclear modification factor has also been studied in 
ALICE~\cite{MartinBlanco:2014pia,Lakomov:2014yga}. In 
these analyses, the event activity is determined from the energy measured along the beam line by the Zero Degree 
Neutron (ZN) calorimeter located in the nucleus direction. In the hybrid 
method described in~\cite{Adam:2014qja}, the centrality of the collision in each ZN energy event class is determined 
assuming that the charged-particle multiplicity measured at mid-rapidity is proportional to the number of 
participants in the collision. In the \jpsi case, the data is compatible, within uncertainties, to the binary-scaled 
\pp production for peripheral events at backward and forward rapidity. 
The \jpsi production in \pPb is however significantly modified towards central events. 
At backward rapidity the nuclear modification factor 
is compatible wih unity at low \pt and increases with \pt reaching about $1.4$ at \pt$\sim7$~\GeVc. 
At forward rapidity the suppression of \jpsi production shows an increase towards central events 
specially at low \pt. The \jpsi production was also studied as a function of the relative charged-particle multiplicity 
measured at mid-rapidity as it was already done in \pp collisions~\cite{Abelev:2012rz}. 
The results show an increase with 
the relative multiplicity at backward and forward rapidity. At forward rapidity the multiplicity dependence becomes 
weaker than at backward rapidity for high relative multiplicities. 
In \pp collisions~\cite{Abelev:2012rz} this increase is interpreted in terms of the hadronic activity 
accompanying \jpsi production, from contribution of multiple parton-parton interactions or 
in the parton percolation scenario. In \pPb collisions, in addition to the previous contributions, the cold nuclear 
matter effects should be considered when interpreting these results.      
%Since the centrality determination may suffer from a possible bias, the nuclear modification factor is quoted as $Q_{\pPb}$ instead of the usual notation \rppb. 

Since open and hidden heavy flavour hadrons are characterized by the same production process for the heavy quark pair, 
a direct comparison of their productions, if measured over the entire phase space, is expected to single out
 final-state effects on \jpsi production. 
In \fig{fig:fig05}, the \jpsi \rdau~\cite{Adare:2012qf} is compared to the one of open heavy-flavour 
decay leptons~\cite{Adare:2012yxa,Adare:2013lkk} as measured by PHENIX in central \dAu collisions. 
Despite the fact that the open beauty contribution is not subtracted and 
the measurement is carried out only down to \pt = 1 \GeVc, 
%not for the full single-lepton \pt range but
this comparison may already give some hint on the final-state effects on \jpsi production. 
A similar behaviour across the entire \pt range is observed for \rdau at forward rapidity, suggesting that 
the suppression of \jpsi production is related to the suppression of \ccbar pair production. 
%supporting the argument that the \jpsi suppression is driven by the one from heavy charm pair.
At backward and mid-rapidity the \jpsi is clearly more suppressed than leptons from open heavy-flavour decays at 
low \pt, where the charm contributions dominate over those from bottom~\cite{Adare:2009ic}. 
This difference between \jpsi and open charm may originate from additional effects beyond charm quark pair 
production, such as a longer crossing time \tcross of the \ccbar state in the nuclear matter or 
a larger density of comoving medium~\cite{Ferreiro:2014bia}. This comparison suggests that an additional CNM 
final-state effect significantly affects \jpsi production at backward and mid-rapidity at \snn=200~\GeV. One 
should however emphasize that the comparison of the open heavy-flavour and \jpsi production 
is carried out as a function of \pt. The c quark fragments into a charm mesons which 
in turn decays into a lepton and it is not straightforward to relate the decay lepton momentum to the 
parent quark momentum in order to interpret accurately this comparison.
\begin{figure}[!tbp]
	\centering
	\includegraphics[width=0.8\columnwidth]{QQ-5.pdf}
	\caption{Transverse momentum of \rdau of inclusive J/$\psi$ for three different rapidity ranges in 0--20\% centrality bin~\cite{Adare:2012qf} and comparison to heavy flavour electron and muon in PHENIX~\cite{Adare:2012yxa,Adare:2013lkk}.}
        \label{fig:fig05}
\end{figure}

The binding energy of the excited charmonium states is significantly smaller than that of the ground 
state~\cite{Satz:2005hx}: the \psiP 
has the lowest binding energy (0.05~GeV), following by the \chic (0.20~GeV) and the \jpsi (0.64~GeV). 
The excited charmonium 
states are then expected to be more sensitive to the nuclear environment as compared to the \jpsi. 
%As discussed in Section~\ref{CNM_Intro}, 
%earlier measurements s
%at lower energy~\cite{Leitch:1999ea,Abt2006va,Alessandro:2006jt}
% have shown that the \psiP is more suppressed at mid-rapidity than the \jpsi in \pA collisions and a similar suppression is observed at forward rapidity. 
%Since the charmonium formation time, $\tau_f$, for these measurements at mid-rapidity is estimated to be lower than the crossing time, $\tau_c$ of the \ccbar pair spend into the nucleus. 
The relative suppression of the \psiP to \jpsi from earlier measurements at lower energy and at mid-rapidity
~\cite{Leitch:1999ea,Abt:2006va,Alessandro:2006jt} has been understood as a larger absorption 
of the \psiP in the nucleus since, 
in these conditions, the crossing time \tcross of the \ccbar pair through the nucleus is larger than 
the charmonium formation time \tf. 
At higher energy, \tcross is expected to be always lower than \tf~\cite{Ferreiro:2012mm} except 
maybe for backward rapidity ranges.
This means that the \ccbar is nearly always in a pre-resonant state when traversing the nuclear matter 
and the nuclear break-up should be the same for the \psiP and \jpsi.

The PHENIX experiment has measured $\rdau=0.54\pm0.11\rm{(stat)}^{+0.19}_{-0.16}\rm{(syst)}$ for 
the \psiP and $\rdau=0.77\pm0.41\rm{(stat)}\pm0.18\rm{(syst)}$ for the \chic for $|y|<0.35$~\cite{Adare:2013ezl}. 
While the large uncertainty prevents any conclusion for the \chic, the relative modification factor of the 
\psiP to inclusive \jpsi in d-Au collisions, $\left[ \psiP/\jpsi \right]_{\rm dAu} / \left[ \psiP/\jpsi \right ]_{\rm pp}$
%$\frac{Y_{dAu}^{\psiP}/Y_{dAu}^{\jpsi}}{Y_{pp}^{\psiP}/Y_{pp}^{\jpsi}}$ with $Y$ the invariant yield, 
equivalent to $\rdau^{\psiP}/\rdau^{\jpsi}$, has been found to be $0.68\pm0.14\rm{(stat)}^{+0.21}_{-0.18}\rm{(syst)}$, 
\ie 1.3 $\sigma$ lower than 1. 
The relative modification factor as a function of \Ncoll is shown in the left panel of \fig{fig:fig04}. 
In the most central collisions, the \psiP is more suppressed than the \jpsi by about $2 \,\sigma$. 

ALICE has also measured 
in \pPb collisions at \snn =5.02 TeV the \psiP to \jpsi relative modification factor and has found 
$0.52\pm0.09\rm{(stat)}\pm0.08\rm{(syst)}$ for $-4.46<\ycm<-2.96$ and 
$0.69\pm0.09\rm{(stat)}\pm0.10\rm{(syst)}$ for $2.03<\ycm<3.53$~\cite{Abelev:2014zpa}, 
respectively $4 \,\sigma$ and $2 \,\sigma$ lower than unity. In the right panel of \fig{fig:fig04}, 
the relative modification factor is shown as a function of rapidity. This double ratio 
has also been measured as a function of \pt~\cite{Abelev:2014zpa} and does not exhibit a significant \pt dependence. 
In addition, preliminary results~\cite{Arnaldi:2014kta} show that the nuclear modification factor of the 
\psiP follows a similar trend as the \jpsi as a function of event activity at forward rapidity but is 
significantly more suppressed at backward rapidity towards central events. 

Models based on initial-state effects~\cite{Vogt:2010aa,Ferreiro:2012mm} or coherent energy 
loss~\cite{Peigne:2014uha} do not predict a relative suppression 
of the \psiP production with respect to the \jpsi one. These measurements could indicate that the \psiP production 
is sensitive to final-state 
effects in \pA collisions. A recent theoretical work uses EPS09 LO nPDF and includes 
the interactions of the quarkonium states with a comoving medium~\cite{Ferreiro:2014bia} (COMOV). The COMOV 
calculations are shown in \fig{fig:fig04}. They describe fairly well the PHENIX and 
ALICE results. 
Hot nuclear matter effects were also proposed as a possible explanation for the \psiP relative suppression in central \pPb collisions 
at the LHC~\cite{Liu:2013via}.
\begin{figure}[!tbp]
	\centering
	\includegraphics[width=0.8\columnwidth]{QQ-4.pdf}
	\caption{Left: relative nuclear modification 
($\left[ \psiP/\jpsi \right]_{\rm dAu} / \left[ \psiP/\jpsi \right]_{\rm pp}$) of inclusive 
\jpsi to \psiP as a function of \Ncoll in PHENIX~\cite{Adare:2013ezl}. Right: transverse momentum 
dependence of the relative nuclear modification of \jpsi to $\psi(2S)$ in ALICE~\cite{Abelev:2014zpa}. 
The uncertainties are the same as described in \fig{fig:fig01}.}
        \label{fig:fig04}
\end{figure}
%$\left[ \frac{\sigma_{\psi(2S)}}{\sigma_{J/\psi}} \right]_{pPb} / \left[ \frac{\sigma_{\psi(2S)}}{\sigma_{J/\psi}} \right ]_{pp}$



\paragraph{Bottomonium measurements}

The nuclear modification factor for bottomonium is shown in \fig{fig:fig06} at 
RHIC~\cite{Adare:2012bv,Adamczyk:2013poh} (left) 
and LHC~\cite{Abelev:2014oea,Aaij:2014mza} (right). At RHIC the 3 \ups states can not be measured separately 
due to the poor statistics and 
invariant mass resolution. At \snn~=~200\GeV, the \rdau is compatible 
with no or a slight suppression over the full rapidity range except at mid-rapidity where a 
suppression by a factor of two is found in \dAu with respect to (binary-scaled) \pp collisions. 
The data suggests a larger suppression by about $40\%$ 
at backward rapidity but the uncertainties are large and \rdau is lower than unity by only $1.3 \,\sigma$. 
At \snn~=~5.02\TeV, the \upsa measurements 
from LHCb, despite slightly different rapidity ranges, are systematically higher than those of ALICE but 
the two measurements are consistent within uncertainties. The measured \rppb is consistent with unity 
at backward rapidity and below unity by, at most, $30\%$ at forward rapidity. 

The data are compared to models based on nPDFs (CEM EPS09 NLO, EXT EPS09 LO), coherent energy loss (COH.ELOSS) and gluon saturation (SAT). Given the 
limited statistics, the data can not constrain the models in most of the phase space and is in good agreement with the 
theory calculations. Only at mid-rapidity at 
\snn~=~200\GeV, the observed suppression is challenging for all the models, where no suppression is expected. In the 
nPDF based model, the rapidity range where \rpa is higher than unity corresponds to the anti-shadowing region. Clearly 
the data is not precise enough to conclude on the strength of gluon anti-shadowing.
\begin{figure}[!tbp]
	\centering
	\includegraphics[width=0.8\columnwidth]{QQ-6.pdf}
	\caption{Left: rapidity dependence of $\upsabc$ in PHENIX~\cite{Adare:2012bv} and STAR~\cite{Adamczyk:2013poh}. Right: rapidity dependence of \rppb for \upsa in ALICE~\cite{Abelev:2014oea} and LHCb~\cite{Aaij:2014mza}}
        \label{fig:fig06}
\end{figure}

As in the \jpsi case, the ratio \rfb of the nuclear modification factors for a rapidity range 
symmetric with respect to $\ycm \sim 0$ has also been extracted for the \upsa 
at \snn=5.02\TeV~\cite{Abelev:2014oea,Aaij:2014mza}. 

Comparison of \upsa \rppb to the one from open beauty from \fig{fig:NonPromptJpsi_pPb_LHCb} can give a hint on 
final-state effects on \upsa. A similar level of suppression is observed for the \upsa and the \jpsi from B mesons. 
Larger statistics data however would be needed to rule out any final-state effect on \upsa production in \pPb.

The study of excited bottomonium states in \pPb collisions 
may indicate the presence of final-state effects in bottomonium production. 
The \upsc has the smallest binding energy (0.2\GeV), followed by the \upsb (0.54 \GeV) and the \upsa (1.10\GeV)
~\cite{Satz:2005hx}. Since the bottomonium formation time  is expected to be larger than the nuclear size,
%crossing time \tc 
%of the \bbbar pair through the nucleus, 
the suppression in \pPb is expected to be the same for all \ups states.

\begin{figure}[!t]
	\centering
	\includegraphics[width=0.8\columnwidth]{QQ-7.pdf}
	\caption{Left: $\upsn/\upsa$ ratio in \pp and \pPb collisions in ALICE~\cite{Abelev:2014oea}, LHCb~\cite{Aaij:2014mza} and 
CMS~\cite{Chatrchyan:2013nza}. For a better visibility, the ALICE data points are displaced by +0.2 in rapidity. Right: relative modification factor 
$\left[ \upsn/\upsa \right]_{\rm pPb} / \left[ \upsn/\upsa \right ]_{\rm pp}$ in CMS~\cite{Chatrchyan:2013nza}.}
        \label{fig:fig07}
\end{figure}


\begin{figure}[!t]
	\centering
	\includegraphics[width=0.8\columnwidth]{QQ-8.pdf}
	\caption{Self-normalized cross section ratio $\frac{\upsa}{\left < \upsa \right >}$ vs $\frac{N_{\rm tracklets}}{\left < N_{\rm tracklets} \right >}$ (left) and $\frac{E_{\rm T}}{\left < E_{\rm T} \right >}$ (right) in \pp collisions at 2.76\TeV, \pPb collisions at 5.02\TeV and \PbPb collisions at 2.76\TeV in CMS~\cite{Chatrchyan:2013nza}. Here $N_{\rm tracklets}$ is the charged-track multiplicity measured in $|\eta|<2.4$ and $E_{\rm T}$ the transverse energy measured in $4<|\eta|<5.2$. The dotted line is a linear function with a slope equal to unity.}
        \label{fig:fig08}
\end{figure}



The CMS experiment has measured the ratio of the excited to the ground state cross section, 
 $\upsn/\upsa$, for $\rm n=2,3$ at mid-rapidity in \pPb collisions. 
ALICE (only for $\rm n=2$) and LHCb have performed similar measurements at backward and forward rapidity. 
The measured ratios $\upsn/\upsa$, shown in the left panel of \fig{fig:fig07} are compared to the ratios measured in 
\pp collisions at, however, different energies (\s~=~2.76 and 8\TeV) and in addition for the backward and 
forward rapidities, in slightly different rapidity ranges. 
It is worth noting that the ratio $\upsn/\upsa$ has been measured for $\rm n=2,3$ at 
\s~=~1.8, 2.76 and 7\TeV~\cite{Abe:1995an,Chatrchyan:2013nza,Chatrchyan:2013yna} at mid-rapidity and 
%is found to be independent of \s within the quoted uncertainties and at 
at \s~=~2.76, 7 and 8\TeV~\cite{Aaij:2014nwa,LHCb:2012aa,Aaij:2013yaa} at forward rapidity. 
The ratio is found to be, within the quoted uncertainties, 
independent of \s, and in the rapidity range $2<\ycm<4$, independent of $\ycm$.  
A stronger suppression than in \pp is observed at mid-rapidity in \pPb collisions for \upsb and \upsc as 
compared to \upsa, which could suggest the presence of final-state effects that affect more the excited states 
as compared to the ground state. 
At forward rapidity, the ratios measured by ALICE and LHCb are similar in \pPb and \pp but the measurements are not precise enough to be sensitive to a difference as observed by CMS. 

%Compute $\sigma$ from CMS deviation \pPb vs \pp at 2.76 TeV in CMS
% C.H 
To better quantify the modification between \pp and \pPb and cancel out some of the systematic uncertainties from 
the detector setup, the double ratio 
$\left[ \upsn/\upsa \right]_{\rm pPb} / \left[ \upsn/\upsa \right ]_{\rm pp}$ has also been evaluated by CMS 
at mid-rapidity using \pp collisions at 2.76\TeV\cite{Chatrchyan:2013nza} and is displayed in the right panel of 
\fig{fig:fig07}. The double ratio in \pPb is lower than one by $2.4\,\sigma$ for \upsb and \upsc. The double 
ratios signal the presence of different or stronger final-state effect acting on the excited states compared to the 
ground state from \pp to \pPb collisions. 

As for the charmonium production, the excited states are not expected to be suppressed by none of the models that 
include initial-state effects nor from the coherent energy loss effect. A possible explanation may come from 
a suppression associated to the comoving medium. Precise measurements in a larger rapidity range, which covers 
different comoving medium density, would help to confirm this hypothesis. 


CMS has also performed measurements as a function of the event activity at forward
($4<|\eta|<5.2$ for the transverse energy $E_{\rm T}$) and mid-rapidity ($|\eta|<2.4$ for the charged-track multiplicity 
$N_{\rm tracklets}$)~\cite{Chatrchyan:2013nza}. 
\fig{fig:fig08} shows the \ups self-normalized cross section ratios \upsa/$\left<\upsa\right>$ 
where $\left<\upsa\right>$ is the event-activity integrated value for \pp, \pPb and \pb collisions. 
The self-normalized cross section ratios are found to rise with the 
event activity as measured by these two estimators and similar results are obtained 
for \upsb and \upsc. 
When Pb ions are involved, the increase can be related to the increase in the number of nucleon--nucleon collisions. 
A possible interpretation of the positive correlation between the \ups production yield and the underlying activity 
of the \pp event is related to Multiple-Parton Interactions (MPI) occuring in a single \pp collisions. 
Linear fits performed 
separately for the three collision systems show that the self-normalized ratios have a slope consistent with unity 
in the case of forward event activity. Hence, no significant difference between \pp, \pPb and \pb is observed 
when correlating \ups production yields with forward event activity. 
On the contrary in the case of mid-rapidity event activity, different slopes are found for the three collisions systems. 
These observations are also related to the single cross section ratios $\upsn/\upsa$ as shown in 
\fig{fig:CMS_ups_mult} and discussed in detail in \sect{sec:pp:HadCorrelations}.  
