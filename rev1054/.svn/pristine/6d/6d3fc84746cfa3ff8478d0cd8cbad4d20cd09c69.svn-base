%% Use INSPIRE format for the bibliography
%% http://www.inspirehep.net
%% BibTex Style


@article{Sharma:2009hn,
      author         = "Sharma, Rishi and Vitev, Ivan and Zhang, Ben-Wei",
      title          = "{Light-cone wave function approach to open heavy flavor
                        dynamics in QCD matter}",
      journal        = "Phys.Rev.",
      volume         = "C80",
      pages          = "054902",
      doi            = "10.1103/PhysRevC.80.054902",
      year           = "2009",
      eprint         = "0904.0032",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ph",
      SLACcitation   = "%%CITATION = ARXIV:0904.0032;%%",
}

@article{Sharma:2012dy,
      author         = "Sharma, Rishi and Vitev, Ivan",
      title          = "{High transverse momentum quarkonium production and
                        dissociation in heavy ion collisions}",
      journal        = "Phys.Rev.",
      number         = "4",
      volume         = "C87",
      pages          = "044905",
      doi            = "10.1103/PhysRevC.87.044905",
      year           = "2013",
      eprint         = "1203.0329",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ph",
      SLACcitation   = "%%CITATION = ARXIV:1203.0329;%%",
}


@article{Adil:2006ra,
      author         = "Adil, Azfar and Vitev, Ivan",
      title          = "{Collisional dissociation of heavy mesons in dense QCD
                        matter}",
      journal        = "Phys.Lett.",
      volume         = "B649",
      pages          = "139-146",
      doi            = "10.1016/j.physletb.2007.03.050",
      year           = "2007",
      eprint         = "hep-ph/0611109",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ph",
      SLACcitation   = "%%CITATION = HEP-PH/0611109;%%",
}


@article{Vitev:2007ve,
      author         = "Vitev, Ivan",
      title          = "{Non-Abelian energy loss in cold nuclear matter}",
      journal        = "Phys.Rev.",
      volume         = "C75",
      pages          = "064906",
      doi            = "10.1103/PhysRevC.75.064906",
      year           = "2007",
      eprint         = "hep-ph/0703002",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ph",
      reportNumber   = "LA-UR-07-1216",
      SLACcitation   = "%%CITATION = HEP-PH/0703002;%%",
}

@article{Vitev:2006bi,
      Author         = "Vitev, I. and Goldman, J. Terrance and Johnson, M.B. and
                        Qiu, J.W.",
      title          = "{Open charm tomography of cold nuclear matter}",
      journal        = "Phys.Rev.",
      volume         = "D74",
      pages          = "054010",
      doi            = "10.1103/PhysRevD.74.054010",
      year           = "2006",
      eprint         = "hep-ph/0605200",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ph",
      SLACcitation   = "%%CITATION = HEP-PH/0605200;%%",
}

@article{Neufeld:2010dz,
      author         = "Neufeld, R.B. and Vitev, Ivan and Zhang, Ben-Wei",
      title          = "{A possible determination of the quark radiation length
                        in cold nuclear matter}",
      journal        = "Phys.Lett.",
      volume         = "B704",
      pages          = "590-595",
      doi            = "10.1016/j.physletb.2011.09.045",
      year           = "2011",
      eprint         = "1010.3708",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ph",
      SLACcitation   = "%%CITATION = ARXIV:1010.3708;%%",
}

@article{Huang:2013vaa,
      author         = "Huang, Jinrui and Kang, Zhong-Bo and Vitev, Ivan",
      title          = "{Inclusive b-jet production in heavy ion collisions at
                        the LHC}",
      journal        = "Phys.Lett.",
      volume         = "B726",
      pages          = "251-256",
      doi            = "10.1016/j.physletb.2013.08.009",
      year           = "2013",
      eprint         = "1306.0909",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ph",
      reportNumber   = "LA-UR-13-24055",
      SLACcitation   = "%%CITATION = ARXIV:1306.0909;%%",
}

@article{Kang:2014xsa,
      author         = "Kang, Zhong-Bo and Lashof-Regas, Robin and Ovanesyan,
                        Grigory and Saad, Philip and Vitev, Ivan",
      title          = "{Jet quenching phenomenology from soft-collinear
                        effective theory with Glauber gluons}",
      year           = "2014",
      eprint         = "1405.2612",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ph",
      reportNumber   = "ACFI-T14-10",
      SLACcitation   = "%%CITATION = ARXIV:1405.2612;%%",
}


@article{Kang:2011rt,
      author         = "Kang, Zhong-Bo and Vitev, Ivan",
      title          = "{Photon-tagged heavy meson production in high energy
                        nuclear collisions}",
      journal        = "Phys.Rev.",
      volume         = "D84",
      pages          = "014034",
      doi            = "10.1103/PhysRevD.84.014034",
      year           = "2011",
      eprint         = "1106.1493",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ph",
      SLACcitation   = "%%CITATION = ARXIV:1106.1493;%%",
}