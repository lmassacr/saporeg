

In this section we summarise the recent measurements in \pA collisions at RHIC and at the LHC. 
%
Open heavy-flavour results are described in \sect{sec:CNM:OHF} and hidden heavy-flavour data in 
\sect{sec:CNM:Onia}. 
%
As described in the previous section, in order to understand the role of the CNM effects, 
the interpretation of these measurements 
is commonly obtained by a comparison with measurements in \pp\ collisions
at the same centre-of-mass energy as for p--A and in the same rapidity interval. 
%nucleon-nucleon collisions where the nucleons are not confined in a nucleus, \ie to \pp data. 
At the LHC, so far it has not been possible to carry out \pp measurements
at the same energy and rapidity as for p--Pb.
In \sect{sec:CNM:ppRef} the procedures 
to define the pp reference for $R_{\rm pA}$ are described. 


\subsubsection{Reference for \pA measurements at the LHC}
\label{sec:CNM:ppRef}

%In the next sections we summarise the measurements performed in \pA and $d\mathrm{-A}$ collisions. 
%As described in the previous subsections, in order to understand the role of the CNM effects the interpretation of these measurements 
%is commonly performed  by a comparison to the equivalent measurements in 
%\pp\ collisions.
%%nucleon-nucleon collisions where the nucleus is not at play, 
%%\ie to the proton-proton measurements.
% This requires to perform similar analysis in both \pA and \pp collisions at the same 
%\snn and in the same kinematic regime.
%At LHC energies, so far it has not always been possible to endeavour into such \pp measurements, whereas at RHIC energies 
%the accelerator facilities made this possible. Here we briefly describe the procedures and considerations that were made in the LHC results 
%described hereafter.

The \pp reference for open heavy-flavour measurements at $\s=5.02$\TeV was obtained either from pQCD calculations or by a pQCD-based 
$\s$-scaling of the measurements performed at $\s=7$\TeV. In some cases, it was also possible to evaluate the $\s$-scaled spectra of 
both the 7\TeV and 2.76\TeV data to $\s=5.02$\TeV and combine them. 
The pQCD-based $\s$-scaling procedure is described in reference~\cite{Averbeck:2011ga}. 
The scaling factor is evaluated as the ratio of the theoretical calculation at the two energies. The scaling uncertainties are determined 
considering the prediction uncertainties, the variation of the renormalisation and factorisation scales, the heavy-quark mass and the PDF 
uncertainties. The assumption behind this calculation is that the values of these parameters remain the same at both energies. 
The scaling factor and uncertainties computed with different heavy-quark production models, FONLL~\cite{Cacciari:2003uh, Cacciari:2012ny} 
and GM-VFNS~\cite{Kniehl:2005st,Kniehl:2005mk}, are in excellent agreement. 
This procedure was verified by comparing the D meson CDF measurements at $\s=1.96\TeV$ at mid-rapidity to a $\s$-scaling of the ALICE data~\cite{Averbeck:2011ga}.
%
A different strategy was used in order to evaluate the \pp reference for \jpsi from B decays: the procedure is the same as for \jpsi at forward rapidity and it is described in the following. 


In the quarkonium analyses, different strategies have been adopted depending on the precision of the existing measurements. They are mainly based on phenomenological functions and are briefly described in the following. 

At mid-rapidity in ALICE, 
the \jpsi \pp integrated cross section reference has been obtained by performing an interpolation based on \jpsi measurements at midrapidity 
in \pp collisions 
at \s~=~200\GeV~\cite{Adare:2006kf}, 2.76\TeV~\cite{Abelev:2012kr} and 7\TeV~\cite{Aamodt:2011gj}, and in \ppbar 
collisions at \s~=~1.96\TeV~\cite{Acosta:2004yw}. Several functions (linear, power law and exponential) were used to parametrize the cross 
section dependence as a function of \s.
The interpolation leads to a total uncertainty of $17\%$ on the integrated cross section. The effect of the asymmetric rapidity coverage, 
due to the shift of the rapidity by 0.465 in the centre-of-mass system in \pPb collisions at the LHC, 
was found to be negligible as compared to the overall uncertainty of the interpolation procedure. Then the same method as described in~\cite{Bossu:2011qe} 
was followed to obtain the \pt-dependent cross section. The method is based on the empirical observation that the \jpsi cross sections measured at different 
energy and rapidity scale with \pt/\meanpt. The \meanpt value was evaluated at \s~=~5.02~TeV by an interpolation of the \meanpt measured at 
midrapidity~\cite{Adare:2006kf,Aamodt:2011gj,Acosta:2004yw} using exponential, logarithmic and power law functions. 

At forward rapidity, a similar procedure for the \jpsi cross section interpolation has been adopted by ALICE and LHCb and is 
described in~\cite{ALICE:2013spa}. 
In order to ease the treatment of the systematics correlated with energy, the interpolation was limited to results obtained with a single apparatus. 
The inclusive \jpsi cross sections measured at 2.76~\cite{Abelev:2012kr} and 7\TeV~\cite{Abelev:2014qha} were included in the ALICE procedure while  
the inclusive, prompt \jpsi and \jpsi from B-mesons cross sections measured at 2.76~\cite{Aaij:2012asz}, 7~\cite{Aaij:2011jh} and 8\TeV~\cite{Aaij:2013yaa} 
were considered in the LHCb one. 
The interpolation of the cross section with energy is based, as in the midrapidity case, on three empirical shapes (linear, power law and exponential). 
%In addition, in the ALICE 
%case, additional uncertainties arising from theoretical shapes were considered. 
The resulting interpolated cross section for inclusive \jpsi obtained 
by ALICE and LHCb in $2.5<\ycm<4$ at \s~=~5.02\TeV were found to be in good agreement with a total uncertainty of $\sim8\%$ and $\sim5\%$ 
for ALICE and LHCb, respectively. The interpolation in \s was also performed by ALICE independently for each %finer rapidity and 
\pt interval and outside 
of the rapidity range of \pp data in order to cope with the \pPb centre-of-mass rapidity shift.  
In that case an additional interpolation with rapidity was carried out by using several empirical functions (Gaussian, second and fourth order polynomials).
%Finally the \pt-dependent interpolated cross section was corrected according to the rapidity dependence of the \pt differential distributions measured in \pp collisions~\cite{Aaij:2011jh}.

In the case of the \ups at forward rapidity, the interpolation procedure results also from a common approach by ALICE and LHCb and 
is described in~\cite{LHCb:08082014sca}. It is based on LHCb measurements in \pp collisions at 2.76\TeV~\cite{Aaij:2014nwa}, 7\TeV~\cite{Abelev:2014qha} 
and 8\TeV~\cite{Aaij:2013yaa}. 
Various phenomenological functions and/or the \s-dependence of the CEM and FONLL models are used for the \s-dependence of the cross section, similarly to the
\jpsi interpolation procedure at forward rapidity. This interpolation results into a systematic uncertainty that ranges from 8 to $12\%$ depending on the 
rapidity interval. 

