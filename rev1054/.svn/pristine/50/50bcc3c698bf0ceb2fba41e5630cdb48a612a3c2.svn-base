%!TEX root = ../SaporeGravis.tex



\section{Concluding remarks}
\label{sec:summary}

The first Run of the LHC has provided a wealth of measurements in proton--proton and heavy-ion  
collisions for hadrons with open and hidden charm and beauty. The LHC data 
complement the rich experimental programmes at Tevatron, SPS and RHIC,
 extending by factors of about four,  
fourteen and twenty-five the centre-of-mass energies accessible in pp, A--A and p--A collisions, respectively. 
 
The main features of the data are in general understood. However, the current experimental 
precision (statistical) and accuracy (systematic uncertainties) is in most cases
still limited. This, along with the the lack of 
precise enough guidance from theoretical models, still prevents definite 
conclusions on production mechanisms in pp collisions (for quarkonia), their modification in p--A, and extraction of key quantities for the QGP
produced in A--A collisions. 
%In the following, we briefly summarise the status of these  and address the perspectives
%with the current upgrade programmes and the experiments planned for the mid-term future,
%with focus on those with relevance on quak-gluon matter studies.

\vspace{1mm}

In pp collisions, pQCD calculations at NLO or FONLL describe very well the open charm and beauty 
production cross sections
 within, however, rather large theoretical uncertainties, especially for charm 
at low \pt.
At the LHC, this uncertainty also impacts the scaling of the cross sections measured at top pp centre-of-mass energy
to the lower Pb--Pb and p--Pb energies. Therefore, it is crucial that the future LHC programme includes
adequate pp reference runs at the heavy-ion energies.
%While the inclusion in 
%the calculation of a next order (next-to-next-to-leading) seems far in the future, it remains and interesting 
%question whether the data would allow to impose some constraints on the parameters used
%in the theory, in particular the factorisation and renormalisation scales. 
%An improved set
%of measurements from future LHC and RHIC Runs, with sizeably reduced uncertainties may allow us to attempt this.
In the quarkonium sector, there is a large variety of quarkonium production models. 
To date, none describes consistently the available measurements on production cross 
section and polarization. Future data will allow to constrain the models further
and also address the question whether a single production mechanism is responsible for 
the low- and high-\pt quarkonia. Understanding the production process will provide insight on the 
quarkonium formation time, which is an important aspect for the study of medium-induced effects in p--A and A--A collisions. 
%The quarkonium-like states $X$, Y$, $Z$ are a fascinating subject on their own.
For both open and hidden heavy-flavour hadrons, the correlation of production with 
the event multiplicity is an interesting facet that may shed light on production
mechanisms and the general features of proton--proton collisions at high energy.
The connection of these effects with the studies 
in proton--nucleus and nucleus--nucleus collisions is an open and interesting field of theoretical 
and experimental investigation.

\vspace{1mm}

Initially thought as a reference for nucleus--nucleus studies, p--A collisions
provided a host of interesting results of their own. Nuclear medium effects are observed in p--A collisions on open and hidden heavy flavour at both RHIC and LHC, especially for \jpsi production at forward rapidity. None of the individual cold nuclear matter (CNM) effects are able to describe the data in all kinematic regions, suggesting that a mixture of different effects are at work. The approach of nuclear parton distribution functions with shadowing explains the basic features of open and hidden heavy flavour despite large uncertainties at forward rapidity and the coherent energy loss model explains the main characteristics of quarkonium production. 
Theoretical interpretation of quarkonium excited states is still challenging. 
The impact of these CNM studies for the understanding of the nucleus--nucleus data 
in terms of a combination of cold and hot medium effects is yet to be fully understood.
In addition, it is still an open question whether the possible signals of collective behaviour observed 
in high-multiplicity proton-nucleus collisions in the light-flavour sector could manifest also for heavy-flavour production.
This question could become accessible with future higher-statistics proton--nucleus data samples 
at RHIC and LHC.

\vspace{1mm}

The strong electromagnetic field of lead ions circulating in the LHC is an intense source of quasi-real photons, which allows the study of $\gamma\gamma$, $\gamma\rm{p}$ and $\gamma\rm{Pb}$ reactions at unprecedented high energies. 
The coherent and the incoherent photoproduction of \jpsi and \psiP
is a powerful tool to study the gluon distribution in the target hadron and the first data 
from the LHC using Run 1
already set strong constraints to shadowing models.
%The measurements have also revealed
%two unexpected results: the ratio of coherent production of \psiP to \jpsi seems to be larger than expected in most of the current models 
%and coherent photoproduction of \jpsi has been observed in peripheral hadronic Pb--Pb interactions.
The statistical precision is one of the main, and in some cases the dominant, sources of uncertainty of the current measurements. The large increase in statistics expected for Run 2 and other future data-taking periods, as well as improvements in the detectors, the trigger and the data acquisition systems, will allow a substantial reduction of the uncertainties. These future measurements will then shed a brighter light on the phenomena of shadowing and the gluon structure of dense sources, like lead ions.

\vspace{1mm}

The measurements of open heavy flavour production in nucleus--nucleus, proton--proton and proton--nucleus collisions 
at RHIC and the LHC allow us to conclude that heavy quarks experience energy loss in the
hot and dense QGP.  
A colour charge dependence in energy loss is not clearly emerging from the data, but it is 
implied by the fair theoretical description of the observed patterns.
A quark mass ordering is suggested by the data (some of them still preliminary, though) 
and the corresponding model comparisons. However, this observation is still limited to a 
restricted momentum and centrality domain.
The important question of thermalization of heavy quarks appears to be partly answered for 
charm: the positive elliptic flow observed at both RHIC and LHC indicates
that charm quarks take part in the collective expansion of the QGP.  This is consistent with 
thermalization, but the degree of 
thermalization is not yet constrained. 
For the beauty sector, thermalization remains an open issue entirely.
The role of the different in-medium interaction mechanisms, such as radiative, collisional 
energy loss and in-medium hadronisation, is still not completely clarified, although the
comparison of data with theoretical models suggests the relevance of all these effects.

\vspace{1mm}

For the quarkonium families, the LHC data demonstrated the presence of colour screening 
for both charmonium and bottomonium. In case of \jpsi, the LHC data implies the presence
of other production mechanisms, generically called (re)generation. Whether production
takes place throughout the full (or most of the) lifetime of the deconfined state or
rather suddenly at the confinement transition (crossover) can not be disentangled
using the existing measurements.
The $\Upsilon$ production seems to exhibit a sequential pattern, but several assumed
quantities in this interpretation (e.g. the feed-down contributions) make the situation not satisfactory enough.
%For example, recent measurements of the feed-down contribution from higher resonances disfavour this interpretation.

\vspace{1mm}

The next steps in the study of heavy-flavour hadron production in
heavy-ion collisions will lead to a stage of quantitative understanding of the data, towards 
the extraction of the charm and beauty quarks transport coefficients and the temperature 
history of the deconfined state, including the temperature of the confinement crossover.
An incremental, but nevertheless important, progress is expected with the existing
experimental setups at RHIC and the LHC (where in particular the increased collision 
energy enhances the relevance of the data in the next three years).
The ultimate goal can only be achieved with upgraded or new detectors, which will
allow the extension of the set of observables and the precision of the measurements over
a broad range of collision energies.

%Experimental tools: $D_s$, $\Lambda_c$, prompt and non-prompt \jpsi (cross sections, polarization, energy loss, \vtwo); $R_{AA}$ of excited states (i.e. $R_{AA}$($\psi$') / $R_{AA}$(J/$\psi$)); \ups states measurements down to \pt = 0; improved statistical and systematic uncertainties

\vspace{1mm}

This experimental effort needs to be matched on the theory side. Even though the field
of study of extreme deconfined matter with heavy quarks seems to be driven by experiment,
the contribution of theory is of crucial importance. In particular,  
accurate theoretical guidance and modelling are required to interpret the measurements in terms of 
the QGP properties mentioned in 
the previous paragraph.
Ultimately, the quantitative stage 
can only be reached in a close collaboration of experiment and theory.

