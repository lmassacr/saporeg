\contentsline {section}{\numberline {1}Introduction}{6}{section.1}
\contentsline {section}{\numberline {2}Heavy flavour and quarkonium production in \ensuremath {\rm pp}\xspace collisions}{8}{section.2}
\contentsline {subsection}{\numberline {2.1}Production mechanisms of open and hidden heavy-flavour in \ensuremath {\rm pp}\xspace collisions}{8}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Open-heavy-flavour production}{8}{subsubsection.2.1.1}
\contentsline {paragraph}{Fixed-Flavour-Number Scheme}{8}{section*.2}
\contentsline {paragraph}{ZM-VFNS}{9}{section*.3}
\contentsline {paragraph}{GM-VFNS}{10}{section*.4}
\contentsline {paragraph}{FONLL}{11}{section*.5}
\contentsline {paragraph}{Monte Carlo generators}{11}{section*.6}
\contentsline {subsubsection}{\numberline {2.1.2}Quarkonium-production mechanism}{12}{subsubsection.2.1.2}
\contentsline {paragraph}{The Colour-Evaporation Model (CEM)}{12}{section*.7}
\contentsline {paragraph}{The Colour-Singlet Model (CSM)}{12}{section*.8}
\contentsline {paragraph}{The Colour-Octet Mechanism (COM) and NRQCD}{13}{section*.9}
\contentsline {paragraph}{Theory prospects}{13}{section*.10}
\contentsline {subsection}{\numberline {2.2}Recent cross section measurements at hadron colliders}{14}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Leptons from heavy-flavour decays}{15}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Open charm}{15}{subsubsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.3}Open beauty}{17}{subsubsection.2.2.3}
\contentsline {subsubsection}{\numberline {2.2.4}Prompt charmonium}{20}{subsubsection.2.2.4}
\contentsline {subsubsection}{\numberline {2.2.5}Bottomonium}{22}{subsubsection.2.2.5}
\contentsline {subsubsection}{\numberline {2.2.6}${\rm B}_c$ and multiple-charm baryons}{24}{subsubsection.2.2.6}
\contentsline {subsection}{\numberline {2.3}Quarkonium polarization studies}{25}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}New observables}{31}{subsection.2.4}
\contentsline {subsubsection}{\numberline {2.4.1}Production as a function of multiplicity}{31}{subsubsection.2.4.1}
\contentsline {subsubsection}{\numberline {2.4.2}Associated production}{33}{subsubsection.2.4.2}
\contentsline {subsection}{\numberline {2.5}Summary and outlook}{37}{subsection.2.5}
\contentsline {section}{\numberline {3}Cold nuclear matter effects on heavy flavour and quarkonium production in p-A collisions}{38}{section.3}
\contentsline {subsection}{\numberline {3.1}Heavy flavour in {\rm p--A} collisions}{38}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Theoretical models for CNM effects}{40}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}Typical timescales}{40}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}Nuclear PDFs}{42}{subsubsection.3.2.2}
\contentsline {paragraph}{$\ensuremath {\mathrm {J}/\psi }\xspace $ and $\Upsilon $ production}{42}{section*.11}
\contentsline {paragraph}{Non-prompt $\ensuremath {\mathrm {J}/\psi }\xspace $ production}{43}{section*.12}
\contentsline {subsubsection}{\numberline {3.2.3}Saturation in the Colour Glass Condensate approach}{44}{subsubsection.3.2.3}
\contentsline {subsubsection}{\numberline {3.2.4}Multiple scattering and energy loss}{46}{subsubsection.3.2.4}
\contentsline {paragraph}{\ensuremath {{Q\overline {Q}}}\xspace \ propagation and attenuation in nuclei}{46}{section*.13}
\contentsline {paragraph}{Initial and final state energy loss, power corrections and Cronin effect}{47}{section*.14}
\contentsline {paragraph}{Coherent energy loss}{48}{section*.15}
\contentsline {subsubsection}{\numberline {3.2.5}Nuclear absorption}{49}{subsubsection.3.2.5}
\contentsline {subsubsection}{\numberline {3.2.6}Summary of CNM models}{50}{subsubsection.3.2.6}
\contentsline {subsection}{\numberline {3.3}Recent RHIC and LHC results}{50}{subsection.3.3}
\contentsline {subsubsection}{\numberline {3.3.1}Reference for {p--A}\xspace measurements at the LHC}{50}{subsubsection.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.2}Open heavy-flavour measurements}{52}{subsubsection.3.3.2}
\contentsline {paragraph}{Heavy-flavour decay leptons}{52}{section*.16}
\contentsline {paragraph}{Dilepton invariant mass}{52}{section*.17}
\contentsline {paragraph}{{\rm D} mesons}{54}{section*.18}
\contentsline {paragraph}{Open beauty measurements}{54}{section*.19}
\contentsline {paragraph}{Heavy-flavour azimuthal correlations}{56}{section*.20}
\contentsline {subsubsection}{\numberline {3.3.3}Quarkonium measurements}{57}{subsubsection.3.3.3}
\contentsline {paragraph}{Charmonium}{57}{section*.21}
\contentsline {paragraph}{Bottomonium measurements}{63}{section*.22}
\contentsline {subsection}{\numberline {3.4}Extrapolation of CNM effects from {\rm p--A} to {\rm A--A} collisions}{65}{subsection.3.4}
\contentsline {paragraph}{Nuclear PDF}{65}{section*.23}
\contentsline {paragraph}{Multiple scattering and energy loss}{65}{section*.24}
\contentsline {paragraph}{Data-driven extrapolation}{66}{section*.25}
\contentsline {subsection}{\numberline {3.5}Summary and outlook}{67}{subsection.3.5}
\contentsline {section}{\numberline {4}Open heavy flavour in nucleus--nucleus collisions}{70}{section.4}
\contentsline {subsection}{\numberline {4.1}Experimental overview: production and nuclear modification factor measurements}{71}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}Inclusive measurements with leptons}{71}{subsubsection.4.1.1}
\contentsline {subsubsection}{\numberline {4.1.2}{\rm D} meson measurements}{74}{subsubsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.3}Beauty production measurements}{76}{subsubsection.4.1.3}
\contentsline {subsubsection}{\numberline {4.1.4}Comparison of \ensuremath {R_{\mathrm {AA}}}\xspace for charm, beauty and light flavour hadrons}{77}{subsubsection.4.1.4}
\contentsline {subsection}{\numberline {4.2}Experimental overview: azimuthal anisotropy measurements}{78}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}Inclusive measurements with electrons}{78}{subsubsection.4.2.1}
\contentsline {subsubsection}{\numberline {4.2.2}{\rm D} meson measurements}{80}{subsubsection.4.2.2}
\contentsline {subsection}{\numberline {4.3}Theoretical overview: heavy flavour interactions in the medium}{81}{subsection.4.3}
\contentsline {subsubsection}{\numberline {4.3.1}pQCD energy loss in a dynamical QCD medium}{82}{subsubsection.4.3.1}
\contentsline {subsubsection}{\numberline {4.3.2}A pQCD-inspired running $\alpha _s$ energy loss model in MC$@_s$HQ and BAMPS}{85}{subsubsection.4.3.2}
\contentsline {subsubsection}{\numberline {4.3.3}Collisional dissociation of heavy mesons and quarkonia in the QGP}{86}{subsubsection.4.3.3}
\contentsline {subsubsection}{\numberline {4.3.4}$T$-Matrix approach to heavy-quark interactions in the QGP}{87}{subsubsection.4.3.4}
\contentsline {subsubsection}{\numberline {4.3.5}Lattice-QCD}{88}{subsubsection.4.3.5}
\contentsline {subsubsection}{\numberline {4.3.6}Heavy-flavour interaction with medium in AdS/CFT}{90}{subsubsection.4.3.6}
\contentsline {subsection}{\numberline {4.4}Theoretical overview: medium modelling and medium-induced modification of heavy-flavour production}{91}{subsection.4.4}
\contentsline {subsubsection}{\numberline {4.4.1}pQCD energy loss in a static fireball (Djordjevic \emph {et al.}\xspace )}{91}{subsubsection.4.4.1}
\contentsline {subsubsection}{\numberline {4.4.2}pQCD embedded in viscous hydro (POWLANG and Duke)}{92}{subsubsection.4.4.2}
\contentsline {subsubsection}{\numberline {4.4.3}pQCD-inspired energy loss with running $\alpha _s$ in a fluid-dynamical medium and in Boltzmann transport}{92}{subsubsection.4.4.3}
\contentsline {subsubsection}{\numberline {4.4.4}Non-perturbative $T$-matrix approach in a fluid-dynamic model (TAMU) and in UrQMD transport}{93}{subsubsection.4.4.4}
\contentsline {subsubsection}{\numberline {4.4.5}lattice-QCD embedded in viscous fluid dynamics (POWLANG)}{94}{subsubsection.4.4.5}
\contentsline {subsubsection}{\numberline {4.4.6}AdS/CFT calculations in a static fireball}{94}{subsubsection.4.4.6}
\contentsline {subsection}{\numberline {4.5}Comparative overview of model features and comparison with data}{95}{subsection.4.5}
\contentsline {subsection}{\numberline {4.6}Heavy-flavour correlations in heavy-ion collisions: status and prospects}{103}{subsection.4.6}
\contentsline {subsection}{\numberline {4.7}Summary and outlook}{105}{subsection.4.7}
\contentsline {section}{\numberline {5}Quarkonia in nucleus--nucleus collisions}{106}{section.5}
\contentsline {subsection}{\numberline {5.1}Theory overview}{110}{subsection.5.1}
\contentsline {subsubsection}{\numberline {5.1.1}Sequential suppression and lattice QCD}{110}{subsubsection.5.1.1}
\contentsline {subsubsection}{\numberline {5.1.2}Effect of nuclear PDFs on quarkonium production in nucleus--nucleus collisions}{112}{subsubsection.5.1.2}
\contentsline {subsubsection}{\numberline {5.1.3}Statistical (re)generation models}{115}{subsubsection.5.1.3}
\contentsline {subsubsection}{\numberline {5.1.4}Transport approach for in-medium quarkonia}{116}{subsubsection.5.1.4}
\contentsline {subsubsection}{\numberline {5.1.5}Non-equilibrium effects on quarkonium suppression}{120}{subsubsection.5.1.5}
\contentsline {subsubsection}{\numberline {5.1.6}Collisional dissociation of quarkonia from final-state interactions}{120}{subsubsection.5.1.6}
\contentsline {subsubsection}{\numberline {5.1.7}Comover models}{120}{subsubsection.5.1.7}
\contentsline {subsubsection}{\numberline {5.1.8}Summary of theoretical models for experimental comparison}{121}{subsubsection.5.1.8}
\contentsline {subsection}{\numberline {5.2}Experimental overview of quarkonium results at RHIC and LHC}{122}{subsection.5.2}
\contentsline {subsubsection}{\numberline {5.2.1}Proton--proton collisions as a reference for \ensuremath {R_{\mathrm {AA}}}\xspace at the LHC}{122}{subsubsection.5.2.1}
\contentsline {subsubsection}{\numberline {5.2.2}$\rm J/\psi $ \ensuremath {R_{\mathrm {AA}}}\xspace results at low \ensuremath {p_{\mathrm {T}}}\xspace }{123}{subsubsection.5.2.2}
\contentsline {subsubsection}{\numberline {5.2.3}$\rm J/\psi $ \ensuremath {R_{\mathrm {AA}}}\xspace results at high \ensuremath {p_{\mathrm {T}}}\xspace }{125}{subsubsection.5.2.3}
\contentsline {subsubsection}{\numberline {5.2.4}$\rm J/\psi $ azimuthal anisotropy}{127}{subsubsection.5.2.4}
\contentsline {subsubsection}{\numberline {5.2.5}$\rm J/\psi $ \ensuremath {R_{\mathrm {AA}}}\xspace results for various colliding systems and beam energies at RHIC}{128}{subsubsection.5.2.5}
\contentsline {subsubsection}{\numberline {5.2.6}Excited charmonium states}{129}{subsubsection.5.2.6}
\contentsline {subsubsection}{\numberline {5.2.7}Bottomonium \ensuremath {R_{\mathrm {AA}}}\xspace results}{130}{subsubsection.5.2.7}
\contentsline {subsection}{\numberline {5.3}Alternative references for quarkonium production in nucleus--nucleus collisions}{134}{subsection.5.3}
\contentsline {subsubsection}{\numberline {5.3.1}Proton--nucleus collisions}{134}{subsubsection.5.3.1}
\contentsline {subsubsection}{\numberline {5.3.2}Open heavy flavour}{134}{subsubsection.5.3.2}
\contentsline {subsection}{\numberline {5.4}Summary and outlook}{136}{subsection.5.4}
\contentsline {section}{\numberline {6}Quarkonium photoproduction in nucleus-nucleus collisions}{136}{section.6}
\contentsline {subsection}{\numberline {6.1}The flux of photons from lead ions at the LHC}{137}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Measurements of photonuclear production of charmonium during the Run\nobreakspace {}1\xspace at the LHC}{138}{subsection.6.2}
\contentsline {subsubsection}{\numberline {6.2.1}Photonuclear production of $\ensuremath {\mathrm {J}/\psi }\xspace $ at RHIC}{139}{subsubsection.6.2.1}
\contentsline {subsubsection}{\numberline {6.2.2}Coherent production of $\ensuremath {\mathrm {J}/\psi }\xspace $ in {Pb--Pb}\xspace UPC at the LHC}{139}{subsubsection.6.2.2}
\contentsline {subsubsection}{\numberline {6.2.3}Coherent production of $\ensuremath {\psi \text {(2S)}}\xspace $ in {Pb--Pb}\xspace UPC at the LHC}{140}{subsubsection.6.2.3}
\contentsline {subsubsection}{\numberline {6.2.4}Incoherent production of \ensuremath {\mathrm {J}/\psi }\xspace in {Pb--Pb}\xspace UPC at the LHC}{140}{subsubsection.6.2.4}
\contentsline {subsubsection}{\numberline {6.2.5}Coherent photonuclear production of $\ensuremath {\mathrm {J}/\psi }\xspace $ in coincidence with a hadronic {Pb--Pb}\xspace collision at the LHC}{141}{subsubsection.6.2.5}
\contentsline {subsection}{\numberline {6.3}Models for photonuclear production of charmonium}{141}{subsection.6.3}
\contentsline {subsubsection}{\numberline {6.3.1}Models based on vector dominance}{141}{subsubsection.6.3.1}
\contentsline {subsubsection}{\numberline {6.3.2}Models based on LO pQCD}{142}{subsubsection.6.3.2}
\contentsline {subsubsection}{\numberline {6.3.3}Models based on the colour dipole approach}{142}{subsubsection.6.3.3}
\contentsline {subsection}{\numberline {6.4}Photonuclear production of charmonium: comparing models to measurements}{143}{subsection.6.4}
\contentsline {subsection}{\numberline {6.5}Summary and outlook}{145}{subsection.6.5}
\contentsline {section}{\numberline {7}Upgrade programmes and planned experiments}{146}{section.7}
\contentsline {subsection}{\numberline {7.1}Introduction}{146}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Collider experiments}{146}{subsection.7.2}
\contentsline {subsubsection}{\numberline {7.2.1}The LHC upgrade programme}{146}{subsubsection.7.2.1}
\contentsline {paragraph}{The ALICE experiment}{146}{section*.26}
\contentsline {paragraph}{The ATLAS experiment}{148}{section*.27}
\contentsline {paragraph}{The CMS experiment}{149}{section*.28}
\contentsline {paragraph}{The LHCb experiment}{150}{section*.29}
\contentsline {subsubsection}{\numberline {7.2.2}The RHIC programme}{150}{subsubsection.7.2.2}
\contentsline {paragraph}{The sPHENIX project}{150}{section*.30}
\contentsline {paragraph}{STAR experiment}{151}{section*.31}
\contentsline {subsection}{\numberline {7.3}The fixed target experiments}{152}{subsection.7.3}
\contentsline {subsubsection}{\numberline {7.3.1}Low energy projects at SPS, Fermilab and FAIR}{152}{subsubsection.7.3.1}
\contentsline {subsubsection}{\numberline {7.3.2}Plans for fixed-target experiments using the LHC beams}{152}{subsubsection.7.3.2}
\contentsline {paragraph}{SMOG -- the first step}{153}{section*.32}
\contentsline {paragraph}{A Fixed-Target ExpeRiment at the LHC, AFTER@LHC}{153}{section*.33}
\contentsline {section}{\numberline {8}Concluding remarks}{154}{section.8}
