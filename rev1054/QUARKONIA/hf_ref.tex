\begin{figure}[!b]
  \centering
  \includegraphics[width=0.45\textwidth]{rhic_lowpt}
  \includegraphics[width=0.45\textwidth]{rhic_highpt}\\
  \hspace{0.45\textwidth}
  \includegraphics[width=0.45\textwidth]{lhc_highpt}
  \caption{Comparison of nuclear modification factors for open and closed charm
    at RHIC (top) and the LHC
    (bottom)~\cite{Adare:2010de,Adare:2006ns,Adamczyk:2012ey,Chatrchyan:2012np,ALICE:2012ab}.
    Transverse momentum integrated results are shown on the left, while high \pt
    \raa are compared on the right. Due to the lack of low \pt open charm data
    at the LHC, the bottom left panel is missing.}
  \label{fig:open_closed_HF}
\end{figure}


To study the effect of a deconfined medium on quarkonium production, we first
recall the underlying dynamics, using the \jpsi for illustration. The production
process in elementary hadronic collisions begins with the formation of a \ccbar
pair; this pair can then either lead to open charm production (about 90\%) or
subsequently bind to form a charmonium state (about 10\% for all charmonia).
%\fig{fig:jpsi_prod} shows the dominant high energy reaction through gluon fusion.
%
%\begin{figure}[b]
 % \centering
  %\includegraphics[width=0.5\textwidth]{ncharm}
  %\label{fig:jpsi_prod}
  %\caption{\ccbar production mechanism in \pp collisions.}
%\end{figure}
%
%The initial \ccbar production can be calculated in terms of the parton
%distribution functions $f_p$ of the relevant hadrons and the perturbative
%partonic cross section. While a full description of charmonium binding has so
%far resisted various theoretical attempts, the process is in good approximation
%independent of the incident hadronic collision
%energy~\cite{Gavai:1994in,Vogt:2009a}. This is a consequence of the fact that
%the heavy quark propagator in the reaction $gg \to \ccbar$ strongly dampens the
%mass variation of the \ccbar pair with incident energy. Thus the fractions of
%the produced \ccbar system into hidden vs.\ open charm as well as those for the
%different charmonium states are approximately constant; once determined at one
%energy, they remain the same also for different collision energies.
%
%
%A further important aspect of quarkonium production in elementary collisions is
%that the observed (1S) ground states \jpsi and \ups are in both cases partially
%produced through feed-down from higher excited states, leading to sequential
%suppression patterns~\cite{Karsch:2005nk}.
%
%
%Given the patterns observed in elementary collisions, it is interesting to see
%how they are modified in the presence of a medium, as provided by nuclear
%collisions. From the point of view of production dynamics, one way such
%modifications can arise is as {\it initial state effects}, taking place before
%the \ccbar pair is produced, as nuclear modifications of the parton distribution
%functions, and a possible energy loss of the partons passing through the nuclear
%medium to produce the \ccbar. Once produced, the pair can encounter {\it final
%  state effects}, either in the form of a phase space shift already of the
%\ccbar, \eg, through an energy loss of the unbound charm quarks, or through
%effects on the nascent or fully formed charmonium state. Such effects may arise
%from the passage through the cold nuclear medium, or because of the presence of
%the hot medium newly produced in the nuclear collision. In such a hot medium,
%colour screening in a QGP will decrease the binding force, both in strength and
%in its spatial range, and this should, for sufficiently energetic
%nucleus-nucleus collisions, suppress quarkonium formation.
%
%One has thus to specify the concept of quarkonium survival. 
Since quarkonium
production is to be used as a tool to study the medium produced in nuclear
collisions, the primary concern is not if such collisions produce more or fewer
\ccbar pairs than proton-proton collisions, but rather if the presence of the
medium modifies the fraction of produced \ccbar pairs going into charmonium
formation. In other words, the crucial quantity is the amount of charmonium
production relative to that of open charm. 
%To illustrate: in \pp collisions,
%about 2\% of the total \ccbar production goes into \jpsi. If in high energy
%nuclear collisions the total \ccbar production rate were reduced by a factor
%two, but still 2\% of these were going into \jpsi, then evidently \AAcoll
%collisions do not modify the \jpsi binding. 
Hence the relevant observable is the
fraction of charmonia to open charm, or more generally, that of quarkonia to the
relevant open heavy flavour production~\cite{Satz:1993pb,Satz:2013ama}. In this
quantity, if measured over the entire phase space, down to $\pt=0$, the effects
of possible initial state nuclear modifications cancel out, so that whatever
changes it shows relative to the \pp pattern is due to final state effects. Here
it should be noted that, since the distribution of the different open charm
channels is in good approximation energy-independent, the measurement of a
single such channel is sufficient ---it gives, up to a constant, the total open
charm cross-section~\cite{Satz:2013ama}.



A direct comparison of measured open and closed heavy-flavour cross sections has
not been performed yet at RHIC or the LHC. However, one can compare the measured
nuclear modification factors of D mesons (or heavy-flavour decay electrons as
their proxy) and \jpsi. At RHIC, the open charm cross section has been measured
in \pp and \AuAu via non-photonic single electrons from semileptonic charm
decays~\cite{Adare:2010de}. As shown in the top left of
\fig{fig:open_closed_HF}, the resulting \raa shows no deviation from binary
scaling, though the uncertainties are sizeable. Hence, one can conclude that the
modification of the \jpsi \raa at RHIC~\cite{Adare:2006ns} is a true final state
effect and not just a reduction of charm production by initial state effects. As
evident from the large uncertainties, the measurement of the total charm cross
section is extremely challenging. At the LHC this has not
been achieved yet, preventing such a comparison in the bottom left of
\fig{fig:open_closed_HF}. Instead one can try to make a comparison at high \pt
where both, open and closed charm, have been
measured~\cite{Chatrchyan:2012np,ALICE:2012ab}. This then opens the question
which \pt intervals are appropriate for such a comparison. A comparison of D and
\jpsi in the same \pt range will not access the same charm quarks and/or gluons.
This is an issue to be addressed on theoretical grounds. The comparison is
anyway performed, as shown in the bottom right panel of
\fig{fig:open_closed_HF}. The \raa of high-\pt D and \jpsi show a surprising
similar centrality dependence. This, however, is nothing new at the LHC. Already
at RHIC the same trend can be observed~\cite{Adare:2010de,Adamczyk:2012ey}, as
shown in the top right of \fig{fig:open_closed_HF}. The suppression of high-\pt
D mesons has been linked to charm quark energy loss inside the QGP. While the
\jpsi itself is a colourless object, its coloured precursor may be subject to
similar energy loss though current models underestimate the \jpsi suppression at
high \pt in \pb collisions at the LHC~\cite{Sharma:2012dy}.



%Possible alternative variables to consider are the production ratios of the
%different quarkonium states; this has very recently become of particular
%interest for bottomonium studies. Since \upsa, \upsb, and \upsc lie quite close
%to each other in mass, in the ratio of their rates again initial state effects
%are expected cancel out.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../SaporeGravis"
%%% End: 
