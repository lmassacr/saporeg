Lattice studies on quarkonium mostly consist in the calculations of 
spectral functions for temperatures in the range explored by the 
experiments.  To motivate these studies and, at the same time,
to get a feeeling of their limitations, we can consider the relation
between the rate of production  of muon pairs 
$\frac{dN_{\mu \bar \mu} }{d^4xd^4q}$
and the spectral functions $\rho(\omega)$:
$\frac{dN_{\mu \bar \mu} }{d^4xd^4q} = F(q,T,...)\rho(\omega)$.
The connection between the invariant mass distribution 
and the spectral function is clear, although the dynamical
factor $F$ is largely unknown. Understanding in detail this connection
is an important aspect of ongoing research, outside the scope of
lattice studies: on the lattice we 
concentrate on the calculations of spectral functions at equilibrium. 

Lattice calculations of equilibrium properties of thermal QCD are mostly
motivated by the following consideration: it turns out 
that  there is a large  temperature range  around the phase
transition to the quark gluon plasma  
 which is not amenable to any analytic approaches, even when
the most sophisticated high temperature expansions and model 
analyses are being used.
This range includes the region explored by experiments, and this is  where
 lattice simulations are being performed~\cite{Ding:2014kva,Lat,Conf,Aarts:2014cda,Aarts:2013kaa}. Bulk thermodynamics is by now studied with confidence, with agreement
among different groups around the critical temperature, where
the charm quark can be ignored in the dynamics of the gauge fields. 
Thermodynamics  at larger temperatures (above $400$ MeV), 
where the charm becomes relevant, is now under scrutiny. 

Spectral functions  
play an important role in understanding how
elementary excitations are modified in a thermal medium. 
They are the power spectrum of autocorrelation functions in real time,
hence provide a direct information on  large time propagation.
In the lattice approach  such real time evolution is not directly
accessible:  the theory is formulated in a four dimensional box -- 
three dimensions are spatial dimensions, the fourth is the imaginary 
(Euclidean) time.  The lattice temperature $T_L$
is realized through (anti)periodic boundary conditions in the Euclidean time 
direction -- $T_L = 1/N_\tau$, where $N_\tau$ is the extent of the time 
direction, and can be converted to physical units once the lattice spacing
is known.  The spectral functions appear now in the 
decomposition  of a  (zero-momentum)  Euclidean propagator $G(\tau)$:  
$
G(\tau) = \int_{0}^\infty \rho[\omega) \frac{d\omega}{2\pi}\,  K(\tau,\omega)$,
with  $K(\tau,\omega) =
\frac{\left(e^{-\omega\tau} + e^{-\omega(1/T - \tau)}\right)}
{1 - e^{-\omega/T}}$.
The $\tau$ dependence of the kernel $K$  reflects the periodicity
of the relativistic propagator in imaginary  time, as well
as its $T$ symmetry. The Bose--Einstein distribution, 
intuitively,  describes the wrapping around the periodic box which
becomes increasingly important at higher temperatures. 

The simple recipe is then: generate an appropriate ensemble of lattice
gauge fields at a temperature of choice, compute on such ensemble the
Euclidean propagators $G(\tau)$, 
and extract  the spectral functions.  To
 briefly summarize the current outcome of such program, 
any study of charmonium and bottomonium  
produced  sensible qualitative results: at {\em some} temperature T 
above, and  {\em not too far} from the critical temperature, and possibly coinciding with it, a given status melts. The devil is in the details, and
a final consensus on quantitative issues has not been reached yet. Why?
Firstly,  experiences with lattice calculations has taught us that
it is extremely important to have results in the continuum limit, and with the
proper matter content. This  means that the masses of the 
dynamical quark fields which
are used in the generation of the gauge ensembles must be as close as possible
to the physical ones,and the lattice spacing should be fine enough to allow
contact with continuum physics.  
These systematic effects, which have been studied in detail for bulk thermodynamics, are still under scrutiny for the spectral functions.Second, the calculation of spectral 
functions using Euclidean propagators as an input is a difficult,
ill-defined problem.  It has been mostly tackled by using the Maximum Entropy
Method (MEM) \cite{Asakawa:2000tr}, which has proven successful in a
variety of applications. 
Recently, an alternative Bayesian reconstruction of the spectral
functions has been proposed in refs. \cite{Rothkopf:2011ef,Burnier:2013nla},
and applied to the analysis of HotQCD configurations \cite{Kim:2014iga}.

\noindent
{\em Charm}
Most calculations of charmonium spectral functions have been performed
in the quenched approximation, although recently the 
spectral functions of the charmonium states have been studied  
as a function of both temperature and momentum, using as input 
relativistic propagators with two light quarks
\cite{Aarts:2007pk,Kelly:2013cpa} and, more recently, including
  the strange quark, for
temperatures ranging between $0.76 T_c$ and $1.9 T_c$. The sequential
dissolution of the peaks corresponding to the S- and P-wave
states is clearly seen. 
 The results are consistent with the expectation
that charmonium melts at high temperature, however as of today they lack 
quantitative precision and control over systematic errors. 


\noindent
{\em Beauty}
For bottomonium the lattice systematic errors affecting charmonium study are 
to some extent lessened.  
This is at a first sight surprising, as the large mass of
bottomonium apparently calls for a finer lattice spacing, which in turn
calls for a larger number of sites.  However, this apparent 
complication --  a larger mass -- 
allows for an important semplification, namely the
possibility of a non-relativistic study\cite{Aarts:2010ek}. 
NRQCD has proven very successful at zero temperature for beauty,
and since the involved temperatures are still much smaller than the
bottom threshold it remains applicable in  thermal systems. 
Also the second source of 
uncertainties -- the reconstruction of spectral functions themselves --
is easier for beauty: when
the significant frequency range greatly exceeds the temperature,  
$K(\tau,\omega) \simeq \left(e^{-\omega\tau} + e^{-\omega(1/T - \tau)}\right)$:
backwards and forwards propagations are  decoupled and the spectral
relation reduces to 
$
G(\tau) = \int_{\omega_0}^\infty\frac{d\omega'}{2\pi}\, \exp(-\omega'\tau)\rho(\omega').$
This approximation holds true for the beauty:
the interesting physics takes place around the two-quark threshold,
$\omega\sim 2M \sim 8$ GeV for $b$ quarks, which is still much
larger than our 
temperatures $T < 0.5$ GeV. 

Recent results for bottomonium
\cite{Aarts:2014cda} have been obtained by analysing gauge
field configurations with two active light quarks and one heavier quark,
using the 
NRQCD approximation for the bottom quark 
The MEM results~\cite{Aarts:2014cda} for the Upsilon 
clearly demonstrate the persistence of
the fundamental state  above $T_c$ as well as the suppression
of the excited states.  Results have been obtained also using a novel Bayesian
approach \cite{Burnier:2013nla,Lat,Conf}. In this channel 
the results are in substantial agreement, even if there are differences 
in the detailed shapes of the spectral functions. 
The observed  patterns should be contrasted, for instance,
with the one observed by the CMS experiment: for an estimated temperature
of about $420$ MeV the excited peaks of the invariant mass distribution
are suppressed.  For the $P$-wave $\chi_{b1}$ the results are less clear. 
The differences among the two reconstruction methods 
are more pronounced, the MEM suggesting melting at $T_c$ and the
novel Bayesian method persistence\cite{Burnier:2013nla,Lat,Conf}.   Here  
checks of the systematic errors are still in progress, and in particular
we would like to assess the fate of the fundamental state at $T_c$, possibly
before experimental results --- which are still lacking in this sector --- 
appear!


One important next step is a full control over matter content in our lattice
simulations. The gauge fields used in the NRQCD analysis, for instance,  
have been generated with
$\frac{m_\pi}{m_\rho} \simeq 0.4$, and with $m_s$
set to its physical value.  We aim at physical $m_{u,d,s}$ masses
which should correspond to the correct matter
content in the range $T \le 400$ MeV. Above $400$ MeV a dynamical
charm quark might become relevant as well,and investigations of spectral
functions in this temperature range might require its inclusion. 
On the methodological side,  applications of a  generalised integral transform
might ease the inversion task \cite{Pederiva:2014qea}, and
model calculations will provide very useful testbeds for the different techiques
\cite{Colangelo:2012jy}.  These studies are in progress,
with the final aim of obtaining a controlled set of results for the
spectral functions of charmonia and bottomonia in the range of temperatures
explored at the LHC. 

% 
% %
% \begin{thebibliography}{99}
% %
% \bibitem{Ding:2014kva} 
%   H.~T.~Ding,
%   %``Recent lattice QCD results and phase diagram of strongly interacting matter,''
%   arXiv:1408.5236 [hep-lat].
%   %%CITATION = ARXIV:1408.5236;%%
% %\bibitem{Ding}  H.-T. Ding, {\em Recent results from lattice QCD and the phase diagram of strongly interacting matter}, plenary 
% %talk at {\em Quark Matter 2014 - XXIV International Conference on Ultrarelativistic Nucleus-Nucleus Collisions},  to appear in the Proceedings
% \bibitem{Lat}{\sc FASTSUM+},  
% T. Harris {\em et al.}, talk presented by T. Harris 
% at {\em Lattice 2014}, to appear in the Proceedings 
% \bibitem{Conf} {\sc FASTSUM+}, J.-I. Skullerud {\em et al.}, talk presented by J.-I. Skullerud
% at {\em Confinement 2014}, to appear in the Proceedings
% \bibitem{Aarts:2014cda}
%   G.~Aarts, C.~Allton, T.~Harris, S.~Kim, M.~P.~Lombardo, S.~M.~Ryan and J.~I.~Skullerud,
%   %``The bottomonium spectrum at finite temperature from N$_{f}$ = 2 + 1 lattice QCD,''
%   JHEP {\bf 1407} (2014) 097
% \bibitem{Aarts:2013kaa}
%   G.~Aarts, C.~Allton, S.~Kim, M.~P.~Lombardo, S.~M.~Ryan and J.-I.~Skullerud,
%   %``Melting of P wave bottomonium states in the quark-gluon plasma from lattice NRQCD,''
%   JHEP {\bf 1312} (2013) 064
% \bibitem{Burnier:2007qm}
%   Y.~Burnier, M.~Laine and M.~Vepsalainen,
%   %``Heavy quarkonium in any channel in resummed hot QCD,''
%   JHEP {\bf 0801} (2008) 043
% \bibitem{Asakawa:2000tr}
%   M.~Asakawa, T.~Hatsuda and Y.~Nakahara,
%   %``Maximum entropy analysis of the spectral functions in lattice QCD,''
%   Prog.\ Part.\ Nucl.\ Phys.\  {\bf 46} (2001) 459
% \bibitem{Rothkopf:2011ef}
%   A.~Rothkopf,
%   %``Improved Maximum Entropy Analysis with an Extended Search Space,''
%   J.\ Comput.\ Phys.\  {\bf 238} (2013) 106
% \bibitem{Burnier:2013nla} 
%   Y.~Burnier and A.~Rothkopf,
%   %``Bayesian Approach to Spectral Function Reconstruction for Euclidean Quantum Field Theories,''
%   Phys.\ Rev.\ Lett.\  {\bf 111}, no. 18, 182003 (2013)
% \bibitem{Kim:2014iga}
%   S.~Kim, P.~Petreczky and A.~Rothkopf,
%   %``Lattice NRQCD study of S- and P-wave bottomonium states in a thermal medium with $N_f=2+1$ light flavors,''
%   arXiv:1409.3630 [hep-lat]
% \bibitem{Aarts:2007pk}
%   G.~Aarts, C.~Allton, M.~B.~Oktay, M.~Peardon and J.~I.~Skullerud,
%   %``Charmonium at high temperature in two-flavor QCD,''
%   Phys.\ Rev.\ D {\bf 76} (2007) 094513
% \bibitem{Kelly:2013cpa}
%   A.~Kelly, J.~I.~Skullerud, C.~Allton, D.~Mehta and M.~B.~Oktay,
%   %``Spectral functions of charmonium from 2 flavour anisotropic lattice data,''
%   PoS LATTICE {\bf 2013} (2013) 170
% \bibitem{Aarts:2010ek}
%   G.~Aarts, S.~Kim, M.~P.~Lombardo, M.~B.~Oktay, S.~M.~Ryan, D.~K.~Sinclair and J.-I.~Skullerud,
%   %``Bottomonium above deconfinement in lattice nonrelativistic QCD,''
%   Phys.\ Rev.\ Lett.\  {\bf 106} (2011) 061602
% 
% \bibitem{Pederiva:2014qea}
%   F.~Pederiva, A.~Roggero and G.~Orlandini, Phys. Rev. {\bf B88} (2013) 094302;
%   %``Use of the Sumudu transform to extract response functions from Quantum Monte Carlo calculations,''
%   J.\ Phys.\ Conf.\ Ser.\  {\bf 527} (2014) 012011
% 
% \bibitem{Colangelo:2012jy}
%   P.~Colangelo, F.~Giannuzzi and S.~Nicotri,
%   %``In-medium hadronic spectral functions through the soft-wall holographic model of QCD,''
%   JHEP {\bf 1205} (2012) 076
% 
% 
% 
