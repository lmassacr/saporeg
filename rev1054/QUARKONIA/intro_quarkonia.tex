Quarkonia are considered important probes of the QGP formed in heavy-ion
collisions. In a hot and deconfined medium quarkonium production is
expected to be significantly suppressed with respect to the proton-proton yield,
scaled by the number of binary nucleon-nucleon collisions, as long as the total
charm cross section remains unmodified\footnote{As open heavy flavour and
  quarkonia are produced via the same processes, any modifications of the
  initial state will not modify the yield ratio of quarkonia to open heavy
  flavour states.}. The origin of such a suppression, taking place in the QGP,
is thought to be the colour screening of the force that binds the \ccbar
(\bbbar) state~\cite{Matsui:1986dk}. In this scenario, quarkonium suppression
should occur sequentially, according to the binding energy of each meson:
strongly bound states, such as the \upsa\ or the \jpsi, should melt at higher
temperatures with respect to the more loosely bound ones, such as the \chib,
\upsb, or \upsc for the bottomonium family or the \psiP and the \chic for the
charmonium one. As a consequence, the in-medium dissociation probability of
these states should provide an estimate of the initial temperature reached in
the collisions~\cite{Digal:2001ue}. However, the prediction of a sequential
suppression pattern is complicated by several factors. Feed-down
decays of higher-mass resonances, and of $b$-hadrons in the case of charmonium, 
contribute to the observed yield of quarkonium states. Furthermore, other hot and cold
matter effects can play a role, competing with the suppression mechanism.

On the one hand, the production of $c$ and $\overline{c}$ quarks increases with increasing 
centre-of-mass energy. Therefore, at high energies, as at the LHC,
the abundance of $c$ and $\overline{c}$ quarks might lead to a new charmonium
production source: the (re)combination of these quarks throughout the collision
evolution~\cite{Thews:2000rj} or at the hadronization
stage~\cite{BraunMunzinger:2000px,Stachel:2013zma}. This additional charmonium
production mechanism, taking place in a deconfined medium, enhances the \jpsi
yield and might counterbalance the expected \jpsi suppression. Also the \bbbar
cross section increases with energy, but, given the smaller number of \bbbar
pairs, with respect to \ccbar, this contribution is less important for
bottomonia even in high-\snn collisions.

On the other hand, quarkonium production is also affected by several effects
related to cold matter (the so-called cold nuclear matter effects, CNM)
discussed in \sect{Cold nuclear matter effects}. For example, the production
cross section of the \QQbar pair is influenced by the kinematic parton
distributions in nuclei, which are different from those in free protons and
neutrons (the so-called nuclear PDF effects). In a similar way, approaches based
on the Colour-Glass Condensate (CGC) effective theory assume that a gluon
saturation effect sets in at high energies. This effect influences the
quarkonium production occurring through fusion of gluons carrying small values
of the Bjorken-$x$ in nuclei. Furthermore, parton energy loss in the nucleus may
decrease the pair momentum, causing a reduction of the quarkonium production at
large longitudinal momenta. Finally, while the \QQbar pair evolves towards the
fully-formed quarkonium state, it may also interact with partons of the crossing
nuclei and eventually break-up. This effect is expected to play a dominant role
only for low-\snn collisions, where the crossing time of the (pre)-resonant
state in the nuclear environment is rather large. On the contrary, this
contribution should be negligible at high-\snn, where, due to the decreased
crossing time, resonances are expected to form outside the nuclei.
%
Cold nuclear matter effects are investigated in proton-nucleus collisions.
 Since these effects are present also in
nucleus-nucleus interactions, a precise knowledge of their role is crucial in
order to correctly quantify the effects related to the formation of hot QCD
matter.

The in-medium modification of quarkonium production, induced by either hot or
cold matter mechanisms, is usually quantified through the nuclear modification
factor \raa, defined as the ratio of the
quarkonium yield in \AAcoll collisions ($N_{\rm AA}^{\QQbar}$) and the expected
value obtained by scaling the production cross section in pp collisions ($\sigma_{\rm
  pp}^{\QQbar}$) by the average nuclear overlap function ($\av{\taa}$), evaluated through a
Glauber model calculation~\cite{Aamodt:2010cz}:
\begin{equation}
  \centering
  \raa = \frac{N_{\rm AA}^{\QQbar}}{\langle T_{\rm AA} \rangle \times \sigma_{\rm pp}^{\QQbar}}\,.
\end{equation}
\raa is expected to equal unity if nucleus--nucleus collisions behave
as a superposition of nucleon--nucleon interactions. This is, \eg, the case for
electroweak probes (direct $\gamma$, W, and Z) that do not interact
strongly~\cite{Afanasiev:2012dg,Chatrchyan:2012vq,Chatrchyan:2012nt,Aad:2012ew,Chatrchyan:2014csa}.
Such a scaling is assumed to approximately hold for the total charm cross
section, although an experimental verification has large uncertainties at RHIC
($\approx 30\%$)~\cite{Adare:2010de,Adamczyk:2014uip} and is still lacking at the LHC
(see discussion in \sect{OHF}).  A value of \raa
different from unity implies that the quarkonium production in \AAcoll is
modified with respect to a binary nucleon-nucleon scaling. Further insight on
the in-medium modification of quarkonium production can be obtained by 
investigating the rapidity and transverse momentum dependence of the nuclear
modification factor.

The information from \raa can be complemented by the study of the quarkonium
azimuthal distribution with respect to the reaction plane, defined by the beam
axis and the impact parameter vector of the colliding nuclei. The second
coefficient of the Fourier expansion of the 
azimuthal distribution, \vtwo, is called elliptic flow, as explained in
\sect{OHF}. Being sensitive to the dynamics of the partonic stages of
heavy-ion collisions, \vtwo can provide details on the
quarkonium production mechanisms: in particular, \jpsi produced through a
recombination mechanism, should inherit the elliptic flow of the charm quarks in
the QGP, acquiring a positive \vtwo.

Studies performed for thirty years, first at the SPS (\snn = 17\GeV) and then at
RHIC (\snn = 39--200\GeV)\footnote{References to experimental results
  are reported in Tables~\ref{tab:expSummary_SPS}
  and~\ref{tab:expSummary_RHIC}.}, have indeed shown a reduction of the \jpsi
yield beyond the expectations from cold nuclear matter effects (such as nuclear
shadowing and \ccbar break-up). Even if the centre-of-mass energies differ by a
factor of ten, the amount of suppression, with respect to \pp collisions,
observed by SPS and RHIC experiments at midrapidity is rather similar.
Furthermore, \jpsi suppression at RHIC is, unexpectedly, smaller at midrapidity
than at forward rapidity ($y$), in spite of the higher energy density which is
reached close to $y \sim 0$. These observations suggest the existence of an
additional contribution to \jpsi production at midrapidity, interpreted in terms of the
previously mentioned (re)combination process. (Re)combination might, therefore,
set in already at RHIC energies and can compensate for some of the quarkonium
suppression due to screening in the QGP.

The measurement of charmonium production is especially promising at
the LHC, where the higher energy density reached in the medium and the larger
number of \ccbar pairs produced in central \PbPb collisions (increased by a
factor ten with respect to RHIC energies, see \fig{fig:pp:CharmAndBottomXsec}) should help to disentangle suppression
and (re)combination scenarios. Furthermore, at LHC energies also bottomonium
states, which were barely accessible at lower energies, are abundantly produced.
Bottomonium resonances should shed more light on the processes affecting the
quarkonium behaviour in the hot matter. The \ups mesons are, as previously
discussed, expected to be less affected by production through (re)combination
processes, due to the much smaller abundance of $b$ and $\overline{b}$ quarks in
the medium with respect to $c$ and $\overline{c}$ (in central \PbPb collisions
at LHC energies, the number of \ccbar is a factor $\sim 20$ higher than the
number of \bbbar pairs). Furthermore, due to the larger mass of the $b$ quark,
cold nuclear matter effects, such as shadowing, are expected to be less
important for bottomonium than for charmonium states.

\begin{figure}[!t]
  \centering
   \includegraphics[width=0.4\textwidth]{LHC_Jpsi_acceptance}
   \includegraphics[width=0.4\textwidth]{LHC_Ups_acceptance}
   \caption{Left: \pt-$y$ acceptance coverage of the ALICE (red) and CMS (blue)
     experiments for \jpsi. Right: \pt-$y$ acceptance coverage of the ALICE and
     CMS experiments for \upsn. Filled areas correspond the the ranges
     investigated in recent ALICE and CMS quarkonium publications. The hashed
     areas correspond to the acceptance range which can potentially be covered
     by the experiments. In fact, while the high-\pt reach in ALICE is limited
     by statistics, the low-\pt \jpsi coverage by CMS is limited by the muon
     identification capabilities, affected by the large background in \PbPb
     collisions.}
  \label{fig:Acceptance}
\end{figure}

\begin{table*}[!t]
  \centering
  \caption{Quarkonium results obtained in \AAcoll at SPS. The nucleon-nucleon
    energy in the centre-of-mass frame (\snn), the covered kinematic range, the
    probes and observables are reported.}
  \label{tab:expSummary_SPS}
  \begin{tabular*}{\textwidth}{@{\extracolsep{\fill}}llllll@{~}l@{}}
    % \begin{tabular}{|p{0.11\textwidth}|p{0.08\textwidth}|p{0.06\textwidth}|p{0.12\textwidth}|p{0.12\textwidth}|p{0.3\textwidth}|}
    \hline
    Probe & Colliding  & \snn & $y$ & \pt  & Observables & Ref. \\
    & system & (\GeV) &  & (\GeVc)& & \\
    \hline
    \multicolumn{7}{c}{NA38}\\
    \hline
    \jpsi & S--U & 17.2 & $0<y<1$ & $\pt>0$& $\sigma_{\jpsi}$, $\sigma_{\jpsi}/\sigma_{\text{Drell-Yan}}(\text{cent.})$ & \cite{Abreu:1998wx}\\
    \psiP & & & & & $\sigma_{\psiP}$, $\sigma_{\psiP}/\sigma_{\text{Drell-Yan}}(\text{cent.})$ & \\
    \hline
    \multicolumn{7}{c}{NA50}\\
    \hline
    \jpsi & \pb & 17.2 & $0<y<1$ & $\pt>0$& yield(\pt), $\sigma_{\jpsi}$ and $\sigma_{\jpsi}/\sigma_{\text{Drell-Yan}}(\text{cent.})$ & \cite{Abreu:1997ji,Abreu:1997jh,Abreu:1999qw,Abreu:2000ni,Abreu:2000xe,Abreu:2001kd,Alessandro:2004ap}\\
    \psiP & & & & & yield(\pt), $\sigma_{\psiP}/\sigma_{\text{Drell-Yan}}$ and $\sigma_{\psiP}/\sigma_{\jpsi}(\text{cent.})$ & \cite{Abreu:2000xe,Alessandro:2006ju}\\
    \hline
    \multicolumn{7}{c}{NA60} \\
    \hline
    \jpsi & In--In & 17.2 & $0<y<1$ & $\pt>0$& $\sigma_{\jpsi}/\sigma_{\text{Drell-Yan}}(\text{cent.})$ & \cite{Arnaldi:2007zz,Arnaldi:2009ph}\\
    & & & & & polarization & \cite{Arnaldi:2009ph}\\ 
    \hline
  \end{tabular*}
\end{table*}
\begin{table*}[!h]
  \centering
  \caption{Quarkonium results obtained in \AAcoll from RHIC experiments. The
    experiment, the probes, the collision energy (\snn), the covered kinematic range
    and the observables are indicated.}
 \label{tab:expSummary_RHIC}
  \begin{tabular*}{\textwidth}{@{\extracolsep{\fill}}llllll@{~}l@{}}
    % \begin{tabular}{|p{0.11\textwidth}|p{0.08\textwidth}|p{0.06\textwidth}|p{0.12\textwidth}|p{0.14\textwidth}|p{0.37\textwidth}|}
    \hline
    Probe  & Colliding  & \snn  & $y$  & \pt  & Observables & Ref. \\
     & system & (\GeV) &  & (\GeVc)& & \\
    \hline
    \multicolumn{7}{c}{PHENIX} \\
    \hline
    \jpsi & \AuAu & 200 & $1.2<|y|<2.2$  & $\pt>0$ & yield and $\raa(\text{cent.},\,\pt,\,y)$&\cite{Adler:2003rc,Adare:2006ns,Adare:2011yf} \\
    & & & $|y|<0.35$ & & & \\
    & & & & $0<\pt<5$ & $\vtwo(\pt,\,y)$ & \cite{Silvestre:2008tw}\\
    & & & $1.2<|y|<2.2$ & & & \cite{Atomssa:2009ek}\\
    & \CuCu & & $1.2<|y|<2.2$ & $\pt>0$ & yield and $\raa(\text{cent.},\,\pt,\,y)$ & \cite{Adare:2008sh}\\
    &  &   & $|y|<0.35$ & & &\\
    & \CuAu & & $1.2<|y|<2.2$ & & yield and $\raa(\text{cent.},\,y)$ &\cite{Adare:2014nsa}\\
    & \AuAu & 62.4 & & & yield$(\text{cent.},\,\pt)$, $\raa(\text{cent.})$ & \cite{Adare:2012wf}\\
    & & 39 & & & &\\
    \upsabc & & 200  & $|y|<0.35$ & & yield, $\raa\text{(cent.)}$& \cite{Adare:2014hje}\\
    \hline
    \multicolumn{7}{c}{STAR}\\
    \hline
    \jpsi & \AuAu & 200 & $|y|<1$ & $\pt>0$ & yield and $\raa(\text{cent.},\,\pt)$ & \cite{Adamczyk:2012ey,Adamczyk:2013tvk}\\
     & & & & & $\vtwo(\text{cent.},\,\pt)$ & \cite{Adamczyk:2012pw}\\
     & \CuCu & & & & yield and $\raa(\text{cent.},\,\pt)$  & \cite{Abelev:2009qaa,Adamczyk:2013tvk}\\
     & \UU & 193 & & & $\raa(\pt)$ & \cite{Zha:2014nia}\\
     & \AuAu & 62.4 & & & yield, $\raa(\text{cent.},\,\pt)$ & \\
     & & 39 & & & & \\
     \upsa & & 200 & & & $\sigma$ and $\raa(\text{cent.})$& \cite{Adamczyk:2013poh}\\
     \upsabc & & & & & $\raa(\text{cent.})$ & \\
     & \UU & 193 & & & & \cite{Zha:2014nia}\\
    \hline
  \end{tabular*}
\end{table*}


The four large LHC experiments (ALICE, ATLAS, CMS, and LHCb) have carried out
studies on quarkonium production either in \PbPb collisions at \snn =
2.76\TeV\footnote{References to experimental results are reported in
  \tab{tab:expSummary_LHC}.} or in \pPb collisions at \snn = 5.02\TeV. Quarkonium
production has been also investigated in \pp interactions at \s = 2.76, 7 and
8\TeV. 
The four
experiments are characterised by different kinematic coverages, allowing one to
investigate quarkonium production in $|y|<4$, down to zero transverse momentum.

ATLAS and CMS are designed to measure quarkonium production by reconstructing the
various states in their dimuon decay channel. They both cover the mid-rapidity
region: depending on the quarkonium state under study and on the \pt range
investigated, the CMS rapidity coverage can reach up to $|y|<2.4$, and a similar
$y$ range is also covered by ATLAS. ALICE measures quarkonium in two rapidity
regions: at mid-rapidity ($|y|<0.9$) in the dielectron decay channel and at
forward rapidity ($2.5<y<4$) in the dimuon decay channel, in both cases down to
zero transverse momentum. LHCb has taken part only in the \pp and \pA LHC
programmes during Run~1 and their results on quarkonium production, reconstructed through the
dimuon decay channel, are provided at forward rapidity ($2<y<4.5$), down to zero
\pt. As an example, the \pt-$y$ acceptance coverages of the ALICE and CMS
experiments are sketched in \fig{fig:Acceptance} for \jpsi (left) and \ups
(right).

In Tables ~\ref{tab:expSummary_SPS}--\ref{tab:expSummary_LHC}, a summary of the
charmonium and bottomonium results obtained in \AAcoll collisions by the SPS,
RHIC, and LHC experiments are presented, respectively.


\begin{table*}[!t]
  \centering
  \caption{Quarkonium results obtained in \AAcoll from LHC experiments. The
    experiment, the probes, the collision energy (\snn), the covered kinematic range
    and the observables are indicated.}
  \label{tab:expSummary_LHC}
  \begin{tabular*}{\textwidth}{@{\extracolsep{\fill}}llllll@{~}l@{}}
    % \begin{tabular}{|p{0.11\textwidth}|p{0.08\textwidth}|p{0.06\textwidth}|p{0.12\textwidth}|p{0.14\textwidth}|p{0.37\textwidth}|}
    \hline
    % Experiment and probe & Beam mode & \snn (\TeV) & $y_{cms}$ range& \pt range (\GeVc)& Observables [Ref] \\
    Probe & Colliding  & \snn  & $y$ & \pt  & Observables & Ref. \\
      & system &  (\TeV) &  & (\GeVc)& & \\
    \hline
    \multicolumn{7}{c}{ALICE} \\
    \hline
    \jpsi & \pb & 2.76 & $|y|<0.9$ & $\pt>0$ & $\raa(\text{cent.,\,\pt})$ & \cite{Abelev:2013ila,Adam:2015rba}\\ 
    & & & $2.5<y<4$ & $\pt>0$ & $\raa(\text{cent.},\,\pt,\,y)$ & \cite{Abelev:2012rv,Abelev:2013ila}\\ 
    & & & & $0<\pt<10$ & $\vtwo(\text{cent.},\,\pt)$ & \cite{ALICE:2013xna}\\
    \psiP & & & & $\pt<3$ & $\frac{(N_{\psiP}/N_{\jpsi})_{\mathrm{Pb-Pb}}}{(N_{\psiP}/N_{\jpsi})_{\mathrm{pp}}}(\text{cent.})$ & \cite{Arnaldi:2012bg}\\
    & & & & $3<\pt<8$ & & \\
    \upsa & & & & $\pt>0$ & $\raa(\text{cent.},\,y)$ & \cite{Abelev:2014nua} \\
    \hline
    \multicolumn{7}{c}{ATLAS} \\
    \hline
    \jpsi & \pb & 2.76 & $|\eta|<2.5$ & $\pt\gtrsim6.5$ & $\rcp(\text{cent.})$ & \cite{Aad:2010aa}\\
    \hline
    \multicolumn{7}{c}{CMS} \\
    \hline
    \jpsi (prompt) & \pb & 2.76   & $|y|<2.4$ & $6.5<\pt<30$& yield and $\raa(\text{cent.},\,\pt,\,y)$ & \cite{Chatrchyan:2012np}\\ 
    & & & & & $\vtwo(\text{cent.},\,\pt,\,y)$ & \cite{CMS:2013dla}\\
    & & & $1.6<|y|<2.4$ & $3<\pt<30$ & &\\
    & & & $|y|<1.2$ & $6.5<\pt<30$ & yield and \raa & \cite{Chatrchyan:2012np}\\
    & & & $1.2<|y|<1.6$ & $5.5<\pt<30$ & & \\
    & & & $1.6<|y|<2.4$ & $3<\pt<30$ & &\\
    % &   &     &  & &   (prompt \jpsi identification)\\
    \psiP (prompt) & & & $1.6<|y|<2.4$ & $3<\pt<30$ & \raa, $\frac{(N_{\psiP}/N_{\jpsi})_{\mathrm{Pb-Pb}}}{(N_{\psiP}/N_{\jpsi})_{\mathrm{pp}}}(\text{cent.})$ & \cite{Khachatryan:2014bva}\\
    & & & $|y|<1.6$ & $6.5<\pt<30$ & & \\
    \upsa & & & $|y|<2.4$ & $\pt>0$ & yield and $\raa(\text{cent.},\,\pt,\,y)$ & \cite{Chatrchyan:2012np}\\
    \upsn & & & $|y|<2.4$ & $\pt>0$ & $\raa(\text{cent.})$ & \cite{Chatrchyan:2011pe,Chatrchyan:2012lxa}\\ 
    & & & & & $\frac{(N_{\upsb}/N_{\upsa})_{\mathrm{Pb-Pb}}}{(N_{\upsb}/N_{\upsa})_{\mathrm{pp}}}(\text{cent.})$ & \cite{Chatrchyan:2013nza}\\ 
    \hline
  \end{tabular*}
\end{table*}

%
This section is organised as follows. In the first part, a theoretical overview is
presented, in which the sequential suppression pattern of quarkonia and the
lattice calculations are introduced. Other effects, such as modifications of the
parton distribution functions inside nuclei and their influence on
nucleus-nucleus collisions are discussed. Along with the suppression, the
enhancement of quarkonia is also considered through two different approaches to
(re)generation: the statistical hadronization model and transport models. In the
context of bottomonium studies, 
non-equilibrium effects on quarkonium suppression in the anisotropic
hydrodynamic framework are also discussed. Finally, the collisional
dissociation model and the comover interaction model are briefly
introduced.

In the second part, experimental quarkonium results are reviewed. The recent LHC results,
starting with a brief discussion on the quarkonium production cross sections in
\pp collisions as necessary references to build the nuclear modification
factors, are presented. The description of the experimental \raa results for
\jpsi production, both at low and high \pt is then addressed. The LHC
results are compared to those at RHIC energies and to 
theoretical models. A similar discussion is also introduced for the \jpsi
azimuthal anisotropy. Results obtained at RHIC from variations of the
beam-energy and collision-system are also addressed. The charmonium section is
concluded with a discussion of \psiP production.
Next, the bottomonium results on ground and excited states at RHIC and LHC energies are discussed. 

Finally, other possible
references for the quarkonium behaviour in nucleus-nucleus collisions, namely
proton--nucleus collisions and open heavy flavour, production are discussed.




%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../SaporeGravis"
%%% End: 
