The space-time evolution of the phase-space distribution, $f_{\cal Q}$, of a
quarkonium state ${\cal Q} = \Psi, \Upsilon$ ($\Psi$=$J/\psi, \chi_c, \dots $;
$\Upsilon$=$\Upsilon(1S),\chi_b, \dots$) in hot and dense matter may be
described by the relativistic Boltzmann equation,
\begin{equation}
  p^\mu \partial_\mu f_{\cal Q}(\vec r,\tau;\vec p) =
  - E_p \ \Gamma_{\cal Q}(\vec r,\tau;\vec p) \ f_{\cal Q}(\vec r,\tau;\vec p) +
  E_p \ \beta_{\cal Q}(\vec r,\tau;\vec p)  \ 
\label{eq:boltz}
\end{equation}   
where $p_0=E_p=(\vec p^2 +m_{\cal Q}^2)^{1/2}$, $\Gamma_{\cal Q}$ denotes the
dissociation rate, and a possible mean-field term has been neglected. The gain
term, $\beta_{\cal Q}$, depends on the phase-space distribution of the
individual heavy (anti-) quarks, $Q$=$c,b$ in the QGP (or $D$, $\bar D$ mesons
in hadronic matter). If the open-charm states are thermalized, and in the limit
of a spatially homogeneous medium, one may integrate over the spatial and
3-momentum dependencies to obtain the rate
equation~\cite{Grandchamp:2003uw,Grandchamp:2005yw,Rapp:2009my}
\begin{equation}
  \frac{dN_{\cal Q}}{d\tau} =  -\Gamma_{\cal Q}(T) [ N_{\cal Q} - N_{\cal Q}^{\rm eq}(T) ] \ , 
  \label{eq:rate}
\end{equation}
which explicitely exhibits the approach toward equilibrium (when both sides
vanish).  The key ingredients to the rate equation are the ``transport
coefficients'': the inelastic reaction rate, $\Gamma_{\cal Q}$, for both
dissociation and formation (detailed balance), and the quarkonium equilibrium
limit, $N_{\cal Q}^{\rm eq}(T)$.

The reaction rate can be calculated from inleastic scattering amplitudes of
quarkonia on the constituents of the medium (light quarks and gluons, or light
hadrons). The relevant processes depend on the (in-medium) properties of the
bound state~\cite{Grandchamp:2001pf}. In the QGP, for a tightly bound state
(binding energy $E_B\geq T$), an efficient process is
gluo-dissociation~\cite{Bhanot:1979vb}, $g+{\cal Q}\to Q+\bar Q$, where all of
the incoming gluon energy is available for break-up. However, for loosely bound
states ($E_B <T$ for excited and partially screened states), the phase space for
gluo-dissociation rapidly shuts off, rendering ``quasi-free" dissociation,
$p+{\cal Q}\to Q+\bar Q+p$ ($p=q,\bar q,g$), the dominant
process~\cite{Grandchamp:2001pf}, cf.~Fig.~\ref{fig:trans} left.

To calculate the quarkonium equilibrium limit, one starts from 
\begin{equation}
  N_{\cal Q}^{\rm eq} = 
  V_{\mathrm{FB}} \ n_{\cal Q}^{\rm eq}(m_{\cal Q};T,\gamma_Q)
  = d_{\cal Q} \ \gamma_Q^2 \int \frac{d^3p}{(2\pi)^3} f_{\cal Q}^B(E_p;T) \ 
  \label{eq:Neq}
\end{equation}
($V_{\rm FB}$: fireball volume, $d_{\cal Q}$: spin degeneracy, $f_{\cal Q}^B$:
Bose distribution). The ratio of Bessel functions, $I_1/I_0$, enforces the
canonical limit for small $N_{\rm op} \le1$. The number of $Q\bar Q$ pairs in
the fireball (usually determined by the initial hard production) is then matched
to the total heavy-flavor (HF) content in the system using a fugacity factor
$\gamma_Q=\gamma_{\bar Q}$ as
\begin{equation}
  N_{Q\bar Q}=\frac{1}{2} N_{\rm{op}}\frac{I_1(N_{\rm{op}})}{I_0(N_{\rm{op}})}+
  V_{\mathrm{FB}} \ \gamma_Q^2\sum\limits_{\cal Q} n_{\cal Q}^{\rm eq}(T)
  \ .
  \label{eq:NQQ}
\end{equation}
The open HF number, $N_{\rm{op}}$, follows from the corresponding equilibrium
densities, \eg, 
\begin{equation}
N_{\rm{op}}=N_c+N_{\bar c} = V_{\rm FB} 12\gamma_Q \int
{d^3p}/{(2\pi)^3} f_{Q}^F(E_p;T)
\end{equation}
for heavy anti-/quarks in the QGP. The
quarkonium equilibrium limit is thus coupled to the open HF spectrum in medium;
\eg, a smaller $c$-quark mass increases the $c$-quark density, which decreases
$\gamma_c$ and therefore reduces $N_{J/\psi}^{\rm eq}$, by up to an order of
magnitude for $m_c=1.8 \to 1.5$\,GeV, cf.~Fig.~\ref{fig:trans} right.
\begin{figure}[h]
  \centering
  \includegraphics[width=0.45\textwidth]{Gampsi}
  \includegraphics[width=0.45\textwidth]{Npsi-eq-T-prl92}
  \caption{Transport coefficients of charmonia in the QGP. Left: inelastic
    reaction rates for $J/\psi$ and $\chi_c$ in strong- ($V$=$U$) and
    weak-binding ($V$=$F$) scenarios.  Right: $J/\psi$ equilibrium numbers for
    conditions in central Pb-Pb and Au-Au at full SPS and RHIC energies,
    respectively, using diffeerent values of the in-medium $c$-quark mass in the
    QGP ($T\ge180$\,MeV) and for $D$-mesons in hadronic matter
    (($T\le180$\,MeV); in practice the equilibrium numbers are constructed as
    continuous across the transition region. }
  \label{fig:trans}
\end{figure}

In practice, furhter corrections to $N_{\cal Q}^{\rm eq}$ are needed for more
realistic applications in heavy-ion collisions. First, heavy quarks cannot be
expected to be thermalized throughout the course of a heavy-ion collision;
harder heavy-quark (HQ) momentum distributions imply reduced phase-space overlap
for quarkonium production, thus suppressing the gain term. In the rate equation
approach this has been implemented through a relaxation factor ${\cal R} =
1-\exp(-\int d\tau/\tau_Q^{\rm therm})$ multiplying $N_{\cal Q}^{\rm eq}$, where
$\tau_Q^{\rm therm}$ represents the kinetic relaxation time of the HQ
distributions~\cite{Grandchamp:2002wp,Grandchamp:2003uw}. This approximation has
been quantitatively verified in Ref.~\cite{Song:2012at}.  Second, since HQ pairs
are produced in essentially pointlike hard collisions, they do not necessarily
explore the full volume in the fireball. This has been accounted for by
intorducing a correlation volume in the argument of the Bessel functions, in
analogy to strangeness production at lower energies~\cite{Hamieh:2000tk}.

An important aspect of this transport approach is a controlled implementation of
in-medium properties of the quarkonia~\cite{Grandchamp:2003uw,Zhao:2010nk}.
Color-screening of the QCD potential reduces the quarkonium binding energies,
which, together with the in-medium HQ mass, $m_Q^*$, determines the bound-state
mass, $m_{\cal Q} = 2m_Q^*- E_B$. As discussed above, the interplay of $m_{\cal
  Q}$ and $m_Q^*$ determines the equilibrium limit, $N_{\cal Q}^{\rm eq}$, while
$E_B$ also affects the inelastic reaction rate, $\Gamma_{\cal Q}(T)$. To
constrain these properties, pertinent spectral functions have been used to
compute euclidean correlators for charmonia, and required to approximately agree
with results from lattice QCD~\cite{Zhao:2010nk}. Two basic scenarios have been
put forward for tests against charmonium data at SPS and RHIC: a strong-binding
scenario (SBS), where the $J/\psi$ survives up to temperatures of about
2\,$T_c$, and a weak-binding scenario (WBS) with $T_{\rm diss}\simeq 1.2\,T_c$,
cf.~Fig.~\ref{fig:med}. These scenarios are motivated by microscopic $T$-matrix
calculations~\cite{Riek:2010fk} where the HQ internal ($U_{\bar QQ}$) or free
energy ($F_{\bar QQ}$) have been used as potential, respectively. A more
rigorous definition of the HQ potential, and a more direct implementation of the
quarkonium properties from the $T$-matrix approach would be warrantable for
future work.  The effects of the hadronic phase are generally small for $J/\psi$
and bottomonia, but important for the $\psi'$ especially if its direct decay
channel $\psi'\to \bar DD$ is open (due to reduced masses and/or finite widths
of the $D$ mesons)~\cite{Grandchamp:2002wp,Grandchamp:2003uw}.
\begin{figure}[h]
  \centering
  \includegraphics[width=0.45\textwidth]{Eb-jpsi}
  \includegraphics[width=0.45\textwidth]{mc-med}
  \caption{Temperature dependence of $J/\psi$ binding energy (left panel) and
    charm-quark mass (right panel) in the QGP in the strong- and weak-binding
    scenarios (solid ($V$=$U$) and dashed lines ($V$=$F$), respectively) as
    implemented into the rate equation approach~\cite{Zhao:2010nk}.}
  \label{fig:med}
\end{figure}

The rate equation approach has been extended to compute $p_T$ spectra of
charmonia in heavy-ion collisions~\cite{Zhao:2007hh}. Toward this end, the loss
term was solved with a 3-momentum dependent dissociation rate and a spatial
dependence of the charmonium distribution function, while for the gain term
blast-wave distributions at the phase transition were assumed (this should be
improved in the future by an explict evaluations of the gain term from the
Boltzmann equation using realistic time-evolving HQ distributions, see
Ref.~\cite{Zhao:2010ti} for initial studies). In addition, formation time
effects are included which affect quarkonium suppression at high
$p_T$~\cite{Zhao:2008vu}.

To close the quarkonia rate equations, several input quantities are required
which are generally taken from experimental data in $pp$ and $pA$ collisions,
\eg, quarkonia and HQ production cross sections (with shadowing corrections at
LHC and at forward rapidity at RHIC), and primordial nuclear absoprtion effects
encoded in phenomenological absorption cross sections. Feeddown effects from
excited quarkonia (and $B$-meson decays into $J/\psi$) are accounted for.  The
space-time evolution of the medium is constructed using an isotropically
expanding fireball model reproducing the measured hadron yields and their $p_T$
spectra. The fireball resembles the basic features of hydrodynamic
models~\cite{vanHees:2014ida}, but an explicit use of the latter is desirable
for future purposes.

Two main model parameters have been utilized to calibrate the rate equation
approach for charmonia using the centrality dependence of inclusive $J/\psi$
production in Pb-Pb($\sqrt{s_{NN}}$=0.017\,TeV) at SPS and in
Au-Au($\sqrt{s_{NN}}$=0.2\,TeV) at RHIC: the strong coupling constant
$\alpha_s$, controlling the inelastic reaction rate, and the $c$-quark
relaxation time affecting the gain term through the amended charmonium
equilibrium limit. With $\alpha_s\simeq0.3$ and $\tau_c^{\rm
  therm}\simeq$\,4-6(1.5-2)\,fm/$c$ for the SBS (WBS), the inclusive $J/\psi$
data at SPS and RHIC can be reasonably well reproduced, albeit with different
decompositions into primordial and regenerated yields (the former are larger in
the SBS than in the WBS). The $\tau_c^{\rm therm}$ obtained in the SBS is in
range of values calculated microscopicllay from the $T$-matrix approach using
the $U$-potential~\cite{Riek:2010fk}, while for the WBS it is much smaller than
calculated from the $T$-matrix using the $F$-potential.  Thus, from a
theoretical point of view, the SBS is the preferred scenario.  With this setup,
quantitative predictions for Pb-Pb($\sqrt{s_{NN}}$=2.76\,GeV) at the LHC have
been carried out, for the centrality dependence and $p_T$ spectra of the
$J/\psi$~\cite{Zhao:2011cv}, as well as for $\Upsilon(1S)$, $\chi_c$ and
$\Upsilon(2S)$ production~\cite{Emerick:2011xu}.


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../SaporeGravis"
%%% End: 
