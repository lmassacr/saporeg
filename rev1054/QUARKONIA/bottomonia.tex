With the advent of the LHC, bottomonia have become a new probe of the QGP. While
their production rate is 200 times smaller than the one of \jpsi, they offer
several advantages. The three S-wave states \upsa, \upsb, and \upsc have very
different binding energies and appear at very similar rates in the \mumu decay
channel. Their relative abundances are $7:2:1$, while the \jpsi to \psiP ratio
is $50:1$. Hence these three states, which include with the \upsa the strongest
bound state of all quarkonia, allow one to probe a much wider temperature range
than previously accessible with charmonia. A further advantage is the absence of
feed down from heavier-flavour decays, that are a background for high \pt
charmonium studies. The higher masses also ease theoretical calculations. In the
context of sequential dissociation, bottomonia may provide another advantage:
the approximately twenty times smaller beauty production cross section will lead
to a smaller contribution from regeneration that complicates the picture for
charmonia. However, the closed to open heavy flavour production ratio for beauty
is roughly ten times smaller than for charm, which increases the relative
contribution of recombination to bottomonia and complicates the situation.

Unfortunately, feed down contributions to the \upsa from excited state decays
that are crucial for a quantitative understanding of a sequential dissociation
are not very well understood at low \pt. Measurements of feed-down fractions
exist only for $\pt>6\GeVc$, where about 30\% of \upsa result from decays of
$\chi_b(nP)$ and \upsbc decays, reaching $\approx50\%$ at higher
\pt~\cite{Affolder:1999wm,Aaij:2012se,Aad:2011ih,Aaij:2014caa}.

At RHIC, where the \ups production cross sections are low, a measurement of the
\ups suppression in \dAu and \AuAu collisions was performed by the PHENIX and
STAR experiments~\cite{Adare:2012bv,Adare:2014hje,Adamczyk:2013poh}. Integrating
the yield of the three \ups states, they observe a reduction of the yield in
central \AuAu collisions, compared to the binary scaled \pp reference as shown
in the left panel of \fig{fig:RHIC_Ups}. Because of the large statistical
uncertainties, the experiments cannot yet assess a possible centrality
dependence in \AuAu. STAR finds in the 10\% most central collisions a nuclear
modification factor of $\raa = 0.49 \pm 0.13\,\text{(\AuAu stat.)} \pm
0.07\,\text{(\pp stat.)} \pm 0.02\,\text{(\AuAu syst.)} \pm 0.06\,\text{(pp
  syst.)}$. Constraining the measurement to the \upsa alone, as shown in the
right panel of \fig{fig:RHIC_Ups}, only the \raa for the most central \AuAu
collisions exhibits a significant suppression. Assuming a feed-down contribution
of $\approx50\%$ this could signal the onset of a suppression of excited states
in central \AuAu collisions. However, the \raa in most central \AuAu collisions
is also comparable to the \rdau, so more precise measurements are necessary
before drawing such a conclusion.

A comparison of TAMU and aHYDRO calculations with the measured \upsabc nuclear
modification factors shows good agreement within the experimental uncertainties.
Experimental data cannot yet constrain the $\eta/s$ free parameter of aHYDRO.
The band of the TAMU curve represents the uncertainty on cold nuclear matter
effects. These are included by employing nuclear absorption cross sections of
1.0 and 3.1\,mb, but the data cannot yet constrain their size. The \upsa
suppression, however, seems to be slightly overpredicted by both models, though
not beyond the experimental uncertainties, with the data preferring small values
of $\eta/s$.

\begin{figure}[t]
  \centering
  \includegraphics[width=0.45\textwidth]{RAA_Y123_RHIC_Rapp_Strickland}
  \includegraphics[width=0.45\textwidth]{RAA_Y1_RHIC_Rapp_Strickland}
  \caption{Left: \raa of \upsabc as a function of centrality measured by PHENIX
    in $|y|<0.35$~\cite{Adare:2012bv,Adare:2014hje} and STAR in
    $|y|<0.5$~\cite{Adamczyk:2013poh} compared to TAMU (grey band) and aHYDRO
    (lines). Right: STAR measurement of the \raa of \upsa with $|y|<0.5$ as a
    function of centrality compared to the same models.}
  \label{fig:RHIC_Ups}
\end{figure}


\begin{figure}[t]
  \centering
  \includegraphics[width=0.45\textwidth]{UpsilonQM2014}
  \caption{\upsn \raa as a function of centrality. \upsa \raa is measured in
    $2.5<|y|<4$ by ALICE~\cite{Abelev:2014nua}. CMS measured the centrality
    dependence of the \upsa and \upsb \raa at
    $|y|<2.4$~\cite{Chatrchyan:2011pe,Chatrchyan:2012lxa}. Centrality integrated
    values are shown in the right panel, including an upper limit at 95\%
    confidence level of the \upsc \raa by CMS in $|y|<2.4$.}
  \label{fig:CMS-ALICE_Ups}
\end{figure}


\begin{figure}[!h]
  \centering
  \includegraphics[width=0.44\textwidth]{Raa_SBS.pdf}
  \includegraphics[width=0.44\textwidth]{upsilon_RAA_Strickland.pdf}
  \includegraphics[width=0.44\textwidth]{2014-Oct-31-Raa_th_centrEm.pdf}
  \includegraphics[width=0.44\textwidth]{2014-Oct-31-Raa_th_centrStrick.pdf}
  \caption{\upsa\ \raa\ versus centrality at
    $|y|<2.4$~\cite{Chatrchyan:2011pe,Chatrchyan:2012lxa} (top) and
    $2.5<y<4$~\cite{Abelev:2014nua} (bottom), compared to TAMU (left) and aHYDRO
    (right) model calculations discussed in Sections~\ref{sec:transport}
    and~\ref{sec:non-equilib}.}
  \label{fig:Upsilon_centrality_theory}
%\end{figure}
%\begin{figure}[h]
  \centering
  \includegraphics[width=0.44\textwidth]{RAA_Y1S_rap_Emerick}
  \includegraphics[width=0.44\textwidth]{RAA_Y1S_rap_Strickland}
  \caption{\upsa\ \raa\ versus rapidity from ALICE~\cite{Abelev:2014nua} and
    CMS~\cite{Chatrchyan:2011pe,Chatrchyan:2012lxa}. In the bottom row, the \raa
    is to TAMU (left) and aHYDRO (right) model calculations discussed in
    Sections~\ref{sec:transport} and~\ref{sec:non-equilib}.}
  \label{fig:Upsilon_rap_CMS-ALICE}
\end{figure}


CMS measured the suppression of the first three S-states integrated over all \pt
and the rapidity range $|y|<2.4$ in \pb collisions at \snn =
2.76\TeV~\cite{Chatrchyan:2011pe,Chatrchyan:2012lxa}. Following a first
tantalising indication in 2011 that the excited states are suppressed relative
to the \upsa, this was confirmed a year later. The centrality integrated \raa
was measured for all three states, exhibiting a clear ordering with binding
energy: $\raa(\upsa) = 0.56 \pm 0.08\text{(stat.)} \pm 0.07\text{(syst.)}$, $
\raa(\upsb) = 0.12 \pm 0.04\text{(stat.)} \pm 0.02\text{(syst.)}$, and the \upsc
being so strongly suppressed that only an upper limit of $\raa(\upsc)<0.10$ at
95\% CL could be quoted. The centrality dependence of the \upsa and \upsb \raa
are shown in \fig{fig:CMS-ALICE_Ups}. With the \upsb and \upsc essentially
completely suppressed in central \pb collisions, a more precise understanding of
the feed down contributions to the \upsa is required to assess whether any
directly produced \upsa are suppressed in such collisions. Furthermore, the role
of the $\chib\text{(nP)}$ states in \pb are (and may remain) completely unknown
so far.

In the top row of \fig{fig:Upsilon_centrality_theory}, the centrality dependence
of the CMS \upsa and \upsb results are compared to TAMU (left) and aHYDRO
(right) calculations, described in Sections~\ref{sec:transport}
and~\ref{sec:non-equilib}, respectively. Both models reproduce the data
reasonably well, simultaneously describing the \upsa and \upsb suppression over
the full centrality range. The aHYDRO approach has maybe some slight tension
describing both states with the same choice for $\eta/s$, though the
experimental uncertainties are large enough to account for the differences.
Regarding the TAMU model, it is worth to highlight that it includes a
non-negligible regeneration contribution. In fact, it is the sole source of
\upsb in central \pb collisions. It is also interesting to point out that
regeneration favours the production of \upsb over \upsa, which is opposite to
the predictions for charmonia. This difference is the result of temperature
dependent dissociation rates and equilibrium numbers that enter the rate
equation (\eq{eq:rate}). In contrast to the other states, which all have
dissociation temperatures in the vicinity of $T_c$, the strong binding energy
will stop the dissociation of \upsa much earlier, when the equilibrium number is
still small~\cite{Emerick:2011xu}. Significantly less regeneration of \upsa is
necessary to reach this equilibrium number.

The production of excited \ups states in \pb collisions has also been reported
by CMS as fully corrected cross section ratio relative to the \upsa:
$\sigma(\upsb)/\sigma(\upsa) = 0.09 \pm 0.02\,(\text{stat.}) \pm
0.02\,(\text{syst.}) \pm 0.01\,(\text{glob.})$ integrated over centrality, \pt,
and $|y|<2.4$~\cite{Chatrchyan:2013nza}. For the ratio
$\sigma(\upsc)/\sigma(\upsa)$ an upper limit of 0.04 at 95\% CL has been set.
These values can be directly compared to theoretical expectations, \eg the
statistical hadronization model, which predicts $\sigma(\upsb)/\sigma(\upsa)
\approx 0.032$~\cite{Andronic:2014sga}. This value is consistent with the
measured cross section ratio, though quite a bit lower than the central value of
the measurement.

A comparison of the CMS measurement at midrapidity to $\raa(\upsa) = 0.30 \pm
0.05\text{(stat.)} \pm 0.04\text{(syst.)}$ measured by ALICE at forward rapidity
($2.5<y<4$)~\cite{Abelev:2014nua}, integrated over \pt and centrality, as well
as the centrality dependence overlaid in \fig{fig:CMS-ALICE_Ups}, reveal a
surprising similarity to the \jpsi suppression observed at RHIC: \upsa are more
suppressed at forward rapidity than at midrapidity. At RHIC such rapidity
dependence was explained with a larger contribution of regeneration at
midrapidity and/or stronger shadowing effects at forward rapidity. 
%Should the
%\upsa at the LHC have indeed met the same fate as the \jpsi at RHIC? 
This similarity is
also reflected in the centrality integrated rapidity dependence of $\raa(\upsa)$
shown in \fig{fig:Upsilon_rap_CMS-ALICE}. However, the large
statistical uncertainties on the CMS measurement~\cite{Chatrchyan:2012np} that
is still based on the first \pb and \pp runs at \snn = 2.76\TeV prevents
conclusions on the \raa in the intermediate rapidity range.

The simultaneous description of ALICE and CMS data provides a real challenge for
the models so successful in reproducing the midrapidity data. As shown in Figures~\ref{fig:Upsilon_centrality_theory}
and~\ref{fig:Upsilon_rap_CMS-ALICE}, they completely fail to predict the
rapidity dependence of $\raa(\upsa)$. The aHYDRO model, curves taken from
Ref.~\cite{Strickland:2012cq}, predicts a disappearance of the suppression at
forward rapidity and does not get even close to the ALICE data. The TAMU
transport model approach, including a regeneration component, predicts a rather
constant rapidity dependence of the suppression though still overshoots the
forward rapidity data slightly. In both models the \upsa\ suppression is
dominated by the in-medium dissociation of the higher mass bottomonium states.
Therefore, a precise measurement of \upsa\ feed-down contributions, as well as
an accurate estimate of CNM effects in the kinematic ranges probed by ALICE and
CMS is required in order to make a more stringent comparison with data.



It is interesting to compare the \raa of the three bottomonium states to the
\raa of the \jpsi and \psiP at high \pt. The charmonium states follow nicely the
established pattern of the \ups states of a reduced suppression with increasing
binding energy as predicted by the sequential dissociation picture. If one,
however, uses the \pt integrated \jpsi \raa, one observes a deviation from this
pattern that can be explained with a (re)generation contribution. It will be
interesting to see how low \pt \psiP will fit in.

The picture may be complicated further by the observed multiplicity dependence
of the $\upsb/\upsa$ and $\upsc/\upsa$ ratios in \pp and \pPb
collisions~\cite{Chatrchyan:2013nza} that 
is discussed in Sections~\ref{sec:pp:HadCorrelations} and~\ref{sec:CNM:Onia}.
It is unclear whether the dependence is caused by a suppression of the excited
states by surrounding particles or by the multiplicity being biased by the
presence of the \ups states.




%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../SaporeGravis"
%%% End: 
