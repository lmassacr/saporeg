The predictions for $J/\psi$ suppression, considering only NLO modifications of
the parton densities in the nucleus (shadowing), are described in this section.
There are other possible cold matter effects on $J/\psi$ production in addition
to shadowing: breakup of the quarkonium state due to inelastic interactions with
nucleons (absorption) or produced hadrons (comovers) and energy loss in cold
matter. Since the quarkonium absorption cross section decreases with
center-of-mass energy, we can expect that shadowing is the most important cold
matter effect at midrapidity, see
Refs.~\cite{Lourenco:2008sk,McGlinchey:2012bp}. Here we show results for the
rapidity dependence of shadowing on $J/\psi$ and $\Upsilon$ production at
$\sqrt{s_{_{NN}}} = 2.76$ TeV Pb+Pb collisions, neglecting absorption.

The results are obtained in the color evaporation model (CEM) at next-to-leading
order in the total cross section. In the CEM, the quarkonium production cross
section in $pp$ collisions is some fraction, $F_C$, of all $Q \overline Q$ pairs
below the $H \overline H$ threshold where $H$ is the lowest mass heavy-flavor
hadron,
\begin{eqnarray}
  \sigma_C^{\rm CEM}(s)  =  F_C \sum_{i,j} 
  \int_{4m^2}^{4m_H^2} ds
  \int dx_1 \, dx_2~ f_i^p(x_1,\mu_F^2)~ f_j^p(x_2,\mu_F^2)~ 
  \hat\sigma_{ij}(\hat{s},\mu_F^2, \mu_R^2) \, 
  \, , \label{sigtil}
\end{eqnarray} 
where $ij = q \overline q$ or $gg$ and $\hat\sigma_{ij}(\hat s)$ is the
$ij\rightarrow Q\overline Q$ subprocess cross section. The normalization factor
$F_{J/\psi}$ is fit to the forward (integrated over $x_F > 0$) $J/\psi$ cross
section data on only $p$, Be, Li, C, and Si targets. The fits are restricted to
the forward cross sections only. The $\Upsilon$ normalization factor is fit to
the sum $B d\sigma(\sum_i \Upsilon(iS))/dy|_{y=0}$ for proton and light nclear
targets since the fixed-target experiments did not separate the $\Upsilon$
states. By restricting ourselves to light targets, uncertainties due to ignoring
any cold nuclear matter effects, on the order of a few percent in light targets,
are avoided.

The same values of the central charm quark mass and scale parameters are
employed as those found for open charm, $m_c = 1.27 \pm 0.09$~GeV/$c^2$,
$\mu_F/m_c = 2.10 ^{+2.55}_{-0.85}$, and $\mu_R/m_c = 1.60 ^{+0.11}_{-0.12}$
\cite{Nelson:2012bc}. The normalization $F_C$ is obtained for the central set,
$(m_c,\mu_F/m_c, \mu_R/m_c) = (1.27 \, {\rm GeV}, 2.1,1.6)$. The calculations
for the extent of the mass and scale uncertainties are multiplied by the same
value of $F_C$ to obtain the extent of the $J/\psi$ uncertainty band
\cite{Nelson:2012bc}. We calculate $\Upsilon$ production in the same manner,
with the central result obtained for $(m_b,\mu_F/m_b, \mu_R/m_b) = (4.65 \, {\rm
  GeV}, 1.4,1.1)$. The scale uncertainties are \cite{Nelson_inprog}. All the
calculations are NLO in the total cross section and assume that the intrinsic
$k_T$ broadening is the same in $p+p$ as in Pb+Pb.

\begin{figure*}[htpb]
  \centering
  \includegraphics[width=0.45\textwidth]{psiy_nlo_lo_shad_276}
  \includegraphics[width=0.45\textwidth]{psipt_for_shad_276}\\
  \includegraphics[width=0.45\textwidth]{upsy_nlo_lo_shad_276}
  \includegraphics[width=0.45\textwidth]{upspt_for_shad_276}
  \caption{The nuclear modification factor $R_{\rm PbPb}$ for $J/\psi$ (upper)
    and $\Upsilon$ (lower) production calculated using the EPS09 modifications.
    The results are presented as a function of rapidity (left) and $p_T$
    (right). The dashed red histogram shows the EPS09 NLO uncertainties. The
    blue curves show the LO modification and the corresponding uncertainty band
    as a function of rapidity
    only. %The NLO $J/\psi$ results were originally shown in
          %Ref.~\protect\cite{Albacete:2013ei}.
  }
    \label{fig:JpsiUpsypt}
\end{figure*}

The mass and scale uncertainties are calculated based on results using the one
standard deviation uncertainties on the quark mass and scale parameters. If the
central, upper and lower limits of $\mu_{R,F}/m$ are denoted as $C$, $H$, and
$L$ respectively, then the seven sets corresponding to the scale uncertainty are
$\{(\mu_F/m,\mu_F/m)\}$ = $\{$$(C,C)$, $(H,H)$, $(L,L)$, $(C,L)$, $(L,C)$,
$(C,H)$, $(H,C)$$\}$. The uncertainty band can be obtained for the best fit sets
by adding the uncertainties from the mass and scale variations in quadrature.
The envelope containing the resulting curves,
\begin{eqnarray}
  \sigma_{\rm max} & = & \sigma_{\rm cent}
  + \sqrt{(\sigma_{\mu ,{\rm max}} - \sigma_{\rm cent})^2
    + (\sigma_{m, {\rm max}} - \sigma_{\rm cent})^2} \, \, , \label{sigmax} \\
  \sigma_{\rm min} & = & \sigma_{\rm cent}
  - \sqrt{(\sigma_{\mu ,{\rm min}} - \sigma_{\rm cent})^2
    + (\sigma_{m, {\rm min}} - \sigma_{\rm cent})^2} \, \, , \label{sigmin}
\end{eqnarray}
defines the uncertainty. The EPS09 NLO band is obtained by calculating the
deviations from the central value for the 15 parameter variations on either side
of the central set and adding them in quadrature. With the new uncertainties on
the charm cross section, the band obtained with the mass and scale variation is
narrower than that with the EPS09 NLO variations, as shown for $p+$Pb collisions
at $\sqrt{s_{NN}} = 5$ TeV in Ref.~\cite{Albacete:2013ei}.

The upper left-hand side of Fig.~\ref{fig:JpsiUpsypt} shows the uncertainty in
the shadowing effect on $J/\psi$ due to the variations in the 30 EPS09 NLO sets
\cite{Eskola:2009uj} (red). The uncertainty band calculated in the CEM at LO
with the EPS09 LO sets is shown for comparison. It is clear that the LO results,
represented by the smooth magenta curves in Fig.~\ref{fig:JpsiUpsypt}, exhibit a
larger shadowing effect. This difference between the LO results, also shown in
Ref.~\cite{Vogt:2010aa}, and the NLO calculations arises because the LO and NLO
gluon shadowing parameterizations differ significantly at low $x$
\cite{Eskola:2009uj}.

In principle the shadowing results should be the same for LO and NLO.
Unfortunately, however, the gluon modifications, particularly at low $x$ and
moderate $Q^2$, are not yet sufficiently constrained. The lower left panel shows
the same calculation for $\Upsilon$ production. Here the difference between the
LO and NLO calculations is reduced because the mass scale, as well as the range
of $x$ values probed, is larger. Differences in LO results relative to e.g. the
color singlet model arise less from the production mechanism than from the
different mass and scale values assumed.

We note that the convolution of the two nuclear parton densities results in a
$\sim 20$\% suppression at NLO for $|y|\leq 2.5$ with a mild decrease in
suppression at more forward rapidities. The gluon antishadowing peak at $|y|
\sim 4$ for $J/\psi$ and $|y| \sim 2$ for $\Upsilon$ with large $x_1$ in the
nucleus is mitigated by the shadowing at low $x_2$ in the opposite nucleus with
the NLO parameterization. The overall effect due to nPDFs in both nuclei is a
result with mild rapidity dependence and $R_{\rm PbPb}^{J/\psi} < 1$ for
$|y|\leq 5$ and $R_{\rm PbPb}^{\Upsilon} \sim 1$ for $|y| \leq 3$. The EPS09
nPDF uncertainty is reduced at NLO for the $J/\psi$ and becomes even narrower
for the higher scale of the $\Upsilon$. The nPDF effect gives more suppression
at central rapidity than at forward rapidity, albeit less so for the LHC
energies than for RHIC where the antishadowing peak at $\sqrt{s_{NN}} = 200$ GeV
is at $|y| \sim 2$.

We finally remark that the uncertainty is larger in the LO CEM calculation for
several reasons. First, the $x$ values in the $2 \rightarrow 1$ kinematics at LO
is somewhat lower than the $2 \rightarrow 2$ and $2\rightarrow 3$ kinematics
(for the LO+virtual and real NLO contributions respectively) of the NLO CEM
calculation. Next, the $p_T$ scale enters in the complete NLO calculation where
it does not in the LO, leading to both a slightly larger $x$ value for higher
$p_T$ as well as a larger scale so that the NLO calculation is on average at a
higher scale than the LO. Finally, the uncertainties in the EPS09 LO sets are
larger than those of the NLO sets.

The right-hand side of the figure shows the $p_T$ dependence of the effect at
forward rapidity for $J/\psi$ (upper) and $\Upsilon$ (lower). There is no LO
comparison here because the $p_T$ dependence cannot be calculated in the LO CEM.
The effect is rather mild and increases slowly with $p_T$. There is little
difference between the $J/\psi$ and $\Upsilon$ results for $R_{\rm PbPb}(p_T)$
because, for $p_T$ above a few GeV, the $p_T$ scale is dominant.


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../SaporeGravis"
%%% End: 
