The predictions for quarkonium suppression in \AAcoll collisions, considering
only modifications of the parton densities in the nucleus, the so called nuclear
PDFs, are described in this subsection. There are other possible cold matter
effects on quarkonium production in matter in addition to shadowing: breakup of
the quarkonium state due to inelastic interactions with nucleons (absorption) or
produced hadrons (comovers) and energy loss in cold matter, as discussed
in \sect{Cold nuclear matter effects}. The midrapidity quarkonium
absorption cross section for breakup by nucleon interactions decreases with
centre-of-mass energy~\cite{Lourenco:2008sk,McGlinchey:2012bp}, becoming
negligible at LHC energies. In addition, cold matter suppression due to energy
loss does not have a strong rapidity dependence. Thus, shadowing is expected to be
the dominant cold matter effect in what concerns the modification of the shape
of the quarkonium rapidity distribution. It will also produce a relatively small
effect on the shape of the quarkonium \pt distribution at low \pt.

\begin{figure*}[!t]
  \centering
  \includegraphics[width=0.45\textwidth]{psiy_nlo_lo_shad_276}
  \includegraphics[width=0.45\textwidth]{psipt_for_shad_276}\\
  \includegraphics[width=0.45\textwidth]{upsy_nlo_lo_shad_276}
  \includegraphics[width=0.45\textwidth]{upspt_for_shad_276}
  \caption{The nuclear modification factor $R_{\rm AA}$ for \jpsi (upper)
    and \ups (lower) production, calculated in the CEM model using the EPS09 modifications~\cite{Eskola:2009uj}, is shown for \PbPb collisions at \snn=2.76 \TeV.
    The results are presented as a function of rapidity (left) and \pt
    (right)~\cite{Vogt:2010aa}. The dashed red histogram shows the EPS09 NLO uncertainties. The
    blue curves show the LO modification and the corresponding uncertainty band
    as a function of rapidity
    only. %The NLO \jpsi results were originally shown in
          %Ref.~\protect\cite{Albacete:2013ei}.
  }
    \label{fig:JpsiUpsypt}
\end{figure*}


\begin{figure}[t!]
\vskip 0.3cm
\begin{minipage}[t]{.45\textwidth}
\begin{center}
\includegraphics[width=1.0\textwidth]{fig1ferreiro.png}
\end{center}
\end{minipage}
\begin{minipage}[t]{.45\textwidth}
\begin{center}
\includegraphics[width=0.86\textwidth]{fig2ferreiro.png}
\end{center}
\end{minipage}
\vskip -0.4cm
\caption{\jpsi rapidity (left) and \pt dependence (right) of the EKS98 LO and
  nDSg LO shadowing corrections performed using the CSM model according
  to~\cite{Ferreiro:2008wc,Rakotozafindrabe:2011rw} in \pb collisions at \snn =
  2.76\TeV. The bands for the EKS98/nDSg models shown in the figure correspond
  to the variation of the factorisation scale.}
\label{fig1and2ferreiro}
\hfill
%\end{figure}
%\begin{figure}[h!]
\begin{center}
\includegraphics[width=1.0\textwidth]{fig3ferreiro.png}
\end{center}
\caption{ \jpsi centrality dependence of the EKS98 LO and nDSg LO shadowing
  corrections performed using the CSM model according
  to~\cite{Ferreiro:2008wc,Rakotozafindrabe:2011rw} in \pb collisions at \snn =
  2.76\TeV. The bands for the EKS98/nDSg models shown in the figure correspond
  to the uncertainty in the factorisation scale.}
\label{fig3ferreiro}
\end{figure}



\fig{fig:JpsiUpsypt} shows the results for the dependence of shadowing on rapidity,
transverse momentum, and centrality are shown for \jpsi and \ups production in
\pb collisions at \snn = 2.76\TeV, neglecting absorption. Results
obtained in the colour evaporation model (CEM) at next-to-leading order (NLO) in
the total cross section (leading order in \pt) are discussed first, followed by
results from a leading order colour singlet model (CSM) calculation.

The CEM calculation was described in  \sect{Cold nuclear matter effects}. Here only a
few pertinent points are repeated. In the CEM, the
quarkonium production cross section in \pp collisions is some fraction, $F_C$,
of all \QQbar pairs below the $H \overline H$ threshold where $H$ is the lowest-mass heavy-flavour hadron,
\begin{eqnarray}
\sigma_C^{\rm CEM}(s)  =  F_C \sum_{i,j} 
\int_{4m^2}^{4m_H^2} \dd\hat{s}
\int \dd x_i \, \dd x_j~ f_i^p(x_i,\mu_F^2)~ f_j^p(x_j,\mu_F^2)~ 
\mathcal{J}\hat{\sigma}_{ij}(\hat{s},\mu_F^2, \mu_R^2) \, 
\, , \label{sigtil}
\end{eqnarray} 
where $ij = \qqbar$ or $gg$ and $\hat\sigma_{ij}(\hat s)$ is the $ij\rightarrow
\QQbar$ subprocess cross section at centre-of-mass energy $\hat{s}$, while
$\mathcal{J}$ is an appropriate Jacobian with dimension $1/\hat{s}$. The
normalisation factor $F_{\jpsi}$ is fitted to an appropriate subset of the
available data, restricting the fits to measurements on light nuclear targets to
avoid any significant cold matter effects. For the results shown here, the
normalisation $F_C$ is based on the same central mass and scale parameter values
as those obtained for open charm, $(m_c,\mu_F/m_c, \mu_R/m_c) =
(1.27\mathrm{\,GeV}/c^2,\,2.1,\,1.6)$ ~\cite{Nelson:2012bc}, and bottom,
$(m_b,\mu_F/m_b, \mu_R/m_b) = (4.65 \, {\rm GeV}/c^2, 1.4,1.1)$
~\cite{Nelson_inprog}. The mass and scale uncertainties on the CEM calculation
are shown in the previous section. They are smaller than those due to the
uncertainties of the EPS09 shadowing parameterisation~cite{Eskola:2009uj}. All
the CEM calculations are NLO in the total cross section and assume that the
intrinsic \kt broadening is the same in \pb as in pp.


The upper left-hand panel of \fig{fig:JpsiUpsypt} shows the uncertainty in the
shadowing effect on \jpsi due to the variations in the 30 EPS09 NLO
sets~\cite{Eskola:2009uj} (red). The uncertainty band calculated in the CEM at
LO with the EPS09 LO sets is shown for comparison (blue).  It is clear that the LO
results
exhibit a larger shadowing effect. This difference between the LO results, also
shown in Ref.~\cite{Vogt:2010aa}, and the NLO calculations arises because the
EPS09 LO and NLO gluon shadowing parameterisations differ significantly at low
$x$~\cite{Eskola:2009uj}.

In principle, the shadowing results should be the same for LO and NLO.
Unfortunately, however, the gluon modifications, particularly at low $x$ and
moderate $Q^2$, are not yet sufficiently constrained. The lower left panel shows
the same calculation for \ups production. Here, the difference between the LO
and NLO calculations is reduced because the mass scale, as well as the range of
$x$ values probed, is larger. Differences in LO results relative to, \eg, the
colour singlet model arise less from the production mechanism than from the
different mass and scale values assumed, as we discuss below.


It should be noted that the convolution of the two nuclear parton densities
results in a $\sim 20$\% suppression at NLO for $|y|\leq 2.5$ with a mild
decrease in suppression at more forward rapidities. The gluon antishadowing peak
at $|y| \sim 4$ for \jpsi and $|y| \sim 2$ for \ups with large $x$ in the
nucleus is mitigated by the shadowing at low $x$ in the opposite nucleus with
the NLO parameterisation. The overall effect due to NLO nPDFs in both nuclei is
a result with moderate rapidity dependence and $R_{\rm AA}^{\jpsi} \sim 0.7$ for
$|y|\leq 5$ and $R_{\rm AA}^{\ups} \sim 0.84$ for $|y| \leq 3$. The nPDF effect
gives more suppression at central rapidity than at forward rapidity, albeit less
so for the LHC energies than for RHIC where the antishadowing peak at \snn =
200\GeV is at $|y| \sim 2$. The difference between the central value of
$R_{\rm AA}$ at LO and NLO is $\sim 30$\% for the \jpsi and $\sim 10$\% for the
\ups. If a different nPDF set with LO and NLO parameterisations, such as nDSg~\cite{deFlorian:2003qf},
is used, the difference between LO and NLO is reduced to a few percent since the
difference between the underlying LO and NLO proton parton densities at low $x$
is much smaller for nDSg than for EPS09~\cite{RVinprog}.

The uncertainty is larger in the LO CEM calculation for several reasons. First
and foremost is the choice of the underlying proton parton densities. If the $x$
and $Q^2$ dependence at LO and NLO is very different, the resulting nuclear
parton densities will reflect this~\cite{RVinprog}. Other factors play a smaller
role. For example, the $x$ values in the $2 \rightarrow 1$ kinematics at LO is
somewhat lower than the $2 \rightarrow 2$ and $2\rightarrow 3$ kinematics (for
the LO+virtual and real NLO contributions respectively) of the NLO CEM
calculation. Next, the \pt scale enters in the complete NLO calculation where it
does not in the LO, leading to both a slightly larger $x$ value for higher \pt
as well as a larger scale so that the NLO calculation is on average at a higher
scale than the LO.

The right panels of \fig{fig:JpsiUpsypt} show the \pt dependence of the effect
at forward rapidity for \jpsi (upper) and \ups (lower). 
The effect is rather mild and increases slowly with \pt. There is little
difference between the \jpsi and \ups results for $R_{\text{\pb}}(\pt)$ because,
for \pt above a few GeV, the \pt scale is dominant.
There is no LO
comparison here because the \pt dependence cannot be calculated in the LO CEM.

%%%%%%%%%%%

However, the leading order colour single model calculation (LO CSM) of \jpsi
production, shown to be compatible with the magnitude of the of the
\pt-integrated cross-sections, is a $2 \rightarrow 2$ process, $g + g
\rightarrow \jpsi + g$, which has a calculable \pt dependence at LO, as in the
so-called {\it extrinsic} scheme~\cite{Ferreiro:2008wc}.
 
In this approach, one can use the partonic differential cross section computed
from any $2 \rightarrow 2$ theoretical model that satisfactorily describes the
data down to low \pt. Here, a generic $2\to 2$ matrix element which matches the
\pt dependence of the data has been used and
%
% In fact, one can use a generic $2\to 2$ matrix elements which matches the \pt
% dependence of the data instead of a $2\to 1$ process as it can be in the LO
% CEM model or in the Color-Octet Model at low \pt.
%
%
% Concerning the choice of the order, fits performed at different orders may
% show differences which may not be reflecting any specific physical phenomenon
% but a particular sensitivity to QCD corrections of some observables used in
% the fit at scales different than the ones used here. When using partonic cross
% sections evaluated at Born (LO) order, the common practice is thus to employ a
% LO (n)PDF set. Yet, nothing forbids us to use a NLO one as a default choice.
% In a sense, any difference observed is an indication of the uncertainty
% attributable to the neglect of unknown higher QCD corrections. Moreover, as
% mentioned above, the LO and NLO gluon shadowing parameterizations differ
% significantly at low $x$.
%
%In the extrinsic approach, 
the parameterisations EKS98 LO~\cite{Eskola:1998df} and nDSg
LO~\cite{deFlorian:2003qf} have been employed. The former
%
coincides with the mid value of EPS09 LO~\cite{Eskola:2009uj}. The error bands
for the EKS98 and nDSg models shown in ~\fig{fig1and2ferreiro} correspond to the
variation of the factorisation scale ($0.5 m_{\rm T} < \mu_F < 2 m_{\rm T}$).



%Note that the nuclear-effect predictions based on nPDFs parameterisations
%significantly depend on the factorisation scale $\mu_F$ at which they are
%evaluated. It introduces an additional --significant-- uncertainty which is
%often overlooked, whereas it is known to be already large in the description of
%\pp collisions for which the PDFs are better known.


The spatial dependence of the nPDF has been included in this approach through a
probabilistic Glauber Monte-Carlo framework, {\sf JIN}~\cite{Ferreiro:2008qj},
assuming an inhomogeneous shadowing proportional to the local
density~\cite{Klein:2003dj,Vogt:2004dh}. Results are shown in
\fig{fig3ferreiro}.




%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../SaporeGravis"
%%% End: 
