
Thanks to the large heavy-flavour samples available at hadron colliders, studies of the production of open or hidden heavy-flavour production in association with another particle (light- or heavy-hadrons, quarkonium, or vector boson) are possible. The cross section of these processes is heavily sensitive to the particle production mechanisms and can help distinguishing between them. 
% 
In addition, these final states can also results from multiple parton-parton interactions (or double-parton scatterings, DPS), where several hard parton-parton interactions occur in the same event, without any correlation between them~\cite{Alner:1985wj, Wang:1991us, Sjostrand:1987su, Bartalini:2010su}. 
%
Analogously, heavy-flavour production dependence with the underlying event multiplicity brings information into their production mechanisms. 
%
A complete understanding of heavy-flavour production in hadronic collisions is mandatory to interpret heavy-flavour measurements in \pA and \AAcoll collisions, and disentangle cold (see \sect{Cold nuclear matter effects}) and hot (see \sects \ref{OHF} and~\ref{sec:quarkonia}) nuclear matter effects at play. 


\subsubsection{Production as a function of multiplicity}
\label{sec:pp:HadCorrelations}


The correlation of open or hidden heavy-flavour yields with charged particles produced in hadronic collisions can provide insight into their production mechanism and into the interplay between hard and soft mechanisms in particle production. 
In high energy hadronic collisions, multiple parton-parton interactions may also affect heavy-flavour production~\cite{Maciula:2012rs,Porteboeuf:2010dw}, in competition to a large amount of QCD-radiation associated to hard processes. 
In addition to those initial-state effects, heavy-flavour production could suffer from final-state effects due to the high multiplicity environment produced in high energy \pp collisions~\cite{Werner:2010ny, Lang:2013ex}. 


At the LHC, \jpsi yields were measured as a function of charged-particle density at mid-rapidity by the ALICE collaboration in \pp collisions at \s = 7\TeV~\cite{Abelev:2012rz}. \fig{fig:ALICE_JPsi_mult} shows the \jpsi yields at forward rapidity, studied via the dimuon decay channel at $2.5 < y < 4$, and at mid-rapidity, analysed in its dielectron decay channel at $|y| < 0.9$. The results at mid- and forward-rapidity are compatible within the measurement uncertainties, indicating similar correlations over three units of rapidity and up to four times the average charged-particle multiplicity. The relative \jpsi yield increases with the relative charged-particle multiplicity. This increase can be interpreted in terms of the hadronic activity accompanying \jpsi production, as well as multiple parton-parton interactions, or in the percolation scenario~\cite{Ferreiro:2012fb}. 
%
%
\begin{figure}[!b]
	\centering
	\includegraphics[width=0.5\columnwidth]{pp/Figures/correlation/2012-Sep-11-yields_vs_mult.eps}
	\caption{
	\label{fig:ALICE_JPsi_mult}
	\jpsi yield as a function of the charged-particle density at mid-rapidity in \pp collisions at \s = 7\TeV~\cite{Abelev:2012rz}. 
	Both the yields at forward- ($\jpsi \to \mumu$, $2.5 < y < 4$) and at mid-rapidity ($\jpsi \to \ee$, $|y| < $ 0.9) are shown.
	}
\end{figure}



A similar study of the \ups yields was performed by the CMS collaboration in \pp collisions at \s = 2.76\TeV~\cite{Chatrchyan:2013nza}. 
The self-normalized cross sections of $\upsa/\langle \upsa \rangle$, $\upsb/\langle \upsb \rangle$ and $\upsc/\langle \upsc \rangle$ at mid-rapidity are found to increase with the charged-particle multiplicity. 
To unveil possible variations of the different \ups states, the ratio of the $\upsb$ and $\upsc$ yields with respect to the $\upsa$ yield is shown in \fig{fig:CMS_ups_mult}. The left figure presents the production cross section ratio as a function of the transverse energy ($E_{\rm T}$) measured in $4.0 < |\eta| < 5.2$, whereas the right figure shows the values with respect to the number of charged tracks ($N_{\rm tracks}$) measured in $|\eta| < 2.4$. 
The excited-to-ground-states cross-section ratios seem independent of the event activity when they are evaluated as a function of the forward-rapidity $E_{\rm T}$. These ratios seem to decrease with respect to the mid-rapidity $N_{\rm tracks}$, behaviour that can not be confirmed nor ruled out within the uncertainties. 
The \upsa is produced on average with two extra charged tracks than excited states. Feed-down contribution can not solely explain the observed trend. If \ups states were originated from the same initial partons, the mass difference between the ground and the excited states could generate extra particles produced with \upsa. 
%
%. Another interpretation is final state effects. If \ups interacts with the produced hadronic environement, excited state are supposed to be more affected due to lower binding energy and larger size.
%
\begin{figure}[!t]
	\centering
	\includegraphics[width=0.47\columnwidth]{pp/Figures/correlation/figs_hf_combi0_glbErrBand0.png}
	\includegraphics[width=0.47\columnwidth]{pp/Figures/correlation/figs_trk_combi0_glbErrBand0.png}
	\caption{
		\label{fig:CMS_ups_mult}
	Cross section ratio of $\upsb/\upsa$ and $\upsc/\upsa$ for $|y| < 1.93$ as a function of the transverse energy ($E_{\rm T}$) measured in $4.0 < |\eta| < 5.2$ (left) and the number of charged tracks ($N_{\rm tracks}$) measured in $|\eta|<2.4$ (right), in \pp collisions at \s = 2.76\TeV (open symbols) and \pPb collisions at \snn=5.02\TeV~(filled symbols)~\cite{Chatrchyan:2013nza}.
	}
\end{figure}


The measurement of open heavy-flavour production (via D mesons and non-prompt \jpsi) as a function of charged-particle multiplicity at mid-rapidity in \pp collisions at \s = 7\TeV was recently carried out by the ALICE collaboration ~\cite{Adam:2015ota}. \fig{fig:ALICE_D_mult} (right) presents the results 
for D mesons in four \pt bins compared to the percolation scenario ~\cite{Ferreiro:2012fb, Ferreiro:2015gea}, EPOS~3 with or without hydro ~\cite{Drescher:2000ha,Werner:2013tya} and PYTHIA~8  simulations ~\cite{Sjostrand:2007gs,Sjostrand:2006za}. D-meson per-event yields are independent of \pt within the 
measurement uncertainties  ($1 < \pt < 12\GeVc$) and increase with multiplicity faster than linearly at high multiplicities. \fig{fig:ALICE_D_mult} (left) shows non-prompt \jpsi yields compared to PYTHIA~8 simulations. D-meson and non-prompt \jpsi yields present a similar increase with charged-particle multiplicity. The heavy-flavour relative yield enhancement as a function of the charged-particle multiplicity is qualitatively described by the percolation model, EPOS 3 and PYTHIA~8 for D mesons and PYTHIA~8 for non-prompt \jpsi.  However, the PYTHIA~8 event generator seems to under-estimate the increase of heavy flavour yields with the charged-particle multiplicity at high multiplicities. Open (D and non-prompt \jpsi) and hidden (inclusive \jpsi) heavy-flavour yields present a similar increase with the charged-particle multiplicity at mid-rapidity. This similarity suggests that the enhancement is likely related to heavy-quark production mechanisms and is not significantly influenced by hadronisation. It could be described by the hadronic  activity associated to heavy-flavour production, multiple parton-parton interactions, or the percolation scenario ~\cite{Maciula:2012rs,Werner:2010ny,Ferreiro:2015gea}.
\begin{figure}[!htbp]
	\centering
	\includegraphics[width=0.47\columnwidth]{pp/Figures/correlation/2015-May-04-D_data_theory_summary.png}
	\includegraphics[width=0.47\columnwidth]{pp/Figures/correlation/2015-May-04-B_data_theory_summary.png}
	\caption{
		\label{fig:ALICE_D_mult}
	D-meson production (left) and non-prompt \jpsi (right) as a function of charged-particle multiplicity in \pp collisions at \s = 7\TeV ~\cite{Adam:2015ota} compared to PYTHIA~8 ~\cite{Sjostrand:2007gs,Sjostrand:2006za}, EPOS~3 ~\cite{Drescher:2000ha,Werner:2013tya} and the percolation scenario ~\cite{Ferreiro:2012fb, Ferreiro:2015gea}.
	}
\end{figure}


Hidden and open heavy-flavour production measurements as a function of the event activity were initiated during the LHC \RunOne leading to unexpected results with impact on our understanding of the production mechanisms and the interpretation of \pPb and \PbPb results. \RunTwo data, with the increased centre-of-mass energy of 13\TeV in \pp collisions and larger luminosities, will allow to reach higher multiplicities and to perform $\pt$-differential studies of hidden and open heavy-flavour hadron production.
 