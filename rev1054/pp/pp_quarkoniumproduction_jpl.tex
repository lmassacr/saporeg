The theoretical study of quarkonium-production processes involves
both pertubative and non-perturbative aspects of QCD. On one side, the production
of the heavy-quark pair, \QQbar, which will subsequently form the quarkonium, is expected to be perturbative
since it involves momentum transfers at least as large as the mass of the considered heavy quark, as
for open-heavy-flavour production discussed in the previous section.
On the other side, the evolution of the \QQbar pair into the physical quarkonium state is non-perturbative, over long distances, 
with typical momentum scales such as the momentum of the heavy-quarks in 
the bound-state rest frame, $m_Q v$ and their binding energy $m_Q v^2$.

In nearly all the models or production mechanisms discussed nowadays, the idea of a factorisation
between the pair production and its binding is introduced. Different approaches differ essentially
in the treatment of the hadronisation, although some may also introduce new ingredients in the description 
of the heavy-quark-pair production. In the following, we briefly describe three of them which can be distinguished
in their treatment of the non-perturbative part: the Colour-Evaporation Model (CEM), the Colour-Singlet Model (CSM), the Colour-Octet 
Mechanism (COM), the latter two being encompassed in an effective theory referred to as Non-Relativistic QCD (NRQCD).



\paragraph{The Colour-Evaporation Model (CEM)}

This approach is in line with the principle of quark-hadron duality~\cite{Fritzsch:1977ay,Halzen:1977rs}. As such,
the production cross section of quarkonia is expected to be directly connected to that to  produce a \QQbar pair
in an invariant-mass region where its hadronisation into a quarkonium is possible, that is
between  the kinematical threshold to produce a quark pair, $2m_Q$, and that 
to create the lightest open-heavy-flavour hadron pair, $2m_{H}$. 

The cross section to produce a given quarkonium state is then supposed to be obtained after
a multiplication by a phenomenological factor $F_{\cal Q}$ related to a process-independent probability that the
pair eventually hadronises into this state. One assumes that a number of non-perturbative-gluon emissions
occur once the $Q \overline Q$ pair is produced and that the quantum state of the pair at its hadronisation
is essentially decorrelated --at least colourwise-- with that at its production. From the reasonable
assumption~\cite{Amundson:1995em} that one ninth --one colour-{\it singlet} \QQbar 
configuration out of 9 possible-- of the pairs in the suitable kinematical region 
hadronises in a quarkonium,  a simple statistical counting~\cite{Amundson:1995em} was proposed
based on the spin $J_{\cal Q}$ of the quarkonium ${\cal Q}$,
$F_{\cal Q}= {1}/{9} \times {(2 J_{\cal Q} +1)}/{\sum_i (2 J_i +1)}$,
where the sum over $i$ runs  over all the charmonium states below the open heavy-flavour threshold. It was shown to 
reasonably account for existing \jpsi hadroproduction data of the late 90's and, in fact, is comparable to 
the fit value in~\cite{Bedjidian:2004gd}.

Mathematically, one has
\begin{equation}
\sigma^{\rm (N)LO}_{\cal Q}= F_{\cal Q}\int_{2m_Q}^{2m_H} 
\frac{\dd\sigma_{Q\overline Q}^{\rm (N)LO}}{\dd m_{Q\overline Q}}\dd m_{Q\overline Q}
\label{eq:sigma_CEM}
\end{equation}
In the latter formula, a factorisation between the short-distance \QQbar-pair production and its hadronisation is the 
quarkonium state is of course implied although it does not rely on any factorisation proof. In spite of this, 
this model benefits --as some figures will illustrate it in the next section-- from a successful phenomenology but for the absence of predictions for polarisation 
observables and discrepancies in some transverse momentum spectra.  

\paragraph{The Colour-Singlet Model (CSM)}

The second simplest model to describe quarkonium production relies on the rather opposite assumption 
that the quantum state of the pair does {\it not} evolve between its production and its hadronisation, neither
in spin, nor in colour~\cite{Chang:1979nn,Baier:1981uk,Baier:1983va} -- gluon emissions from the heavy-quark are
suppressed by powers of $\alpha_s(m_Q)$. In principle, they are taken into account in the (p)QCD corrections 
to the hard-scattering part account for the \QQbar-pair production.
If one further assumes that the quarkonia are non-relativistic bound states
with a highly peaked wave function in the momentum space, it can be shown that  
partonic cross section for quarkonium production should then be expressed as that for the production of a heavy-quark 
pair with zero relative velocity, $v$, in a colour-singlet state and 
in the same angular-momentum and spin state as that of the to-be produced quarkonium, 
and the square of the Schr\"odinger wave function at the origin in the position space. In the case of hadroproduction, 
which interests us most here, 
one should further account for the parton $i,j$ densities in the 
colliding hadrons, $f_{i,j}(x)$, in order to get the following hadronic cross section 
\begin{equation}
d\sigma[{\cal Q}+X]
=\sum_{i,j}\!\int\! \dd x_{i} \,\dd x_{j} \,f_{i}(x_i,\mu_F) \,f_{j}(x_j,\mu_F) \dd\hat{\sigma}_{i+j\rightarrow (Q\overline{Q})+X} (\mu_R,\mu_F) |\psi(0)|^2
\label{eq:sigma_CSM}
\end{equation}
In the case of $P$-waves, $|\psi(0)|^2$ vanishes and, in principle, one should consider its derivative and that of 
the hard scattering. In the CSM, $|\psi(0)|^2$ or $|\psi'(0)|^2$ also appear in decay processes and can be extracted
from decay-width measurements. The model then becomes  fully predictive but for the usual unknown values of the unphysical
factorisation and renormalisation scales and of the heavy-quark mass entering the hart part.
A bit less than ten years ago, appeared the first evaluations of the QCD 
corrections~\cite{Campbell:2007ws,Artoisenet:2007xi,Gong:2008sn,Gong:2008hk,Artoisenet:2008fc}
to the yields of \jpsi and \ups (also commonly denoted $\cal Q$) in 
hadron  collisions in the CSM. It is now widely accepted~\cite{Lansberg:2008gk,ConesadelValle:2011fw,Brambilla:2010cs} 
that $\alpha^4_s$ and $\alpha^5_s$ corrections to the CSM are significantly larger than the LO  contributions at $\alpha^3_s$
at mid and large \pt and that they should systematicaly be accounted for in any study of their \pt spectrum. 

Possibly due to its high predictive power, the CSM has faced several phenomenological issues although
it accounts reasonnably well for the bulk of hadroproduction data from RHIC to LHC energies~\cite{Brodsky:2009cf,Lansberg:2010cn,Feng:2015cba}, 
\ee data at $B$ factories~\cite{Ma:2008gq,Gong:2009kp,He:2009uf} and photo-production data at HERA~\cite{Aaron:2010gz}. 
Taking into account NLO --one loop-- corrections and approximate NNLO contributions (dubbed as NNLO$^\star$ in the following) 
has reduced the most patent discrepancies in particular for \pt up to a couple of $m_{\cal Q}$~\cite{Lansberg:2010vq,Lansberg:2011hi,Lansberg:2012ta,Lansberg:2013iya}.
A full NNLO computation (\ie~at $\alpha^5_s$) is however needed to confirm this trend.

It is however true that the CSM is affected by infrared divergences in the case of $P$-wave decay at NLO, which were 
earlier regulated by an ad-hoc binding energy~\cite{Barbieri:1976fp}. These can nevertheless 
be rigorously cured~\cite{Bodwin:1992ye} in the more general framework of NRQCD which we discuss now and which
introduce the concept of colour-octet states.

\paragraph{The Colour-Octet Mechanism (COM) and NRQCD}

Based on the effective theory NRQCD, one can express in a more rigorous way the hadronisation probability
of a heavy-quark pair into a quarkonium via long-distance matrix elements (LDMEs). In addition to the usual
expansion in powers of $\alpha_s$, NRQCD further introduces an expansion in $v$. It is then natural to account
for the effect of higher-Fock states (in $v$) where the \QQbar pair is in an octet state with a different 
angular-momentum and spin states --the sole consideration of the {\it leading} Fock state (in $v$) amounts to the CSM, which is thus
{\it a priori} the {\it leading} NRQCD contribution (in $v$). However, this opens the possibility for non-perturbative transitions between
these coloured states and the physical meson. One of the virtues of this is the consideration of $^3S_1^{[8]}$ states
in $P$-wave productions, whose contributions cancel the aforementioned divergences in the CSM. The necessity for 
such a cancellation does not however fix the relative importance of these contributions. In this precise case, 
it depends on an unphysical scale $\mu_\Lambda$. 

As compared to the \eq{eq:sigma_CSM}, one has to further
consider additional quantum numbers (angular momentum, spin and colour), generically denoted $n$, involved in the production mechanism:
\begin{equation}
\dd\sigma[{\cal Q}+X]
=\sum_{i,j,n}\!\int\! \dd x_{i} \,\dd x_{j} \,f_{i}(x_i,\mu_F) \,f_{j}(x_j,\mu_F) \dd\hat{\sigma}_{i+j\rightarrow (Q\overline{Q})_{n}+X} (\mu_R,\mu_F,\mu_\Lambda) \langle{\cal O}_{\cal Q}^{n} \rangle.
\label{eq:sigma_NRQCD}
\end{equation}

Instead of the Schr\"odinger wave function at the origin squared, the former equation involves 
the aforementioned LDMEs, $\langle{\cal O}_{\cal Q}^{n} \rangle$, which {\it cannot} 
be fixed by decay-width measurements nor lattice studies -- but the leading CSM ones of course. Only 
relations based on Heavy-Quark Spin Symmetry (HQSS) can relate some of them.

Three groups (Hamburg~\cite{Butenschoen:2012px}, IHEP~\cite{Gong:2012ug} and PKU~\cite{Chao:2012iv}) 
have, in the recent years, carried out a number of NLO studies\footnote{A recent LO study has also been performed including LHC data in 
the  used sample~\cite{Sharma:2012dy}.}  of cross-section fits to determine
the NRQCD LDMEs. A full description 
of the differences between these analyses is beyond the scope of this review, it is however important
 to stress that they somehow contradict each other in their results as regards the polarisation observables.
In particular, in the case of the \jpsi, the studies of the Hamburg group, which is the only one to fit low \pt data from 
hadroproduction, electroproduction and \ee collisions at $B$ factories, predict a strong transverse polarised yield at variance with the experimental data.

\paragraph{Theory prospects}


Although NRQCD is 20 years old, there does not exist yet a complete proof of factorisation, in particular, in the case of 
hadroproduction. 
A discussion of the difficulties in establishing NRQCD factorisation can be found in~\cite{Brambilla:2010cs}.
A first step was achieved in 2005 by the demonstration~\cite{Nayak:2005rt,Nayak:2005rw} that, in the large-\pt region where a 
description in terms of 
fragmentation functions is justified, the infrared poles at NNLO could be absorbed in the NRQCD LDMEs,
provided that the NRQCD production operators were modified to include nonabelian phases. 

As mentioned above, it seems that the mere expansion of the hard matrix elements in $\alpha_s$ is probably
not optimal since higher QCD corrections receive contributions which are enhanced by powers
of $\pt/m_{\cal Q}$. It may therefore be expedient to organise the quarkonium-production cross section in powers of $\pt/m_{\cal Q}$
before performing the  $\alpha_s$-expansion of the short distance coefficients for the \QQbar production. This 
 is sometimes referred to as the fragmentation-function approach (see \cite{Kang:2011mg,Ma:2014svb}) which offers new
perspectives in the theoretical description of quarkonium hadroproduction especially at mid and large \pt.

At low \pt, it was recently emphasised in \cite{Feng:2015cba} that one-loop results show
an intriguing energy dependence which might hint at a break-down of NRQCD factorisation in this kinematical region.
In any case, as for now, past claims that colour-octet transitions are the dominant source of the low-\pt 
 \jpsi and \ups cannot be confirmed at one loop accuracy.
Approaches such as the \kt factorisation based on the Lipatov action in the quasi multi Regge kinematics
(see~\cite{Hagler:2000dd,Yuan:2000qe} for quarkonium studies), the TMD factorisation 
(see~\cite{Boer:2012bt,Ma:2012hh} for recent applications to quarkonium production) or
the combined use of the CGC formalism and NRQCD \cite{Kang:2013hta,Ma:2014mri}  may therefore bring 
their specific bricks in the building 
of a consistent theory of quarkonium production. Finally, let us mention the relevance of the colour-transfer 
mechanism~\cite{Nayak:2007mb}, beyond NRQCD, in the case of production of a 
quarkonium in the vicinity of another heavy quark.

