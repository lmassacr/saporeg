% !TEX root = ../SaporeGravis.tex

Within a weak-coupling approach the interaction of heavy quarks with the medium can be described in terms of the uncorrelated scatterings with the light quarks and gluons of the hot deconfined plasma. Neglecting radiative processes one can then attempt an evaluation of the heavy-flavour transport coefficients arising from the $2\to 2$ elastic collisions suffered in the medium: this was the approach followed in \cis{Alberico:2013bza,Alberico:2011zy}, which we briefly summarise. The approach developed by the authors to simulate the propagation of the heavy quarks in the QGP was based on the relativistic Langevin equation (here written in its discretised form)
\begin{equation}
  \frac{\Delta\vec{p}}{\Delta t}=-\eta_D(p)\vec{p}+\vec{\xi}(t)
 \quad{\rm with}\quad \langle\xi^i(t)\xi^j(t')\rangle
\label{eq:lange_r_d}
\end{equation} 
where  
\begin{equation} 
b^{ij}(\vec{p})= \kappa_{\rm L}(p)\hat{p}^i\hat{p}^j+\kappa_{\rm T}(p)
(\delta^{ij}-\hat{p}^i\hat{p}^j).
\end{equation}
The right-hand side is given by the sum of a deterministic friction force and a stochastic noise term. The interaction with the background medium is encoded in the transport coefficients $\kappa_{T/L}$ describing the average squared transverse/longitudinal momentum per unit time exchanged with the plasma. In \cis{Alberico:2013bza,Alberico:2011zy} the latter were evaluated within a weak-coupling setup, accounting for the collisions with the gluons and light quarks of the plasma. In particular, hard interactions were evaluated through kinetic pQCD calculation; soft collisions, involving the exchange of long-wavelength gluons, required the resummation of medium effects, the latter being provided by the Hard Thermal Loop (HTL) approximation. The friction coefficient $\eta_D(p)$ appearing in \eq{eq:lange_r_d} has to be fixed in order to ensure the approach to thermal equilibrium through the Einstein fluctuation-dissipation relation
\begin{equation}
\eta_D^{\rm (Ito)}(p)\equiv\frac{\kappa_{\rm L}(p)}{2TE_p}-\frac{1}{2p}\left[\partial_p\kappa_{\rm L}(p)+\frac{d-1}{2}(\kappa_{\rm L}(p)-\kappa_{\rm T}(p))\right].\label{eq:friction}
\end{equation}
In the above, the second term in the right-hand side is a correction (sub-leading by a $T/p$ factor) depending on the discretisation scheme employed in the numerical solution of the stochastic differential equation~(\eq{eq:lange_r_d}) in the case of momentum dependent transport coefficients (for more details see \ci{Beraudo:2009pe}): it ensures that, in the continuum $\Delta t\to 0$ limit, one recovers a Fokker-Planck equation with a proper Maxwell-J\"uttner equilibrium distribution as a stationary solution. Here we have written its expression in the so-called Ito scheme~\cite{Ito:1951}, which is the most convenient for a numerical implementation. Results for the friction coefficient $\eta_D(p)$ of \cquark and \bquark quarks are displayed in \fig{fig:friction}.
\begin{figure}
\begin{center}
\includegraphics[clip,width=0.48\textwidth]{charm_etaD}
\includegraphics[clip,width=0.48\textwidth]{beauty_etaD}
\caption{The charm (left panel) and beauty (right panel) friction coefficients in a Quark Gluon Plasma at a temperature $T = 400$\MeV. Continuous curves refer to the results of the Einstein relation $\eta_D=\kappa_{\rm L}/2ET$, while dashed curves include the discretisation correction in \eq{eq:friction} within the Ito scheme~\cite{Ito:1951}. The sensitivity of the results to the intermediate cutoff $|t|*$ separating hard and soft collisions is also displayed.}
\label{fig:friction}
\end{center}
\end{figure}  
