%\documentclass{appolb}
\documentclass{article}
%\usepackage[margin=0.3in]{geometry}

\usepackage[margin=2.3cm]{geometry}
\textheight=23cm
\textwidth=17cm
%\hoffset=-2cm
%\voffset=-1cm


%\usepackage[dvips]{graphicx} 
%\usepackage{graphics}
\usepackage{graphicx}
%\DeclareGraphicsExtensions{.eps,.ps,.pdf,.png,.jpeg}
%\usepackage{subcaption}
% epsfig package included for placing EPS figures in the text
%------------------------------------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                %
%    BEGINNING OF TEXT                           %
%                                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\fig}[1]{Fig.\ \ref{#1}}
\newcommand{\eq}[1]{Eq.\ (\ref{#1})}

\begin{document}
% \eqsec  % uncomment this line to get equations numbered by (sec.num)
\title{AdS/CFT}

\date{}

\author{W.\ A.\ Horowitz} 


\maketitle

\section{HF Interaction with Medium in AdS/CFT}

The anti-de-Sitter/conformal field theory (AdS/CFT) correspondence \cite{CasalderreySolana:2011us,DeWolfe:2013cua} is a conjectured dual between field theories in $n$ dimensions and string theories in $n+1$ dimensions (times some compact manifold).  The correspondence is most well understood between $\mathcal{N} = 4$ super Yang-Mills (SYM) and Type IIB string theory; these two theories are generally considered exact duals of one another.  The calculational advantage provided by the conjecture is that there is generally speaking an inverse relationship between the strength of the coupling in the dual theories: when the field theory is weakly-coupled the string theory is strongly-coupled, and vice-versa.  The advantage for QCD physics accessible at current colliders is clear: the temperatures reached at RHIC and LHC are at most a few times $\Lambda_{\mathrm{QCD}}$; it is therefore reasonable to expect that much of the dynamics in these collisions is dominated by QCD physics that is strongly-coupled and hence theoretically accessible only via the lattice, which is then generally restricted to imaginary time correlators, or via the methods of AdS/CFT.  The leading order contribution to string theory calculations (corresponding to a very strong coupling limit in the field theory) comes from classical gravity; one uses the usual tools of Einsteinian GR.  Although much research is focused on finding dual string theories ever closer to QCD, no one has yet found an exact dual; nevertheless, one hopes to gain nontrivial insight into QCD physics by investigating the relevant physics from known AdS/CFT duals.  An obvious limitation of the use of AdS/CFT is that it is difficult to quantify the corrections one expects in going from the dual field theory in which a derivation is performed to actual QCD.

The main thrust of open heavy flavor suppression research that uses the AdS/CFT correspondence assumes that all couplings are strong, regardless of scale (calculations for light quarks with all couplings assumed strong and calculations for which some couplings are strong and some are weak have also been performed; see \cite{CasalderreySolana:2011us,DeWolfe:2013cua} and references therein for a review).  For reasons soon to be seen, the result is known as ``heavy quark drag'';
%
%There are two main thrusts of open heavy flavor research that exploit the AdS/CFT correspondence: ``heavy quark drag'' and ``$\hat{q}$.'' %(there is also a significant effort in the direction of light flavor energy loss, heavy quark potentials, and low-$p_T$ phenomena).  
%In the first case, ``heavy quark drag,'' all couplings are assumed strong and the entire process of probe quark and thermal medium is modeled in the string theory; 
see \fig{horowitz:f:setup} %(\subref{horowitz:f:hanging}) 
for a picture of the setup.  The heavy quark is modeled as a string with one endpoint near (or, for an infinitely massive quark, at) the boundary of the AdS space; the string hangs down in the fifth dimension of the spacetime towards a black hole horizon (the Hawking temperature of the black hole is equal to the temperature of the Yang-Mills plasma).  As the string endpoint near the boundary moves, momentum flows down the string; this momentum is lost to the thermal plasma.  For a heavy quark moving with constant velocity $v$ in $\mathcal{N} = 4$ SYM, one finds \cite{Herzog:2006gh,Gubser:2006bz}
\begin{equation}
	\label{horowitz:e:adsdrag}
	\frac{dp}{dt} = -\frac{\pi\sqrt{\lambda}}{2}T_{SYM}^2\frac{v}{\sqrt{1-v^2}} \;\; \Longrightarrow \;\; \frac{dp}{dt} = -\mu_Q\,p,
\end{equation}
where $\mu_Q = \pi\sqrt{\lambda}T_{SYM}^2/2m_Q$, and the energy loss reduces to a simple drag relationship in the limit of a very heavy quark, for which corrections to the usual dispersion relation $p/m_Q \, = \, v/\sqrt{1-v^2}$ are small.  

As the string is dragged, an induced black hole horizon forms in the induced metric on the worldsheet of the string.  This horizon emits Hawking radiation that is dual in the field theory to the influence of the thermal fluctuations of the plasma on the motion of the heavy quark.  The diffusion coefficients have been derived in the large mass, constant motion limit \cite{Gubser:2006nz,Son:2009vu} as
\begin{equation}
	\label{horowitz:e:adsdiffusion}
	\kappa_T = \pi\sqrt{\lambda}\gamma^{1/2}T_{SYM}^3,\;\;\kappa_L = \pi\sqrt{\lambda}\gamma^{5/2}T_{SYM}^3.
\end{equation}
Note that for $v\,\ne\,0$ the above diffusion coefficients deviate from the usual ones found via the fluctuation-dissipation theorem, which, for the $\mu_Q$ of \eq{horowitz:e:adsdrag}, yields $\kappa_T\,=\,\kappa_L\,=\,\pi\sqrt{\lambda}\gamma \, T_{SYM}$.  The entire setup breaks down at a characteristic ``speed limit,'' $\gamma_{crit}^{SL} = (1 + 2 \, m_Q / \sqrt{\lambda}\,T_{SYM})^2 \, \approx 4\,m_Q^2 / \lambda \, T^2$, which corresponds to the velocity at which the induced horizon on the worldsheet moves above the string endpoint (equivalently, if an electric field maintains the constant velocity of the heavy quark, at the critical velocity the field strength is so large that it begins to pair produce heavy quarks) \cite{Gubser:2006nz}.  Above the critical velocity, it no longer makes sense to treat the heavy quark as heavy, and one must resort to light flavor energy loss methods in AdS/CFT. 

%In the ``$\hat{q}$'' case, only the $\hat{q}$ of the BDMPS approach, which encapsulates the interaction of the probe quark with the thermal medium, is calculated using AdS/CFT; the heavy quark loses energy via weakly-coupled pQCD radiation.  In this approach one computes the expectation value of a light-like Wilson line

% In one common set of coordinates, the five dimensional background has metric 
% \begin{equation}
% 	\label{horowitz:e:metric}
% 	ds^2 = \frac{L^2}{u^2}\left[-f(u)dt^2 + d\mathbf{x}^2 + \frac{du^2}{f(u)}  \right],
% \end{equation}
% where $f(u) \, = \, 1 - (u/u_h)^4$ is the blackening factor and $L$ is the curvature radius for the AdS space.  The boundary of the AdS space is at $u\,=\,0$ and the $4D$ slice of spacetime is identical to vacuum Minkowski space.  

% The heavy quark is modeled as a string hanging down

\begin{figure}[!htbp]
	\centering
    \includegraphics[width=2.15in]{horowitzhangingstring.pdf}
\caption{Setup for the heavy quark drag calculation \cite{DeWolfe:2013cua}.}
\label{horowitz:f:setup}
\end{figure}

% \begin{figure}[!htbp]
% 	\centering
% \begin{subfigure}[b]{.1in}
%     \captionsetup{skip=-15pt,margin=-10pt}
%     \caption{}
%     \label{horowitz:f:hanging}
% \end{subfigure}
% \begin{subfigure}[b]{2.15in}
%     \centering
%     \includegraphics[width=2.15in]{horowitzhangingstring.pdf}
% \end{subfigure}\quad
% \begin{subfigure}[b]{.15in}
%     \captionsetup{skip=-15pt,margin=-20pt}
%     \caption{}
%     \label{horowitz:f:qhat}
% \end{subfigure}
% \begin{subfigure}[b]{2.15in}
%     \centering
%     \includegraphics[width=2.15in]{horowitzwilsonqhat.pdf}
% \end{subfigure}
% \vspace{5pt}
% \caption{(\subref{horowitz:f:hanging}) Setup for the heavy quark drag calculation.  (\subref{horowitz:f:qhat}) Setup for the $\hat{q}$ calculation.}
% \label{horowitz:f:setup}
% \end{figure}

%Write your contribution here. Example of reference here \cite{Andronic:2013zim}.

\section{AdS/CFT Calculations Compared to Data}
The curves labeled (BLAH) in FIG (BLAH) represent the heavy flavor suppression predicted based on the application of the AdS/CFT drag energy loss to RHIC and LHC data.  In the model \cite{Horowitz:2007su,Horowitz:2011wm}, FONLL \cite{Cacciari:2012ny} provides both the heavy flavor production and the fragmentation to $e^\pm$ and $D$ and $B$ mesons.  The medium background is based on the Glauber model with $T(\vec{x},\,\tau)\propto\rho_{part}(\vec{x})\tau^{-1/3}$.  The energy loss of a heavy quark propagating through the plasma is then given by the AdS/CFT drag derivation, \eq{horowitz:e:adsdrag}, starting at an early thermalization time $\tau_0 \, = \, 0.6$ fm and continuing until $T \, = \, T_{hadronization} \, = \, 160$ MeV.  

It is nontrivial to connect the parameters of QCD to those of the SYM theory in which the AdS/CFT derivations were performed.  Two common prescriptions \cite{Gubser:2006nz} for determining the parameters in the SYM theory are to take: 1) $\alpha_{SYM} \, = \, \alpha_s$ and $T_{SYM} \, = \, T_{QCD}$ or 2) $\lambda_{SYM} \, = \, 5.5$ and $e_{SYM} \, = \, e_{QCD}$ (and hence $T_{SYM} \, = \, T_{QCD} / 3^{1/4}$).  In the first prescription, the SYM coupling is taken equal to the QCD coupling and the temperatures are equated.  In the second prescription, the energy densities of QCD and SYM are equated and the coupling is fitted by comparing the static quark--anti-quark force from AdS/CFT to lattice results.  

As one can see from FIGURES, the pure drag energy loss is qualitatively consistent with RHIC electrons from the semileptonic decays of heavy mesons \cite{Abelev:2006db,Adare:2010de}.  In the RHIC calculation, the proportionality constant between the medium temperature and the Glauber participant density is set such that, in the Stefan-Boltzmann limit, the rapidity density of gluons in the medium is $dN_g/dy \, = \, 1000$, which is of a number similar to that required by perturbative energy loss calculations and is not too different from the entropy one expects from the measured hadronic multiplicity \cite{Wicks:2005gt}.  With that proportionality constant fixed, one may easily perform predictions for the suppression at LHC, assuming the temperature of the medium scales with the measured hadronic multiplicity \cite{Aamodt:2010cz}.  The resulting predictions are not inconsistent with the current $B$ meson suppression measurement from CMS \cite{Chatrchyan:2012np}.  However, the $D$ meson prediction has about a factor of 5 greater suppression than that measured by ALICE \cite{ALICE:2012ab}.  There are two major experimental caveats: first, the significant enhancement of $e^\pm$ from open heavy flavor in preliminary d+A measurements reported by PHENIX \cite{Tarafdar:2012ef} might indicate significant cold nuclear matter effects not taken into account in this calculation and second, while the $B$ meson suppression prediction is consistent with the current measurement from CMS, it is likely that future measurements will be more discriminating.  %although the measurement current 

Although the pure drag results are oversuppressed at LHC compared to the data, one can make an estimate as to the $p_T$ scale at which including momentum fluctuations becomes important.  By comparing the momentum lost to drag to the potential momentum gain of the fluctuations, one expects that momentum fluctuations become important at a scale $\gamma_{crit}^{\Delta p^2} \, = \, m_Q^2 / 4 \, T^2$.  One can see from above that the speed limit at which the entire calculational framework breaks down, $\gamma_{crit}^{SL}$, is parametrically in $\lambda$ smaller than $\gamma_{crit}^{\Delta p^2}$; however, numerically for the finite values of $\lambda$ phenomenologically relevant at RHIC and LHC $\gamma_{crit}^{\Delta p^2} \, < \, \gamma_{crit}^{SL}$.  In particular, one expects nontrivial corrections to the drag results for $e^\pm$ and $D$ mesons from open heavy flavor at $p_T\sim4-5$ GeV/c.  % the Parametrically, the speed limit is entire calculational framework breaks down earlier than
%
Other calculations \cite{Moore:2004tg,Akamatsu:2008ge} have attempted to include the effect of fluctuations; however their diffusion coefficients were set by the Einstein relations and not those derived from AdS/CFT (recall that the derived diffusion coefficients are qualitatively different from those based on the fluctuation-dissipation theorem except in the limit of $v\,=\,0$).

%Only three significant attempts have been made to compare the AdS/CFT heavy flavor results to data

%So far only two types of attempts have been made to 

\bibliographystyle{ieeetr}
\bibliography{SapHorowitzBib}

\end{document}