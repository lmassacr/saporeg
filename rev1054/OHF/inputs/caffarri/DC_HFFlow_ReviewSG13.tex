%\documentclass{appolb}
\documentclass{article}
%\usepackage[dvips]{graphicx} 
%\usepackage{graphics}
\usepackage{graphicx}
\usepackage{amssymb} 
\usepackage{amsthm} 
\usepackage{lineno}
\usepackage{hyperref}
\usepackage{sidecap}

\newcommand{\Dzero}{{\rm D^0}}
\newcommand{\Dstar}{{\rm D^{*+}}}
\newcommand{\Dplus}{{\rm D^+}}
\newcommand{\pp}{{\rm pp}}
\newcommand{\PbPb}{\mbox{Pb--Pb}}
\newcommand{\AuAu}{\mbox{Au--Au}}
\newcommand{\sqrts}{\sqrt{s}}
\newcommand{\sqrtsNN}{\sqrt{s_{\rm \scriptscriptstyle NN}}}
\newcommand{\GeV}{\mathrm{GeV}}
\newcommand{\TeV}{\mathrm{TeV}}
\newcommand{\pt}{\ensuremath{p_{\rm T}}}
\newcommand{\Raa}{R_{\rm AA}}
\newcommand{\vtwo}{v_{\rm 2}}

%\DeclareGraphicsExtensions{.eps,.ps,.pdf,.png,.jpeg}
% epsfig package included for placing EPS figures in the text
%------------------------------------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                %1
%    BEGINNING OF TEXT                           %
%                                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
%\eqsec  % uncomment this line to get equations numbered by (sec.num)
\title{Heavy Flavour $\vtwo$ measurements }

\date{}

\author{Davide Caffarri (CERN) } 

%\maketitle
 
The evidence of charm quark energy loss in strongly-interacting matter has been studied with the measurement of the modification of the transverse momentum distributions of heavy-flavour hadrons (Sec. ). The suppression observed trough the nuclear modification factor ($\Raa$) reflects a strong interaction of charm quarks with medium~\cite{aliceDRAA}. For low-momentum heavy-quarks this interaction might be related to the collective evolution of the system and the achievement of a possible thermal equilibrium~\cite{batsouli,grecoetal}. Moreover low-momentum heavy quarks could hadronize via recombination mechanisms with other quarks from the medium~\cite{grecoetal,andronicetal}. 
High-momentum heavy quarks might also be sensitive to different amount of energy loss due to the different path length they have to traverse to get out of the medium~\cite{thoma, bdmps}.

The study of the azimuthal anisotropy of heavy flavour hadrons might bring insight on the different physics mechanisms described previously, since heavy quarks are produced in the early stage and they can experience the full system expansion~\cite{PBM}.
Non-central nucleus--nucleus collisions are characterised by an initial
geometrical anisotropy with respect to the reaction plane (the plane
defined by the beam direction and the impact parameter). 
As a consequence of the different pressure gradients in the in-plane
and out-of-plane regions, this spatial anisotropy is converted to a momentum anisotropy of produced low-$\pt$ particles. Due to the spatial anisotropy, higher-$\pt$ heavy quarks emitted in the direction of the reaction plane (in-plane) have a short in-medium path length than patrons directed orthogonally to it (out-of-plane)~\cite{Ollitrault,hydro}. 

Both the momentum anisotropy and the different path length can be quantified with the coefficients $v_{\rm{n}}$ and the symmetry planes $\Psi_n$ of the Fourier expansion of the particle distribution $d^2 N / d\pt d\phi$. The second coefficient of this expansion $\vtwo$ is called
elliptic flow and can be expressed as the average of all particles in all events of the angular correlation $cos[2(\phi-\Psi_2)]$~\cite{Ollitrault}.

In order to determine the second harmonic coefficient $\vtwo$, the $Q$ vector is defined from the azimuthal distribution of charged particles as in Eq.~\ref{eq:QVectorFormula} 

\noindent\begin{minipage}{.5\linewidth}
\begin{equation}
\label{eq:QVectorFormula}
 \vec{Q}= \left(\frac{\sum^{N _i=1} w_i cos 2\phi_i}{\sum^{N _i=1} w_i sin 2\phi_i}\right)
\end{equation}
\end{minipage}%
\begin{minipage}{.5\linewidth}
\begin{equation}
\label{eq:EventPlaneAngle}
\psi_n = \frac{1}{2} tan^-1 \left(\frac{Q_x}{Q_y}\right)
\end{equation}
\end{minipage}

The azimuthal angle of the $Q$ vector (Eq.~\ref{eq:EventPlaneAngle}) is called event plane angle and it can be used to estimate the second harmonic symmetry plane $\Psi_2$. 
Three methods are usually used to compute the elliptic flow: event plane (EP)~\cite{ref:ep}, scalar product (SP)~\cite{SP} and two or four-particles cumulants~\cite{ref:BSVCumulants}.

In the event plane method, particles yields are directly measured in $90^o$-wide intervals of the difference between the azimuthal angle of the particle and the event plane angle. The measured anisotropy is then corrected for the event plane resolution that depends on the method used to compute the event plane itself, on the multiplicity and on the $\vtwo$ of the particles used to compute the Q vector~\cite{ref:ep}. 
The scalar product method is instead based on the correlation between particles in two different pseudo rapidity regions, in order to suppress the contribution of short distance correlations that might enhance the elliptic flow estimate~\cite{SP}.
The multi-particle cumulants method is based on the azimuthal distribution of the different particles in particular on the azimuthal correlation between 2 or 4 particles, exploiting the unique relationship between $\vtwo$ and the so-called Q-cumulants~\cite{ref:BSVCumulants}. The usage of two or four particle correlation gives a different estimate of the elliptic flow ($\vtwo\rm{ \{2\} }$ and $\vtwo\rm{ \{4\}}$) because the two methods have different sensitivity to event-by-event flow fluctuations and non-flow effect (correlations related to jets, resonances decay, $\dots$). While event-by-event flow fluctuations give on average no clear bias on the flow extraction, the non-flow correlations tend to increase the measured flow present in the event. The usage of four particles correlations or the introduction of a pseudo rapidity ($\eta$) gap between the Q vector measurement and the particles of interest allows to reduce those correlations.

The measurement of heavy-flavour electrons coming from the semi-leptonic decays of charm and beauty hadrons has been presented in Sect ... .   
To determine the heavy-flavour electrons $\vtwo$, the starting point is the measurements of inclusive electrons $\vtwo$. The inclusive electrons spectra is composed mainly by photonic electrons and possible hadrons contamination and heavy-flavour electrons. Exploiting the additivity of the $\vtwo$ then, the heavy-flavour electrons $\vtwo$ can be obtained from the subtraction of the photonic electrons and hadrons $\vtwo$ from the inclusive electrons one, weighted by the correspondent yield.

\begin{figure}[t]
\begin{center}
\includegraphics[width=0.48\textwidth]{figures/fig_PHENIX_HFe_minBias.eps}
\includegraphics[width=0.48\textwidth]{figures/fig_PHENIX_HFe_62GeV.eps}
\end{center}
\caption{(a) PHENIX non-photonic electrons $\vtwo$ at $\sqrt{s_{\rm NN}} = 200~\GeV$ measured with Event Plane method with a pseudo-rapidity between the electrons measurement and the event plane estimate in order to reduce the non-flow effects. (b) PHENIX non-photonic electrons $\vtwo$ measured for different $\AuAu$ collisions energy: $62.4~\rm{and}~200~\GeV$ in $20-40\%$ centrality class}.
\label{fig:HFePHENIX}
\end{figure}

The PHENIX Collaboration measured the heavy-flavour electrons $\vtwo$ in $\AuAu$ collisions at $\sqrt{s_{\rm NN}} = 62.4~\rm{and}~200~\GeV$~\cite{Adare:2010de, Adare:2014rly} with the event plane method. In the PHENIX experiment, electrons have been measured at mid rapidity at $|\eta| < 0.35$ between $0.3 < \pt < 5~\GeV/c$. The event plane is instead reconstructed using the particle detected in the region $3.0 < |\eta| < 3.9$ with forward scintillator detectors. The large pseudo rapidity gap between the two measurements should reduce the non-flow effects in the event plane measurement. 

Fig.~\ref{fig:HFePHENIX} (left) shows the non-photonic electrons $\vtwo$ for minimum-bias events ($0-100\%$)~\cite{Adare:2010de}. A finite $\vtwo$ is measured and it shows an increase up to $\pt=2.0~\GeV/c$. For higher momenta, the large statistical uncertainties don't allow to conclude if a saturation or a decrease trend is visibile, but the results are similar to the ones of $\pi$ or $K$ $\vtwo$. The PHENIX Collaboration also studied the centrality dependence of the heavy flavour electrons $\vtwo$. The two semi-peripheral centrality classes ($20-40\%$ and $40-60\%$) show an higher $\vtwo$ than the most central and the most peripheral ones, pointing to a larger effect due to the different geometric anisotropy for those collision classes~\cite{Adare:2010de}. 

The PHENIX Collaboration also delivered results on heavy flavour electrons in $\AuAu$ collisions at $\sqrt{s_{\rm{NN}}} = 62.4~\GeV$~\cite{Adare:2014rly}. The results on the $R_{\rm{AA}}$ at this energy show no evidence of suppression, even if with large statistical uncertainties, mainly due to the lack of $pp$ reference data at the same energy. Also the $\vtwo$ measurement shows a milder effect: for heavy-flavour electrons of $1.3 < p_{T} < 2.5~\GeV/c$ the $\vtwo$ at $\sqrt{s_{\rm{NN}}} = 62.4~\GeV$ is about a factor of two lower than at $\sqrt{s_{\rm{NN}}} = 200~\GeV$ for the centrality class 20-40$\%$ (Fig.~\ref{fig:HFePHENIX} right). Also in this case the results don't allow to draw any strong conclusion due to the large statistical and systematic uncertainties~\cite{Adare:2014rly}. 
 
The STAR Collaboration measured the heavy-flavour electrons $\vtwo$ in $\AuAu$ collisions at $\sqrt{s_{\rm{NN}}} = 39, ~62.4$ and 200 GeV in $0-60\%$ central collisions~\cite{Adamczyk:2014yew}. The two particles cumulants method has been used to measure the elliptic flow for the two lower collisions energy, the event plane, and both two and four particles cumulants methods have been used for the data collected at $\sqrt{s_{\rm NN}} = 200~\GeV$.
Fig.~\ref{fig:HFeSTAR} (left) shows the results on the heavy-flavour electrons $\vtwo$ with the methods described before and for minimum bias and data triggered with the electromagnetic calorimeter. A $\vtwo\{2\}$ and $\vtwo\{4\}$ larger than zero is observed for $\pt = 0.5~\GeV/c$ in $\AuAu$ collisions at $\sqrt{s_{\rm NN}} = 200~\GeV$~\cite{Adamczyk:2014yew}. At high-$\pt$ the $\vtwo\{2\}$ and the $\vtwo{\rm{\{EP\}}}$ are consistent within each other. An increase of the $\vtwo$ for $\pt > 4 ~\GeV/c$ is observed, that is probably due to jet-like correlations. The black line in the Fig.~\ref{fig:HFeSTAR} (left) shows the jet-like correlation expected in $\AuAu$ collisions based on estimate of non-flow correlations measurement in $pp$ collisions at $\sqrt{s} = 200~\GeV$ and scaled by the hadron multiplicity in $\AuAu$. Those correlations can explain the rise of $\vtwo\{2\}$ and $\vtwo{\rm{\{EP\}}}$ with $\pt$ as observed in the data and it is likely they are dominating the $\vtwo$ measurement for $\pt > 4 ~\GeV/c$.
In Fig.~\ref{fig:HFeSTAR} (left) STAR results are also compared with PHENIX ones where electors are measured in the region $|\eta| < 0.35$ and with a large pseudo rapidity gap between the electrons and the Q vector estimate has been introduced, in order to reduce non-flow effects. The results of the two experiments are consistent in the range where they overlap~\cite{Adamczyk:2014yew}. 

Fig.~\ref{fig:HFeSTAR} (right) shows the non-photonic $\vtwo$ measured with two particles cumulants in the three centre of mass energy. At  $\sqrt{s_{\rm{NN}}} = 39$ and $62.4~\GeV$, the $\vtwo\{2\}$ is consistent with zero up to $\pt = 1.6~\GeV/c$. 
The difference observed between the results at the top RHIC energy and the two lower energies is quantify in terms of separation in number of sigma considering the samples to be independent. For heavy-flavour electrons of $\pt = 0.58~\GeV/c$ the difference between 200 and $62.4~\GeV$ is $3\sigma$ and between 200 and $39~\GeV$ is $2.8\sigma$. This is the $\pt$ interval where the difference is larger with respect to the others~\cite{Adamczyk:2014yew}.

\begin{figure}[t]
\begin{center}
\includegraphics[width=0.48\textwidth]{figures/fig_STAR_HFe_v2Methods.pdf}
\includegraphics[width=0.48\textwidth]{figures/fig_STAR_HFe_sqrts.pdf}
\end{center}
\caption{ (a) STAR non-photonic electrons $\vtwo$ at $\sqrt{s_{\rm NN}} = 200 GeV$ measured with different methods and compared with PHENIX results. Non-flow contribution estimate using non-photonic electrons hadron correlation in pp collisions and PYTHIA (black line). (b) STAR non-photonic electrons $\vtwo{2}$ measured for different $\AuAu$ collisions energy: $39,~62.4~\rm{and}~200~\pt/c$.}
\label{fig:HFeSTAR}
\end{figure}

Also the ALICE Collaboration delivered some preliminary results on non photonic electrons $\vtwo$ measured at $\sqrt{s_{\rm{NN}}} = 2.76~\TeV$~\cite{Dubla:2014cpa}. Those results are in agreement with what found at RHIC energies. 

At the LHC energies, the ALICE experiment also studied the $\vtwo$ of prompt charm D mesons reconstructed in the central barrel $|\eta| < 0.8$, between $2 < p_{T} < 16~\GeV/c$. 
The D mesons ($\Dzero$, $\Dplus$ and $\Dstar$) are reconstructed via their hadronic decay channels, and exploiting the separation of about a few hundred $\mu$m between the primary and the secondary
vertices in order to reduce the combinatorial background.
The measurement of D mesons $\vtwo$ have been performed using the Event Plane method: the invariant mass distribution of reconstructed D meson candidates is considered in the in-plane and
out-of-plane regions as define in the introduction; the signal yield in the two regions is extracted based on a Gaussian fit. 

\begin{figure}[t]
\begin{center}
\includegraphics[width=0.48\textwidth]{figures/fig_ALICE_Dmes_3050.eps}
\includegraphics[width=0.48\textwidth]{figures/fig_ALICE_Dmes_threeCentr.eps}
\end{center}
\caption{(left) Comparison of prompt $\Dzero$ meson and charged particle $\vtwo$ in $30-50\%$ centrality class as a function of $\pt$. Both results are obtained with the event plane method, for charged particles with the introduction of a pseudo rapidity gap. (right) Centrality evolution of D mesons 
$\vtwo$ for three $\pt$ bins. An indication of increasing $\vtwo$ going from central to semi-peripheral collisions is observed.}
\label{fig:DmesALICE}
\end{figure}

Fig.~\ref{fig:DmesALICE} (right) shows the average of  $\Dzero$, $\Dplus$, $\Dstar$ mesons $\vtwo$ computed with the Event Plane method of in the $30-50\%$ centrality class~\cite{Abelev:2013lca}. The results show a non-zero D mesons $\vtwo$ in the interval $2 < \pt< 6~\GeV/c$ with a $5.7\sigma$ significance. This result indicates that the interaction with the medium constituents,  transfer to charm quarks information on the azimuthal anisotropy of the system.
In the same figure, the results are compared with the $\vtwo$ of charged particles for the same centrality class. The magnitude of $\vtwo$ is similar for charmed and light-flavour hadrons. 
Fig.~\ref{fig:DmesALICE} (left) show the dependence with centrality of the $\Dzero$ meson $\vtwo$ for three $\pt$ intervals: $2 < \pt < 3~\GeV/c$, $3 < \pt < 4~\GeV/c$ and $4 < \pt < 6~\GeV/c$. An increasing trend of $\vtwo$ towards more peripheral collisions is observed, as expected because of the increasing initial geometry anisotropy~\cite{Abelev:2014ipa}. The D mesons $\vtwo$ is also compared to the light flavour one for the same centrality classes reported in Fig.~\ref{fig:DmesALICE} (right) and the two show a similar trend as a function of centrality, though the large statistical uncertainties that affect the D mesons measurement.

In ALICE, the measurement of the D mesons $\vtwo$ is also performed using the scalar product and the particle cumulants methods~\cite{Abelev:2014ipa}. For these methods D mesons candidates were reconstructed in bins of $\eta$  and $\pt$ and the independent particle correlators were computed for each bin, obtaining a distribution of $\vtwo$ as a function of mass of the D meson. The $\vtwo$ of the signal is then disantageled from the  one of background with a simultaneous fit of the invariant mass yield and on the $\vtwo$ vs mass distribution, considering the additivity property of the elliptic flow quantity.  
The results of the three methods and consistent within statistical uncertainties for all three mesons species~\cite{Abelev:2014ipa}.

The $\vtwo$ measurements with the Event Plane and the Scalar Product methods have been performed both with and without introducing an $\eta$-gap between the D mesons and the $\vec{Q}$ reconstruction in order to reduce non-flow effects. Results are compatible within statical uncertainties indicating that the bias due to non-flow correlations is within the statistical precision of the measurements~\cite{Abelev:2014ipa}. 

As reported in the introduction the azimuthal dependence of the nuclear modification factor might bring 
insight on the path length dependence of charm quarks energy loss. The measurement of the nuclear modification factor ($\Raa$) has been already introduced in Sect. ... . For this more differential measurement, the yields are extracted as for the $\vtwo$ in the regions in-plane and out-of-plane. The efficiencies and acceptance correction factors have been checked to be not dependent on the event plane interval and a further correction related to the event plane resolution has been applied~\cite{Abelev:2014ipa}.

The nuclear modification factor of $\Dzero$ mesons in the $30-50\%$ centrality class are shown in Fig,~\ref{fig:DmesRaavsEPALICE} for the in-plane and out-of-plane directions with respect to the event plane~\cite{Abelev:2014ipa}. The event plane has been estimated using the central rapidity tracks, in a similar region where also the D mesons are reconstructed. A large suppression is observed in both directions with respect to the event plane for $\pt > 4~\GeV/c$. A stronger suppression is observed in the out-of-plane direction. At low-$\pt$ the suppression seems to be reduced reaching unity for the in plance direction. The different suppression in the two event plane regions is equivalent to the observation of $\vtwo > 0$, as expected. 

\begin{figure}[t]
\begin{center}
\includegraphics[width=0.80\textwidth]{figures/fig_ALICE_Dmes_RaavsEP.eps}
\end{center}
\caption{Comparison of prompt $\Dzero$ meson and charged particle $\vtwo$ in three centrality classes as a function of $\AuAu$. Both results are obtained with the event plane method, for charged particles with the introduction of a pseudo rapidity gap.}
\label{fig:DmesRaavsEPALICE}
\end{figure}


% Write your contribution here. Example of reference here \cite{Andronic:2013zim}.

\bibliographystyle{ieeetr}
\bibliography{biblio}
\begin{thebibliography}{99}
%\cite{aliceDRAA}
\bibitem{aliceDRAA}
  B.~Abelev {\it et al.}  [ALICE Collaboration],
  %``Suppression of high transverse momentum D mesons in central Pb-Pb collisions at $\sqrt{s_{NN}}=2.76$ TeV,''
  JHEP {\bf 1209} (2012) 112.
  
 \bibitem{batsouli}
  S.~Batsouli, S.~Kelly, M.~Gyulassy and J.~L.~Nagle,
  %``Does the charm flow at RHIC?,''
  Phys.\ Lett.\ B {\bf 557} (2003) 26.
%  [nucl-th/0212068].
  %%CITATION = NUCL-TH/0212068;%%

\bibitem{grecoetal}
V. Greco, C. M. Ko and R. Rapp, Phys. Lett. B {\bf 595} (2004) 202.

\bibitem{andronicetal}
  A.~Andronic, P.~Braun-Munzinger, K.~Redlich and J.~Stachel,
  %``Statistical hadronization of charm in heavy ion collisions at SPS, RHIC and LHC,''
  Phys.\ Lett.\ B {\bf 571} (2003) 36.
%  [nucl-th/0303036].

\bibitem{bdmps}
R. Baier, Y.~L. Dokshitzer, A.~H. Mueller, S. Peigne and D. Schiff, 
%Radiative energy loss and pT broadening of high-energy partons in nuclei. 
Nucl. Phys. B {\bf 484} (1997) 265.

\bibitem{thoma}
M. H. Thoma and M. Gyulassy, Nucl. Phys. B {\bf 351} (1991) 491;\\
E. Braaten and M. H. Thoma, Phys. Rev. D {\bf 44} (1991) 1298; Phys. Rev. D {\bf 44} (1991) 2625.


\bibitem{PBM}
P.~Braun-Munzinger,
  %``Quarkonium production in ultra-relativistic nuclear collisions: Suppression versus enhancement,''
  J.\ Phys.\ G {\bf 34} (2007) S471.
%  [nucl-th/0701093 [NUCL-TH]].
  %%CITATION = NUCL-TH/0701093;%%
  
\bibitem{Ollitrault} J. Y. Ollitrault, Phys. Rev. D {\bf 46} (1992) 229.

\bibitem{hydro} P.~F.~Kolb, U.~W.~Heinz in Hwa, R.C. (ed.) {\it et al.}: Quark gluon plasma, 634-714 [nucl-th/0305084].

\bibitem{ref:ep}
 A.~M.~Poskanzer and S.~A.~Voloshin,
  %``Methods for analyzing anisotropic flow in relativistic nuclear collisions,''
  Phys.\ Rev.\ C {\bf 58} (1998) 1671.
 % [nucl-ex/9805001].
  %%CITATION = NUCL-EX/9805001;%%

\bibitem{SP}
C.~Adler {\it et al.}  [STAR Collaboration],
  %``Elliptic flow from two and four particle correlations in Au+Au collisions at s(NN)**(1/2) = 130-GeV,''
  Phys.\ Rev.\ C {\bf 66} (2002) 034904.
%  [nucl-ex/0206001].
  %%CITATION = NUCL-EX/0206001;%%

\bibitem{ref:BSVCumulants}
  A.~Bilandzic, R.~Snellings and S.~Voloshin,
  %``Flow analysis with cumulants: Direct calculations,''
  Phys.\ Rev.\ C {\bf 83} (2011) 044913.
  %[arXiv:1010.0233 [nucl-ex]].
  %%CITATION = ARXIV:1010.0233;%%

%\cite{Adare:2010de}
\bibitem{Adare:2010de}
  A.~Adare {\it et al.}  [PHENIX Collaboration],
  %``Heavy Quark Production in $p+p$ and Energy Loss and Flow of Heavy Quarks in Au+Au Collisions at $\sqrt{s_{NN}}=200$ GeV,''
  Phys.\ Rev.\ C {\bf 84} (2011) 044905
  [arXiv:1005.1627 [nucl-ex]].
  
  %\cite{Adare:2014rly}
\bibitem{Adare:2014rly}
  A.~Adare {\it et al.}  [PHENIX Collaboration],
  %``Heavy-quark production and elliptic flow in Au$+$Au collisions at $\sqrt{s_{_{NN}}}=62.4$ GeV,''
  arXiv:1405.3301 [nucl-ex].

%\cite{Adamczyk:2014yew}
\bibitem{Adamczyk:2014yew}
  L.~Adamczyk {\it et al.}  [STAR Collaboration],
  %``Elliptic flow of non-photonic electrons in Au+Au collisions at $\sqrt{s_{\rm NN}} = $ 200, 62.4 and 39 GeV,''
  arXiv:1405.6348 [hep-ex].

%\cite{Dubla:2014cpa}
\bibitem{Dubla:2014cpa}
  A.~Dubla [ALICE Collaboration],
  %``Measurements of heavy-flavour production and azimuthal anisotropy in Pb--Pb collisions with the ALICE detector,''
  arXiv:1408.4948 [hep-ex].

%\cite{Abelev:2013lca}
\bibitem{Abelev:2013lca}
  B.~Abelev {\it et al.}  [ALICE Collaboration],
  %``D meson elliptic flow in non-central Pb-Pb collisions at $\sqrt{s_{\rm NN}}$ = 2.76TeV,''
  Phys.\ Rev.\ Lett.\  {\bf 111} (2013) 102301
  [arXiv:1305.2707 [nucl-ex]].
  
  %\cite{Abelev:2014ipa}
\bibitem{Abelev:2014ipa}
  B.~B.~Abelev {\it et al.}  [ALICE Collaboration],
  %``Azimuthal anisotropy of D meson production in Pb-Pb collisions at $\sqrt{s_{\rm NN}} = 2.76$ TeV,''
  Phys.\ Rev.\ C {\bf 90} (2014) 034904
\end{thebibliography}

\end{document}