\documentclass[11pt]{article}

\usepackage[margin=2.3cm]{geometry}
\textheight=23cm
\textwidth=17cm

\usepackage{cite}
\usepackage[english]{babel}
\usepackage{fancyhdr}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{verbatim}
\usepackage{lineno}
\usepackage{amsfonts}
% \usepackage{makeidx}
\usepackage{color}
\usepackage{epsfig}
\usepackage{epstopdf}
\usepackage{hyperref}

\newcommand{\pT}{p_{\rm T}}
\newcommand{\Raa}{R_{\rm AA}}
\begin{document}
%\centerline{\sc {\bf HF correlations}}
\title{Heavy-flavour correlations: status and prospects}
\author{Elena Bruna, Marlene Nahrgang} 
\maketitle

\vspace{0.5cm}

Angular correlations of charged hadrons proved to be key measurements~\cite{Iaa}  at RHIC and LHC energies to study energy loss and QGP properties, providing measurements that are complementary to single-particle observables like the $R_{\rm AA}$ and $v_2$.

On the near side ($\Delta\varphi\sim0$), they provide information on the properties of the jet leaving the medium, while on the away side  ($\Delta\varphi\sim \pi$) they reflect the surviving probability of the back-to-back parton traversing the medium. Di-hadron correlation measurements typically carry geometrical and kinematical biases~\cite{geo}. Triggering on a high-$\pT$ particle tends to favour the selection of partons produced near the surface of the medium which lost only marginal energy and could still fragment in hadrons at high $\pT$ (geometrical bias). In addition, when comparing to the vacuum case (pp collisions) with the same conditions on the trigger particles, one might have different contributions of quark and gluon jets and different partonic energies in AA and pp collisions (parton and kinematical biases).
Together with single particle measurements and fully reconstructed jets, di-hadron correlations can constrain quenching models by adding information on the path length dependence of the energy loss and the relative contributions of collisional and radiative energy loss.

Such correlation measurements are of interest also for heavy-flavour quarks, which are expected to be affected by less gluon radiation and more elastic energy loss compared to light quarks because of their larger mass. 

A couple of recent works \cite{Nahrgang,Cao,Uphoff,Renk} have shown that the azimuthal distributions of heavy quark-antiquark pairs are sensitive to the different interaction mechanisms, collisional and radiative. The relative angular broadening of the $Q\bar{Q}$ pair does not only depend on the drag coefficient discussed above (see section...)
% it is certainly discussed in chapter 1 already
but strongly on the momentum which is acquired perpendicular to the initial direction of the heavy quarks, $\langle p_\perp^2\rangle$. This quantity is shown in Fig.~\ref{fig:pperp} as a function of the initial momentum $p_{||}^{\rm ini}$ of charm quarks for the purely collisional and collisional plus radiative (LPM) interactions as applied within the MC@sHQ-model (see chapter...). One sees that for all initial momenta $\langle p_\perp^2\rangle$ is larger in a purely collisional interaction mechanism. $\langle p_\perp^2\rangle$ has similar numerical values for charm and for bottom quarks.  

\begin{figure}[h]
 \includegraphics[width=0.49\textwidth]{pperpca.pdf}
 % pperpca.eps: 0x0 pixel, 300dpi, 0.00x0.00 cm, bb=171 494 429 664
 \caption{Perpendicular momentum of charm quarks acquired in a QGP medium at $T=400$~MeV as a function of the initial momentum $p_{||}^{\rm ini}$.}
 \label{fig:pperp}
\end{figure}

A larger $\langle p_\perp^2\rangle$ leads a more efficient change of the initial relative pair azimuthal angle $\Delta\phi$ during the evolution in the medium. This means that for a purely collisional interaction mechanism one expects a stronger broadening of the initial correlation at $\Delta\phi=\pi$ as seen in  Fig.~\ref{fig:correlation}. In the left plot the $\Delta\phi$ distribution of all initially correlated pairs is shown after hadronization into $D\bar{D}$-pairs. Since no cut in $p_T$ is performed these distributions are dominated by low-momentum pairs, while in the right plot a cut of $p_T>3$~GeV is made. The low-momentum pairs show the influence of the radial flow of the underlying QGP medium, which tends to align the directions of the quark and the antiquarks toward smaller opening angles. It again happens more efficiently for larger  $\langle p_\perp^2\rangle$ of the underlying interaction mechanism. This phenomena which was called ``partonic wind'' effect is thus only seen for the purely collisional interaction mechanism. A lower momentum cut reveals clearly the residual correlation around $\Delta\phi\sim\pi$. Here in the purely collisional scenario one sees a larger background of pairs that decorrelated during the evolution in the QGP than for the collisional plus radiative (LPM) scenario. 
 \begin{figure}[ht]
 \begin{minipage}[t]{0.49\textwidth}
 \centering
 \includegraphics[width=1.0\textwidth]{correlationa.pdf}%eps
 \end{minipage}
 \hfill
 \begin{minipage}[t]{0.47\textwidth}
 \centering
 \includegraphics[width=1.0\textwidth]{correlation3pta.pdf}%eps
 \end{minipage}
 \caption{Angular correlations of $D\bar{D}$ pairs in Pb+Pb collisions at LHC without (left) and with (right) a lower momentum cut.  }
 \label{fig:correlation}
 \end{figure}
 
For these calculations an initial back-to-back correlation has been assumed. Next-to-leading order processes, however, destroy this strict initial correlation already in proton-proton collisions. Unfortunately the theoretical uncertainties on these initial distributions are very large, especially for the lighter charm quarks. Here, a thorough experimental study of heavy-flavor correlations in proton-proton and proton-lead collisions is very important for validating different initial models. Also enhanced theoretical effort in these reference systems is necessary. 

Very recent activities include also the coupling of D mesons to a hadronic cascade after the QGP evolution. This includes the soft particles emitted at the end of the fluid dynamical evolution as well as the full information on the products of the heavy quark fragmentation. In this way a better comparison to the available experimental data of correlations of heavy-flavor particles with hadrons will be possible. These correlations, however, might be dominated by medium effects in the soft sector only, like flow of hadrons, and the fragmentation process of the heavy quarks, and angular decorrelation effects might be hidden. It is therefore necessary to directly couple heavy-flavor particles with each other, like heavy-flavor electrons and D mesons.

\vspace{1cm}

The experimental challenges in measurements like D-D correlations in heavy-ion collisions come from the reconstruction of both the hadronic decays of the back-to-back D mesons,  which require large statistics to cope with low branching ratios and low S/B in A-A collisions. As an alternative, correlations of D mesons with charged hadrons (D-h), correlations of electrons/muons from decays of heavy-flavour particles with charged hadrons (e-h) and correlations of D-e, e$^+$-e$^-$,  $\mu^+$-$\mu^-$ and e-$\mu$ (where electrons and muons come from heavy-flavour decays) can be measured in different collision systems (pp, p-Pb collisions) to study the modification of the heavy-quark fragmentation, the structure of the underlying event in the presence of heavy-flavours and therefore the properties of the medium created in central Pb-Pb collisions. Such observables might however hide decorrelation effects as mentioned above. In addition, in the case of correlations triggered by electrons or muons from heavy-flavour decays, the interpretation of the results is complicated by the fact that the lepton does not carry all the momentum of the parent meson. This makes the understanding of pp collisions as baseline a very crucial aspect of these analyses.

Heavy-flavour azimuthal correlations in pp collisions also allow for studies of heavy-quark fragmentation and jet structure at different collision energies, which help constraining Monte Carlo models  and the different production processes for heavy flavours (i.e. flavour creation, flavour excitation and gluon splitting) modeled in Monte Carlo event generators. 

Studies of heavy-flavour correlations in hadronic collisions were carried on at Tevatron with  D-D correlations~\cite{cdf} in p$\bar{\rm  p}$ collisions at $\sqrt s=$1.96 TeV and at RHIC with e-$\mu$ correlations~\cite{phenix} (where electrons and muons come from heavy-flavour decays) in pp collisions at $\sqrt s=$200 GeV. Results on heavy-flavour correlation measurements were also reported by the LHC experiments~\cite{LHCbcorrpp, ATLAScorrpp, CMSbbar, ALICEDhpp} with pp collisions at  $\sqrt s=$7 TeV, as shown in Fig.~\ref{HFcorrpp2}. The results at different collision energies are sensitive to different heavy-flavour production mechanisms, providing constraints for leading and higher-order contributions in Monte Carlo event generators.

%\begin{figure}[!htp]
 %\begin{center}
 % \includegraphics[angle=0,width=0.49\textwidth]{DzeroDstar_xsec_sum.eps}
  %\includegraphics[angle=0,width=0.48\textwidth]{phenixemu.pdf}
   %\end{center}
 %\caption{Left: D-D angular correlation measurements from CDF compared to Pythia and its different production processes~\cite{cdf}. Right: heavy-flavour e-$\mu$ correlations from PHENIX, compared to different Monte Carlo event generators~\cite{phenix}.}
 %\label{HFcorrpp} 
%\end{figure}

\begin{figure}[!htp]
 \begin{center}
  \includegraphics[angle=0,width=0.49\textwidth]{CMSbbar_pp.pdf}
   \includegraphics[angle=0,width=0.49\textwidth]{DhALICEMC_58_05.pdf}
 \end{center}
 \caption{Left: angular correlations of B-$\bar{\rm B}$ mesons measured by CMS in different ranges of the leading jet $\pT$ and compared to PYTHIA~\cite{CMSbbar}. Right: D-h angular correlations measured by ALICE, compared to different PYTHIA tunes~\cite{ALICEDhpp}.}
 \label{HFcorrpp2} 
\end{figure}

Heavy-flavour azimuthal correlations are also being studied in d+Au collisions at RHIC and p-Pb collisions at the LHC to understand how the presence of the nucleus might affect the properties of heavy-flavour jets. Furthermore, it is interesting to investigate whether or not the long range correlations along $\Delta \eta$ observed for light flavors in high-multiplicity events at the LHC~\cite{lfpPb} are also present in the heavy-flavour sector.

D-h correlations measured by ALICE at intermediate/high $\pT$ for trigger and associate particles are compatible in pp and p-Pb collisions, indicating no strong effects in the jet properties in the measured kinematic ranges ~\cite{ALICEDhpp}.  On the other side, at lower $\pT$ the results from RHIC on e-$\mu$ azimuthal correlations in d+Au collisions seem to suggest that, in the measured kinematic ranges, the yields of heavy-flavour e-$\mu$ pairs are modified in the cold nuclear medium compared to binary-scaled pp collisions (Fig.~\ref{HFcorrpPb}, left). Effects from the cold nuclear matter in the heavy-flavour sector were also observed by ALICE with measurements of e-h correlations in high-multiplicity p-Pb collisions, which show indications for long-range correlations in $\Delta \eta$ at low $\pT$, similar to what was observed for light flavours~\cite{lfpPb}.

\begin{figure}[!htp]
 \begin{center}
  \includegraphics[angle=0,width=0.46\textwidth]{PHENIXemu.pdf}
  \includegraphics[angle=0,width=0.45\textwidth]{ehALICEpPb.pdf}
     \end{center}
 \caption{Heavy-flavour e-$\mu$ pair yield in pp and d+Au collisions from PHENIX~\cite{phenixdAu}. $\Delta \eta$-$\Delta \varphi$ correlations of electrons from heavy-flavour decays and charged hadrons from ALICE in high-multiplicity p-Pb collisions~\cite{ehALICEpPb}.}
 \label{HFcorrpPb} 
\end{figure}

Measurements of heavy-flavour correlations in A-A collisions were carried on at both RHIC and LHC with e-h correlations~\cite{phenixehAu, aliceehPb} (where electrons come from heavy-flavour decays), but the current statistics is preventing from drawing quantitative conclusions. Such measurements are expected to provide more information about how the charm and bottom quarks propagate through the hot and dense medium and how this affects and modifies the correlation structures. 
In particular, PHENIX reported a decrease of the ratio of yields in the away-side region ($2.51<\varphi<\pi$) to those in the shoulder region  ($1.25<\varphi<2.51$)  from p+p to Au+Au collisions (Fig.~\ref{HFcorrPb}, left). 

Further measurements of heavy-flavour triggered azimuthal correlations  will be promising in future data takings at both RHIC (with the new silicon tracker detectors) and LHC (with the machine and detector upgrades). As reported in Fig.~\ref{HFcorrPb}, right, the relative uncertainty on the away-side yield in D-h correlations in central Pb-Pb collisions with the ALICE and LHC upgrades will be $\sim 15 \%$ for low $\pT$ D mesons and only  $1-2 \%$ for intermediate/high D meson $\pT$. 

Therefore, different heavy-flavour correlation observables sensitive to the partonic content of the jet will be needed to compare the energy loss mechanism for heavy quarks, light quarks and gluons and to determine the extent of modification of away-side heavy-jet fragmentation for heavy flavours. 
This can be achieved with correlations of trigger and associate particles both coming from heavy quarks (D-e, e$^+$-e$^-$,  $\mu^+$-$\mu^-$ and e-$\mu$) or by tagging jets containing charm and beauty particles. In particular, as discussed above, correlations of heavy-flavour particles with each other will reduce medium effects allowing for a more quantitative study of the different energy loss contributions in the heavy-flavour sector. With the ALICE and LHC upgrades,  we expect to reach a relative uncertainty on the away-side yield of D-e correlations (where the electron comes from decays of heavy-flavour particles) of less than 10 \% for electron $\pT>1$ GeV/$c$, opening the way to a new class of observables aiming to a quantitative understanding of the heavy-flavour production and energy loss.
 

\begin{figure}[!htp]
 \begin{center}
  \includegraphics[angle=0,width=0.46\textwidth]{phenixehAu.pdf}
  \includegraphics[angle=0,width=0.43\textwidth]{DhRun3.pdf}
     \end{center}
 \caption{Left: ratio of yields in the away-side region ($2.51<\varphi<\pi$) to those in the shoulder region  ($1.25<\varphi<2.51$)  in pp and Au+Au collisions from PHENIX~\cite{phenixehAu}. Right: relative uncertainty on the away-side yield in D-h correlations as a function of the charged hadron $\pT$ for three ranges of D-meson transverse momenta, with the ALICE and LHC upgrades~\cite{aliceDhPb}.}
 \label{HFcorrPb} 
\end{figure}


\begin{thebibliography}{9}
\bibitem{Iaa} I. Arsene et al. (BRAHMS Collaboration), Nucl. Phys. A
757, 1 (2005); K. Adcox et al. (PHENIX Collaboration),
Nucl. Phys. A 757, 184 (2005); B. B. Back et al., Nucl.
Phys. A 757, 28 (2005); J. Adams et al. (STAR
Collaboration), Nucl. Phys. A 757, 102 (2005); S. Chatrchyan et al. (CMS Collaboration), J. High Energy
Phys. 07 (2011) 076; K. Aamodt et al. (ALICE Collaboration), Phys. Rev. Lett. 108, 092301 (2012).
\bibitem{Nahrgang} M. Nahrgang, J. Aichelin, P. B. Gossiaux, and K. Werner, ``Azimuthal correlations of heavy quarks in Pb+Pb collisions at LHC ($\sqrt s$ = 2.76 TeV)'', 2013.
\bibitem{Cao} S. Cao, G.-Y. Qin, and S. A. Bass, ``Dynamical Evolution, Hadronization and Angular De-correlation of Heavy Flavor in a Hot and Dense QCD Medium'', 2014.
\bibitem{Uphoff} J. Uphoff, F. Senzel, Z. Xu, and C. Greiner, ``Momentum imbalance of D mesons in ultra-relativistic heavy- ion collisions at LHC'', Phys.Rev., vol. C89, p. 064906, 2014.
\bibitem{Renk} T. Renk, ``Charm energy loss and D-D correlations from a shower picture'', Phys. Rev. , vol. C89, p. 054906, 2014.
\bibitem{geo} T. Renk, K. Eskola, Phys. Rev. C 75 (2007) 054910.
\bibitem{cdf} B. Reisert (CDF Collaboration) Proceedings Beauty Conference 2006, Nucl.Phys B (2007) 243.
\bibitem{phenix} A. Adare et al. (PHENIX Collaboration), Phys. Rev. C. 89 (2014) 034915.
\bibitem{LHCbcorrpp} LHCb Coll., J. High Energy Phys. 06 (2012) 141.
\bibitem{ATLAScorrpp} ATLAS Coll, Eur. Phys. J. C 71 (2011) 1846.
\bibitem{CMSbbar} CMS Collaboration,  J. High Energy Phys. 1103 (2011) 136.
\bibitem{ALICEDhpp} S. Bjelogrlic, ALICE Collaboration, to be published in the Proceedings for the Quark Matter 2014 Conference.
\bibitem{lfpPb} CMS Collaboration, Phys. Lett. B 718 (2013) 795. ATLAS Collaboration, Phys. Rev. Lett 110 (2013) 182302. ALICE Collaboration, Phys. Lett. B 719 (2013) 29.
\bibitem{phenixdAu} PHENIX Coll., arXiv:1311.1427v1.
\bibitem{ehALICEpPb} E. Pereira, ALICE Collaboration, Proceedings for the Hard Probes Conference 2013,  arXiv:1404.3983.
\bibitem{phenixehAu} PHENIX Collaboration, Phys. Rev. C 83 (2011) 044912.
\bibitem{aliceehPb} D. Thomas, ALICE Collaboration, J. Phys. Conf. Ser. 509 (2014) 012079.
\bibitem{aliceDhPb} A. Rossi, ALICE Collaboration, poster presented at the Hard Probes 2013 Conference.

\end{thebibliography}
\end{document}