% !TEX root = ../SaporeGravis.tex

\label{sec:interaction}

First principle non-perturbative results for the transport coefficients can be obtained, although within a limited kinematic domain and with sizeable systematic uncertainties, from lattice QCD (l-QCD) calculations. The theoretical setup employed to extract the momentum diffusion coefficient $\kappa$~\cite{CasalderreySolana:2006rq} is described in the following. This approach is valid in the non-relativistic limit (for this calculation heavy quarks on the lattice are taken as static colour sources) where the transport of heavy quarks reduces to the Langevin equation
\begin{equation}
\frac{\dd p^i}{\dd t}=-\eta_D p^i+ \xi^i(t),
\end{equation}
where $\eta_D$ and $\kappa$ are the friction and diffusion coefficients and where $\boldsymbol \xi$ are stochastic forces
auto-correlated according to $\langle\xi^i(t)\xi^j(t')\rangle\!=\!\delta^{ij}\delta(t-t')\kappa$.
Hence, in the $p\!\to\!0$ limit, $\kappa$ is given by the Fourier transform of the following force-force correlator 
\begin{equation}
\label{eq:ber1}
\kappa=\frac{1}{3}\int_{-\infty}^{+\infty}\!\!\!\dd t\langle\xi^i(t)\xi^i(0)\rangle_{\rm HQ}
\approx\frac{1}{3}\int_{-\infty}^{+\infty}\!\!\!\dd t{\langle F^i(t)F^i(0)\rangle_{\rm HQ}}\equiv\frac{1}{3}\int_{-\infty}^{+\infty}\!\!\!\dd t D^>(t)=\frac{1}{3}D^>(\omega\!=\!0),
\end{equation}
where the expectation value is taken over an ensemble of states containing, besides thermal light partons, a static ($m_Q\!=\!\infty$) heavy quark.
In a thermal bath, correlators are related by the Kubo-Martin-Schwinger condition, entailing for the spectral function the relation ${\sigma(\omega)}\!\equiv\!D^>(\omega)\!-\!D^<(\omega)={(1-e^{-\beta\omega})D^>(\omega)}$, so that
\begin{equation}
{\kappa}\equiv\lim_{\omega\to 0}\frac{{D^>(\omega)}}{3}=\lim_{\omega\to 0}\frac{1}{3}{\frac{\sigma(\omega)}{1-e^{-\beta\omega}}}\underset{\omega\to 0}{\sim}\frac{1}{3}\frac{T}{{\omega}}{\sigma(\omega)}.\label{eq:kappa}
\end{equation}
In the static limit magnetic effects vanish and the force felt by the heavy quark can only be due to the chromo-electric field
\begin{equation}
{\vec F}(t)=g\int \!\dd{\vec x} \,Q^\dagger(t,{\vec x})t^aQ(t,{\vec x})\,{\vec E}^a(t,{\vec x}).
\end{equation}
\eq{eq:kappa} shows how $\kappa$ depends on the low-frequency behaviour of the spectral density $\sigma(\omega)$ of the electric-field correlator in the presence of a heavy quark
 in the original thermal average in \eq{eq:ber1}. The latter can be evaluated on the lattice for imaginary times $t\!=\!-i\tau$~\cite{CaronHuot:2009uh}: 
\begin{equation}
{D_E(\tau)}=-\frac{\langle{\rm Re\,Tr}[U(\beta,\tau)gE^i(\tau,{\bf 0})U(\tau,0)gE^i(0,{\bf 0})]\rangle}{\langle{\rm Re\,Tr}[U(\beta,0)]\rangle}.
\end{equation}
In the above equation the expectation value is now taken over a thermal ensemble of states of gluons and light quarks, with the Wilson lines $U(\tau_1,\tau_2)$ reflecting the presence of a static heavy quark. As it is always the case when attempting to get information on real-time quantities from l-QCD simulations, the major difficulty consists in reconstructing the spectral density $\sigma(\omega)$ from the inversion of
\begin{equation}
{D_E(\tau)}=\int_0^{+\infty}\frac{\dd\omega}{2\pi}\frac{\cosh(\tau-\beta/2)}{\sinh(\beta\omega/2)}{\sigma(\omega)},
\end{equation}
where one knows the correlator $D_E(\tau)$ for a limited set of times $\tau_i\!\in\!(0,\beta)$. Lattice results for the heavy quark diffusion coefficient are currently available for the case of a pure $SU(3)$ gluon plasma~\cite{Banerjee:2012zj,Francis:2011gc}. In transport calculations, depending on the temperature, one relied on the values $\kappa/T^3\!\equiv\!\overline{\kappa}\!\approx\!2.5-4$ obtained in \ci{Francis:2011gc}, which the authors are currently trying to extrapolate to the continuum (\ie zero lattice-spacing) limit.

Being derived in the static $m_Q\!=\!\infty$ limit and lacking any information on their possible momentum dependence, the above results for $\kappa$ have to be taken with some grain of salt when facing the present experimental data (mostly referring to charm at not so small \pt); however they could represent a really solid theoretical benchmark when beauty measurements, for which $M\!\gg\!T$, at low $\pt$ will become available. Bearing in mind the above caveats and neglecting any possible momentum dependence of $\kappa$, the above l-QCD transport coefficients (the friction coefficient $\eta_D=\kappa/2ET$ being fixed by the Einstein relation) were implemented into POWLANG code~\cite{Alberico:2013bza} in order to provide predictions for D mesons, heavy-flavour electrons and \jpsi from B decays. One can estimate what the above results would entail for the average heavy-quark energy-loss: 
\begin{equation}
\langle{\dd E}/{\dd x}\rangle=\langle{\dd p}/{\dd t}\rangle=-\eta_D\,p=-(\kappa/2ET)\,p=-(\overline{\kappa}T^2/2)\,v.
\end{equation}   
with $v$, the heavy-quark velocity. Numerically, this would imply a stopping power $\langle -\dd E/\dd x\rangle\approx\overline{\kappa}\cdot 0.4\cdot v$\GeV/fm at $T=400$\MeV and  $\langle -\dd E/\dd x\rangle\approx\overline{\kappa}\cdot 0.1\cdot v$\GeV/fm at $T=200$\MeV.
