% !TEX root = ../SaporeGravis.tex

Heavy-flavour hadrons
are effective probes of the conditions of the high-energy-density QGP medium formed 
in ultra-relativistic nucleus--nucleus collisions. 

Heavy quarks,
% with masses $m_{c}\approx 1.3$\GeVcc and  
%$m_{b}\approx 4.5$\GeVcc,
are produced in primary hard QCD scattering processes 
in the early stage of the nucleus--nucleus collision
and the time scale of their production (or coherence time) 
is, generally, shorter than the formation time of the QGP,
$\tau_0\sim 0.1$--1~fm$/c$.
More in detail, the coherence time of the heavy quark-antiquark pair
is of the order of the inverse of the virtuality $Q$ of the hard scattering, 
$\Delta\tau\sim 1/Q$. 
The minimum virtuality $2\,m_{c,b}$ 
in the production of a \ccbar or \bbbar pair implies a space-time scale 
of $\sim 1/3\GeV^{-1}\sim 0.07~{\rm fm}$ and $\sim 1/10\GeV^{-1}\sim 0.02~{\rm fm}$ for 
charm and for beauty, respectively.
One exception to this picture are configurations where the quark and antiquark are
produced with a small relative opening angle in the so-called gluon splitting processes $g\to \qqbar$. 
In this case, the coherence time is increased by a boost factor $E_g/(2\,m_{c,b})\sim E_{c,b}/m_{c,b}$ and becomes 
$\Delta t \sim E_{c,b}/(2\,m_{c,b}^2)$.  
This results, for example, in a coherence time of about 1~fm/$c$ (0.1~fm/$c$) for charm (beauty) quarks with energy of 15\GeV, and of about 1~fm/$c$
for beauty quark jets with energy of about 150\GeV.
The fraction of heavy quarks produced in gluon splitting processes has been estimated using 
perturbative calculations and Monte Carlo event generators, resulting in moderate 
values of the order of 10--20\% for charm~\cite{Mueller:1985zz,Mangano:1992qq} and large values of the order of 50\% for beauty~\cite{Banfi:2007gu}.
Given that the gluon splitting fraction is moderate for charm and the coherence time is small for beauty from gluon splitting
when $\pt$ is smaller than about 50\GeVc,
it is reasonable to conclude that heavy-flavour hadrons in this \pt range probe the heavy quark in-medium interactions.

During their propagation through the medium, heavy quarks interact with its constituents
 and lose a part of their energy, thus being sensitive to the medium properties.
 Various approaches have been developed to describe the interaction of the heavy quarks with the surrounding plasma.
In a perturbative treatment, QCD energy loss is expected to occur
via both inelastic (radiative energy loss, via medium-induced gluon radiation)~\cite{Gyulassy:1990ye,Baier:1996sk} and elastic (collisional energy loss)~\cite{Thoma:1990fm,Braaten:1991jj,Braaten:1991we}
processes. However, this distinction is no longer meaningful in strongly-coupled approaches relying for instance on the AdS/CFT conjecture~\cite{Herzog:2006gh,Gubser:2006bz}. 
In QCD, quarks have a smaller colour coupling factor with respect to gluons, 
so that the energy loss for quarks is expected to be smaller than for gluons. 
In addition, the ``dead-cone effect'' should reduce small-angle gluon radiation 
for heavy quarks with moderate 
energy-over-mass values, thus further attenuating 
the effect of the medium. This idea was first introduced in~\cite{Dokshitzer:2001zm}. 
Further theoretical studies confirmed the reduction of the total induced gluon radiation~\cite{Armesto:2003jh,Djordjevic:2003zk,Zhang:2003wk,Wicks:2007am},
although they did not support the expectation of a ``dead cone''.
Other mechanisms such as in-medium hadron formation and dissociation~\cite{Adil:2006ra,Sharma:2009hn}, would determine a
stronger suppression effect on heavy-flavour hadrons than light-flavour hadrons, because of their smaller formation 
times.

In contrast to light quarks and gluons, which can be produced or annihilated 
during the entire evolution of the medium, heavy quarks are produced 
in initial hard scattering processes 
and their annihilation rate 
is small~\cite{BraunMunzinger:2007tn}.
Secondary ``thermal" charm production from processes like $gg\to \ccbar$ in the QGP 
is expected to be negligible, unless initial QGP temperatures much larger than 
that accessible at RHIC and LHC are assumed~\cite{Zhang:2007dm}.
Therefore, heavy quarks preserve their flavour and mass identity while traversing the medium and can be tagged throughout all momentum ranges, 
from low to high \pt, through the measurement of heavy-flavour hadrons in the final state of the collision. 

%The nuclear modification factor \raa
%is well-established as a sensitive observable 
%for the study of the interaction of hard partons 
%with the medium. This factor is defined as 
%the ratio of particle production measured in nucleus--nucleus
%to the one measured in \pp and scaled by the 
%average number \av{\Ncoll} of binary 
%nucleon--nucleon collisions per inelastic nucleus--nucleus collision. 
%Using the nuclear overlap function, which is defined as the convolution 
%of the nuclear density profiles of the colliding ions in the Glauber 
%model~\cite{Glauber,Miller:2007ri}, the nuclear modification factor 
%of the transverse momentum distribution can be expressed as: 
%\begin{equation}
%\label{eq:Raa}
%\raa(\pt)=
%\frac{1}{\av{\taa}} \cdot 
%\frac{\dd N_{\rm AA}/\dd\pt}{\dd\sigma_{\rm pp}/\dd\pt}\,,
%\end{equation}
%where the \AAcoll spectrum corresponds to a given collision-centrality class and 
%\av{\taa} is the average nuclear overlap function for that
%centrality class and is proportional to \av{\Ncoll}.
%At large \pt,  \raa is
%expected to be mostly sensitive to the average energy loss of
%heavy quarks in the hot medium.  



The nuclear modification factor
\begin{equation}
\label{eq:Raa}
\raa(\pt)=
\frac{1}{\av{\taa}} \cdot 
\frac{\dd N_{\rm AA}/\dd\pt}{\dd\sigma_{\rm pp}/\dd\pt}
\end{equation}
---a detailed definition is given in \sect{Cold nuclear matter effects}---
is well-established as a sensitive observable 
for the study of the interaction of hard partons 
with the medium. 
At large \pt,  \raa is
expected to be mostly sensitive to the average energy loss of
heavy quarks in the hot medium.  
The study of more differential observables can provide important insights
into the relevance of the various interaction mechanisms and the properties of the medium. In particular, the dependence of 
the partonic energy loss on the in-medium path length is expected to be different for each mechanism (linear for collisional processes~\cite{Thoma:1990fm,Braaten:1991jj,Braaten:1991we} and close
to quadratic for radiative processes in a plasma~\cite{Baier:1996sk}).
Moreover, it is still unclear if low-momentum heavy quarks can reach thermal equilibrium with the medium constituents and participate in the collective expansion of the system~\cite{Batsouli:2002qf,Greco:2003vf}.
%In addition, it is an open question whether low-momentum heavy quarks participate, through their interactions with the medium, 
%in the collective expansion of the system and whether they can reach thermal equilibrium with the medium 
%constituents~\cite{Batsouli:2002qf,Greco:2003vf}. 
It was also suggested that low-momentum heavy quarks could hadronise
not only via fragmentation in the vacuum, but also via the mechanism of recombination with other quarks from the medium~\cite{Greco:2003vf,Andronic:2003zv}.

These questions can be addressed both with the study of the \raa at low and intermediate \pt (smaller than about five times
the heavy-quark mass) and with
azimuthal anisotropy measurements of heavy-flavour hadron production with respect to the reaction plane,
defined by the beam axis and the impact parameter of the collision.
For non-central collisions, the two nuclei overlap in an approximately lenticular region, the short axis of which lies in the reaction plane.
Hard partons are produced at an early stage, when the geometrical anisotropy is not yet reduced by the system expansion. Therefore, partons emitted
in the direction of the reaction plane (in-plane) have, on average, a shorter in-medium path length than partons emitted orthogonally (out-of-plane),
leading {\it a priori} to a stronger high-\pt suppression in the latter case.
In the low-momentum region, the in-medium interactions can also modify the parton emission directions, thus translating the initial spatial anisotropy into a momentum anisotropy of the final-state particles. 
Both effects cause a momentum anisotropy that can be characterised with the coefficients 
$v_n$ and the symmetry planes $\Psi_n$ of the Fourier expansion of the \pt-dependent particle distribution $\dd^2N/\dd\pt\dd\phi$ 
in azimuthal angle $\phi$.
The elliptic flow is the second Fourier coefficient \vtwo.
%Since light quarks and gluons can be produced or annihilated during the entire partonic phase of the medium, the two mechanisms are usually modelled separately. 


The final ambitious goal of the heavy-flavour experimental programmes in nucleus--nucleus collisions is the characterisation of the properties of the produced QCD matter, in particular getting access to the transport coefficients of the QGP.  Theoretical calculations % based (mostly) on numerical implementations of the relativistic Langevin equation and 
encoding the interaction of the heavy quarks with the plasma into a few transport coefficients (see \eg~\cite{Rapp:2009my}) provide the tools to achieve this goal: through a comparison of the experimental data with the numerical outcomes obtained with different choices of the transport coefficients it should be possible, in principle, to put tight constraints on the values of the latter. 
This would be the analogous of the way of extracting information on the QGP viscosity through the comparison of soft-particle spectra with predictions from fluid dynamic models.
An even more intriguing challenge would be to derive the heavy-flavour transport coefficients through a first principle QCD calculation and confront them with experimental data, via model implementations that describe the medium evolution.
%This would require focusing on observable at the same time experimentally accessible (now or in the near future) and for which theoretical calculations are based on solid grounds.   
This chapter reviews the present status of this quest, from the experimental and theoretical viewpoints.

The chapter is organised as follows. The first part of the chapter presents a brief overview of the available data of heavy-flavour production 
in nucleus--nucleus collisions at the RHIC and LHC colliders: in particular, \sect{sec:OHFRAA} describes the measurements of the nuclear modification factor 
\raa, while \sect{sec:OHFv2} focuses on the azimuthal anisotropy. The published RHIC and LHC data are summarised in 
\tabs~\ref{tab:ohf_expSummary_RHIC} and~\ref{tab:ohf_expSummary_LHC}, respectively.
The second part of the chapter includes a review of the theoretical models for heavy-quark interactions and energy loss in the medium, 
with a detailed description of the model ingredients in terms of the quark--medium interaction (\sect{sec:OHFmodelsInt})
and of the medium modelling (\sect{sec:OHFmodelsMed}). A comparative overview of the models 
and comprehensive comparison with data from RHIC and LHC are presented in \sect{sec:modelcomparison}.
Finally, the theoretical and experimental prospects for the study of heavy-flavour correlations are discussed in \sect{sec:OHFcorr}.
