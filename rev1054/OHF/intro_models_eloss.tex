% !TEX root = ../SaporeGravis.tex

The approaches describing the heavy-quark--medium interactions
aim at determining the probability 
${\cal P}_{Q\rightarrow H}(p_Q^{\rm in},p_{H}^{\rm fin})$ that a given heavy quark produced with a 
4-momentum $p_Q^{\rm in}$ escapes the medium as a heavy-flavour hadron of 4-momentum 
$p_{H}^{\rm fin}$. 

All the approaches include a description of the interactions that occur between the heavy quarks 
and the partonic constituents of the QGP.
For ultra-relativistic heavy quarks ($p_Q\gg m_Q$, say $>10\,m_Q$), the dominant source of energy loss is commonly considered to be the
radiation of gluons resulting from the scattering of the heavy quark on the medium constituents. These are $2\to 3$ processes $q(g)Q\to q(g)Qg$,
where $q(g)$ is a medium light quark (or gluon).
As this mechanism proceeds through long formation times, several scatterings contribute
coherently and quantities like the total energy loss $\Delta E(L)=p_Q^{\rm in}-p_Q^{\rm fin}$ can only be evaluated at the end of the in-medium path length $L$. 
This feature is shared by all schemes that have been developed to evaluate radiative energy loss of
ultra-relativistic partons~\cite{Armesto:2003jh,Zhang:2003wk,Djordjevic:2003zk}.
%, including the one of Djordjevic \etal discussed in section \ref{sec:pQCDEloss}.
For merely relativistic heavy quarks (say $p_Q<10\,m_Q$), elastic (collisional) processes 
are believed to have an important role as well. These are $2\to 2$ process $q(g)Q\to q(g)Q$.
The in-medium interactions are gauged by the following, closely related, variables: the \textit{mean free path} $\lambda=1/(\sigma\rho)$ is related to the medium density $\rho$ 
and to the cross section $\sigma$ of the parton-medium interaction (for $2\to 2$ or $2\to 3$ processes);
the \textit{Debye mass} $m_D$ is the inverse 
of the screening length of the colour electric fields in the medium and it is proportional to the temperature $T$ of the medium; the \textit{transport coefficients} 
encode the momentum transfers with the medium (more details are given in the next paragraph).

In the relativistic regime, the gluon formation time for radiative processes becomes small enough that the energy loss probability
${\cal P}(\Delta E)$ can be evaluated as the result of some local transport equation
-- like the Boltzmann equation, relying on local cross sections -- evolving from initial to final time. 
This simplification can be applied also to collisional processes. 
When the average momentum transfer is small with respect to the quark mass\footnote{This is the so called
``grazing expansion''~\cite{balescu1975equil}, well justified for non-relativistic heavy quarks.},
the Boltzmann equation can be reduced to the Fokker-Planck equation, which is often further simplified 
to the Langevin stochastic equation (see~\cite{Rapp:2009my} for 
a recent review).  These linear partial-differential equations describe the time-evolution of the momentum distribution $f_Q$ of heavy quarks. 
The medium properties are encoded in three transport coefficients: a) the \textit{drift coefficient}
-- also called \textit{friction} or \textit{drag coefficient} -- which represents the fractional momentum loss per unit of time
in the absence of fluctuations and admits various equivalent symbolic representations ($\eta_D,\,A_Q,\,\ldots$)
and b) the \textit{longitudinal} and \textit{transverse momentum diffusion coefficients} $B_{\rm L}$
and $B_{\rm T}$ (or $B_1$ and $B_0$, $\kappa_{\rm L}$ and $\kappa_{\rm T}$,\ldots, depending on the authors), 
which represent the increase of the variance of $f_Q$ per unit of time. For small momentum, the drift and diffusion coefficients are linked through the Einstein relation 
$B=m_Q\,\eta_D\,T$
and 
also uniquely related to the spatial diffusion coefficient $D_s$, which describes the spread of the distribution in 
physical space with time. Although the Fokker-Planck approach has some drawbacks\footnote{The Einstein relation is not necessarily satisfied for all momenta $p_Q$ 
in an independent calculation of $B$ and $\eta_D$
and hence has to be enforced.}, it can also be deduced from more general 
considerations~\cite{risken1989fp}, so that it may still be considered as a valid approach for describing heavy-quark transport even when the Boltzmann equation does not apply, as for instance in the strong-coupling limit. 

Some of the approaches consider only partonic interactions and define the 
${\cal P}_{Q\rightarrow H}$ probability as a convolution 
of ${\cal P}_{Q\rightarrow Q'}(p_Q^{\rm in},p_Q^{\rm fin})$ -- the probability for the heavy quark
to lose $p_Q^{\rm in}-p_Q^{\rm fin}$ in the medium -- with the unmodified fragmentation function.
A number of approaches also include, for low-intermediate momentum heavy quarks, a contribution of 
hadronisation via recombination (also indicated as coalescence). Finally, some approaches 
consider late-stage interactions of the heavy-flavour hadrons with the partonic or hadronic medium.


In this section, we summarise the various approaches for the calculation of the heavy-quark interactions within the medium. 
\begin{itemize}
\item \sects~\ref{sec:pQCDEloss} and \ref{sec:mcatshq} are devoted to pQCD and pQCD-inspired calculations
of radiative and collisional energy loss, as developed
by Gossiaux \etal (MC$@_s$HQ), Beraudo \etal (POWLANG), Djordjevic \etal, Vitev \etal and Uphoff \etal (BAMPS);
examples of the relative energy loss ($\Delta E/E$) and the momentum loss per unit length ($\dd P/\dd t$) for \cquark and \bquark quarks are shown.
\item \sect{sec:sharmaVitev_dissociation} focuses on the calculation by Vitev \etal of in-medium formation and dissociation of heavy-flavour hadrons;
this proposed mechanism is expected to effectively induce an additional momentum loss with respect to radiative and collisional heavy-quark in-medium interactions alone.
\item \sects~\ref{sec:Tmatrix} and~\ref{sec:lQCDeloss} describe the calculation of transport coefficients
through $T$-matrix approach supplemented with a non-perturbative potential extracted from lattice QCD (Rapp \etal, TAMU) or 
through direct \textit{ab initio} lattice-QCD calculations (Beraudo \etal, POWLANG); the transport coefficients that are discussed are 
the spatial diffusion coefficient (or friction coefficient), for which examples are shown, and the momentum diffusion coefficient. 
% {\bf Can we put a short definition of these, in words?}  
\item \sect{sec:AdSCFTmodel} presents the AdS/CFT
approach for the calculation of the transport coefficients, developed by Horowitz \etal.
\end{itemize}

The implementation of these various approaches in full models that allow to compute the final heavy-flavour hadron 
kinematic distributions will be described in Section~\ref{sec:OHFmodelsMed}, with particular emphasis on 
the modelling of the QGP and its evolution.
