%!TEX root = ../SaporeGravis.tex

In the Monte Carlo at Heavy Quark approach~\cite{Gossiaux:2008jv,Gossiaux:2009mk,Nahrgang:2013saa} (MC$@_s$HQ), 
heavy quarks lose and gain energy by interacting with light partons from the medium (assumed to be in thermal equilibrium) according to rates which include both collisional and radiative types of processes.

For the collisional energy loss, the elements of the transition matrix are calculated from the pQCD Born
approximation~\cite{Combridge:1978kx,Svetitsky:1987gq}, supplemented by a running coupling constant $\alpha_s(Q^2)$ evaluated 
according to 1-loop renormalisation for $|Q^2|\gg \Lambda_{QCD}^2$ and chosen to saturate at small $Q^2$ in order to satisfy the universality relation~\cite{Dokshitzer:1995qm,Peshier:2008bg}:
\begin{align}
\label{alpha_s_continued}
 \alpha_s(Q^2)= \frac{4\pi}{\beta_0} \begin{cases}
  L_-^{-1}  & Q^2 < 0\\
  \frac12 - \pi^{-1} {\rm arctan}( L_+/\pi ) &  Q^2 > 0
\end{cases}
\end{align}
with $\beta_0 = 11-\frac23\, n_f$  and $L_\pm = \ln(\pm Q^2/\Lambda^2)$ with $\Lambda=200$\MeV and $n_f = 3$. 
The $t$ channel requires infrared regularisation which describes the physics of the screening at long 
distances~\cite{Weldon:1982aq}. For this purpose one adopts, in a first stage, a similar HTL polarisation as in the usual weak-coupling calculation of the energy loss~\cite{Braaten:1991jj,Braaten:1991we} for the small momentum-transfers, including the running $\alpha_s$ (\eq{alpha_s_continued}), while a semi-hard propagator is adopted for the large momentum-transfers.
Then the model is simplified by resorting to an effective scalar propagator $\frac{1}{t-\kappa\tilde{m}_D^2(T)}$
for the exchanged thermal gluon, with a self-consistent Debye mass evaluated as $\tilde{m}_D^2(T)= \frac{N_c}{3} \left(1+\frac{n_f}{6}\right)4\pi\,\alpha_s(-\tilde{m}_D^2(T))\,T^2$~\cite{Peshier:2006hi} and 
an optimal value of $\kappa$ fixed to reproduce the value of the energy loss obtained at the first stage.
The resulting model leads to a stronger coupling than previous calculations 
performed with fixed-order $\alpha_s=0.3$. It is also found to be compatible with the calculation of \ci{Peigne:2008nd} -- where the running of $\alpha_s$ is rigorously implemented -- in the region where the latter is applicable.

A similar model is implemented in BAMPS~\cite{Uphoff:2010sh,Uphoff:2011ad,Uphoff:2013rka,Uphoff:2014hza}, although 
with some variations. In BAMPS the Debye mass $m_D^2$ is calculated dynamically from the non-equilibrium distribution functions $f$ of gluons and light quarks via~\cite{Xu:2004mz}  
$m_D^2 = \pi \alpha_s \nu_g \int \frac{\dd ^3p}{(2\pi)^3} \frac{1}{p} 
( N_c f_g + n_f f_q)
%\label{md2}
$,
where $N_c=3$ denotes the number of colours and $\nu_g = 16$ is the gluon degeneracy. While
MC@$_s$HQ applies the equilibrium Debye mass with quantum statistics for temperatures extracted from the fluid dynamic background, BAMPS treats all particles as Boltzmann particles, due to the non-equilibrium nature of the cascade. Moreover, in BAMPS the scale of the running coupling in the Debye mass is evaluated at the momentum transfer of the process, \eg $\alpha_s(t)$. 
% In MC@$_s$HQ the coupling is evaluated self consistently at the Debye mass itself, $\alpha_s(m_D^2)$.
The differences in the treatment lead to a larger energy loss of about a factor of two in MC@$_s$HQ compared to BAMPS.
% However, we have checked that if exactly the same scenarios are considered  both models give the same results.

As for the radiative energy loss, the model mostly concentrates on the case of intermediate energy for which coherence effects do not play the leading role. Exact momentum conservation and scattering on dynamical partons have however to be implemented exactly. In the MC@$_s$HQ approach~\cite{Gossiaux:2010yx,Aichelin:2013mra}, the calculations of \ci{Gunion:1981qs} are
thus extended for incoherent radiation off a single massless parton to the case of massive quarks. For the central ``plateau'' of radiation, one obtains that the cross section
$\dd \sigma(Qq\rightarrow Qqg)$ is dominated by a gauge-invariant subclass of diagrams. It can be factorised as the product of the elastic cross section $\dd \sigma(Qq\rightarrow Qq)$ and a factor $P_g$ representing the conditional probability of radiation
per elastic collision, which is collinear-safe thanks to the heavy-quark mass $m_Q$. Moreover, it was shown 
in~\ci{Aichelin:2013mra} that a fair agreement with the exact power spectra can be achieved by considering the eikonal limit in $P_g$ and preserving the phase-space condition. The ensuing relation reads $\dd \sigma(Qq\rightarrow Qqg)=\dd \sigma(Qq\rightarrow Qq) P_g^{\rm eik}$,
with  
\begin{equation}
P_g^{\rm eik}(x,\vec{k}_t,\vec{l}_t)=\frac{3\alpha_s}{\pi^2}\frac{1-x}{x}
  \bigg(\frac{\vec{k}_t}{k_t^2+x^2m_Q^2}
  -\\
  \frac{\vec{k}_t-\vec{l}_t}{(\vec{k}_t-\vec{l}_t)^2 +x^2 m_Q^2}\bigg)^2\,,
  \label{eq:OHF_gossiaux_pg}
  \end{equation}
where $x$ is the fraction of 4-momentum carried by the radiated gluon, $\vec{k}_t$ its transverse momentum and $\vec{l}_t$ the
momentum exchanged with the light parton. For the radiation in a medium at finite temperature, the radiated gluon acquires
a thermal mass, which leads to a modification $x^2 m_Q^2 \rightarrow x^2 m_Q^2 +(1-x) m_g^2$ in \eq{eq:OHF_gossiaux_pg}.
As a consequence, the power spectra are vastly reduced. In MC@$_s$HQ, an explicit realisation of 
the elastic process is achieved first, and the radiation factor $P_g$ is then sampled along the variables 
$x$ and $\vec{k}_t$. In \ci{Gossiaux:2012cv}, the implementation of radiative processes was generalised to include the coherent radiation, through an interpolation between single and multiple scatterings
matched to the BDMPS result \cite{Baier:1996kr}. However it neglects the finite path length effects which are important for thin plasmas.  Hereafter, this will be referred to as ``LPM-radiative''. For further description of the model, the reader is referred to~\ci{Nahrgang:2013saa}.

Similar considerations apply for radiative energy loss in BAMPS~\cite{Abir:2011jb,Fochler:2013epa}. Due to the semi-classical transport nature of BAMPS, the LPM effect is included effectively by comparing the formation time of the emitted gluon to the mean free path of the jet \cite{Fochler:2010wn}. Furthermore, the emitted gluon is treated as a massless particle.

 \begin{figure}[tb]
%   \centering
\includegraphics[width=0.48\textwidth]{twopiTDs_vs_T_col_and_col_plus_rad_gossiaux}
\includegraphics[width=0.48\textwidth]{drag_for_c_and_b_T400_gossiaux}
 \caption{Macroscopic properties for both elastic and elastic plus LPM-radiative model. On the left panel, the diffusion 
 coefficient $2\pi T D_s$ is plotted vs $T/T_c$ and compared to the l-QCD calculations of~\cite{Ding:2012sp,Banerjee:2011ra}. On the right panel,
 the average momentum loss per unit of time is plotted vs heavy-quark momentum both for \cquark and \bquark quarks.}
 \label{fig:gossiaux_properties}
 \end{figure}
\fig{fig:gossiaux_properties} illustrates two properties of this energy loss model as implemented in 
MC$@s$HQ model. Both the pure elastic case as well
as a combination of the elastic and LPM-radiative energy loss are considered. In both cases, the model is calibrated by applying a multiplicative 
$K$-factor to the interaction cross sections, in order to describe the \raa of D mesons for intermediate \pt range in \pb collisions 
at $\snn=2.76$\TeV in the 0--20\% centrality class.  This leads to $K_{\rm el}=1.5$ and 
$K_{\rm el+LPM-rad}=0.8$, while one obtains $K_{\rm el}=1.8$ and 
$K_{\rm el+LPM-rad}=1$ following a similar procedure at RHIC. For the spatial diffusion coefficient $D_s$, one sees that both combinations are compatible with the l-QCD
calculations of~\cis{Ding:2012sp,Banerjee:2011ra} and thus provide some systematic ``error band'' of the approach. The 
corresponding average momentum loss per unit of time (or length), shown on the right panel of \fig{fig:gossiaux_properties}, illustrates
the mass-hierarchy, found to be stronger for the radiative component (black lines in the figure).
