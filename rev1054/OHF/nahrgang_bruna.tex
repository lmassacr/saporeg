%!TEX root = ../SaporeGravis.tex

Angular correlations of charged hadrons proved to be key observables at RHIC and LHC energies to study energy loss and QGP properties~\cite{Arsene:2004fa,Adcox:2004mh,Back:2004je,Adams:2005dq,Chatrchyan:2011eka,Aamodt:2011vg}, providing measurements that are complementary to single-particle observables like the \raa and \vtwo.
Two-particle correlation distributions are defined in terms of the ($\Delta\phi$, $\Delta\eta$) distance between a \pt-selected trigger particle and a (set of) associated particles, generally with 
lower \pt than the trigger particle. 
On the near side ($\Delta\phi\sim0$), the correlations provide information on the properties of the jet leaving the medium, while on the away side  ($\Delta\phi\sim \pi$) they reflect the ``survival" probability of the recoiling parton that traverses the medium. Di-hadron correlation measurements typically carry geometrical and kinematical biases~\cite{Renk:2006pk}. Triggering on a high-\pt particle tends to favour the selection of partons produced near the surface of the medium, which lost a small fraction of their energy and could still fragment to hadrons at high \pt (geometrical bias). In addition, when comparing to the vacuum case (\pp collisions) with the same conditions on the trigger particles, one might have different contributions of quark and gluon jets and different partonic energies in nucleus--nucleus and \pp collisions (parton and kinematical biases).
Together with single particle measurements and fully reconstructed jets, di-hadron correlations can constrain energy loss models by adding information on the path length dependence of the energy loss and the relative contributions of collisional and radiative energy loss.

%Such correlation measurements are of interest also for heavy-flavour quarks, which are expected to be affected by less gluon radiation and more elastic energy loss compared to light quarks because of their larger mass. 

Recent works~\cite{Nahrgang:2013saa,Cao:2014pka,Uphoff:2013rka,Renk:2013xaa,Lang:2013wya,Beraudo:2014boa} have shown that the azimuthal distributions of heavy quark-antiquark pairs are sensitive to the different interaction mechanisms, collisional and radiative. The relative angular broadening of the \QQbar pair does not only depend on the drag coefficient discussed above (see \sect{sec:OHFmodelsInt})
% it is certainly discussed in chapter 1 already
but also on the momentum broadening in the direction perpendicular to the initial quark momentum, $\langle p_\perp^2\rangle$, which is not probed
directly in the traditional \raa and \vtwo observables.  This is one of the motivation for measuring azimuthal correlations of heavy-flavour.

The experimental challenges in measurements like D--$\rm \overline{D}$ correlations in heavy-ion collisions come from the reconstruction of both the hadronic decays of the back-to-back D mesons, which require large statistics to cope with low branching ratios and low signal-to-background in nucleus--nucleus collisions. As an alternative, correlations of D mesons with charged hadrons (D--h), correlations of electrons/muons from decays of heavy-flavour particles with charged hadrons (\electron--h) and correlations of D--\electron, $e^+$--$e^-$,  $\mu^+$--$\mu^-$ and \electron--$\mu$ pairs (where electrons and muons come from heavy-flavour decays) can be studied.
%measured in different collision systems (\pp, p--nucleus and nucleus--nucleus collisions) to study the modification of the heavy-quark fragmentation, the structure of the underlying event in the presence of heavy-flavours and therefore the properties of the medium created in central \pb collisions. 
Such observables might, however, hide decorrelation effects intrinsic to the decay of heavy mesons. 
In addition, in the case of correlations triggered by electrons or muons from heavy-flavour decays, the interpretation of the results is complicated by the fact that the lepton carries only a fraction of the momentum of the parent meson. This makes the understanding of \pp collisions as baseline a very crucial aspect of these analyses (see \sect{sec:pp:AngleCorrelations}).

%Heavy-flavour azimuthal correlations in pp collisions also allow for studies of heavy-quark fragmentation and jet structure at different collision energies, which help constraining Monte Carlo models  and the different production processes for heavy flavours (i.e. flavour creation, flavour excitation and gluon splitting) modelled in Monte Carlo event generators. 
%
%Studies of heavy-flavour correlations in hadronic collisions were carried on at Tevatron with  D-D correlations~\cite{Reisert:2007zza} in p$\bar{\rm  p}$ collisions at $\sqrt s=$1.96 TeV and at RHIC with e-$\mu$ correlations~\cite{Adare:2013xlp} (where electrons and muons come from heavy-flavour decays) in pp collisions at $\sqrt s=$200 GeV. Results on heavy-flavour correlation measurements were also reported by the LHC experiments~\cite{Aaij:2012dz,ATLAS:2011ac,Khachatryan:2011wq,Bjelogrlic:2014kia} with pp collisions at  $\sqrt s=$7 TeV, as shown in Fig.~\ref{HFcorrpp2}. The results at different collision energies are sensitive to different heavy-flavour production mechanisms, providing constraints for leading and higher-order contributions in Monte Carlo event generators.

%\begin{figure}[!htp]
 %\begin{center}
 % \includegraphics[angle=0,width=0.49\textwidth]{DzeroDstar_xsec_sum.eps}
  %\includegraphics[angle=0,width=0.48\textwidth]{phenixemu.pdf}
   %\end{center}
 %\caption{Left: D-D angular correlation measurements from CDF compared to Pythia and its different production processes~\cite{Reisert:2007zza}. Right: heavy-flavour e-$\mu$ correlations from PHENIX, compared to different Monte Carlo event generators~\cite{Adare:2013xlp}.}
 %\label{HFcorrpp} 
%\end{figure}

%\begin{figure}[!htp]
% \begin{center}
%  \includegraphics[angle=0,width=0.49\textwidth]{CMSbbar_pp.pdf}
%   \includegraphics[angle=0,width=0.49\textwidth]{DhALICEMC_58_05.pdf}
% \end{center}
% \caption{Left: angular correlations of B-$\bar{\rm B}$ mesons measured by CMS in different ranges of the leading jet \pt\ and compared to PYTHIA~\cite{Khachatryan:2011wq}. Right: D-h angular correlations measured by ALICE, compared to different PYTHIA tunes~\cite{Bjelogrlic:2014kia}.}
% \label{HFcorrpp2} 
%\end{figure}

Heavy-flavour azimuthal correlations are
being studied in \dAu collisions at RHIC and \pPb collisions both at RHIC and at the LHC to understand how the presence of the nucleus might affect the properties of heavy-flavour pair production.
The results are discussed in \sect{sec:CNM:OHF}.

%%% THIS SHOULD BE MOVED TO THE CNM SECTION
%Furthermore, it is interesting to investigate whether or not the long range correlations along $\Delta \eta$ observed for light flavours in high-multiplicity events at the LHC~\cite{CMS:2012qk,Aad:2012gla,Abelev:2012ola} are also present in the heavy-flavour sector.
%
%D--h correlations measured by the ALICE Collaboration at intermediate/high \pt for trigger and associate particles are compatible in \pp and \pPb collisions, indicating no strong effects in the jet properties in the measured kinematic ranges~\cite{Bjelogrlic:2014kia}.  On the other side, at lower \pt the results from RHIC on \electron--$\mu$ azimuthal correlations in \dAu collisions, measured by the PHENIX Collaboration, seem to suggest that, in the measured kinematic ranges (with the electron measured at mid-rapidity and the muon at forward rapidity), the yields of heavy-flavour \electron--$\mu$ pairs are modified in the cold nuclear medium compared to binary-scaled \pp collisions (\fig{fig:pA:HFeHFm}). Effects from the cold nuclear matter in the heavy-flavour sector were also observed by ALICE with measurements of \electron--h correlations in high-multiplicity \pPb collisions~\cite{Bjelogrlic:2014kia}, which show indications for long-range correlations in $\Delta \eta$ at low \pt, similar to what was observed for light flavours~\cite{CMS:2012qk,Aad:2012gla,Abelev:2012ola}.

\begin{figure}[!ht]
% \begin{center}
%  \includegraphics[angle=0,width=0.46\textwidth]{PHENIXemu.pdf}
%%  \includegraphics[angle=0,width=0.45\textwidth]{ehALICEpPb.pdf}
%     \end{center}
% \caption{Heavy-flavour \electron--$\mu$ pair yield in \pp and \dAu collisions from PHENIX~\cite{Adare:2013xlp}.}
% %$\Delta \eta$-$\Delta \phi$ correlations of electrons from heavy-flavour decays and charged hadrons from ALICE in high-multiplicity p-Pb collisions~\cite{Filho:2014vba}.
% \label{HFcorrpPb} 
%\end{figure}
%\begin{figure}
 \begin{center}
  \includegraphics[angle=0,width=0.5\textwidth]{phenixehAu.pdf}
  \includegraphics[angle=0,width=0.36\textwidth]{DhRun3.pdf}
     \end{center}
 \caption{Left: ratio of yields in the away-side region ($2.51<\phi<\pi$) to those in the shoulder region  ($1.25<\phi<2.51$)  in \pp and \AuAu collisions from PHENIX~\cite{Adare:2010ud}. Right: relative uncertainty on the away-side yield in D--h correlations as a function of the charged hadron \pt for three ranges of D meson transverse momenta, with the ALICE and LHC upgrades~\cite{Colamaria:2014uja}.}
 \label{HFcorrPb} 
\end{figure}

\begin{figure}[!t]
 \includegraphics[width=0.33\textwidth]{pperpca}
 \includegraphics[width=0.33\textwidth]{correlationa}
 \includegraphics[width=0.32\textwidth]{correlation3pta}
 \caption{Perpendicular momentum of charm quarks acquired in a QGP medium at $T=400$\MeV as a function of the initial momentum $p_{||}^{\rm ini}$ (left).
 Angular correlations of $\mathrm{D\overline{D}}$ pairs in \pb collisions at LHC without (centre) and with (right) a lower momentum cut~\cite{Nahrgang:2013saa}.}
 \label{fig:correlation}
 \end{figure}


Measurements of heavy-flavour correlations in nucleus--nucleus collisions were carried out at both RHIC and LHC with \electron--h correlations~\cite{Adare:2010ud,Wang:2008kha,Thomas:2014cwa} (where electrons come from heavy-flavour decays), but the current statistics prevents us from drawing quantitative conclusions. Such measurements are expected to provide more information about how the charm and beauty quarks propagate through the hot and dense medium and how this affects and modifies the correlation structures. 
In particular, PHENIX reported a decrease of the ratio of yields in the away-side region ($2.51<\Delta\phi<\pi$) to those in the shoulder region  ($1.25<\Delta\phi<2.51$)  from \pp to \AuAu collisions (left panel of \fig{HFcorrPb}). 

Further measurements of heavy-flavour triggered azimuthal correlations  will be promising in future data takings at both RHIC (with the new silicon tracker detectors) and LHC (with the machine and detector upgrades). As reported in \fig{HFcorrPb}, right, the relative uncertainty on the away-side yield in D--h correlations in central \pb collisions with the ALICE and LHC upgrades will be $\approx 15 \%$ for low-\pt D mesons and only a few percents for intermediate/high \pt. 

%Therefore, different heavy-flavour correlation observables sensitive to the partonic content of the jet will be needed to compare the energy loss mechanism for heavy quarks, light quarks and gluons and to determine the extent of modification of away-side heavy-jet fragmentation for heavy flavours. 
%This can be achieved with correlations of trigger and associate particles both coming from heavy quarks (D-e, e$^+$-e$^-$,  $\mu^+$-$\mu^-$ and e-$\mu$) or by tagging jets containing charm and beauty particles. In particular, as discussed above, correlations of heavy-flavour particles with each other will reduce medium effects allowing for a more quantitative study of the different energy loss contributions in the heavy-flavour sector. 
%With the ALICE and LHC upgrades,  we expect to reach a relative uncertainty on the away-side yield of D-e correlations (where the electron comes from decays of heavy-flavour particles) of less than 10 \% for electron $\pt>1$~GeV/$c$, opening the way to a new class of observables aiming to a quantitative understanding of the heavy-flavour production and energy loss.
 
Several theoretical works have recently addressed angular correlations of heavy-flavour particles
in nucleus--nucleus collisions~\cite{Nahrgang:2013saa,Cao:2014pka,Uphoff:2013rka,Renk:2013xaa,Lang:2013wya,Beraudo:2014boa}.
However, none of these approaches presently includes the interactions of D and B mesons in the hadronic phase present in the
late stages of the system evolution. These interactions could add a further smearing on top of QGP-induced modification of the heavy-quark angular correlations. 
For the traditional \raa and \vtwo observables, a first step in this 
direction was made in~\cis{He:2012df,Ozvenchuk:2014rpa,Cao:2014dja}, with effects found of the order of 20\% at most. We now focus on a particular model in order to illustrate the sensitivity of heavy-flavour angular correlations to the type of interaction mechanism~\cite{Nahrgang:2013saa}. In \fig{fig:correlation} (left), the transverse momentum broadening per unit of time is shown as a function of the initial momentum $p_{||}^{\rm ini}$ of charm quarks for the purely collisional and collisional plus radiative (LPM) interactions as applied within the MC@$_s$HQ model (see \sect{sec:mcatshq}). For all initial momenta, $\langle p_\perp^2\rangle$ is larger in a purely collisional interaction mechanism. $\langle p_\perp^2\rangle$ has similar numerical values for charm and for beauty quarks.  
A larger $\langle p_\perp^2\rangle$ leads to a more significant change of the initial relative pair azimuthal angle $\Delta\phi$ during the evolution in the medium. This means that for a purely collisional interaction mechanism one expects a stronger broadening of the initial correlation at $\Delta\phi=\pi$, as seen in the central and right panels of \fig{fig:correlation}. In the central panel, the $\Delta\phi$ distribution of all initially correlated pairs is shown after hadronisation into $\mathrm{D\overline{D}}$ pairs. Since no cut in \pt is applied, these distributions are dominated by low-momentum pairs, while in the right panel a cut of $\pt>3$\GeVc is applied. The low-momentum pairs show the influence of the radial flow of the underlying QGP medium, which tends to align the directions of the quark and the antiquarks toward smaller opening angles. It again happens more efficiently for larger  $\langle p_\perp^2\rangle$ of the underlying interaction mechanism. This effect, which was called ``partonic wind''~\cite{Zhu:2007ne},
is thus only seen for the purely collisional interaction mechanism. A \pt threshold reveals clearly the residual correlation around $\Delta\phi\sim\pi$. Here in the purely collisional scenario one sees a larger background of pairs that decorrelated during the evolution in the QGP than for the collisional plus radiative (LPM) scenario. 
 
For these calculations an initial back-to-back correlation has been assumed. Next-to-leading order processes, however, destroy this strict initial correlation already in proton--proton collisions. Unfortunately the theoretical uncertainties on these initial distributions are very large, especially for charm quarks. Here, a thorough experimental study of heavy-flavour correlations in proton--proton and proton--nucleus collisions is very important for validating different initial models. Also enhanced theoretical effort in these reference systems is necessary. 



% Recent theoretical studies include also the coupling of D mesons to a hadronic cascade after the QGP % evolution~\cite{He:2012df,Ozvenchuk:2014rpa}. This includes the soft particles emitted at the end of % the fluid dynamical evolution as well as the full information on the products of the heavy-quark 
% fragmentation. 
% The goal is to provide a direct comparison to the experimental data of correlations of heavy-flavour % particles with hadrons. These correlations, however, might be dominated by medium effects in the 
% soft sector, like flow of hadrons, and by decorrelation effects due to the fragmentation and decay 
% of heavy quarks, as already mentioned.  This reinforces the need to measure direct correlations 
% between heavy-flavour hadrons or, at least between heavy-flavour hadrons (e.g. D mesons) and 
% heavy-flavour decay electrons.
