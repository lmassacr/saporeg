%!TEX root = SaporeGravis.tex

\section{Quarkonium photoproduction in nucleus-nucleus collisions}
\label{sec:UPC}
%%%%%%%%%%%%%%%%%%%
%  I N T R O D U C T I O N             %
%%%%%%%%%%%%%%%%%%%

In 2011, the LHC produced collisions of lead ions at a centre-of-mass energy per nucleon pair  $\sqrt{s_{\rm NN}} = 2.76$~TeV. These collisions have been
used to perform different measurements of charmonium  photonuclear production. (In 2010 there also were \PbPb collisions, but the integrated luminosity, ten times smaller than in 2011, was not sufficient to perform the measurements described below, although it was enough to measure the coherent production of $\rho^0$~\cite{Adam:2015gsa}.)  All but one of the studies described in this section have been carried out using ultra-peripheral collisions (UPC). These are  interactions  where the impact parameter exceeds the sum of the radii of the colliding nuclei. In such collisions, the cross section for hadronic processes is strongly suppressed, while the cross section for  electromagnetic interactions remains large. The  analysis not related to UPC has investigated the photoproduction of $\jpsi$ overlapped with a standard hadronic \PbPb collision. 

Two types of photonuclear production of charmonium have been studied: coherent and incoherent. In the first case, the incoming quasi-real photon interacts coherently with the whole nucleus to produce the charmonium.
The coherence condition, both in the emission of the photon and in the interaction with the nuclear target, constrains the transverse momentum of the produced vector meson to be of the order of the inverse of the nucleus diameter, which translates into approximately 60 MeV/$c$. In the incoherent case, the quasi-real photon couples only to one nucleon, and thus the transverse momentum of the produced vector meson is constrained by the size of the nucleon, which translates into approximately 300 MeV/$c$. In a fraction of the coherent interactions and in all incoherent processes, one or a few neutrons are produced at the  rapidity of the incoming beams. The experimental signature of these processes is therefore a vector meson with fairly small transverse momentum, possibly one or a few neutrons detected at zero degrees, and nothing else in the detector.


In this section, we review these measurements and discuss the models proposed to describe them. Previous reviews addressing these subjects can be found in  \cite{Baur:1998sr,Baur:2001jj,Bertulani:2005ru,Baltz:2007kq}. This section is organised as follows.
First, in Subsection \ref{UPCsec:photons} we discuss the origin and characteristics of the photon flux at the LHC. Subsection \ref{UPCsec:exp} describes previous results from RHIC and  the existing measurements from LHC. Subsection \ref{UPCsec:theory} presents the current theoretical models and the main differences among them. Subsection \ref{UPCsec:comp} discusses
how the models compare to the experimental results. We conclude in Subection \ref{UPCsec:outlook} with a brief summary of the lessons learnt and with an outlook of what could be possible with the data from the LHC \RunTwo.

%%%%%%%%%%%%%%%%%%%
%  FL U X   O F   P H O T O N S      %
%%%%%%%%%%%%%%%%%%%

\subsection{The flux of photons from lead ions at the LHC}
\label{UPCsec:photons}

The lead beams in the LHC are an intense source of photons, because the electromagnetic field of charged particles accelerated to ultra-relativistic velocities can be seen as a flux of quasi-real photons, according to a proposal made by Fermi \cite{Fermi:1924tc,Fermi:1925fq}, and later refined by Weizs\"acker \cite{vonWeizsacker:1934sx} and Williams \cite{Williams:1934ad}. 

The photons are emitted by the nucleus coherently and thus their virtuality is restricted to be of the order of the inverse of the nucleus diameter, which for  lead  implies an upper limit for the virtuality around 30 MeV/$c$; i.e., the photons can be considered as quasi-real.
The intensity of the flux depends on the square of the electric charge of the incoming particle, so it is large for the lead nuclei at the LHC. In  the semiclassical description (see for example \cite{Baur:2001jj}) the photon flux per unit area is given by 

\begin{equation}
\frac{\dd^3n(k,\vec{b})}{\dd k\dd^2\vec{b}} = \frac{\alpha_{\rm em} Z^2}{\pi^2 kb^2}x^2\left[K^2_1(x)+\frac{1}{\gamma^2}K^2_0(x)\right],
\label{UPCeq:FluxPerArea}
\end{equation}
where $\alpha_{\rm em}$ is the fine structure constant, $k$ is the photon energy in the frame where the
photon emitter has Lorentz factor gamma, $Z$ is the electric charge of the lead nucleus, $K_{0}$ and $K_1$ are modified Bessel functions, $\vec{b}$ is the impact-parameter vector with $b$ its magnitude and $x=kb/\gamma$.

The measurements of ultra-peripheral collisions described in the next sections were obtained requiring the absence of a hadronic collision between the incoming nuclei. This requirement is implemented into the computation of the photon flux in two different ways. The simpler option is to integrate \eq{UPCeq:FluxPerArea}
starting from a minimum impact parameter $b_{\rm min}$ given by the sum of the radii of the incoming nuclei. 
In this case, known as the hard-sphere approximation, the flux of quasi-real photons is given by

\begin{equation}
\frac{\dd n(k)}{\dd k} = \frac{2\alpha_{\rm em} Z^2}{\pi k}\left[ \xi K_0(\xi)K_1(\xi)-\frac{\xi^2}{2}\left(K^2_1(\xi)-K^2_0(\xi)\right)\right],
\label{UPCeq:FluxHS}
\end{equation}
with $\xi=kb_{\rm min}/\gamma$. 

Another option is to convolute the flux per unit area  with the probability of no hadronic interaction, which is obtained using the nuclear overlap function and the total nucleon-nucleon interaction cross section and averaging the flux over the target nucleus
(for further details see \cite{Klein:1999qj}) . The resulting integral can be calculated numerically to obtain  the photon flux $n(k)$.

All the measurements described below are elastic in the sense
that the measurement of the charmonium fixes completely the kinematics of the process.
In this case the energy of the photon can be expressed in terms of the mass $M$ of the charmonium and its rapidity $y$ as

\begin{equation}
k = \frac{M}{2}\exp{(y)},
\end{equation}
and thus, the photon flux can be written as:

\begin{equation}
n(y,M)\equiv k\frac{\dd n(k)}{\dd k}.
\end{equation}

Figure~\ref{UPCfig:FluxHS} shows the flux of quasi-real photons with the energy required to produce a $\jpsi$ at rapidity $y$. 
At large negative $y$ the centre-of-mass energy of the photon-lead system is not large enough to produce the vector meson. For the energies of the lead beams during \RunOne and the case of a $\jpsi$ this happens at $y\approx -6.8$; see e.g. \eq{eq:W2my}. The left panel shows the UPC case, computed with \eq{UPCeq:FluxHS}: as the rapidity increases the flux decreases. At large rapidities the fast decrease of the flux is related to the behaviour of the Bessel functions. The right panel shows the  integration of \eq{UPCeq:FluxPerArea} for the impact parameters corresponding to the 70--90 \% centrality class in hadronic \PbPb collisions according to \cite{Abelev:2013qoq}. This case is discussed in Section \ref{sec:lowptexcess}.

% In this case, the difference between both curves at $|y| \sim 3$ is much smaller than for UPC.

\begin{figure}[t]
\begin{center}$
\begin{array}{c}
\includegraphics[width=0.47\textwidth]{FluxSapore} 
\includegraphics[width=0.47\textwidth]{FluxSaporePer} 
\end{array} $
\end{center}
   \caption{Photon flux for positive and negative values of the rapidity $y$ for the case of  $\jpsi$ coherent photoproduction and an energy of the lead-ion beam of 2.76~GeV per nucleon. Positive rapidities correspond to the direction of the lead-ion. The left panel shows the UPC case given by \eq{UPCeq:FluxHS}, while the right panel shows the integration of \eq{UPCeq:FluxPerArea} for the impact parameters corresponding to the 70--90 \% centrality class in hadronic \PbPb collisions.
  }
\label{UPCfig:FluxHS}
\end{figure}	


%%%%%%%%%%%%%%%%%%%
%  M E A S U R E M E N T             %
%%%%%%%%%%%%%%%%%%%


\subsection{Measurements of photonuclear production of charmonium during the \RunOne at the LHC}
\label{UPCsec:exp}


In both coherent and incoherent photonuclear production of charmonium
the target is not broken by the interaction and in this sense the processes may be considered elastic. Therefore, the measurement of the produced charmonium  completely fixes the kinematics.

As mentioned above, the experimental signature for these processes in UPC consists then in the decay products of a charmonium with fairly small transverse momentum  and in some cases one or a few neutrons at zero degrees. No other event activity is measured in the detector. Figure~\ref{UPCfig:ED} shows two event displays of the coherent photonuclear production of $\jpsi$ and $\psiP$ in UPC as measured with the CMS and ALICE detectors.


\begin{figure}[t]
\begin{center}$
\begin{array}{c}
\includegraphics[width=0.47\textwidth]{cmsJP4} 
\hspace{2mm}
\includegraphics[width=0.47\textwidth]{AliceUPC2} 
\end{array} $
\end{center}
   \caption{Event displays of coherent photonuclear production of $\jpsi \to \mu^+\mu^-$ (left) and $\psiP \to \jpsi \pi^+\pi^- \to  \mu^+\mu^-\pi^+\pi^-$ (right) in UPC as measured with the CMS and ALICE detectors respectively.}
\label{UPCfig:ED}
\end{figure}	

The  cross section for these photonuclear processes in \PbPb collisions, with the charmonium measured at rapidity $y$, has two contributions:
\begin{equation}
\frac{\dd\sigma_{\rm PbPb}(y)}{\dd y}= n(y,M)\sigma_{\gamma \rm Pb}(y) + n(-y,M)\sigma_{\gamma \rm Pb}(-y),
\label{UPCeq:SigPbPb}
\end{equation}
where the first term corresponds to one of the incoming lead nucleus acting as the source of the photon and the second term corresponds to the other incoming nucleus acting as the source of the photon.
When the charmonium is measured at mid rapidities, $y=0$, both terms are equal and can be summed. On the other hand, when the charmonium is measured at rapidities around 3, the flux at positive $y$ is strongly suppressed and  the term at negative $y$ dominates.  Note that for the case of photonuclear production overlapped with a hadronic collision, both fluxes  contribute even at the forward rapidities measured at the LHC. This is illustrated in \fig{UPCfig:FluxHS}.

As mentioned before, the measurement of the charmonium fixes the kinematics. The centre of mass energy of the $\gamma$-Pb system is given by
\begin{equation}
W^2_{\gamma\rm{Pb}} = 2k\sqrt{s_{\rm NN}} = M\exp{(y)}\sqrt{s_{\rm NN}} ,
\label{eq:W2my}
\end{equation}
where in these expressions, both $k$ and $y$ are
evaluated in the nucleus-nucleus centre-of-mass frame. In a leading order pQCD approach $W_{\gamma\rm{Pb}}$ is related to  $x$--Bjorken by
\begin{equation}
x = \frac{M^2}{W^2_{\gamma\rm{Pb}} }.
\end{equation}
According to this prescription, a measurement of  charmonium photonuclear production at large rapidities in UPC samples mainly the low $W_{\gamma\rm{Pb}}$, alternatively large $x$, contribution to \eq{UPCeq:SigPbPb}.

\subsubsection{Photonuclear production of $\jpsi$ at RHIC}

The first measurement of photonuclear production of charmonium in UPC of relativistic heavy ions was performed by the PHENIX Collaboration using \AuAu collisions  at $\sqrt{s_{\rm NN}} = 200$ GeV \cite{Afanasiev:2009hy}.
 The events were triggered by tagging the production of neutrons at zero degrees. PHENIX found 9.9 $\pm$ 4.1 (stat) $\pm$ 1 (syst) $\jpsi$ candidates. The smallness of the sample did not allow to separate the coherent and incoherent contributions. Their measurement corresponded to $W_{\gamma\rm{Au}}\approx24$ GeV ($x\approx 1.5\cdot 10^{-2}$). The  cross section for \AuAu UPC was measured to be 76 $\pm$ 31 (stat) $\pm$ 15 (syst) $\mu$b, which agreed, within the errors, with the theoretical models available at that time. Although the large experimental errors precluded setting strong constraints on the models, this study was very important as a proof of principle.

\subsubsection{Coherent production of $\jpsi$ in \PbPb UPC at the LHC}

The coherent photonuclear production of $\jpsi$ has been measured in three different rapidity (equivalently $W_{\gamma\rm{Pb}}$) ranges at the LHC. ALICE has measured it at mid \cite{Abbas:2013oua}
and forward rapidity \cite{Abelev:2012ba}, while CMS has recently released preliminary results at semi-forward rapidities \cite{CMS:2014ies}. Table~\ref{UPCtab:xs} summarizes  these measurements.

 The ALICE detector \cite{Aamodt:2008zz} 
measures charmonium either in the central barrel using a combination of silicon trackers (ITS), a time projection chamber (TPC) and a time of flight system (TOF); or in the forward part where a muon spectrometer is installed.
In addition to requiring  the decay products of the charmonium to be either in the central barrel or in the muon spectrometer,  the exclusivity condition is realised vetoing activity in a set of two scintillator arrays (VZERO) which cover 4 units of rapidity in the forward/backward region, while the absence of neutrons at zero degrees or the measurements of one or few of them is performed with zero-degree calorimeters (ZDC) located 116 m away and on both sides of the  interaction point.


The first measurement of coherent production of $\jpsi$ in \PbPb UPC was performed by ALICE using the muon spectrometer \cite{Abelev:2012ba}.
 The trigger required a muon above threshold (1 GeV/$c$ of transverse momentum) and no activity in the opposite side of the detector. The coherent contribution was obtained using the distribution of transverse momentum. The cross section in \PbPb UPC was measured to be 1.00 $\pm$ 0.18(stat) $^{+0.24}_{-0.26}$ (syst) mb. 
The dominant contribution to the cross section (about 95\%) corresponded to $W_{\gamma\rm{Pb}}\approx20$ GeV ($x\approx 2.2\cdot 10^{-2}$).  The second measurement was performed at mid rapidity using the central barrel detectors \cite{Abbas:2013oua}. The trigger required hits in ITS and TOF (in TOF with a back-to-back topology) and absence of activity in VZERO. In this case, using the PID capabilities of the ALICE TPC,
 two decay channels have been used: $\mu^+\mu^-$ and $e^+e^-$. The transverse momentum distribution of the $\jpsi$ candidates was used to extract the coherent contribution. The measured \PbPb UPC coherent cross section was 2.38 $\pm$ $^{0.34}_{0.24}$ (stat+syst) mb and corresponded to $W_{\gamma\rm{Pb}}\approx 92$ GeV ($x\approx 10^{-3}$). For this sample, the fraction of coherent events with no activity in the ZDC was measured to be 0.70 $\pm$ 0.05 (stat).

The central barrel of the CMS detector \cite{Chatrchyan:2008aa} contains 
a silicon pixel and strip tracker, a lead tungstate crystal electromagnetic calorimeter and a brass/scintillator hadron calorimeter; all of them within a superconducting solenoid of 6 m internal diameter, providing a magnetic field of 3.8 T.  Muons are measured within pseudorapidity $|\eta|< 2.4$ by gas-ionization detectors embedded in the steel return yoke outside the solenoid. The UPC trigger used by CMS requires ($i$) the presence of at least one muon candidate with a minimal transverse-momentum threshold, ($ii$) at least one track in the pixel detector, ($iii$) rejection of events with activity in the scintillator counters covering the pseudorapidity range between 3.9 and 4.4 on both sides of the  interaction point and ($iv$) energy deposit consistent with at least one neutron in either of the ZDCs. This last requirement is similar to what was done by PHENIX and triggers only on a fraction of the cross section. To obtain the total coherent cross section, models of neutron emission in coherent production were used. CMS reports a preliminary cross section of (5.1$\pm$0.5)$\times$(0.37 $\pm$ 0.04(stat)) mb~\cite{CMS:2014ies}, where the first factor corresponds to the correction to take into account that at least a forward neutron was required to be present and the second term is the actual measured cross section. Note that this result is not corrected for feed-down from $\psiP$. In this case, as the contribution of both terms in \eq{UPCeq:SigPbPb} is important, one cannot assign a unique value of $W_{\gamma\rm{Pb}}$.

 \subsubsection{Coherent production of $\psiP$ in \PbPb UPC at the LHC}
ALICE has measured the coherent production of $\psiP$ in \PbPb UPC \cite{Broz:2014wka}  at mid rapidity using the same trigger and detectors as for the $\jpsi$ case. The $\psiP$ has been identified in the following decay channels: to $l^+l^-$ and to $\jpsi\pi^+\pi^-$, with $\jpsi\to l^+l^-$, where $l=e,\mu$. The right panel of \fig{UPCfig:ED} shows an event display of a coherently produced $\psiP \to \jpsi \pi^+ \pi^-$ candidate. The measured \PbPb UPC cross section was 0.83 $\pm$ 0.19 (stat+syst) mb and corresponded to $W_{\gamma\rm{Pb}}\approx 100$~GeV ($x\approx 1.3\cdot10^{-3}$). 
 
In addition to the measurement of the cross sections for $\jpsi$ and $\psiP$ coherent photonuclear production, the ratio of their production was also measured  \cite{Broz:2014wka}. In this ratio several uncertainties cancel. The reported ratio is
\begin{equation}
\frac{\dd\sigma^{\rm coh}_{\psiP}/\dd y}{\dd\sigma^{\rm coh}_{\jpsi}/\dd y} = 0.34^{+0.08}_{-0.07}\ \ \rm{(stat+syst)}.
\end{equation}
This ratio was higher than the ratios 
0.166 $\pm$ 0.007(stat) $\pm$ 0.008(syst) $\pm$ 0.007(BR),  0.14 $\pm$ 0.05 and 0.19 $\pm$ 0.04
 measured by H1 \cite{Adloff:2002re}, CDF \cite{Aaltonen:2009kg}  and  LHCb \cite{Aaij:2013jxj} respectively.

 \begin{table}%[H] add [H] placement to break table across pages
 \caption{Summary of the measurements of  photonuclear production of charmonium in \PbPb  UPC at $\sqrt{s_{NN}} = 2.76$ TeV at the LHC.}
  \label{UPCtab:xs}
\centering
\begin{tabular}{cccc}
 \hline
 Vector meson & d$\sigma/$d$y$ [mb] & Rapidity range & Collaboration \\
 \hline
Coherent $\jpsi$  & 1.00 $\pm$ 0.18(stat) $^{+0.24}_{-0.26}$ (syst)& $-3.6<y<-2.6$ &  ALICE\\
% \hline
Coherent $\jpsi$  & 2.38 $^{+0.34}_{-0.24}$ (stat+syst) & $-0.9<y<0.9$ &  ALICE \\
 %\hline
Coherent $\jpsi$  & (5.1$\pm$0.5)$\times$(0.37 $\pm$ 0.04(stat) $\pm$ 0.04(syst)) & $1.8<|y|<2.3$ & CMS \\
\hline
Coherent $\psiP$  & 0.83 $\pm$ 0.19 (stat+syst) &  $-0.9<y<0.9$ &  ALICE \\
\hline
Incoherent $\jpsi$  & 0.98 $^{+0.19}_{-0.17}$ (stat+sys) & $-0.9<y<0.9$ &  ALICE \\
\hline 

 \end{tabular}
 \end{table}

\subsubsection{Incoherent production of \jpsi in \PbPb UPC at the LHC}

ALICE has also measured the incoherent production of $\jpsi$ in \PbPb UPC   at mid rapidities \cite{Abbas:2013oua} using the same trigger and detectors as for the coherent case. The incoherent contribution was obtained from the distribution of transverse momentum. The centre of mass energy in the $\gamma$-Pb system is the same as for the coherent case. The measured cross section is 0.98 $^{+0.19}_{-0.17}$ (stat+syst) mb.

\subsubsection{Coherent photonuclear production of $\jpsi$ in coincidence with a hadronic \PbPb collision at the LHC}
\label{sec:lowptexcess}

When studying the inclusive distribution of transverse momentum of $\jpsi$ in hadronic \PbPb collisions at large rapidities (in the range 2.5 to 4.0), a significant excess of $\jpsi$ candidates was found for transverse momentum smaller than 0.3 GeV/$c$ for the centrality bin 70--90\% \cite{Lardeux:2013dla} . One possible explanation of this observation is the coherent photonuclear production of the $\jpsi$ in coincidence with a hadronic interaction. Although the possibility of such a process has been discussed in the past \cite{Joakim2010}, currently there is no theoretical calculation available for this process. Such a calculation is a challenge for theorists. Note that an excess is also observed, with reduced signficance, in the centrality bin 50--70\% and that there is a framework in place to extract the photonuclear coherent cross section in these cases \cite{LaureQM2014}.

%%%%%%%%%%%%%%%%%%%
%  M O D E L S                               %
%%%%%%%%%%%%%%%%%%%

\subsection{Models for photonuclear production of charmonium}
\label{UPCsec:theory}

The following models will be discussed in this section:
\begin{itemize}
\item[] {\bf AB-AN}: Model by Adeluyi and Bertulani \cite{Adeluyi:2012ph} and Adeluyi and Nguyen \cite{Adeluyi:2013tuu}.
\item[] {\bf CSS}: Model by Cisek, Sch\"afer and Szczurek \cite{Cisek:2012yt}.
\item[] {\bf KN}: Model by Klein and Nystrand implemented in the STARLIGHT Monte Carlo program  
\cite{Klein:1999qj,Baltz:2002pp,Klein:2003vd}.
\item[] {\bf LM}: Model by Lappi and Mantysaari \cite{Lappi:2010dd,Lappi:2013am}.
\item[] {\bf GM-GDGM}: Model by Goncalves and Machado \cite{Goncalves:2011vf} and by Gay-Ducati, Griep and Machado \cite{Ducati:2013bya}.
\item[] {\bf RSZ}: Model by Rebyakova,  Strikman, and Zhalov \cite{Rebyakova:2011vf}.
\end{itemize}

All models start from \eq{UPCeq:SigPbPb} which has two ingredients: the photon flux and the photonuclear cross section. The first difference among the models is that some of them (CSS, LM, GM-GDGM) use the hard-sphere approximation of the photon flux; i.e., \eq{UPCeq:FluxHS}, and other models (AB-AN, KN, RSZ-GZ) integrate the convolution of  \eq{UPCeq:FluxPerArea} with the probability of no hadronic interaction. 

Regarding the photonuclear cross section the models contain the following ingredients: ($i$)  an assumption on the nuclear distribution in the transverse plane, ($ii$) an implicit or explicit  prescription for the wave function of the vector meson and finally ($iii$)
all models fix some of the parameters using data on exclusive photoproduction of charmonium off the proton and thus have to include a prescription to link the photoproduction off protons with the photonuclear interaction. In this context the models can be grouped in three different classes: models based on the generalised vector dominance model (KN), on LO pQCD (AB-AN, RSZ) and on  the colour dipole model (CSS, LM, GM-GDGM).

\subsubsection{Models based on  vector dominance}

The only model in this class is KN. There are three main ingredients in this model: ($i$) the vector dominance model (VDM) relates both the $\gamma \mathrm{+Pb}\to\mathrm{Pb+V}$ and the $\gamma \mathrm{+p}\to\mathrm{p+V}$ processes to $\mathrm{Pb+V}\to\mathrm{Pb+V}$ and $\mathrm{p+V}\to\mathrm{p+V}$ respectively (Here $V$ represents a vector meson.); ($ii$) the optical theorem relates these last processes to the total cross section; ($iii$) a classical Glauber model relates the total production cross section off a proton, to that off a nucleus.

In more detail:
\begin{equation}
\sigma_{\gamma \rm Pb}(y) \equiv\sigma(\gamma{\rm + Pb}\to{\rm V+Pb}) =
\left. \frac{\dd\sigma(\gamma{\rm + Pb}\to{\rm V+Pb}) }{\dd t} \right|_{t=0}
\int^\infty_{t_{min}} \dd t |F(t)|^2,
\label{UPCeq:Fwd}
\end{equation}
where  $F(t)$ is the nuclear form factor and $t$ the momentum transferred to the nucleus. 
Using VDM and the optical theorem yields
\begin{equation}
\left. \frac{\dd\sigma(\gamma{\rm + Pb}\to{\rm V+Pb}) }{\dd t} \right|_{t=0}
= \frac{\alpha \sigma^2_{\rm TOT}({\rm V+Pb})}{4f^2_V},
\end{equation}
where $f_V$ is the vector meson photon coupling. A classical Glauber model produces
\begin{equation}
\sigma_{\rm TOT}({\rm V+Pb}) \approx \sigma_{\rm inel}({\rm V+Pb})
=\int \dd^2\vec{b}\left(
1-\exp\left[-\sigma_{\rm TOT}({\rm V+p}) T_{\rm Pb}(\vec{b}) \right]
\right),
\end{equation}
where $T_{\rm Pb}$ is the nuclear thickness function and $\sigma_{\rm TOT}({\rm p+V})$ is obtained from  the optical theorem, now applied at the nucleon level
\begin{equation}
\sigma^2_{\rm TOT}({\rm V+p}) = 16\pi
\left. \frac{\dd\sigma({\rm V+p}\to{\rm V+p}) }{\dd t} \right|_{t=0}.
\end{equation}
Using VDM leads to
\begin{equation}
\left. \frac{\dd\sigma({\rm V+p}\to{\rm V+p}) }{\dd t} \right|_{t=0} = \frac{f^2_V}{4\pi\alpha}
\left. \frac{\dd\sigma(\gamma{\rm + p}\to{\rm V+p}) }{\dd t} \right|_{t=0},
\end{equation}
where  the elementary cross section
\begin{equation}
\left. \frac{\dd\sigma(\gamma{\rm + p}\to{\rm V+p}) }{\dd t} \right|_{t=0}
= b_V\left( XW^\epsilon_{\gamma\rm p} + YW^{-\eta}_{\gamma\rm p}\right)
\end{equation}
is fitted to experimental data to obtain the values for the 
$X$, $Y$, $\epsilon$, $\eta$ and $b_V$  parameters.
 
 \subsubsection{Models based on LO pQCD}

These models start from \eq{UPCeq:Fwd} and use the LO pQCD calculation 
\cite{Ryskin:1992ui,Brodsky:1994kf} for the forward cross section
\begin{equation}
\left. \frac{\dd\sigma(\gamma{\rm + Pb}\to{\rm V+Pb}) }{\dd t} \right|_{t=0}=
\frac{16\pi^3\alpha^2_s \Gamma_{ee}}{3\alpha M^5}
\left[ xG_A(x,Q^2)
\right]^2,
\label{UPCeq:LOpQCD}
\end{equation}
where $\Gamma_{ee}$ is the decay width to electrons and $G_A$ is the nuclear gluon density distribution at a scale $Q^2$, which for the models described below was chosen to be $Q^2 = M^2/4$, although other options are possible and may describe better the experimental data \cite{Guzey:2013qza}. It is important to note that this equation contains implicitly a model for the wave function of the vector meson, but in the final result the only trace of it is the presence of $\Gamma_{ee}$. 

The AB-AN model modifies \eq{UPCeq:LOpQCD} by adding a normalisation parameter to the right side, which should take into account effects missing in the approximation. This factor is then fitted to reproduce HERA data using the same type of equation applied to the $\gamma \mathrm{ + p}\to\mathrm{p}+\jpsi$ case. Nuclear effects are modelled as $G_A(x,Q^2) = g_p(x,Q^2) R^A_g(x,Q^2)$ where $g_p$ is the gluon distribution in the proton and $R^A_g$ is the nuclear modification factor of the gluon distribution.
MSTW08 \cite{Martin:2009iq} is used for the gluon distribution in the proton, while several different choices are made for $R^A_g$ to estimate nuclear effects: EPS08 \cite{Eskola:2008ca},
 EPS09 \cite{Eskola:2009uj}, HKN07 \cite{Hirai:2004wq} and  $R^A_g = 1$ to model the absence of nuclear effects.

The RSZ model computes  $R^A_g$ in the leading twist approach to nuclear shadowing 
\cite{Frankfurt:2011cs}. The
main ingredients are the factorisation theorem for hard diffraction  and the theory of inelastic shadowing by Gribov. The evolution  is done using DGLAP equations. The experimental input to fix the parameters of the model is given by inclusive diffractive parton distribution functions of nucleons as measured at HERA.
For the gluon distribution in the proton the LO distribution from \cite{Martin:2007sb} was used.

\subsubsection{Models based on the colour dipole approach}

\begin{figure}[t]
\begin{center}$
\begin{array}{c}
\includegraphics[width=0.45\textwidth]{CDMdiagram} 
\end{array} $
\end{center}
   \caption{Lowest order diagram for the photoproduction of charmonium within the colour dipole model.}
\label{UPCfig:CDM}
\end{figure}	

The basic idea of this formalism is illustrated in \fig{UPCfig:CDM}: long before the interaction, the photon splits into a  quark-antiquark pair, which forms a colour dipole. Then, this dipole interacts with the target and after another long time the dipole creates a vector meson. The cross section in this formalism is given by
\begin{equation}
\frac{\dd\sigma(\gamma{\rm+Pb}\to\jpsi + {\rm Pb}) }{\dd t} =
\frac{R^2_g(1+\beta^2)}{16\pi}\left|
A(x,Q^2,\vec{\Delta})\right|^2,
\end{equation}
where the so called skewdness correction $R^2_g$ compensate for the fact that only one value of $x$ is used, even though the two gluons participating in the interaction have different $x$ \cite{Shuvaev:1999ce}, while $(1+\beta^2)$ is the correction that takes into account the contribution from the real part of the amplitude \cite{Gribov:1968ie}. The imaginary part of the scattering amplitude is given by
\begin{equation}
A(x,Q^2,\vec{\Delta})= \int \dd z \dd^2\vec{r}\dd^2\vec{b} e^{-i(\vec{b}-(1-z)\vec{r})\cdot\vec{\Delta}}
\left[\Psi^*_{\jpsi}\Psi\right] 2\left[
1-\exp\left\{
-\frac{1}{2}\sigma_{\rm dip}T_{\rm Pb}(b)
\right\}
\right],
\end{equation}

where the integration variable $\vec{r}$ represents the distance between the quark and the antiquark in the plane transverse to the collision, $z$ quantifies the fraction of the photon momentum carried by the quark and $b$ is the distance between the centres of the  target and the dipole; $\vec{\Delta}$ is the transverse momentum transferred to the nucleus; the virtuality of the incoming photon is denoted by $Q^2$ and for the case of photoproduction discussed here is zero; $\Psi$ describes the splitting of the photon into the dipole and $\Psi_{\jpsi}$ is the wave function of the $\jpsi$;  the term $i(1-z)\vec{r}\cdot\vec{\Delta}$ in the exponential is a third correction to take into account non-forward contributions to the wave function $\Psi_{\jpsi}$, which is modelled for the forward case \cite{Bartels:2003yj}; and finally $\sigma_{\rm dip}$ is the universal cross section for the interaction of a colour dipole with a nuclear target.  
The models differ in the functional form of $\Psi_{\jpsi}$, in the corrections that were considered and in the formulation of the universal dipole cross section. 

In the case of LM  the non-forward correction to the wave function was not considered. LM use two different prescriptions for the wave function:  the Gauss-LC \cite{Kowalski:2003hm} and the boosted Gaussian \cite{Nemchik:1994fp,Nemchik:1996cw}. $\sigma_{\rm dip}$ is written in terms of the cross section of a dipole and a proton, $\sigma^p_{\rm dip}$; assuming a Gaussian profile in impact parameter for the proton, $\exp(-b^2/(2B_p))$: 
\begin{equation}
\frac{1}{2}\sigma_{\rm dip} = 2\pi B_p A N(r,x),
\end{equation}
where $N(r,x)$ is the dipole target amplitude. LM use two different models for $N(r,x)$: the IIM model \cite{Iancu:2003ge} which is a parameterisation of the expected behaviour of the solution to the BK equation \cite{Balitsky:1995ub,Kovchegov:1999yj,Kovchegov:1999ua} which includes a non-linear term for the evolution of  $N(r,x)$; and the IPsat model \cite{Kowalski:2003hm,Kowalski:2006hc} which uses DGLAP equations to evolve an eikonalized gluon distribution.

The GM-GDGM model uses the boosted Gaussian prescription for the wave function. The dipole cross section is given by $\sigma_{\rm dip} = R^A_g(x,Q^2)\sigma^{\rm p}_{\rm dip}$, where $\sigma^{\rm p}_{\rm dip}$ is given according to the IIM model and the leading twist approximation is used for $R^A_g(x,Q^2)$.
The CSS model is similar to the GM model, but uses the unintegrated gluon distribution of the nucleus including multiple scattering corrections. It also takes into account higher order fluctuations of the incoming photon.  The Gaussian form of the wave function is used.

%%%%%%%%%%%%%%%%%%%
%  C O M P A R I S O N                   %
%%%%%%%%%%%%%%%%%%%

\subsection{Photonuclear production of charmonium: comparing models to measurements}
\label{UPCsec:comp}

\begin{figure}[t]
\begin{center}$
\begin{array}{c}
\includegraphics[width=0.47\textwidth]{2013-Dec-18-Fig6a} 
\includegraphics[width=0.47\textwidth]{2013-Dec-18-Fig6b} 
\end{array} $
\end{center}
   \caption{Cross section for coherent (left) and incoherent (right) photonuclear production of $\jpsi$ as measured by ALICE compared to theoretical predictions. This is Fig.~6 of  \cite{Abbas:2013oua}.}
\label{UPCfig:ALICEresults}
\end{figure}	

The comparison of the  cross sections measured with ALICE and the predictions of the different models is shown in \fig{UPCfig:ALICEresults}. Several comments are in order. The spread among the different predictions is quite large, so data impose strong constraints into the different ingredients of the models, in particular  at mid-rapidity, which corresponds to the smallest $x$--Bjorken in a pQCD interpretation. The left panel of the figure shows the measurement for \jpsi coherent production. 
The AB-MSTW08 curve, corresponding to the absence of nuclear effect in that model, and the AB-EPS08 model, corresponding to strong shadowing, are both  disfavoured. 
 The other models are closer to the data and there are several natural refinements that can be done. For example, after the publication of ALICE data the model labelled RSZ-LTA was revisited and a study of the variations of the model with different scales for the coupling constant was performed \cite{Guzey:2013qza}. It was found that using data, one could constrain within this model the value of this scale such that data are correctly described. Similar improvements could be made to other models.
The preliminary data from CMS (not shown in the Figure) is also well described by this updated model and by AB-EPS09, which corresponds to mild shadowing.

The situation with respect to the measurement of incoherent photonuclear production of $\jpsi$ is not that clear. There are less models and less data. The LM--fIPsat model slightly underestimates the data, while the prediction of the same model is above the data for the coherent case.  The modification of the RSZ-LTA model from  \cite{Guzey:2013qza} has not such a large effect in the incoherent case, so that this model is still below the measurement.

The prediction of the cross section for coherent photonuclear production of $\psiP$ and its comparison to data is even more difficult. As mentioned above, all models fixed the predictions such that they reproduce data from HERA on photoproduction off protons. But HERA data for $\psiP$ is less abundant and less precise than for the case of $\jpsi$. One of the consequences is that the STARLIGHT and AB models when all nuclear effects are switched off, have the same prediction for $\jpsi$ but a different prediction for $\psiP$. It also happens that the wave function of the $\psiP$ is more complex than that of the $\jpsi$ and not all models take into account the full complexity of the wave function. Finally, the preliminary results from ALICE for the coherent production of $\psiP$ appeared after the publication of the $\jpsi$ measurements, so that some models were already updated to improve the description of ALICE data. Taking into account these caveats  the general conclusion seems to be that models with strong shadowing or without any nuclear effects are disfavoured, while models incorporating a mild form of shadowing are, within the current large experimental uncertainties, relatively close to data.

A somehow surprising result was the ratio of the coherent cross section of $\psiP$ to that of $\jpsi$  at mid rapidity. The measured value is around two times larger than what has been measured in the photoproduction off protons. Note  that the experimental uncertainties are still quite large, so this discrepancy is only a bit larger than two sigmas. All models, except AB, are quite close to the ratio measured at HERA, and thus far from that measured by ALICE \cite{Broz:2014wka}. The AB-EPS09 model is closer to the measured ratio, but one has to say that the wave function of the $\psiP$ was not included in its full complexity, and it is not clear what would happen if it were included. In this case, new data and improved models are much needed.

Another area to watch is the measurement of coherent production of charmonium in peripheral or even semi-central \PbPb collisions. As mentioned above, this possibility was discussed some time ago \cite{Joakim2010}, but no full calculation of the process has been performed. 
The possibility of a measurement at different centralities, which will be possible during \RunTwo, will allow  to test different model implementations. For example, if the coherent source were the spectator nucleons, then the distribution of transverse momentum would depend on centrality. Another idea that has been mentioned is that these processes, being independent of the hadronic interaction, may provide a new and complementary probe of the QGP. 

%%%%%%%%%%%%%%%%%%%
%  S U M M A R Y                            %
%%%%%%%%%%%%%%%%%%%

\subsection{Summary and outlook}
\label{UPCsec:outlook}

The LHC  Collaborations have demonstrated with data from \RunOne that measurements of photonuclear production of charmonium are possible and that these measurements provide valuable information to constrain models and can contribute to a better  understanding of shadowing. 
Present data favour the
existence of a shadowing as predicted by EPS09 in the x range $10^{-3}$---$10^{-2}$.
Furthermore, these data have given a glimpse of two remarkable results: ($i$) the ratio of $\psiP$ to $\jpsi$ cross section for coherent photoproduction seems to be quite sensitive to nuclear effects; and ($ii$) it seems to be possible to measure coherent production of charmonium overlapped with hadronic collisions. During \RunTwo, new data with large statistics will be collected and a definitive answer to these two questions may be given. The new data will also allow to explore the dependence of the $\jpsi$ coherent cross section on transverse momentum with great detail and  potentially the measurement of  $\ups$ production.
The  already existing measurements and the forthcoming ones represent  important  milestones in the path going from HERA towards a future dedicated electron-ion facility \cite{Deshpande:2005wd,Dainton:2006wd}.

%Call first reference \cite{Andronic:2013zim}. Cite the first figure \ref{Fig1}. \\
%
%\begin{figure}[!htp]
%	\centering
%	\includegraphics[width=0.2\columnwidth]{LogosaporeGravis.jpg}
%	\caption{Caption of Figure 1.}
%	\label{Fig1}
%\end{figure}
