#!/bin/bash
if [ ! `which bibtool` ]; then
  echo "bibtool not found. Merging cannot be done"
  exit
fi

outFile="fullBiblio.bib"

bibFiles=$(find . -maxdepth 2 -name "*.bib" -not -name "$outFile" -not -path "./ADDITIONAL_AUTHOR_TEMPLATE/*")

tmpFile="tmp_$outFile"

# First remove duplicated
bibtool -s -d -r bibtool_resources.txt $bibFiles > $tmpFile

#Then treat crossref
bibtool -s -r bibtool_resources.txt -- "sort.format = {{%1.#s(crossref)a#z}$key}" $tmpFile > $outFile

rm $tmpFile

echo ""
echo "Merged files:"
echo $bibFiles
